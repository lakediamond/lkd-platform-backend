package sync

import (
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/exchange"
	"lkd-platform-backend/internal/pkg/repo/models"
	"os"
	"strconv"
	"time"

	"github.com/rs/zerolog/log"
)

func SyncExchangeRate(app *application.Application) {
	log.Info().Msg("Start sync exchange")

	durationFixerMin, err := strconv.ParseInt(os.Getenv("API_EXCHANGE_FIXER_DURATION_MIN"), 10, 64)
	if err != nil {
		log.Info().Err(err).Msg("$API_EXCHANGE_FIXER_DURATION_MIN value processing failure")
	}

	getFixerRates(app)
	go func() {
		for range time.Tick(time.Duration(durationFixerMin) * time.Minute) {
			getFixerRates(app)
		}
	}()

	durationBitsTampMin, err := strconv.ParseInt(os.Getenv("API_EXCHANGE_BITSTAMP_DURATION_MIN"), 10, 64)
	if err != nil {
		log.Info().Err(err).Msg("$API_EXCHANGE_BITSTAMP_DURATION_MIN value processing failure")
	}

	getBitsTampRates(app)
	go func() {
		for range time.Tick(time.Duration(durationBitsTampMin) * time.Minute) {
			getBitsTampRates(app)
		}
	}()
}

func getBitsTampRates(app *application.Application) {

	fixerClient := exchange.InitBitsTampClient()
	data, err := fixerClient.GetBitsTampExchangeRate(models.RATE_CODE_ETH_EUR)
	if err != nil {
		log.Info().Err(err).Msgf("Sync::Exchange -> BitsTampRates")
		return
	}

	rate, err := strconv.ParseFloat(data.High, 64)
	if err != nil {
		log.Info().Err(err).Msgf("Sync::Exchange -> BitsTampRates can not converting High")
		return
	}

	exchangeRate, _ := app.Repo.Exchange.GetExchangeRateByCode(models.RATE_CODE_ETH_EUR)
	exchangeRate.Code = models.RATE_CODE_ETH_EUR
	exchangeRate.Rate = rate

	if exchangeRate.ID == 0 {
		exchangeRate.CreatedAt = time.Now()
	}

	err = app.Repo.Exchange.UpdateExchange(&exchangeRate)
	if err != nil {
		log.Info().Err(err).Msgf("Sync::Exchange -> UpdateExchange")
		return
	}

	err = saveExchangeHistory(app, models.RATE_CODE_ETH_EUR, rate)
	if err != nil {
		log.Info().Err(err).Msgf("Sync::Exchange -> CreateExchangeHistory")
		return
	}
}

func getFixerRates(app *application.Application) {
	fixerClient := exchange.InitFixerClient()
	data, err := fixerClient.GetFixerExchangeRate("EUR", []string{"CHF"})
	if err != nil {
		log.Info().Err(err).Msgf("Sync::Exchange -> FixerRates")
		return
	}

	exchangeRate, _ := app.Repo.Exchange.GetExchangeRateByCode(models.RATE_CODE_EUR_CHF)
	exchangeRate.Code = models.RATE_CODE_EUR_CHF
	exchangeRate.Rate = data.Rates.CHF

	if exchangeRate.ID == 0 {
		exchangeRate.CreatedAt = time.Now()
	}

	err = app.Repo.Exchange.UpdateExchange(&exchangeRate)
	if err != nil {
		log.Info().Err(err).Msgf("Sync::Exchange -> UpdateExchange")
		return
	}

	err = saveExchangeHistory(app, models.RATE_CODE_EUR_CHF, data.Rates.CHF)
	if err != nil {
		log.Info().Err(err).Msgf("Sync::Exchange -> CreateExchangeHistory")
		return
	}
}

func saveExchangeHistory(app *application.Application, code string, rate float64) error {
	var exchangeHistory models.ExchangeHistory

	exchangeHistory.Code = code
	exchangeHistory.Rate = rate
	exchangeHistory.CreatedAt = time.Now()

	err := app.Repo.Exchange.CreateExchangeHistory(&exchangeHistory)
	if err != nil {
		log.Info().Err(err).Msgf("Sync::Exchange -> CreateExchangeHistory")
		return err
	}

	return nil
}
