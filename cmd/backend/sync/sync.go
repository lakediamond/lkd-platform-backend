package sync

import (
	"lkd-platform-backend/internal/pkg/application"
	"os"
)

func InitSync(app *application.Application) {
	sync, present := os.LookupEnv("API_EXCHANGE_ENABLED")
	if present && sync == "true" {
		go SyncExchangeRate(app)
	}
}
