package main

import (
	"lkd-platform-backend/cmd/backend/crons"
	"lkd-platform-backend/cmd/backend/sync"
	"lkd-platform-backend/internal/pkg/googleCloud"
	"lkd-platform-backend/internal/pkg/mailjet"
	"lkd-platform-backend/pkg/environment"
	"net"
	"os"

	"cloud.google.com/go/profiler"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"

	"github.com/rs/zerolog/log"
	"lkd-platform-backend/cmd/backend/initializer"
	"lkd-platform-backend/internal/app/backend/httpserver"
	"lkd-platform-backend/internal/pkg/application"
)

func main() {
	if environment.CheckTracerRun(os.Getenv("ENV")) {
		log.Info().Msg("Start Tracer")
		addr := net.JoinHostPort(
			os.Getenv("DD_AGENT_HOST"),
			os.Getenv("DD_TRACE_AGENT_PORT"),
		)
		tracer.Start(tracer.WithAgentAddr(addr), tracer.WithServiceName("lkd-platform-backend"))
		defer tracer.Stop()

		log.Info().Msg("Start GCP Profiler")
		if err := profiler.Start(profiler.Config{
			Service:   "lkd-platform-backend",
			ProjectID: os.Getenv("GCP_CRON_PROJECT_ID"),
		}); err != nil {
			log.Error().Err(err).Msg("Failed to start GCP Profiler")
		}
	}

	app := application.NewApplication()

	initializer.InitApplication(app)
	app.Mailjet = mailjet.NewClient(false)

	app.GC = googleCloud.NewGCApplication()

	users, _ := app.Repo.User.GetAllUsers()

	sync.InitSync(app)

	cronSub, present := os.LookupEnv("CRON_RUN_SUBSCRIBERS")
	if present && cronSub == "true" {
		go crons.CronSubscribes(app)
	}

	for _, user := range users {
		user.EthAddress = "test_test"
	}

	for _, user := range users {
		log.Print(user.EthAddress)
	}

	httpserver.Serve(app)
}
