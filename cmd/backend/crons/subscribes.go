package crons

import (
	"cloud.google.com/go/pubsub"
	"context"
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/lakediamond/lkd-pubsub-wrapper"
	"gitlab.com/lakediamond/lkd-pubsub-wrapper/pools"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/recoveryPassword"
	"lkd-platform-backend/internal/app/backend/users"
	"lkd-platform-backend/internal/app/backend/users/pipeDrive"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
	"math/big"
	"strconv"
)

const (
	LkdCreateCrmPerson             string = "lkdCreateCrmPerson"
	LkdCreateUpdateCRMDeal         string = "lkdCreateUpdateCRMDeal"
	LkdCreateCRMDealNote           string = "lkdCreateCRMDealNote"
	LkdCreateEmailVerification     string = "lkdCreateEmailVerification"
	LkdCreateEmailRecoveryPassword string = "lkdCreateEmailRecoveryPassword"
	LkdSendSms                     string = "lkdSendSms"
	LkdAddToWhitelist              string = "lkdAddToWhitelist"
)

type MessagePayload struct {
	Action struct {
		Code string `json:"code"`
	} `json:"action"`
}

func CronSubscribes(app *application.Application) {
	config := app.Config.CRON

	client, err := pubsubwrapper.NewClient(config.ProjectId, config.TopicName)
	if err != nil {
		log.Error().Err(err).Msg("Failed to gcpClient")
		return
	}

	defer func() {
		err := client.CloseClient()
		if err != nil {
			log.Error().Err(err).Msg("Failed to close gcpClient")
		}
	}()

	topic, err := client.GetTopic(nil, config.TopicName)
	if err != nil {
		log.Error().Err(err).Msg("Failed get gcpTopic")
		return
	}

	defer topic.Stop()

	sub, err := client.GetSubscription(nil, topic)
	if err != nil {
		log.Error().Err(err).Msg("Failed GetSubscription")
		return
	}

	err = client.PullMessage(nil, sub.String(), topic, func(m *pubsub.Message) {
		message := &MessagePayload{}
		err := json.Unmarshal(m.Data, message)
		if err != nil {
			log.Debug().Msgf("Invalid message event - %v", m.Data)
			return
		}

		switch message.Action.Code {
		case LkdCreateCrmPerson:
			log.Debug().Msgf("CronSubscribe -> message: %+s\n", string(m.Data))
			err := CronCreateNewPerson(app)
			if err != nil {
				log.Error().Err(err).Msg("LkdCreateCrmPerson error")
			}
		case LkdCreateUpdateCRMDeal:
			log.Debug().Msgf("CronSubscribe -> message: %+s\n", string(m.Data))
			err := CronCreateUpdateCRMDeal(app)
			if err != nil {
				log.Error().Err(err).Msg("LkdCreateUpdateCRMDeal error")
			}
		case LkdCreateCRMDealNote:
			log.Debug().Msgf("CronSubscribe -> message: %+s\n", string(m.Data))
			err := CronCreateCRMDealNote(app)
			if err != nil {
				log.Error().Err(err).Msg("LkdCreateCRMDealNote error")
			}
		case LkdCreateEmailVerification:
			log.Debug().Msgf("CronSubscribe -> message: %+s\n", string(m.Data))
			err := CronCreateEmailVerification(app)
			if err != nil {
				log.Error().Err(err).Msg("lkdCreateEmailVerification error")
			}
		case LkdCreateEmailRecoveryPassword:
			log.Debug().Msgf("CronSubscribe -> message: %+s\n", string(m.Data))
			err := CronCreateEmailRecoveryPassword(app)
			if err != nil {
				log.Error().Err(err).Msg("LkdCreateEmailRecoveryPassword error")
			}
		case LkdSendSms:
			log.Debug().Msgf("CronSubscribe -> message: %+s\n", string(m.Data))
			err := CronSendSms(app)
			if err != nil {
				log.Error().Err(err).Msg("LkdSendSms error")
			}
		case LkdAddToWhitelist:
			log.Debug().Msgf("CronSubscribe -> message: %+s\n", string(m.Data))
			err := CronAddToWhitelist(app)
			if err != nil {
				log.Error().Err(err).Msg("LkdAddToWhitelist error")
			}
		}

		return
	})

	if err != context.Canceled {
		log.Error().Err(err).Msg(err.Error())
	}
}

func CronCreateNewPerson(app *application.Application) error {
	events, _ := app.Repo.KYC.GetWorkerByStateAndEvent(models.StateNew, models.EventCreateNewPerson)
	if len(events) == 0 {
		return nil
	}

	tasks := make([]*pools.Task, 0)
	for _, event := range events {
		tasks = append(tasks, pools.NewTask(func(t *pools.Task) error {
			log.Debug().Msgf("Run task CronCreateNewPerson::message[%+s]", t.Message)

			app := t.App.(*application.Application)
			message := t.Message

			eventCreatePerson, err := app.Repo.KYC.GetWorkerByID(message.ID)
			if err != nil {
				return err
			}

			user, err := app.Repo.User.GetUserByID(eventCreatePerson.UserID)
			if err != nil {
				return err
			}

			if eventCreatePerson.State == models.StateDone {
				return nil
			}

			tier, err := app.Repo.KYC.GetTierByStepsStatus(&user, models.StepCustomerConfirmed)
			if err != nil {
				return err
			}

			if len(tier.Steps) == 0 {
				return fmt.Errorf("cronCreateNewPerson[tier.Steps] can not be empty")
			}

			step := tier.Steps[0]
			contentItems := step.ContentItems

			if len(contentItems) == 0 {
				return fmt.Errorf("cronCreateNewPerson[step.ContentItems] can not be empty")
			}

			err = app.Repo.KYC.SetWorkerState(models.StateInProcessing, eventCreatePerson)
			if err != nil {
				return err
			}

			stepCommonInfoForm := &models.StepCommonInfoForm{
				ID:                   step.ID.String(),
				RegAddressCountry:    getKYCTierStepContentItem(contentItems, "reg_address_country"),
				RegAddressZIP:        getKYCTierStepContentItem(contentItems, "reg_address_zip"),
				RegAddressCity:       getKYCTierStepContentItem(contentItems, "reg_address_city"),
				RegAddressDetails:    getKYCTierStepContentItem(contentItems, "reg_address_details_1"),
				RegAddressDetailsExt: getKYCTierStepContentItem(contentItems, "reg_address_details_2"),
				ContributionOption:   getKYCTierStepContentItem(contentItems, "contribution_option"),
				BankName:             getKYCTierStepContentItem(contentItems, "contribution_bank_name"),
				BankIBAN:             getKYCTierStepContentItem(contentItems, "contribution_iban"),
				BankAddress:          getKYCTierStepContentItem(contentItems, "contribution_bank_address"),
				Phone:                eventCreatePerson.Phone,
				Email:                user.Email,
			}

			err = pipeDrive.SendClientCreateNewPerson(app, &user, stepCommonInfoForm)
			if err != nil {
				errDb := app.Repo.KYC.SetWorkerState(models.StateNew, eventCreatePerson)
				if errDb != nil {
					return errDb
				}

				return err
			}

			fields := make(map[string]interface{}, 0)
			fields["state"] = models.StateDone
			err = app.Repo.KYC.UpdateWorker(eventCreatePerson, fields)
			if err != nil {
				return fmt.Errorf("cannot update customer in DB: %+v", err)
			}

			return nil
		}, &pools.Message{ID: event.ID.String()}, app))
	}

	if err := initPools(tasks, app); err != nil {
		return err
	}

	return nil
}

func CronCreateUpdateCRMDeal(app *application.Application) error {
	events, _ := app.Repo.KYC.GetWorkerByStateAndEvent(models.StateNew, models.EventCreateUpdateCRMDeal)
	if len(events) == 0 {
		return nil
	}

	tasks := make([]*pools.Task, 0)
	for _, event := range events {
		tasks = append(tasks, pools.NewTask(func(t *pools.Task) error {
			log.Debug().Msgf("Run task CronCreateUpdateCRMDeal::message[%+s]", t.Message)

			app := t.App.(*application.Application)
			message := t.Message

			eventCreateUpdateCRMDeal, err := app.Repo.KYC.GetWorkerByID(message.ID)
			if err != nil {
				return err
			}

			user, err := app.Repo.User.GetUserByID(eventCreateUpdateCRMDeal.UserID)
			if err != nil {
				return err
			}

			if eventCreateUpdateCRMDeal.State == models.StateDone {
				return nil
			}

			err = app.Repo.KYC.SetWorkerState(models.StateInProcessing, eventCreateUpdateCRMDeal)
			if err != nil {
				return err
			}

			err = pipeDrive.UpdateCustomerCRMDeal(app, &user)
			if err != nil {
				errDb := app.Repo.KYC.SetWorkerState(models.StateNew, eventCreateUpdateCRMDeal)
				if errDb != nil {
					return errDb
				}

				return err
			}

			fields := make(map[string]interface{}, 0)
			fields["state"] = models.StateDone
			err = app.Repo.KYC.UpdateWorker(eventCreateUpdateCRMDeal, fields)
			if err != nil {
				return fmt.Errorf("cannot update customer in DB: %+v", err)
			}

			return nil
		}, &pools.Message{ID: event.ID.String()}, app))
	}

	if err := initPools(tasks, app); err != nil {
		return err
	}

	return nil
}

func CronCreateCRMDealNote(app *application.Application) error {
	events, _ := app.Repo.KYC.GetWorkerByStateAndEvent(models.StateNew, models.EventCreateCRMDealNote)
	if len(events) == 0 {
		return nil
	}

	tasks := make([]*pools.Task, 0)
	for _, event := range events {
		tasks = append(tasks, pools.NewTask(func(t *pools.Task) error {
			log.Debug().Msgf("Run task CronCreateCRMDealNote::message[%+s]", t.Message)

			app := t.App.(*application.Application)
			message := t.Message

			eventCreateCRMDealNote, err := app.Repo.KYC.GetWorkerByID(message.ID)
			if err != nil {
				return err
			}

			user, err := app.Repo.User.GetUserByID(eventCreateCRMDealNote.UserID)
			if err != nil {
				return err
			}

			if user.CRMDealID == 0 {
				log.Debug().Msgf("wait CronCreateCRMDealNote::user.CRMDealID[%d]", user.CRMDealID)
				return nil
			}

			if eventCreateCRMDealNote.State == models.StateDone {
				return nil
			}

			verificationResult, err := app.Repo.VerificationResult.GetByID(eventCreateCRMDealNote.CrmVerificationResultID.String())
			if err != nil {
				return err
			}

			err = app.Repo.KYC.SetWorkerState(models.StateInProcessing, eventCreateCRMDealNote)
			if err != nil {
				return err
			}

			err = pipeDrive.CreateDealNote(app, verificationResult, user.ID)
			if err != nil {
				errDb := app.Repo.KYC.SetWorkerState(models.StateNew, eventCreateCRMDealNote)
				if errDb != nil {
					return errDb
				}

				return err
			}

			fields := make(map[string]interface{}, 0)
			fields["state"] = models.StateDone
			err = app.Repo.KYC.UpdateWorker(eventCreateCRMDealNote, fields)
			if err != nil {
				return fmt.Errorf("cannot update customer in DB: %+v", err)
			}

			return nil
		}, &pools.Message{ID: event.ID.String()}, app))
	}

	if err := initPools(tasks, app); err != nil {
		return err
	}

	return nil
}

func CronCreateEmailVerification(app *application.Application) error {
	events, _ := app.Repo.KYC.GetWorkerByStateAndEvent(models.StateNew, models.EventCreateEmailVerification)
	if len(events) == 0 {
		return nil
	}

	tasks := make([]*pools.Task, 0)
	for _, event := range events {
		tasks = append(tasks, pools.NewTask(func(t *pools.Task) error {
			log.Debug().Msgf("Run task CronCreateEmailVerification::message[%+s]", t.Message)

			app := t.App.(*application.Application)
			message := t.Message

			eventCreateEmailVerification, err := app.Repo.KYC.GetWorkerByID(message.ID)
			if err != nil {
				return err
			}

			user, err := app.Repo.User.GetUserByID(eventCreateEmailVerification.UserID)
			if err != nil {
				return err
			}

			if eventCreateEmailVerification.State == models.StateDone {
				return nil
			}

			err = app.Repo.KYC.SetWorkerState(models.StateInProcessing, eventCreateEmailVerification)
			if err != nil {
				return err
			}

			err = users.SendVerificationEmail(app, &user, eventCreateEmailVerification.LoginChallenge, eventCreateEmailVerification.SpanID)
			if err != nil {
				errDb := app.Repo.KYC.SetWorkerState(models.StateNew, eventCreateEmailVerification)
				if errDb != nil {
					return errDb
				}

				return err
			}

			fields := make(map[string]interface{}, 0)
			fields["state"] = models.StateDone
			err = app.Repo.KYC.UpdateWorker(eventCreateEmailVerification, fields)
			if err != nil {
				return fmt.Errorf("cannot update customer in DB: %+v", err)
			}

			return nil
		}, &pools.Message{ID: event.ID.String()}, app))
	}

	if err := initPools(tasks, app); err != nil {
		return err
	}

	return nil
}

func CronCreateEmailRecoveryPassword(app *application.Application) error {
	events, _ := app.Repo.KYC.GetWorkerByStateAndEvent(models.StateNew, models.EventCreateEmailRecoveryPassword)
	if len(events) == 0 {
		return nil
	}

	tasks := make([]*pools.Task, 0)
	for _, event := range events {
		tasks = append(tasks, pools.NewTask(func(t *pools.Task) error {
			log.Debug().Msgf("Run task CronCreateEmailRecoveryPassword::message[%+s]", t.Message)

			app := t.App.(*application.Application)
			message := t.Message

			eventCreateEmailRecoveryPassword, err := app.Repo.KYC.GetWorkerByID(message.ID)
			if err != nil {
				return err
			}

			user, err := app.Repo.User.GetUserByID(eventCreateEmailRecoveryPassword.UserID)
			if err != nil {
				return err
			}

			if eventCreateEmailRecoveryPassword.State == models.StateDone {
				return nil
			}

			accessRecoveryRequest, err := app.Repo.AccessRecoveryRequest.GetRequestByID(eventCreateEmailRecoveryPassword.AccessRecoveryRequestID)
			if err != nil {
				return err
			}

			err = app.Repo.KYC.SetWorkerState(models.StateInProcessing, eventCreateEmailRecoveryPassword)
			if err != nil {
				return err
			}

			err = recoveryPassword.SendEmailRecoveryPassword(app, &user, accessRecoveryRequest.Token)
			if err != nil {
				errDb := app.Repo.KYC.SetWorkerState(models.StateNew, eventCreateEmailRecoveryPassword)
				if errDb != nil {
					return errDb
				}

				return err
			}

			fields := make(map[string]interface{}, 0)
			fields["state"] = models.StateDone
			err = app.Repo.KYC.UpdateWorker(eventCreateEmailRecoveryPassword, fields)
			if err != nil {
				return fmt.Errorf("cannot update customer in DB: %+v", err)
			}

			return nil
		}, &pools.Message{ID: event.ID.String()}, app))
	}

	if err := initPools(tasks, app); err != nil {
		return err
	}

	return nil
}

func CronSendSms(app *application.Application) error {
	events, _ := app.Repo.KYC.GetWorkerByStateAndEvent(models.StateNew, models.EventSendSMS)
	if len(events) == 0 {
		return nil
	}

	tasks := make([]*pools.Task, 0)
	for _, event := range events {
		tasks = append(tasks, pools.NewTask(func(t *pools.Task) error {
			log.Debug().Msgf("Run task CronSendSms::message[%+s]", t.Message)

			app := t.App.(*application.Application)
			message := t.Message

			eventSendSMS, err := app.Repo.KYC.GetWorkerByID(message.ID)
			if err != nil {
				return err
			}

			if eventSendSMS.State == models.StateDone {
				return nil
			}

			pv, err := app.Repo.PhoneVerification.GetByID(eventSendSMS.PhoneVerificationID.String())
			if err != nil {
				return err
			}

			err = app.Repo.KYC.SetWorkerState(models.StateInProcessing, eventSendSMS)
			if err != nil {
				return err
			}

			err = app.SMSSender.Send(pv.Phone, strconv.Itoa(pv.Code))
			if err != nil {
				errDb := app.Repo.KYC.SetWorkerState(models.StateNew, eventSendSMS)
				if errDb != nil {
					return errDb
				}

				return err
			}

			fields := make(map[string]interface{}, 0)
			fields["state"] = models.StateDone
			err = app.Repo.KYC.UpdateWorker(eventSendSMS, fields)
			if err != nil {
				return fmt.Errorf("cannot update customer in DB: %+v", err)
			}

			return nil
		}, &pools.Message{ID: event.ID.String()}, app))
	}

	if err := initPools(tasks, app); err != nil {
		return err
	}

	return nil
}

func CronAddToWhitelist(app *application.Application) error {
	events, _ := app.Repo.KYC.GetWorkerByStateAndEvent(models.StateNew, models.EventAddToWhitelist)
	if len(events) == 0 {
		return nil
	}

	tasks := make([]*pools.Task, 0)
	for _, event := range events {
		tasks = append(tasks, pools.NewTask(func(t *pools.Task) error {
			log.Debug().Msgf("Run task CronAddToWhitelist::message[%+s]", t.Message)

			app := t.App.(*application.Application)
			message := t.Message

			eventAddToWhitelist, err := app.Repo.KYC.GetWorkerByID(message.ID)
			if err != nil {
				return err
			}

			user, err := app.Repo.User.GetUserByID(eventAddToWhitelist.UserID)
			if err != nil {
				return err
			}

			if eventAddToWhitelist.State == models.StateDone {
				return nil
			}

			crmTokensLimit := big.NewInt(int64(user.KYCInvestmentPlan.MaxLimit))
			err = app.Repo.KYC.SetWorkerState(models.StateInProcessing, eventAddToWhitelist)
			if err != nil {
				return err
			}

			log.Info().Msgf("CronAddToWhitelist::user[%+s] %d", user.ID.String(), crmTokensLimit)

			err = app.EthClient.Internal.AddAddressToWhiteList(user.EthAddress, crmTokensLimit)
			if err != nil {
				errDb := app.Repo.KYC.SetWorkerState(models.StateNew, eventAddToWhitelist)
				if errDb != nil {
					return errDb
				}

				return err
			}

			fields := make(map[string]interface{}, 0)
			fields["state"] = models.StateDone
			err = app.Repo.KYC.UpdateWorker(eventAddToWhitelist, fields)
			if err != nil {
				return fmt.Errorf("cannot update customer in DB: %+v", err)
			}

			return nil
		}, &pools.Message{ID: event.ID.String()}, app))
	}

	if err := initPools(tasks, app); err != nil {
		return err
	}

	return nil
}

func getKYCTierStepContentItem(items []*models.KYCTierStepContentItem, code string) string {
	for _, item := range items {
		if item.Code == code {
			return item.Value
		}
	}
	return ""
}

func initPools(tasks []*pools.Task, app *application.Application) error {

	p, err := pools.NewPool(
		tasks,
		app.Config.CRON.ConcurrencyWorker,
		app.Config.CRON.DurationTasks)
	if err != nil {
		return err
	}
	p.Run()

	for _, task := range p.Tasks {
		if task.Err != nil {
			log.Error().Err(task.Err).Msg("Task error msg.")
		}
	}

	return nil
}
