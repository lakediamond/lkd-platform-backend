package initializer

import (
	"encoding/binary"
	"fmt"
	"lkd-platform-backend/pkg/ws/resource"
	"lkd-platform-backend/pkg/ws/ws-server"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/oauth"
	"lkd-platform-backend/internal/pkg/phoneNumber"
	"lkd-platform-backend/internal/pkg/phoneNumber/twilio"
	"lkd-platform-backend/internal/pkg/pipeDrive"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/sms"
	"lkd-platform-backend/internal/pkg/sms/bitAccess"
	"lkd-platform-backend/pkg/environment"
)

func InitApplication(app *application.Application) {
	env := environment.GetEnv()

	app.Repo = InitDB(env)
	app.IAMClient = initIAMClient(env)
	app.EthClient = InitEthereum(env)
	app.Config.AuthRetries = initAuthRetries(env)
	app.Config.CRON = initCronConfig(env)
	app.Storage = initStorage(env)
	app.PipeDrive = initPipeDrive(env)
	app.SMSSender = initSMSSender(env)
	app.PhoneNumberValidator = initPhoneNumberValidator(env)
	app.WSClient = initWSClient()
}

func InitDB(env map[string]string) *repo.Repo {
	name := env["DATABASE_NAME"]
	user := env["DATABASE_USER"]
	pass := env["DATABASE_PASSWORD"]
	host := env["DATABASE_HOST"]
	port := env["DATABASE_PORT"]

	r, err := repo.GetDbClient(name, user, pass, host, port)
	if err != nil {
		log.Panic().Err(err).Msg("cannot connect to database")
	}

	return r
}

func initIAMClient(env map[string]string) oauth.IdentityManagementClient {
	CookieStoreHash32 := make([]byte, 32)
	CookieStoreHash16 := make([]byte, 16)

	loginRememberParsed, err := strconv.ParseInt(env["OAUTH_LOGIN_REMEMBER_FOR"], 10, 64)
	if err != nil {
		log.Panic().Err(err).Msg("$OAUTH_LOGIN_REMEMBER_FOR value processing failure")
	}

	consentRememberParsed, err := strconv.ParseInt(env["OAUTH_CONSENT_REMEMBER_FOR"], 10, 64)
	if err != nil {
		log.Panic().Err(err).Msg("$OAUTH_CONSENT_REMEMBER_FOR value processing failure %s")
	}

	envCookieStoreHash32, ok := env["HTTP_SESSION_STORE_32B_AUTH_KEY"]
	if !ok {
		log.Panic().Msg("$HTTP_SESSION_STORE_32B_AUTH_KEY value can not be emty")
	}

	envCookieStoreHash16, ok := env["HTTP_SESSION_STORE_16B_AUTH_KEY"]
	if !ok {
		log.Panic().Msg("$HTTP_SESSION_STORE_16B_AUTH_KEY value can not be emty")
	}

	CookieStoreHash32 = []byte(envCookieStoreHash32)
	if size32 := binary.Size(CookieStoreHash32); size32 != 32 {
		log.Panic().Msgf("$HTTP_SESSION_STORE_32B_AUTH_KEY value can not len: %d", size32)
	}

	CookieStoreHash16 = []byte(envCookieStoreHash16)
	if size16 := binary.Size(CookieStoreHash16); size16 != 16 {
		log.Panic().Msgf("$HTTP_SESSION_STORE_16B_AUTH_KEY value can not len: %d", size16)
	}

	config := oauth.IAMConfig{
		AdminURL:     env["OAUTH_HYDRA_ADMIN_API_URL"],
		PublicURL:    env["OAUTH_HYDRA_API_URL"],
		ClientID:     env["OAUTH_CLIENT_ID"],
		ClientSecret: env["OAUTH_CLIENT_SECRET"],
		Scopes:       strings.Split(env["OAUTH_CLIENT_SCOPES"], ","),

		LoginRemember:   loginRememberParsed,
		ConsentRemember: consentRememberParsed,

		ClientLogoutRedirectURL: env["OAUTH_CLIENT_LOGOUT_REDIRECT_URL"],
		ClientRedirectURL:       env["OAUTH_CLIENT_REDIRECT_URL"],
		AuthTokenAudience:       strings.Split(env["OAUTH_AUTH_AUDIENCE"], ","),
		StateToken:              env["OAUTH_FLOW_STATE_TOKEN"],

		CookieStoreHash32: CookieStoreHash32,
		CookieStoreHash16: CookieStoreHash16,
	}

	client, err := oauth.GetIAMClient(&config)
	if err != nil {
		log.Panic().Err(err).Msg("cannot initialize IAM client")
	}

	return client
}

func InitEthereum(env map[string]string) *etherPkg.PlatformEthereumApplication {
	md := initializeMatcherData(env)
	ethApp := etherPkg.InitializePlatformEthereumApplication(md)
	return ethApp
}

//InitializeMatcherData initializing matcher data for event listener
func initializeMatcherData(env map[string]string) *etherPkg.MatcherData {
	return &etherPkg.MatcherData{
		StorageAddress:        env["ETHEREUM_IO_STORAGE_CONTRACT_ADDRESS"],
		LakeDiamond:           env["ETHEREUM_IO_CONTRACT_ADDRESS"],
		NewOrder:              env["ETHEREUM_NEW_ORDER_EVENT"],
		CloseOrder:            env["ETHEREUM_CLOSE_ORDER_EVENT"],
		NewProposal:           env["ETHEREUM_NEW_PROPOSAL_EVENT"],
		WithdrawProposal:      env["ETHEREUM_WITHDRAW_PROPOSAL_EVENT"],
		CompleteProposal:      env["ETHEREUM_COMPLETE_PROPOSAL_EVENT"],
		NewOrderType:          env["ETHEREUM_NEW_ORDER_TYPE_EVENT"],
		CompleteOrder:         env["ETHEREUM_ORDER_COMPLETE_EVENT"],
		NewSubOwner:           env["ETHEREUM_NEW_SUBOWNER_EVENT"],
		RemovedSubOwner:       env["ETHEREUM_REMOVED_SUBOWNER_EVENT"],
		IOContractAddress:     env["ETHEREUM_IO_CONTRACT_ADDRESS"],
		RPCPort:               env["RPC_PORT"],
		PrivateKeyStorageKey:  env["ETHEREUM_PRIVATE_KEY_STORAGE_KEY"],
		PrivateKeyStorageData: env["ETHEREUM_PRIVATE_KEY_STORAGE_DATA"],
	}
}

func initAuthRetries(env map[string]string) *application.AuthRetries {
	limitRetries, err := strconv.ParseInt(env["AUTH_LIMIT_RETRIES"], 10, 64)
	if err != nil {
		log.Fatal().Err(err).Msg("invalid AUTH_LIMIT_RETRIES")
	}

	timeFrame, err := strconv.ParseInt(env["AUTH_TIME_FRAME"], 10, 64)
	if err != nil {
		log.Fatal().Err(err).Msg("invalid AUTH_TIME_FRAME")
	}

	lockTime, err := strconv.ParseInt(env["AUTH_LOCK_TIME"], 10, 64)
	if err != nil {
		log.Fatal().Err(err).Msg("invalid AUTH_LOCK_TIME")
	}

	return &application.AuthRetries{
		LimitRetries: limitRetries,
		TimeFrame:    timeFrame,
		LockTime:     lockTime,
		LockReason:   env["AUTH_LOCK_REASON"],
	}
}

func initStorage(env map[string]string) *application.Storage {
	return application.NewStorage(env["GCP_PROJECT_ID_FULL"], env["STORAGE_BUCKET_NAME"])
}

func initPipeDrive(env map[string]string) pipeDrive.ClientInterface {
	token := env["CRM_TOKEN"]
	if len(token) < 0 {
		log.Fatal().Msg("CRM_TOKEN not found in config")
	}

	baseURL := env["CRM_BASE_URL"]
	if len(token) < 0 {
		log.Fatal().Msg("CRM_BASE_URL not found in config")
	}

	data := pipeDrive.InitData(baseURL, token)

	err := loadDealDataFromConfig(data.GetDeal(), env)
	if err != nil {
		log.Fatal().Err(err).Msg(err.Error())
	}

	return data
}

func loadDealDataFromConfig(crmDeal *pipeDrive.Deal, env map[string]string) error {

	crmStageIDParsed, err := strconv.Atoi(env["CRM_ID_DOCUMENT_FAILURE_STAGE"])
	if err != nil {
		return errors.WithMessage(err, getInvalidValueMsg("CRM_ID_DOCUMENT_FAILURE_STAGE"))
	}
	crmDeal.IDDocumentBitaccessVerificationFailureStage = crmStageIDParsed

	crmStageIDParsed, err = strconv.Atoi(env["CRM_ID_RESTRICTED_COUNTRY_FAILURE_STAGE"])
	if err != nil {
		return errors.WithMessage(err, getInvalidValueMsg("CRM_ID_RESTRICTED_COUNTRY_FAILURE_STAGE"))
	}
	crmDeal.IDDocumentFromAllowedCountryFailureStage = crmStageIDParsed

	crmStageIDParsed, selfieBtcsParseErr := strconv.Atoi(env["CRM_SELFIE_FAILURE_STAGE"])
	if selfieBtcsParseErr != nil {
		return errors.WithMessage(selfieBtcsParseErr, getInvalidValueMsg("CRM_SELFIE_FAILURE_STAGE"))
	}
	crmDeal.SelfieBitaccessVerificationFailureStage = crmStageIDParsed

	crmStageIDParsed, ethScreeningParseErr := strconv.Atoi(env["CRM_ETH_ADDRESS_SCREENING_FAILURE_STAGE"])
	if ethScreeningParseErr != nil {
		return errors.WithMessage(ethScreeningParseErr, getInvalidValueMsg("CRM_ETH_ADDRESS_SCREENING_FAILURE_STAGE"))
	}
	crmDeal.EthereumAddressScorechainScreeningFailureStage = crmStageIDParsed

	crmStageIDParsed, multiFailuresParseErr := strconv.Atoi(env["CRM_MULTIPLE_FAILURES_STAGE"])
	if multiFailuresParseErr != nil {
		return errors.WithMessage(multiFailuresParseErr, getInvalidValueMsg("CRM_MULTIPLE_FAILURES_STAGE"))
	}
	crmDeal.MultipleFailuresStage = crmStageIDParsed

	crmStageIDParsed, err = strconv.Atoi(env["CRM_KYC_TIER_0_STAGE_ID"])
	if err != nil {
		return errors.WithMessage(multiFailuresParseErr, getInvalidValueMsg("CRM_KYC_TIER_0_STAGE_ID"))
	}
	crmDeal.KYCTier0StageID = crmStageIDParsed

	crmStageIDParsed, err = strconv.Atoi(env["CRM_KYC_TIER_1_STAGE_ID"])
	if err != nil {
		return errors.WithMessage(multiFailuresParseErr, getInvalidValueMsg("CRM_KYC_TIER_1_STAGE_ID"))
	}
	crmDeal.KYCTier1StageID = crmStageIDParsed

	crmStageIDParsed, err = strconv.Atoi(env["CRM_KYC_TIER_2_STAGE_ID"])
	if err != nil {
		return errors.WithMessage(multiFailuresParseErr, getInvalidValueMsg("CRM_KYC_TIER_2_STAGE_ID"))
	}
	crmDeal.KYCTier2StageID = crmStageIDParsed

	crmStageIDParsed, err = strconv.Atoi(env["CRM_KYC_TIER_3_STAGE_ID"])
	if err != nil {
		return errors.WithMessage(multiFailuresParseErr, getInvalidValueMsg("CRM_KYC_TIER_3_STAGE_ID"))
	}
	crmDeal.KYCTier3StageID = crmStageIDParsed

	return nil
}

func getInvalidValueMsg(valueName string) string {
	return fmt.Sprintf("invalid %s value", valueName)
}

func initSMSSender(env map[string]string) sms.Sender {
	return bitAccess.NewSender(
		env["COMMUNICATIONS_API_URL"],
		env["COMMUNICATIONS_API_KEY"],
		env["COMMUNICATIONS_API_SECRET"],
	)
}

func initWSClient() *ws_server.Server {
	return ws_server.NewServer(resource.NewClientsPool())
}

func initPhoneNumberValidator(env map[string]string) phoneNumber.Validator {
	return twilio.NewValidator(
		env["COMMUNICATIONS_TWILIO_API_ROOT"],
		env["COMMUNICATIONS_TWILIO_ACCOUNT_SID"],
		env["COMMUNICATIONS_TWILIO_SECRET"],
	)
}

func initCronConfig(env map[string]string) *application.CronConfig {
	concurrencyWorker, err := strconv.Atoi(env["CRON_CONCURRENCY_WORKER"])
	if err != nil {
		log.Fatal().Err(err).Msg("invalid CRON_CONCURRENCY_WORKER")
	}

	durationTasks, err := strconv.ParseInt(env["CRON_TIME_DURATION_TASKS"], 10, 64)
	if err != nil {
		log.Fatal().Err(err).Msg("invalid CRON_TIME_DURATION_TASKS")
	}

	eventTopic := env["GCP_CRON_EVENT_TOPIC"]
	if len(eventTopic) < 0 {
		log.Fatal().Msg("GCP_CRON_EVENT_TOPIC not found in config")
	}

	projectId := env["GCP_CRON_PROJECT_ID"]
	if len(projectId) < 0 {
		log.Fatal().Msg("GCP_CRON_PROJECT_ID not found in config")
	}

	return application.NewCronConfig(projectId, eventTopic, concurrencyWorker, durationTasks)
}
