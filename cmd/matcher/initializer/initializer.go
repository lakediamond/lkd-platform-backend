package initializer

import (
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/matcherApplication"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/pkg/environment"

	"github.com/rs/zerolog/log"
)

func InitMatcherApplication(app *matcherApplication.Application) {
	env := environment.GetEnv()

	app.Config = compileConfig(env)
	app.Repo = initDB(env)
	app.EthClient = initEthereum(env)

}

func compileConfig(env map[string]string) matcherApplication.Config {
	return matcherApplication.Config{
		Env:      env["ENV"],
		LogLevel: env["LOG_LEVEL"],
	}
}

func initDB(env map[string]string) *repo.Repo {
	name := env["DATABASE_NAME"]
	user := env["DATABASE_USER"]
	pass := env["DATABASE_PASSWORD"]
	host := env["DATABASE_HOST"]
	port := env["DATABASE_PORT"]

	r, err := repo.GetDbClient(name, user, pass, host, port)
	if err != nil {
		log.Fatal().Msg("Cannot connect to database")
	}

	return r
}

func initEthereum(env map[string]string) *etherPkg.PlatformEthereumApplication {
	md := initializeMatcherData(env)
	ethApp := etherPkg.InitializePlatformEthereumApplication(md)
	return ethApp
}

//InitializeMatcherData initializing matcher data for event listener
func initializeMatcherData(env map[string]string) *etherPkg.MatcherData {
	return &etherPkg.MatcherData{
		StorageAddress:        env["ETHEREUM_IO_STORAGE_CONTRACT_ADDRESS"],
		LakeDiamond:           env["ETHEREUM_IO_CONTRACT_ADDRESS"],
		NewOrder:              env["ETHEREUM_NEW_ORDER_EVENT"],
		CloseOrder:            env["ETHEREUM_CLOSE_ORDER_EVENT"],
		NewProposal:           env["ETHEREUM_NEW_PROPOSAL_EVENT"],
		WithdrawProposal:      env["ETHEREUM_WITHDRAW_PROPOSAL_EVENT"],
		CompleteProposal:      env["ETHEREUM_COMPLETE_PROPOSAL_EVENT"],
		NewOrderType:          env["ETHEREUM_NEW_ORDER_TYPE_EVENT"],
		CompleteOrder:         env["ETHEREUM_ORDER_COMPLETE_EVENT"],
		NewSubOwner:           env["ETHEREUM_NEW_SUBOWNER_EVENT"],
		RemovedSubOwner:       env["ETHEREUM_REMOVED_SUBOWNER_EVENT"],
		IOContractAddress:     env["ETHEREUM_IO_CONTRACT_ADDRESS"],
		RPCPort:               env["RPC_PORT"],
		PrivateKeyStorageKey:  env["ETHEREUM_PRIVATE_KEY_STORAGE_KEY"],
		PrivateKeyStorageData: env["ETHEREUM_PRIVATE_KEY_STORAGE_DATA"],
	}
}
