package main

import (
	"github.com/rs/zerolog/log"
	"net"
	"os"

	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"

	"lkd-platform-backend/cmd/matcher/initializer"
	"lkd-platform-backend/internal/app/matcherBackend"
	"lkd-platform-backend/internal/pkg/matcherApplication"
	"lkd-platform-backend/pkg/environment"
)

func main() {
	if environment.CheckTracerRun(os.Getenv("ENV")) {
		log.Info().Msg("Start Tracer")
		addr := net.JoinHostPort(
			os.Getenv("DD_AGENT_HOST"),
			os.Getenv("DD_TRACE_AGENT_PORT"),
		)
		tracer.Start(tracer.WithAgentAddr(addr), tracer.WithServiceName("lkd-platform-fulfillment-service"))
		defer tracer.Stop()
	}

	app := matcherApplication.NewMatcherApplication()

	initializer.InitMatcherApplication(app)

	matcherBackend.RunListener(app)
}
