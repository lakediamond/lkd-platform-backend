package main

import (
	"lkd-platform-backend/cmd/migrate/initializer"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/priv"
)

func main() {
	app := application.NewApplication()
	initializer.InitMigration(app)
	priv.Migration(app.Repo.DB)
}
