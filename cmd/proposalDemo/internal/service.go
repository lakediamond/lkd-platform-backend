package internal

import (
	"math/big"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/cmd/proposalDemo/props"
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/tokenContract"
	"lkd-platform-backend/internal/pkg/repo"
)

func AwaitForLastProposal(repo *repo.Repo, prop *props.Proposal) {
	for {
		proposal, _ := repo.Proposal.GetProposalByTxHash(prop.TxHash)
		if proposal.TxHash == prop.TxHash {
			break
		}
		log.Print("await for event listener")
		time.Sleep(2 * time.Second)
	}
}

func UpdateProposal(repo *repo.Repo, prop *props.Proposal) {
	proposal, err := repo.Proposal.GetProposalByTxHash(prop.TxHash)
	if err != nil {
		log.Error().Err(err).Msg(err.Error())
	}

	t := proposal.CreatedOn.AddDate(0, 0, -prop.Date)

	proposal.CreatedOn = t
	err = repo.Proposal.UpdateProposal(&proposal)
	if err != nil {
		log.Error().Err(err).Msg("UpdateProposal error")
	}
}

func SendTokens(ethClient *etherPkg.PlatformEthereumApplication, tokenAddress string) error {
	token, err := tokenContract.NewToken(common.HexToAddress(tokenAddress), ethClient.PlatformEthereum)
	if err != nil {
		return err
	}

	pk, err := ethClient.Internal.GetOwnerPrivateKey()
	if err != nil {
		log.Error().Err(err).Msg("invalid ethereum private key")
		return err
	}

	key, err := crypto.HexToECDSA(pk)
	if err != nil {
		log.Error().Err(err).Msg("invalid ethereum private key")
		return err
	}

	auth := bind.NewKeyedTransactor(key)

	accounts := props.GetAccounts()

	for _, account := range accounts {

		acKey, err := crypto.HexToECDSA(account)
		if err != nil {
			log.Error().Err(err).Msg("invalid ethereum private key")
			return err
		}

	loop:
		tx, err := token.Transfer(auth, crypto.PubkeyToAddress(acKey.PublicKey), big.NewInt(1000000000000000000))
		if err != nil {
			log.Error().Err(err).Msg(err.Error())
			goto loop
		}
		log.Info().Msgf("account %s will receive 1000000000000000000 tokens. TX hash: %s", crypto.PubkeyToAddress(acKey.PublicKey).Hex(), tx.Hash().Hex())

	}

	return nil
}

func MakeApproveForEachAccount(ethClient *etherPkg.PlatformEthereumApplication, tokenAddress string) error {
	token, err := tokenContract.NewToken(common.HexToAddress(tokenAddress), ethClient.PlatformEthereum)
	if err != nil {
		return err
	}

	accounts := props.GetAccounts()

	for _, account := range accounts {
		key, err := crypto.HexToECDSA(account)
		if err != nil {
			log.Error().Err(err).Msg("invalid ethereum private key")
			return err
		}

		auth := bind.NewKeyedTransactor(key)
	loop:
		tx, err := token.Approve(auth, common.HexToAddress(ethClient.MatcherData.LakeDiamond), big.NewInt(1000000000000000000))
		if err != nil {
			log.Error().Err(err).Msg(err.Error())
			goto loop
		}
		log.Info().Msgf("account %s will approve 1000000000000000000 tokens. TX hash: %s", crypto.PubkeyToAddress(key.PublicKey).Hex(), tx.Hash().Hex())

	}

	return nil
}
