package main

import (
	"flag"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/cmd/backend/initializer"
	"lkd-platform-backend/cmd/proposalDemo/internal"
	"lkd-platform-backend/cmd/proposalDemo/props"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/pkg/environment"
)

func main() {
	app := application.NewApplication()

	env := environment.GetEnv()
	app.Repo = initializer.InitDB(env)
	app.EthClient = initializer.InitEthereum(env)

	mode := flag.Int("mode", 1, "application mode")
	token := flag.String("token", "", "token contract address")
	flag.Parse()

	log.Print("mode: ", *mode)
	switch *mode {
	case 0:
		if !common.IsHexAddress(*token) {
			log.Fatal().Msg("invalid ethereum address")
		}
		sendTokensAndApprove(app.EthClient, *token)
	case 1:
		sendProposals(app)
	}
}

func sendTokensAndApprove(ethClient *etherPkg.PlatformEthereumApplication, tokenAddress string) {
	err := internal.SendTokens(ethClient, tokenAddress)
	if err != nil {
		log.Fatal().Err(err).Msg(err.Error())
	}
	err = internal.MakeApproveForEachAccount(ethClient, tokenAddress)
	if err != nil {
		log.Fatal().Err(err).Msg(err.Error())
	}
}

func sendProposals(app *application.Application) {
	proposals := props.InputToProposals()

	for i, proposal := range proposals {

	loop:
		tx, err := app.EthClient.Internal.NewProposal(proposal.From, proposal.Price, proposal.Amount)
		if err != nil {
			log.Error().Err(err).Msgf("create proposal %v error, eth address: %s", proposal, crypto.PubkeyToAddress(proposal.From.PublicKey).String())
			goto loop
		}

		log.Info().Msgf("proposal %v created, hash: %s", proposal, tx.Hash().Hex())
		proposals[i].TxHash = tx.Hash().Hex()
		time.Sleep(5 * time.Second)
	}

	internal.AwaitForLastProposal(app.Repo, &proposals[len(proposals)-1])

	for _, proposal := range proposals {
		internal.UpdateProposal(app.Repo, &proposal)
	}
}
