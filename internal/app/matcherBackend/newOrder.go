package matcherBackend

import (
	"fmt"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/ioContract"
	"lkd-platform-backend/internal/pkg/repo"
	"math/big"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//NewOrder(_from, _price, msg.value, _tknAmount, _type, _metadata);
func newOrder(repo *repo.Repo, ethLog types.Log, blockTimestamp *time.Time) error {

	var orderId = ethLog.Topics[1].Big()
	var from = "0x" + ethLog.Topics[2].String()[26:]
	var orderType = ethLog.Topics[3].Big()

	myAbi, err := abi.JSON(strings.NewReader(ioContract.IOABI))
	if err != nil {
		log.Fatal().Msg("Invalid storageContract abi")
	}

	var data = struct {
		TokenAmount  *big.Int
		Metadata     string
		WeiAmount    *big.Int
		Price        *big.Int
		ExchangeRate *big.Int
	}{}
	myAbi.Unpack(&data, "OrderAdded", ethLog.Data)

	var order models.Order

	order.CreatedBy = from
	order.Price = decimal.NewFromBigInt(data.Price, 0)
	order.EthAmount = decimal.NewFromBigInt(data.WeiAmount, 0)

	order.TokenAmount = decimal.NewFromBigInt(data.TokenAmount, 0)

	order.TypeID = uint(orderType.Uint64())

	orderTypeDB, err := repo.OrderTypes.GetOrderTypeByID(order.TypeID)
	if err != nil {
		return fmt.Errorf("cannot get orderType from DB")
	}
	order.Types = orderTypeDB
	order.Metadata = data.Metadata
	order.Coefficient = int(data.ExchangeRate.Uint64())
	order.TxHash = ethLog.TxHash.String()
	order.BlockchainID = uint(orderId.Uint64())
	order.CreatedOn = *blockTimestamp

	err = repo.Order.UpdateOrder(&order)
	if err != nil {
		return err
	}

	defer func() {
		if err := recover(); err != nil {
			log.Error().Msgf("Recovered after add order fail, err: %s", err)
		}
	}()
	accountActivity.OrderAdded(repo, &order)

	log.Info().Msgf("New order successfully stored. "+
		"TxHash: %s, orderBlockchainID: %d, from: %s, price: %s, ether amount: %s, token amount: %s, type: %d, metadata: %s",
		order.TxHash, order.BlockchainID, order.CreatedBy, order.Price.String(), order.EthAmount.String(), order.TokenAmount.String(), order.TypeID, order.Metadata)

	defer func() {
		if err := recover(); err != nil {
			log.Error().Msgf("Recovered after match order fail, err: %s", err)
		}
	}()

	return nil
}
