package matcherBackend_test

import (
	"context"
	"crypto/ecdsa"
	. "fmt"
	"lkd-platform-backend/internal/app/matcherBackend"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/ioContract"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/storageContract"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/tokenContract"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
	"math/big"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/ethereum/go-ethereum/core/types"

	"github.com/ethereum/go-ethereum/crypto"

	"github.com/ethereum/go-ethereum/common"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"

	"github.com/rs/zerolog/log"

	"github.com/stretchr/testify/suite"
)

type MatcherSuite struct {
	suite.Suite
	App         *application.Application
	MockServer  *httptest.Server
	PrivateKey1 *ecdsa.PrivateKey
	Address1    common.Address
	Auth        *bind.TransactOpts

	Token   *tokenContract.Token
	Storage *storageContract.Storage
	IO      *ioContract.IO

	EthMock *etherPkg.EthMock

	StorageAddress common.Address
}

func (suite *MatcherSuite) SetupTestSuite() {

}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_MatcherSuite(t *testing.T) {
	suite.Run(t, new(MatcherSuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *MatcherSuite) SetupTest() {
	app, _, _ := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()

	ethMock, privKey := etherPkg.GetMockEthClient()

	suite.EthMock = ethMock

	app.EthClient = ethMock
	app.MatcherData = etherPkg.InitializeMatcherData()

	app.Config.CHFCoefficient = big.NewInt(1)

	app.MatcherData.EthereumPrivateKey = Sprintf("%x", crypto.FromECDSA(privKey))

	auth := bind.NewKeyedTransactor(privKey)

	suite.Auth = auth
	suite.Address1 = crypto.PubkeyToAddress(privKey.PublicKey)

	app.Repo.DB.Exec("DELETE FROM users;")
	app.Repo.DB.Exec("DELETE FROM jwt_tokens;")
	app.Repo.DB.Exec("DELETE FROM orders;")
	app.Repo.DB.Exec("DELETE FROM sub_owners;")
	app.Repo.DB.Exec("DELETE FROM proposals;")
	app.Repo.DB.Exec("DELETE FROM support_data;")
	app.Repo.DB.Exec("DELETE FROM order_types;")
	app.Repo.DB.Exec("DELETE FROM account_activities;")

	tokenAddress, _, token, err := tokenContract.DeployToken(auth, ethMock)
	if err != nil {
		log.Debug().Err(err).Msg("DeployToken error")
	}

	suite.EthMock.Commit()

	storageAddress, _, storage, err := storageContract.DeployStorage(auth, ethMock, tokenAddress)
	if err != nil {
		log.Debug().Err(err).Msg("DeployStorage error")
	}

	suite.EthMock.Commit()

	ioAddress, _, io, err := ioContract.DeployIO(auth, ethMock, storageAddress, suite.Address1)
	if err != nil {
		log.Debug().Err(err).Msg("DeployIo error")
	}

	suite.EthMock.Commit()

	_, err = storage.SetContractManager(auth, ioAddress)
	if err != nil {
		log.Debug().Err(err).Msg("SetContractManager error")
	}

	suite.EthMock.Commit()

	app.MatcherData.StorageAddress = storageAddress.String()
	app.MatcherData.LakeDiamond = ioAddress.String()
	app.MatcherData.IOContractAddress = ioAddress.String()

	suite.Token = token
	suite.Storage = storage
	suite.IO = io

	suite.StorageAddress = storageAddress

	suite.App = app
	suite.PrivateKey1 = privKey

	priv.Migration(app.Repo.DB)
	go matcherBackend.ListenEvents(app)
	go matcherBackend.ListenSubOwnerEvents(app)
}

func (suite *MatcherSuite) TestFiveProposalCompleted() {
	//set order type
	_, err := suite.IO.SetOrderType(suite.Auth, big.NewInt(1), "test1")
	if err != nil {
		log.Debug().Err(err).Msg("SetOrderType error")
	}

	suite.EthMock.Commit()
	newTransactionsMatcher(suite)
	//set order type finish

	//create 5 accounts
	key1, _ := crypto.GenerateKey()
	account1 := crypto.PubkeyToAddress(key1.PublicKey)
	auth1 := bind.NewKeyedTransactor(key1)

	key2, _ := crypto.GenerateKey()
	account2 := crypto.PubkeyToAddress(key2.PublicKey)
	auth2 := bind.NewKeyedTransactor(key2)

	key3, _ := crypto.GenerateKey()
	account3 := crypto.PubkeyToAddress(key3.PublicKey)
	auth3 := bind.NewKeyedTransactor(key3)

	key4, _ := crypto.GenerateKey()
	account4 := crypto.PubkeyToAddress(key4.PublicKey)
	auth4 := bind.NewKeyedTransactor(key4)

	key5, _ := crypto.GenerateKey()
	account5 := crypto.PubkeyToAddress(key5.PublicKey)
	auth5 := bind.NewKeyedTransactor(key5)
	//create 5 accounts finish

	//send tokens to these accounts
	_, err = suite.Token.Transfer(suite.Auth, account1, big.NewInt(10000000))
	if err != nil {
		log.Debug().Err(err).Msg("send tokens to account 1 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	_, err = suite.Token.Transfer(suite.Auth, account2, big.NewInt(10000000))
	if err != nil {
		log.Debug().Err(err).Msg("send tokens to account 2 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	_, err = suite.Token.Transfer(suite.Auth, account3, big.NewInt(10000000))
	if err != nil {
		log.Debug().Err(err).Msg("send tokens to account 3 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	_, err = suite.Token.Transfer(suite.Auth, account4, big.NewInt(10000000))
	if err != nil {
		log.Debug().Err(err).Msg("send tokens to account 4 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	_, err = suite.Token.Transfer(suite.Auth, account5, big.NewInt(10000000))
	if err != nil {
		log.Debug().Err(err).Msg("send tokens to account 5 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)
	//send tokens to these accounts finish

	//send ether to each account
	nonce, err := suite.EthMock.PendingNonceAt(context.Background(), suite.Address1)
	if err != nil {
		log.Fatal()
	}

	tx := types.NewTransaction(nonce, account1, big.NewInt(100000000000000000), 50000000, big.NewInt(100), nil)

	signedTx, err := types.SignTx(tx, types.HomesteadSigner{}, suite.PrivateKey1)
	if err != nil {
		log.Fatal()
	}

	suite.EthMock.SendTransaction(context.Background(), signedTx)

	suite.EthMock.Commit()

	tx = types.NewTransaction(nonce+1, account2, big.NewInt(100000000000000000), 50000000, big.NewInt(100), nil)

	signedTx, err = types.SignTx(tx, types.HomesteadSigner{}, suite.PrivateKey1)
	if err != nil {
		log.Fatal()
	}

	suite.EthMock.SendTransaction(context.Background(), signedTx)

	suite.EthMock.Commit()

	tx = types.NewTransaction(nonce+2, account3, big.NewInt(100000000000000000), 50000000, big.NewInt(100), nil)

	signedTx, err = types.SignTx(tx, types.HomesteadSigner{}, suite.PrivateKey1)
	if err != nil {
		log.Fatal()
	}

	suite.EthMock.SendTransaction(context.Background(), signedTx)

	suite.EthMock.Commit()

	tx = types.NewTransaction(nonce+3, account4, big.NewInt(100000000000000000), 50000000, big.NewInt(100), nil)

	signedTx, err = types.SignTx(tx, types.HomesteadSigner{}, suite.PrivateKey1)
	if err != nil {
		log.Fatal()
	}

	suite.EthMock.SendTransaction(context.Background(), signedTx)

	suite.EthMock.Commit()

	tx = types.NewTransaction(nonce+4, account5, big.NewInt(100000000000000000), 50000000, big.NewInt(100), nil)

	signedTx, err = types.SignTx(tx, types.HomesteadSigner{}, suite.PrivateKey1)
	if err != nil {
		log.Fatal()
	}

	suite.EthMock.SendTransaction(context.Background(), signedTx)

	suite.EthMock.Commit()
	//send ether to each account finish

	_, err = suite.IO.AddSubOwners(suite.Auth, []common.Address{account1})

	//approve tokens to storage contract
	_, err = suite.Token.Approve(auth1, suite.StorageAddress, big.NewInt(10000000))
	if err != nil {
		log.Debug().Err(err).Msg("approve tokens from account 1 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	_, err = suite.Token.Approve(auth2, suite.StorageAddress, big.NewInt(10000000))
	if err != nil {
		log.Debug().Err(err).Msg("approve tokens from account 2 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	_, err = suite.Token.Approve(auth3, suite.StorageAddress, big.NewInt(10000000))
	if err != nil {
		log.Debug().Err(err).Msg("approve tokens from account 3 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	_, err = suite.Token.Approve(auth4, suite.StorageAddress, big.NewInt(10000000))
	if err != nil {
		log.Debug().Err(err).Msg("approve tokens from account 4 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	_, err = suite.Token.Approve(auth5, suite.StorageAddress, big.NewInt(10000000))
	if err != nil {
		log.Debug().Err(err).Msg("approve tokens from account 5 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	//approve tokens to storage contract finish

	//create proposals
	_, err = suite.IO.CreateProposal(auth5, big.NewInt(500), big.NewInt(5000))
	if err != nil {
		log.Debug().Err(err).Msg("create proposal 5 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	_, err = suite.IO.CreateProposal(auth1, big.NewInt(100), big.NewInt(1000))
	if err != nil {
		log.Debug().Err(err).Msg("create proposal 1 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	_, err = suite.IO.CreateProposal(auth3, big.NewInt(300), big.NewInt(3000))
	if err != nil {
		log.Debug().Err(err).Msg("create proposal 3 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	_, err = suite.IO.CreateProposal(auth2, big.NewInt(200), big.NewInt(2000))
	if err != nil {
		log.Debug().Err(err).Msg("create proposal 2 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	_, err = suite.IO.CreateProposal(auth4, big.NewInt(400), big.NewInt(4000))
	if err != nil {
		log.Debug().Err(err).Msg("create proposal 4 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)

	newTransactionsMatcher(suite)
	newTransactionsMatcher(suite)
	newTransactionsMatcher(suite)

	//ownerBalance, _ := suite.EthMock.BalanceAt(context.Background(), suite.Address1, nil)

	//user1Balance, _ := suite.EthMock.BalanceAt(context.Background(), account1, nil)
	user2Balance, _ := suite.EthMock.BalanceAt(context.Background(), account2, nil)
	user3Balance, _ := suite.EthMock.BalanceAt(context.Background(), account3, nil)
	user4Balance, _ := suite.EthMock.BalanceAt(context.Background(), account4, nil)
	user5Balance, _ := suite.EthMock.BalanceAt(context.Background(), account5, nil)

	newTransactionsMatcher(suite)
	newTransactionsMatcher(suite)
	newTransactionsMatcher(suite)

	//create an order
	auth1.Value = big.NewInt(10000000000000)
	_, err = suite.IO.CreateOrder(auth1, big.NewInt(1), "qweqwe", big.NewInt(250), big.NewInt(2000))
	if err != nil {
		log.Debug().Err(err).Msg("create order 1 error")
	}
	suite.EthMock.Commit()
	newTransactionsMatcher(suite)
	newTransactionsMatcher(suite)
	newTransactionsMatcher(suite)

	time.Sleep(5 * time.Second)

	opts := bind.CallOpts{Pending: true, From: suite.Address1, Context: context.Background()}
	proposal1, _ := suite.Storage.Proposals(&opts, big.NewInt(0))
	log.Print(proposal1)

	proposal2, _ := suite.Storage.Proposals(&opts, big.NewInt(1))
	log.Print(proposal2)

	proposal3, _ := suite.Storage.Proposals(&opts, big.NewInt(2))
	log.Print(proposal3)

	proposal4, _ := suite.Storage.Proposals(&opts, big.NewInt(3))
	log.Print(proposal4)

	proposal5, _ := suite.Storage.Proposals(&opts, big.NewInt(4))
	log.Print(proposal5)

	time.Sleep(2 * time.Second)

	//ownerBalance2, _ := suite.EthMock.BalanceAt(context.Background(), suite.Address1, nil)
	//user1Balance2, _ := suite.EthMock.BalanceAt(context.Background(), account1, nil)

	user2Balance2, _ := suite.EthMock.BalanceAt(context.Background(), account2, nil)
	user3Balance2, _ := suite.EthMock.BalanceAt(context.Background(), account3, nil)
	user4Balance2, _ := suite.EthMock.BalanceAt(context.Background(), account4, nil)
	user5Balance2, _ := suite.EthMock.BalanceAt(context.Background(), account5, nil)

	//suite.Equal(ownerBalance, ownerBalance2)

	//suite.Equal(user2Balance.Add(user2Balance, big.NewInt(500)), user2Balance2)
	suite.Equal(user2Balance.Add(user2Balance, big.NewInt(100*1000)), user2Balance2)

	suite.Equal(user3Balance, user3Balance2)

	//suite.Equal(user4Balance.Add(user4Balance, big.NewInt(500)), user4Balance2)
	suite.Equal(user4Balance.Add(user4Balance, big.NewInt(200*1000)), user4Balance2)
	//suite.Equal(user4Balance, user4Balance2)

	suite.Equal(user5Balance, user5Balance2)

}

func newTransactionsMatcher(suite *MatcherSuite) {
	for i := 0; i < 20; i++ {
		suite.EthMock.Commit()
	}
}
