package matcherBackend

import (
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/ioContract"
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func newOrderType(repo *repo.Repo, ethLog types.Log) error {
	var orderType models.OrderType

	id := ethLog.Topics[1].Big()
	orderType.ID = uint(id.Uint64())

	myAbi, err := abi.JSON(strings.NewReader(ioContract.IOABI))
	if err != nil {
		log.Fatal().Err(err).Msg("invalid definition for ethereum string")
	}

	var data = struct {
		Description string
	}{}
	if err = myAbi.Unpack(&data, "OrderTypeSet", ethLog.Data); err != nil {
		return err
	}

	orderType.Name = data.Description

	_, err = repo.OrderTypes.GetOrderTypeByID(orderType.ID)
	if err != nil {
		accountActivity.OrderTypeAdded(repo, &orderType)
	} else {
		accountActivity.OrderTypeUpdated(repo, &orderType)
	}

	err = repo.OrderTypes.SaveOrderType(&orderType)
	if err != nil {
		return err
	}

	log.Debug().Msgf("New order type successfully stored. Id: %d, description: %s", orderType.ID, orderType.Name)

	return nil
}
