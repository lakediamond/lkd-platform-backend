package matcherBackend

import (
	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/ioContract"
	"lkd-platform-backend/internal/pkg/repo"
	"math/big"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/repo/models"
)

//event ProposalWithdrawn(uint indexed amount, uint indexed id, bool isActive);
func withdrawProposal(repo *repo.Repo, ethLog types.Log, blockTimestamp *time.Time) error {
	id := ethLog.Topics[1].Big()
	withdrawnBy := "0x" + ethLog.Topics[2].String()[26:]

	myAbi, err := abi.JSON(strings.NewReader(ioContract.IOABI))
	if err != nil {
		log.Fatal().Err(err).Msg("invalid definition for ethereum string")
	}

	var data = struct {
		TokenAmount *big.Int
	}{}
	if err = myAbi.Unpack(&data, "ProposalWithdrawn", ethLog.Data); err != nil {
		return err
	}

	proposal, err := repo.Proposal.GetProposalByBlockchainID(uint(id.Uint64()))
	if err != nil {
		return err
	}

	if !proposal.Amount.IsZero() {
		proposal.WithdrawnAmount = decimal.NewFromBigInt(data.TokenAmount, 0)
		proposal.Amount = decimal.New(0, 0)
		proposal.Status = models.Cancelled
		proposal.WithdrawalTxHash = ethLog.TxHash.String()
		proposal.WithdrawnOn = blockTimestamp
		proposal.WithdrawnBy = withdrawnBy

		err = repo.Proposal.UpdateProposal(&proposal)
		if err != nil {
			return err
		}

		accountActivity.ProposalWithdrawn(repo, &proposal)

		log.Debug().Msgf("Proposal successfully withdrawn. BlockchainId: %d, amount withdrawn: %s, proposal status: %s",
			proposal.BlockchainID, proposal.WithdrawnAmount.String(), proposal.Status)
	} else {
		log.Debug().Msgf("Can`t withdraw proposal %d with token amount 0. Probably event duplication", id.Uint64())
	}

	return nil
}