package matcherBackend

import (
	"context"
	"encoding/hex"
	"math/big"
	"time"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/matcherApplication"

	"github.com/ethereum/go-ethereum/ethclient"
)

type EventInterface interface {
	Closes()
	IsEthClient() bool
	ReconnectedClient(app *matcherApplication.Application) *Event
}

type Event struct {
	app                  *matcherApplication.Application
	EthClient            *ethclient.Client
	eventChannel         chan types.Log
	eventSubError        chan error
	reconnectedEthClient chan bool
}

func (e *Event) IsEthClient() bool {
	if e.EthClient == nil {
		return false
	}

	if _, err := e.EthClient.NetworkID(context.Background()); err != nil {
		return false
	}

	return true
}

func (e *Event) ReconnectedClient(app *matcherApplication.Application) *Event {
	log.Info().Msg("Setting WS connection")
	rpcPort := app.EthClient.MatcherData.RPCPort

	ethClient, err := ethclient.Dial(rpcPort)
	if err != nil {
		log.Error().Err(err).Msg("Node connection broken. Reconnecting in 5 second")
		time.Sleep(5 * time.Second)
		e.ReconnectedClient(app)
	}

	e.EthClient = ethClient
	return e
}

func (e *Event) Closes() {
	close(e.eventChannel)
	close(e.eventSubError)
}

func NewEvent(app *matcherApplication.Application) *Event {
	client := app.EthClient.PlatformEthereum.(*ethclient.Client)
	event := &Event{app,
		client,
		make(chan types.Log),
		make(chan error),
		make(chan bool),
	}

	if !event.IsEthClient() {
		event.ReconnectedClient(app)
	}

	return event
}

// init all events
func initEvents(event *Event) {
	go listenPastEvents(event)
	go ListenEvents(event)
}

//RunListener starting event listener after start program or reconnect to ethereum node
func RunListener(app *matcherApplication.Application) {
	event := NewEvent(app)
	initEvents(event)
	syncEthChan(event, app)
}

// sync chan events
func syncEthChan(event *Event, app *matcherApplication.Application) {
	for {
		select {
		case reconnectedEthClient, ok := <-event.reconnectedEthClient:
			if !ok {
				log.Debug().Msgf("can not used chan reconnectedEthClient")
				return
			}

			if reconnectedEthClient {
				log.Debug().Msgf("Retry reconnectedEthClient")
				event.ReconnectedClient(app)
				initEvents(event)
			}

		case eventSubError, ok := <-event.eventSubError:
			if !ok {
				log.Debug().Msgf("can not used chan eventSubError")
				return
			}

			if eventSubError != nil { //  && eventSubError.Error() != "EOF"
				log.Debug().Msgf("reconnected subscribeStart")
				event.ReconnectedClient(app)
				go ListenEvents(event)
			}

		case eventChannel, ok := <-event.eventChannel:
			if !ok {
				log.Debug().Msgf("can not used chan eventChannel")
				return
			}

			checkLakeDiamondLog(event.app, eventChannel)
		}
	}
}

func listenPastEvents(event *Event) {
	log.Debug().Msgf("listenPastEvents start")
	lastBlock := event.app.Repo.SupportData.GetLastBlock()

	if !event.IsEthClient() {
		log.Error().Msgf("Can not connected eth client")
		event.reconnectedEthClient <- true
		return
	}

	var filter ethereum.FilterQuery
	filter.Addresses = []common.Address{common.HexToAddress(event.app.EthClient.MatcherData.LakeDiamond)}
	filter.FromBlock = big.NewInt(int64(lastBlock + 1))

	logs, filterErr := event.EthClient.FilterLogs(context.Background(), filter)
	if filterErr != nil {
		time.Sleep(5 * time.Second)
		log.Error().Err(filterErr).Msgf("Failed to get filter logs for contract: %s. Retrying", event.app.EthClient.MatcherData.LakeDiamond)
		event.reconnectedEthClient <- true
		return
	}

	for _, result := range logs {
		log.Debug().Msgf("listenPastEvents[event.eventChannel]: %+s", result.Topics[0].String())
		event.eventChannel <- result
		lastBlock = uint(result.BlockNumber)
	}

	if lastBlock != 0 {
		dbLastBlock := event.app.Repo.SupportData.GetLastBlock()
		if lastBlock > dbLastBlock {
			event.app.Repo.SupportData.SetLastBlock(lastBlock)
		}
	}

	event.reconnectedEthClient <- false
	log.Debug().Msg("ListenSubOwnerPastEvents successfully finished")
}

//ListenEvents listen events from IO smart contract
func ListenEvents(event *Event) {
	log.Debug().Msgf("ListenEvents start")
	if !event.IsEthClient() {
		event.reconnectedEthClient <- true
		return
	}

	log.Debug().Msgf("ListenEvents sync")
	var filter ethereum.FilterQuery
	filter.Addresses = []common.Address{common.HexToAddress(event.app.EthClient.MatcherData.LakeDiamond)}
	s, err := event.EthClient.SubscribeFilterLogs(context.Background(), filter, event.eventChannel)
	if err != nil {
		time.Sleep(5 * time.Second)
		log.Error().Err(err).Msgf("Unable to start event listener, retrying")
		event.reconnectedEthClient <- true
		return
	}
	log.Info().Msg("Main contract event listener started")
	event.reconnectedEthClient <- false

	for {
		select {
		case errorSub, ok := <-s.Err():
			log.Debug().Err(errorSub).Msgf("ListenEvents::[SubscribeFilterLogs] errors -> status %_+v", ok)
			if ok {
				if errorSub != nil {
					event.eventSubError <- errorSub
				}
			}
		}
	}
}

func checkLakeDiamondLog(app *matcherApplication.Application, ethLog types.Log) {

	log.Debug().Msgf("ethLogTopic: %+s", ethLog.Topics[0].String())

	blockTimestamp := app.EthClient.Internal.GetBlockTimestamp(big.NewInt(int64(ethLog.BlockNumber)), 0)

	var errCheck error

	switch ethLog.Topics[0].String() {
	case app.EthClient.MatcherData.NewSubOwner:
		errCheck = newSubOwner(app.Repo, ethLog)
		if errCheck != nil {
			log.Debug().Err(errCheck).Msg("New subOwner event error")
		}

	case app.EthClient.MatcherData.RemovedSubOwner:
		errCheck = removedSubOwner(app.Repo, ethLog)
		if errCheck != nil {
			log.Debug().Err(errCheck).Msg("Removed subOwner event error")
		}

	case app.EthClient.MatcherData.NewOrder:
		errCheck = newOrder(app.Repo, ethLog, &blockTimestamp)
		if errCheck != nil {
			log.Debug().Err(errCheck).Msg("New order event error")
		}

	case app.EthClient.MatcherData.NewProposal:
		errCheck = newProposal(app.Repo, ethLog, &blockTimestamp)
		if errCheck != nil {
			log.Debug().Err(errCheck).Msg("New proposal event error")
		}

	case app.EthClient.MatcherData.WithdrawProposal:
		errCheck = withdrawProposal(app.Repo, ethLog, &blockTimestamp)
		if errCheck != nil {
			log.Debug().Err(errCheck).Msg("Withdraw proposal event error")
		}

	case app.EthClient.MatcherData.NewOrderType:
		errCheck = newOrderType(app.Repo, ethLog)
		if errCheck != nil {
			log.Debug().Err(errCheck).Msg("New order type event error")
		}

	case app.EthClient.MatcherData.CompleteProposal:

		errCheck = completeProposal(app.Repo, ethLog, &blockTimestamp)
		if errCheck != nil {
			log.Debug().Err(errCheck).Msg("Complete proposal event error")
		}

	case app.EthClient.MatcherData.CompleteOrder:
		errCheck = closeOrder(app.Repo, ethLog, &blockTimestamp)
		if errCheck != nil {
			log.Debug().Err(errCheck).Msg("Complete order event error")
		}
	default:
		log.Debug().Msgf("Unhandled with topic[0]: %s", hex.EncodeToString(ethLog.Topics[0].Bytes()))
	}

	err := app.Repo.SupportData.SetLastBlock(uint(ethLog.BlockNumber))
	if err != nil {
		log.Error().Err(err).Msg("SetLastBlock error")
	}
}
