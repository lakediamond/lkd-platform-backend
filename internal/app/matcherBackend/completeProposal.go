package matcherBackend

import (
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/ioContract"
	"math/big"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func completeProposal(repo *repo.Repo, ethLog types.Log, blockTimestamp *time.Time) error {
	id := ethLog.Topics[1].Big()
	orderId := ethLog.Topics[2].Big()

	myAbi, err := abi.JSON(strings.NewReader(ioContract.IOABI))
	if err != nil {
		log.Fatal().Err(err).Msg("invalid definition for ethereum string")
	}

	var data = struct {
		TokenAmount *big.Int
		WeiAmount   *big.Int
		IsActive    bool
	}{}
	if err = myAbi.Unpack(&data, "ProposalCompleted", ethLog.Data); err != nil {
		return err
	}

	match, err := repo.Matches.GetMatchByOrderIDAndProposalId(int(orderId.Uint64()), int(id.Uint64()))
	if err != nil {
		log.Error().Err(err).Msg(err.Error())
		return err
	}

	if match != nil {
		log.Error().Msgf(
			"Match with proposal id %s and order id %s already created. Event data: token amount: %s, wei amount: %s, is active: %v",
			id.String(),
			orderId.String(),
			data.TokenAmount.String(),
			data.WeiAmount.String(),
			data.IsActive)

		return nil
	}

	proposal, err := repo.Proposal.GetProposalByBlockchainID(uint(id.Uint64()))
	if err != nil {
		log.Error().Err(err).Msg(err.Error())
		return err
	}

	proposal.Amount = proposal.Amount.Sub(decimal.NewFromBigInt(data.TokenAmount, 0))
	if !data.IsActive {
		proposal.Status = models.Completed

		proposal.CompletedOn = blockTimestamp
		accountActivity.ProposalClosed(repo, &proposal)
	}

	err = repo.Proposal.UpdateProposal(&proposal)
	if err != nil {
		log.Error().Err(err).Msg(err.Error())
		return err
	}

	log.Debug().Msgf("Proposal successfully completed. BlockchainId: %d, amount rest: %s, proposal status: %s",
		proposal.BlockchainID, proposal.Amount.String(), proposal.Status)

	match = &models.Matches{
		TxHash:               ethLog.TxHash.String(),
		ProposalBlockchainID: proposal.BlockchainID,
		OrderBlockchainID:    uint(orderId.Uint64()),
		Tokens:               decimal.NewFromBigInt(data.TokenAmount, 0),
		Ether:                decimal.NewFromBigInt(data.WeiAmount, 0),
	}

	err = repo.Matches.CreateNewMatch(match)
	if err != nil {
		log.Error().Err(err).Msg("Cannot save match")
		return err
	}

	return nil
}
