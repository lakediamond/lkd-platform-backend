package matcherBackend

import (
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/ioContract"
	"lkd-platform-backend/internal/pkg/repo"
	"math/big"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/accountActivity"
)

func closeOrder(repo *repo.Repo, ethLog types.Log, blockTimestamp *time.Time) error {

	id := ethLog.Topics[1].Big()

	order, err := repo.Order.GetOrderByBlockchainID(uint(id.Uint64()))
	if err != nil {
		log.Error().Err(err).Msg("GetOrderByBlockchainID error")
		return err
	}

	myAbi, err := abi.JSON(strings.NewReader(ioContract.IOABI))
	if err != nil {
		log.Fatal().Err(err).Msg("invalid definition for ethereum string")
	}

	var data = struct {
		Rest *big.Int
	}{}
	if err = myAbi.Unpack(&data, "OrderProcessed", ethLog.Data); err != nil {
		return err
	}

	order.EthAmountLeft = decimal.NewFromBigInt(data.Rest, 0)
	order.CompletedOn = *blockTimestamp

	err = repo.Order.UpdateOrder(&order)
	if err != nil {
		log.Error().Err(err).Msg(err.Error())
		return err
	}

	accountActivity.OrderMatched(repo, &order)

	log.Debug().Msgf("New order successfully closed. OrderBlockchainID: %d",
		order.BlockchainID)

	return nil
}
