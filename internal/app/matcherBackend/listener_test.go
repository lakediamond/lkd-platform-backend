package matcherBackend_test

import (
	"context"
	"crypto/ecdsa"
	. "fmt"
	"lkd-platform-backend/internal/app/matcherBackend"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/ioContract"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/storageContract"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/tokenContract"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
	"math/big"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/ethereum/go-ethereum/core/types"

	"github.com/ethereum/go-ethereum/crypto"

	"github.com/ethereum/go-ethereum/common"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"

	"github.com/rs/zerolog/log"

	"github.com/stretchr/testify/suite"
)

type ListenerTestSuite struct {
	suite.Suite
	App         *application.Application
	MockServer  *httptest.Server
	PrivateKey1 *ecdsa.PrivateKey
	Address1    common.Address
	Auth        *bind.TransactOpts

	Token   *tokenContract.Token
	Storage *storageContract.Storage
	IO      *ioContract.IO

	EthMock *etherPkg.EthMock

	StorageAddress common.Address
}

func (suite *ListenerTestSuite) SetupTestSuite() {

}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_ListenerTestSuite(t *testing.T) {
	suite.Run(t, new(ListenerTestSuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *ListenerTestSuite) SetupTest() {
	app, _, _ := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()

	ethMock, privKey := etherPkg.GetMockEthClient()

	suite.EthMock = ethMock

	app.EthClient = ethMock
	app.MatcherData = etherPkg.InitializeMatcherData()

	app.MatcherData.EthereumPrivateKey = Sprintf("%x", crypto.FromECDSA(privKey))

	auth := bind.NewKeyedTransactor(privKey)

	suite.Auth = auth
	suite.Address1 = crypto.PubkeyToAddress(privKey.PublicKey)

	app.Repo.DB.Exec("DELETE FROM users;")
	app.Repo.DB.Exec("DELETE FROM jwt_tokens;")
	app.Repo.DB.Exec("DELETE FROM orders;")
	app.Repo.DB.Exec("DELETE FROM proposals;")
	app.Repo.DB.Exec("DELETE FROM support_data;")
	app.Repo.DB.Exec("DELETE FROM order_types;")
	app.Repo.DB.Exec("DELETE FROM sub_owners;")
	app.Repo.DB.Exec("DELETE FROM account_activities;")

	tokenAddress, _, token, err := tokenContract.DeployToken(auth, ethMock)
	if err != nil {
		log.Debug().Err(err).Msg("DeployToken error")
	}

	suite.EthMock.Commit()

	storageAddress, _, storage, err := storageContract.DeployStorage(auth, ethMock, tokenAddress)
	if err != nil {
		log.Debug().Err(err).Msg("DeployStorage error")
	}

	suite.EthMock.Commit()

	ioAddress, _, io, err := ioContract.DeployIO(auth, ethMock, storageAddress, suite.Address1)
	if err != nil {
		log.Debug().Err(err).Msg("DeployIo error")
	}

	suite.EthMock.Commit()

	_, err = storage.SetContractManager(auth, ioAddress)
	if err != nil {
		log.Debug().Err(err).Msg("SetContractManager error")
	}

	suite.EthMock.Commit()

	app.MatcherData.StorageAddress = storageAddress.String()
	app.MatcherData.LakeDiamond = ioAddress.String()

	suite.Token = token
	suite.Storage = storage
	suite.IO = io

	suite.StorageAddress = storageAddress

	suite.App = app
	suite.PrivateKey1 = privKey

	priv.Migration(app.Repo.DB)
	go matcherBackend.ListenEvents(app)
	go matcherBackend.ListenSubOwnerEvents(app)
}

func (suite *ListenerTestSuite) TestNewOrderType() {
	orderType, err := suite.App.Repo.OrderTypes.GetOrderTypeByID(1)
	suite.Require().Error(err)

	_, err = suite.IO.SetOrderType(suite.Auth, big.NewInt(1), "test1")
	if err != nil {
		log.Debug().Err(err).Msg("SetOrderType error")
	}

	suite.EthMock.Commit()

	newTransactions(suite)

	orderType, err = suite.App.Repo.OrderTypes.GetOrderTypeByID(1)
	if err != nil {
		log.Fatal()
	}

	suite.Assert().Equal("test1", orderType.Name, "check order type in db")

	_, err = suite.IO.SetOrderType(suite.Auth, big.NewInt(1), "test2")
	if err != nil {
		log.Debug().Err(err).Msg("SetOrderType error")
	}

	suite.EthMock.Commit()

	newTransactions(suite)

	orderType, err = suite.App.Repo.OrderTypes.GetOrderTypeByID(1)
	if err != nil {
		log.Fatal()
	}

	suite.Assert().Equal("test2", orderType.Name, "check order type in db")
}

func (suite *ListenerTestSuite) TestNewOrder() {
	_, err := suite.Token.Approve(suite.Auth, suite.StorageAddress, big.NewInt(1000000))
	if err != nil {
		log.Debug().Err(err).Msg("Approve error")
	}

	suite.EthMock.Commit()

	_, err = suite.IO.SetOrderType(suite.Auth, big.NewInt(1), "test1")
	if err != nil {
		log.Debug().Err(err).Msg("SetOrderType error")
	}

	suite.EthMock.Commit()

	var orderData struct {
		orderType   *big.Int
		metadata    string
		price       *big.Int
		tokenAmount *big.Int
	}

	orderData.orderType = big.NewInt(1)
	orderData.metadata = "basic metadata"
	orderData.price = big.NewInt(123)
	orderData.tokenAmount = big.NewInt(2142)

	//message value
	suite.Auth.Value = big.NewInt(1000)

	_, err = suite.IO.CreateOrder(suite.Auth, orderData.orderType, orderData.metadata, orderData.price, orderData.tokenAmount)
	if err != nil {
		log.Debug().Msg(err.Error())
	}

	suite.EthMock.Commit()
	newTransactions(suite)

	order, err := suite.App.Repo.Order.GetOrderByBlockchainID(0)

	var orderData2 struct {
		orderType   *big.Int
		metadata    string
		price       *big.Int
		tokenAmount *big.Int
	}

	orderData2.orderType = big.NewInt(int64(order.TypeID))
	orderData2.metadata = order.Metadata

	var price = new(big.Int)
	price, _ = price.SetString(order.Price.String(), 16)
	orderData2.price = price

	var amount = new(big.Int)
	amount, _ = amount.SetString(order.TokenAmount.String(), 16)
	orderData2.tokenAmount = amount

	suite.Equal(order.EthAmount.Coefficient(), suite.Auth.Value, "eth amount must be equal")
	suite.Equal(order.Price.Coefficient(), orderData.price, "eth amount must be equal")
	suite.Equal(order.TokenAmount.Coefficient(), orderData.tokenAmount, "eth amount must be equal")

	//create order again

	orderData.orderType = big.NewInt(1)
	orderData.metadata = "basic metadata 2"
	orderData.price = big.NewInt(1242)
	orderData.tokenAmount = big.NewInt(111)

	suite.Auth.Value = big.NewInt(333)

	_, err = suite.IO.CreateOrder(suite.Auth, orderData.orderType, orderData.metadata, orderData.price, orderData.tokenAmount)
	if err != nil {
		log.Debug().Msg(err.Error())
	}

	suite.EthMock.Commit()
	newTransactions(suite)

	order, err = suite.App.Repo.Order.GetOrderByBlockchainID(1)

	orderData2.orderType = big.NewInt(int64(order.TypeID))
	orderData2.metadata = order.Metadata
	orderData2.price = order.Price.Coefficient()
	orderData2.tokenAmount = order.TokenAmount.Coefficient()

	suite.Equal(orderData, orderData2, "input data must be equal")
	suite.Equal(order.EthAmount.Coefficient(), suite.Auth.Value, "eth amount must be equal")
}

func (suite *ListenerTestSuite) TestNewProposal() {
	key, _ := crypto.GenerateKey()
	userAddress := crypto.PubkeyToAddress(key.PublicKey)

	userAuth := bind.NewKeyedTransactor(key)

	nonce, err := suite.EthMock.PendingNonceAt(context.Background(), suite.Address1)
	if err != nil {
		log.Fatal()
	}

	tx := types.NewTransaction(nonce, userAddress, big.NewInt(100000000000000000), 50000000, big.NewInt(100), nil)

	signedTx, err := types.SignTx(tx, types.HomesteadSigner{}, suite.PrivateKey1)
	if err != nil {
		log.Fatal()
	}

	suite.EthMock.SendTransaction(context.Background(), signedTx)

	suite.EthMock.Commit()

	_, err = suite.Token.Transfer(suite.Auth, userAddress, big.NewInt(10000))
	if err != nil {
		log.Debug().Msg(err.Error())
	}
	suite.EthMock.Commit()

	_, err = suite.Token.Approve(userAuth, suite.StorageAddress, big.NewInt(10000))
	if err != nil {
		log.Debug().Msg(err.Error())
	}
	suite.EthMock.Commit()

	_, err = suite.IO.CreateProposal(userAuth, big.NewInt(100), big.NewInt(200))
	if err != nil {
		log.Debug().Msg(err.Error())
	}

	suite.EthMock.Commit()
	newTransactions(suite)

	proposal, err := suite.App.Repo.Proposal.GetProposalByBlockchainID(0)
	if err != nil {
		log.Fatal()
	}

	suite.Equal("100", proposal.Price.String())
	suite.Equal("200", proposal.Amount.String())

	suite.Equal(proposal.CreatedBy, strings.ToLower(userAddress.String()))
}

func (suite *ListenerTestSuite) TestWithdrawProposal() {
	key, _ := crypto.GenerateKey()
	userAddress := crypto.PubkeyToAddress(key.PublicKey)

	userAuth := bind.NewKeyedTransactor(key)

	nonce, err := suite.EthMock.PendingNonceAt(context.Background(), suite.Address1)
	if err != nil {
		log.Fatal()
	}

	tx := types.NewTransaction(nonce, userAddress, big.NewInt(100000000000000000), 50000000, big.NewInt(100), nil)

	signedTx, err := types.SignTx(tx, types.HomesteadSigner{}, suite.PrivateKey1)
	if err != nil {
		log.Fatal()
	}

	suite.EthMock.SendTransaction(context.Background(), signedTx)

	suite.EthMock.Commit()

	_, err = suite.Token.Transfer(suite.Auth, userAddress, big.NewInt(10000))
	if err != nil {
		log.Debug().Msg(err.Error())
	}
	suite.EthMock.Commit()

	_, err = suite.Token.Approve(userAuth, suite.StorageAddress, big.NewInt(10000))
	if err != nil {
		log.Debug().Msg(err.Error())
	}
	suite.EthMock.Commit()

	_, err = suite.IO.CreateProposal(userAuth, big.NewInt(100), big.NewInt(200))
	if err != nil {
		log.Debug().Msg(err.Error())
	}

	suite.EthMock.Commit()
	newTransactions(suite)

	_, err = suite.IO.WithdrawProposal(userAuth, big.NewInt(0))
	if err != nil {
		log.Debug().Msg(err.Error())
	}

	suite.EthMock.Commit()
	newTransactions(suite)

	proposal, err := suite.App.Repo.Proposal.GetProposalByBlockchainID(0)
	if err != nil {
		log.Fatal()
	}

	suite.Equal("100", proposal.Price.String())
	suite.Equal("0", proposal.Amount.String())
	suite.Equal(models.Cancelled, proposal.Status)
}

func (suite *ListenerTestSuite) TestNewSubOwner() {
	key, _ := crypto.GenerateKey()
	userAddress := crypto.PubkeyToAddress(key.PublicKey)

	var user models.User

	user.Email = "1@2.test"
	user.EthAddress = strings.ToLower(userAddress.String())
	user.Role = models.UserRole

	suite.App.Repo.User.CreateNewUser(&user)
	suite.Equal(models.UserRole, user.Role)

	suite.Auth.Value = big.NewInt(0)

	_, err := suite.IO.AddSubOwners(suite.Auth, []common.Address{userAddress})
	if err != nil {
		log.Debug().Err(err).Msg("SetOrderType error")
	}

	suite.EthMock.Commit()

	newTransactions(suite)

	user, _ = suite.App.Repo.User.GetUserByEthAddress(strings.ToLower(userAddress.String()))

	suite.Equal(string(models.AdminRole), string(user.Role))
}

func (suite *ListenerTestSuite) TestWithdrawSubOwner() {
	key, _ := crypto.GenerateKey()
	userAddress := crypto.PubkeyToAddress(key.PublicKey)

	var user models.User

	user.Email = "1@3.test"
	user.EthAddress = strings.ToLower(userAddress.String())
	user.Role = models.UserRole

	suite.App.Repo.User.CreateNewUser(&user)
	suite.Equal(models.UserRole, user.Role)

	_, err := suite.IO.AddSubOwners(suite.Auth, []common.Address{userAddress})
	if err != nil {
		log.Debug().Err(err).Msg("SetOrderType error")
	}

	suite.EthMock.Commit()

	newTransactions(suite)
	newTransactions(suite)
	newTransactions(suite)

	user, _ = suite.App.Repo.User.GetUserByEthAddress(strings.ToLower(userAddress.String()))

	suite.Equal(models.AdminRole, user.Role)

	_, err = suite.IO.RemoveSubOwners(suite.Auth, []common.Address{userAddress})
	if err != nil {
		log.Debug().Err(err).Msg("SetOrderType error")
	}

	suite.EthMock.Commit()

	newTransactions(suite)
	newTransactions(suite)
	newTransactions(suite)

	user, _ = suite.App.Repo.User.GetUserByEthAddress(strings.ToLower(userAddress.String()))

	suite.Equal(models.UserRole, user.Role)
}

func newTransactions(suite *ListenerTestSuite) {
	for i := 0; i < 20; i++ {
		_, _ = suite.Token.Approve(suite.Auth, suite.Address1, big.NewInt(100))
		suite.EthMock.Commit()
	}
}
