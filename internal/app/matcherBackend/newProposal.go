package matcherBackend

import (
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/ioContract"
	"lkd-platform-backend/internal/pkg/repo"
	"math/big"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//event ProposalCreated(address indexed from, uint indexed price, uint indexed amount, uint id);
func newProposal(repo *repo.Repo, ethLog types.Log, blockTimestamp *time.Time) error {
	var from = "0x" + ethLog.Topics[2].String()[26:]

	id := ethLog.Topics[1].Big()

	myAbi, err := abi.JSON(strings.NewReader(ioContract.IOABI))
	if err != nil {
		log.Fatal().Err(err).Msg("invalid definition for ethereum string")
	}

	var data = struct {
		Price       *big.Int
		TokenAmount *big.Int
	}{}
	if err = myAbi.Unpack(&data, "ProposalCreated", ethLog.Data); err != nil {
		return err
	}

	var proposal models.Proposal

	proposal.BlockchainID = uint(id.Uint64())
	proposal.TxHash = ethLog.TxHash.String()
	proposal.Price = decimal.NewFromBigInt(data.Price, 0)
	proposal.Amount = decimal.NewFromBigInt(data.TokenAmount, 0)
	proposal.Status = models.Active
	proposal.OriginalTokens = decimal.NewFromBigInt(data.TokenAmount, 0)
	proposal.CreatedBy = from
	proposal.CreatedOn = *blockTimestamp

	err = repo.Proposal.UpdateProposal(&proposal)
	if err != nil {
		return err
	}

	accountActivity.ProposalCreated(repo, &proposal)

	log.Debug().Msgf("new proposal successfully stored. "+
		"Tx hash: %s, blockchainId: %d,  from: %s, price: %s, amount %s",
		proposal.TxHash, proposal.BlockchainID, from, proposal.Price.String(), proposal.Amount.String())

	return nil
}
