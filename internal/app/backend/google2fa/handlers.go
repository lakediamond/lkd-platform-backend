package google2fa

import (
	"net/http"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

type CompleteGoogle2FaInput struct {
	Password string `json:"password"`
}

type DisableGoogle2FaInput struct {
	Password      string `json:"password"`
	Google2FAPass string `json:"google_2_fa_pass"`
}

var EnableGoogle2FAHandler = handlers.ApplicationHandler(enableGoogle2FA)

func enableGoogle2FA(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)
	user := application.GetUser(r)

	origin := r.Header.Get("origin")
	qr, err := generateQR(app.Repo, &user, origin)
	if err != nil {
		log.Error().Err(err).Msg("generateQR error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(safeEncoding(map[string]string{"qr_code": qr}))
}

var CompleteGoogle2FaHandler = handlers.ApplicationHandler(completeGoogle2Fa)

func completeGoogle2Fa(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)
	user := application.GetUser(r)

	tokenDetails := application.GetTokenDetails(r)

	var completeGoogle2FaInput CompleteGoogle2FaInput

	err := httputil.ParseBody(w, r.Body, &completeGoogle2FaInput)
	if err != nil {
		return
	}

	defer r.Body.Close()

	err = completeGoogle2FaService(app.Repo, &user, &completeGoogle2FaInput)
	if err != nil {
		log.Debug().Err(err).Msg("completeGoogle2FaService error")
		w.WriteHeader(http.StatusForbidden)
		httputil.WriteErrorMsg(w, err)
		return
	}

	//hydra logout
	app.IAMClient.Logout(tokenDetails.Sub, "")

	//remove session cookies
	httputil.RemoveCookies(w)

	w.WriteHeader(http.StatusOK)
}

var DisableGoogle2FaHandler = handlers.ApplicationHandler(disableGoogle2Fa)

func disableGoogle2Fa(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)
	user := application.GetUser(r)

	var disableGoogle2FaInput DisableGoogle2FaInput
	err := httputil.ParseBody(w, r.Body, &disableGoogle2FaInput)
	if err != nil {
		return
	}

	defer r.Body.Close()

	err = disableGoogle2FaService(app.Repo, &user, &disableGoogle2FaInput)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		httputil.WriteErrorMsg(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}
