package google2fa

import (
	"bytes"
	"encoding/json"
	"math/rand"

	"github.com/dgryski/dgoogauth"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

type TwoFAInputData struct {
	Password string `json:"password"`
}

func generateQR(repo *repo.Repo, user *models.User, origin string) (string, error) {
	secret := randomString(16)
	log.Debug().Msgf("generated 2FA secret for user: %s, secret: %s", user.Email, secret)

	user.Google2FASecret = secret
	err := repo.User.UpdateUser(user)
	if err != nil {
		return "", err
	}

	otpc := &dgoogauth.OTPConfig{
		Secret:      secret,
		WindowSize:  3,
		HotpCounter: 0,
	}

	return otpc.ProvisionURIWithIssuer(user.Email, origin), nil

}

func randomString(len int) string {
	b := make([]byte, len)
	for i := 0; i < len; i++ {
		b[i] = byte(65 + rand.Intn(25))
	}
	return string(b)
}

func safeEncoding(v interface{}) []byte {
	b, _ := json.Marshal(v)

	b = bytes.Replace(b, []byte("\\u003c"), []byte("<"), -1)
	b = bytes.Replace(b, []byte("\\u003e"), []byte(">"), -1)
	b = bytes.Replace(b, []byte("\\u0026"), []byte("&"), -1)

	return b
}

func Check2FA(user *models.User, password string) bool {

	otpc := &dgoogauth.OTPConfig{
		Secret:      user.Google2FASecret,
		WindowSize:  3,
		HotpCounter: 0,
	}

	ok, err := otpc.Authenticate(password)
	if err != nil {
		return false
	}

	return ok
}
