package events

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/pkg/handlers"
)

// S3EventsHandler responsible for handling & validation http requests from AWS SNS with S3 notification events
var S3EventsHandler = handlers.ApplicationHandler(s3Events)

func s3Events(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)
	eventData := new(models.S3EventNotification)

	err := json.NewDecoder(r.Body).Decode(eventData)
	if err != nil {
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	if eventData.EventName == "" {
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	if eventData.EventName != models.S3Put {
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	log.Debug().Msgf("S3 event received - %v", eventData)
	err = updateKYCStepProgress(app, eventData)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

func getMediaItemType(event *models.S3EventNotification) (string, error) {
	i := strings.Split(event.Key, "/")
	if uploadedFile := i[3]; len(uploadedFile) > 0 {
		if tmp := strings.Split(uploadedFile, "."); len(tmp) == 2 {
			log.Debug().Msgf("media item type: ", tmp[0])
			return tmp[0], nil
		}
	}
	return "", fmt.Errorf("failed to extract media item type from event - %v", event)
}

func getMediaItemUserID(event *models.S3EventNotification) (uuid.UUID, error) {
	var userUID = uuid.UUID{}
	tokens := strings.Split(event.Key, "/")
	if len(tokens) != 4 {
		return userUID, fmt.Errorf("invalid event key format, expected to be split into 4 parts - %s", event.Key)
	}

	userUID = uuid.FromStringOrNil(tokens[1])
	return userUID, nil
}

func updateKYCStepProgress(app *application.Application, data *models.S3EventNotification) error {
	photoToken, err := getMediaItemType(data)
	log.Debug().Err(err).Msg("Photo token error")
	if err != nil {
		return err
	}

	userID, err := getMediaItemUserID(data)
	log.Debug().Err(err).Msg("User id err")
	if err != nil {
		return err
	}

	log.Debug().Msgf("User id from event - %v", userID)

	user, err := app.Repo.User.GetUserByID(&userID)
	if err != nil {
		return err
	}

	log.Debug().Msgf("User record from db - %v", user)

	if user.IsValidMediaType(photoToken) {
		log.Debug().Msg("Valid media type identified, execute status update")

		err = app.Repo.KYC.UpdateMediaItemStatus(&user, photoToken, models.MediaItemUploaded)
		if err != nil {
			log.Error().Err(err).Msg(err.Error())
			return err
		}
	} else {
		return errors.New("unknown photo token")
	}

	return nil
}
