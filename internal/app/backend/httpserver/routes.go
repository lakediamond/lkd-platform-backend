package httpserver

import (
	"lkd-platform-backend/internal/app/backend/frontEndpoints/dashboard"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/exchange"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/ws"
	"lkd-platform-backend/pkg/environment"
	"lkd-platform-backend/pkg/handlers"
	"net/http"
	"os"

	"github.com/justinas/alice"
	"gopkg.in/DataDog/dd-trace-go.v1/contrib/julienschmidt/httprouter"

	"lkd-platform-backend/internal/app/backend/events"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/admins"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/changePassword"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/matchSimulator"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/oauth"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/orderPrice"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/orderTypes"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/orders"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/proposals"
	"lkd-platform-backend/internal/app/backend/frontEndpoints/recoveryPassword"
	"lkd-platform-backend/internal/app/backend/google2fa"
	"lkd-platform-backend/internal/app/backend/users"
	"lkd-platform-backend/internal/app/backend/users/kyc"
	"lkd-platform-backend/internal/app/backend/users/pipeDrive"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/httputil"
)

func protectedRoute(app *application.Application, h http.Handler) http.Handler {
	if !environment.CheckDockerEnv(os.Getenv("ENV")) {
		return alice.New(app.CORS, application.RequestID, app.WithApplicationContext, app.RequireTokenAuthentication).Then(h)
	}
	return alice.New(application.RequestID, app.WithApplicationContext, app.RequireTokenAuthentication).Then(h)
}

func adminRoute(app *application.Application, h http.Handler) http.Handler {
	if !environment.CheckDockerEnv(os.Getenv("ENV")) {
		return alice.New(app.CORS, application.RequestID, app.WithApplicationContext, app.RequireTokenAuthentication, app.RequireAdminAccess).Then(h)
	}
	return alice.New(application.RequestID, app.WithApplicationContext, app.RequireTokenAuthentication, app.RequireAdminAccess).Then(h)
}

func defaultRoute(app *application.Application, h http.Handler) http.Handler {
	if !environment.CheckDockerEnv(os.Getenv("ENV")) {
		return alice.New(app.CORS, application.RequestID, app.WithApplicationContext, httputil.WithIPConfig).Then(h)
	}
	return alice.New(application.RequestID, app.WithApplicationContext, httputil.WithIPConfig).Then(h)
}

func handleCors(app *application.Application, h http.Handler) http.Handler {
	if !environment.CheckDockerEnv(os.Getenv("ENV")) {
		return alice.New(app.CORS, application.RequestID, app.WithApplicationContext).ThenFunc(app.HandleCORS)
	}
	return alice.New(application.RequestID, app.WithApplicationContext).ThenFunc(app.HandleCORS)
}

func basicAuth(app *application.Application, h http.Handler) http.Handler {
	if !environment.CheckDockerEnv(os.Getenv("ENV")) {
		return alice.New(app.CORS, application.RequestID, app.WithApplicationContext, app.BasicAuth).Then(h)
	}
	return alice.New(application.RequestID, app.WithApplicationContext, app.BasicAuth).Then(h)
}

// NewHTTPRouter will return application router.
// Require instance of application to properly mount routes to application and pass application context to HTTP Handlers
func NewHTTPRouter(app *application.Application) *httprouter.Router {
	router := httprouter.New()

	routes := application.Routes{

		application.Route{Alias: "register_account", Method: http.MethodPost, Path: "/user/register", Handler: defaultRoute(app, users.RegisterAccountHandler)},
		application.Route{Alias: "register_account", Method: http.MethodOptions, Path: "/user/register", Handler: handleCors(app, users.RegisterAccountHandler)},

		application.Route{Alias: "authenticate_account", Method: http.MethodPost, Path: "/user/authenticate", Handler: defaultRoute(app, users.AuthenticateAccountHandler)},
		application.Route{Alias: "authenticate_account", Method: http.MethodOptions, Path: "/user/authenticate", Handler: handleCors(app, users.AuthenticateAccountHandler)},

		application.Route{Alias: "user_logout", Method: http.MethodPost, Path: "/user/logout", Handler: protectedRoute(app, users.UserLogoutHandler)},
		application.Route{Alias: "user_logout", Method: http.MethodOptions, Path: "/user/logout", Handler: handleCors(app, users.UserLogoutHandler)},

		application.Route{Alias: "email_confirmation", Method: http.MethodGet, Path: "/user/email_confirmation", Handler: defaultRoute(app, users.ConfirmEmailHandler)},
		application.Route{Alias: "email_confirmation", Method: http.MethodOptions, Path: "/user/email_confirmation", Handler: handleCors(app, users.ConfirmEmailHandler)},

		application.Route{Alias: "get_all_admins", Method: http.MethodGet, Path: "/admin/subowner", Handler: adminRoute(app, admins.GetAllAdminsHandler)},
		application.Route{Alias: "get_all_admins", Method: http.MethodOptions, Path: "/admin/subowner", Handler: handleCors(app, admins.GetAllAdminsHandler)},

		// TO DO deprecated
		application.Route{Alias: "get_all_proposals", Method: http.MethodGet, Path: "/user/proposal/all", Handler: adminRoute(app, proposals.GetAllProposalsHandler)},
		application.Route{Alias: "get_all_proposals", Method: http.MethodOptions, Path: "/user/proposal/all", Handler: handleCors(app, proposals.GetAllProposalsHandler)},

		application.Route{Alias: "get_all_user_proposals", Method: http.MethodGet, Path: "/user/proposal", Handler: protectedRoute(app, proposals.GetAllUserProposalsHandler)},
		application.Route{Alias: "get_all_user_proposals", Method: http.MethodOptions, Path: "/user/proposal", Handler: handleCors(app, proposals.GetAllUserProposalsHandler)},

		application.Route{Alias: "get_proposal_owner", Method: http.MethodGet, Path: "/proposal/owner", Handler: protectedRoute(app, proposals.GetOwnersProposalHandler)},
		application.Route{Alias: "get_proposal_owner", Method: http.MethodOptions, Path: "/proposal/owner", Handler: handleCors(app, proposals.GetOwnersProposalHandler)},

		application.Route{Alias: "get_proposal_position_hint", Method: http.MethodGet, Path: "/user/proposal/estimate_placement", Handler: defaultRoute(app, proposals.GetNewProposalPositionHint)},
		application.Route{Alias: "get_proposal_position_hint", Method: http.MethodOptions, Path: "/user/proposal/estimate_placement", Handler: handleCors(app, proposals.GetNewProposalPositionHint)},

		application.Route{Alias: "check_user_phone_format", Method: http.MethodGet, Path: "/user/phone/format", Handler: protectedRoute(app, users.ValidatePhoneFormatHandler)},
		application.Route{Alias: "check_user_phone_format", Method: http.MethodOptions, Path: "/user/phone/format", Handler: handleCors(app, users.ValidatePhoneFormatHandler)},

		application.Route{Alias: "create_user_phone_verification", Method: http.MethodPost, Path: "/user/phone", Handler: protectedRoute(app, users.CreatePhoneVerificationHandler)},
		application.Route{Alias: "create_user_phone_verification", Method: http.MethodDelete, Path: "/user/phone", Handler: protectedRoute(app, users.DeletePhoneVerificationHandler)},
		application.Route{Alias: "create_user_phone_verification", Method: http.MethodOptions, Path: "/user/phone", Handler: handleCors(app, users.CreatePhoneVerificationHandler)},

		application.Route{Alias: "verify_user_phone_verification", Method: http.MethodPost, Path: "/user/phone/verify", Handler: protectedRoute(app, users.VerifyPhoneHandler)},
		application.Route{Alias: "verify_user_phone_verification", Method: http.MethodOptions, Path: "/user/phone/verify", Handler: handleCors(app, users.VerifyPhoneHandler)},

		application.Route{Alias: "resend_user_phone_verification", Method: http.MethodPost, Path: "/user/phone/resend", Handler: protectedRoute(app, users.ResendPhoneVerificationHandler)},
		application.Route{Alias: "resend_user_phone_verification", Method: http.MethodOptions, Path: "/user/phone/resend", Handler: handleCors(app, users.ResendPhoneVerificationHandler)},

		application.Route{Alias: "user_blockchain_provider", Method: http.MethodPatch, Path: "/user/profile/blockchain_provider", Handler: protectedRoute(app, users.SetBlockchainProviderHandler)},
		application.Route{Alias: "user_blockchain_provider", Method: http.MethodOptions, Path: "/user/profile/blockchain_provider", Handler: handleCors(app, users.SetBlockchainProviderHandler)},

		application.Route{Alias: "get_pagination_all_orders", Method: http.MethodGet, Path: "/user/order", Handler: protectedRoute(app, orders.GetPaginationAllOrdersHandler)},
		application.Route{Alias: "get_pagination_all_orders", Method: http.MethodOptions, Path: "/user/order", Handler: handleCors(app, orders.GetPaginationAllOrdersHandler)},

		application.Route{Alias: "get_orders_cumulative_data", Method: http.MethodGet, Path: "/admin/orders_cumulative_data", Handler: adminRoute(app, orders.GetOrdersCumulativeDataHandler)},
		application.Route{Alias: "get_orders_cumulative_data", Method: http.MethodOptions, Path: "/admin/orders_cumulative_data", Handler: handleCors(app, orders.GetOrdersCumulativeDataHandler)},

		application.Route{Alias: "get_all_order_types", Method: http.MethodGet, Path: "/admin/order_type", Handler: adminRoute(app, orderTypes.GetAllOrderTypesHandler)},
		application.Route{Alias: "get_all_order_types", Method: http.MethodOptions, Path: "/admin/order_type", Handler: handleCors(app, orderTypes.GetAllOrderTypesHandler)},

		application.Route{Alias: "user_lock", Method: http.MethodPost, Path: "/user/lock", Handler: adminRoute(app, users.LockUserHandler)},
		application.Route{Alias: "user_lock", Method: http.MethodOptions, Path: "/user/lock", Handler: handleCors(app, users.LockUserHandler)},

		application.Route{Alias: "user_profile", Method: http.MethodGet, Path: "/user/profile", Handler: protectedRoute(app, users.GetUserProfile)},
		application.Route{Alias: "user_profile", Method: http.MethodOptions, Path: "/user/profile", Handler: handleCors(app, users.GetUserProfile)},

		application.Route{Alias: "get_investment_plan_options", Method: http.MethodGet, Path: "/user/profile/kyc/plan", Handler: protectedRoute(app, users.GetInvestmentPlanOptionsHandler)},
		application.Route{Alias: "get_investment_plan_options", Method: http.MethodOptions, Path: "/user/profile/kyc/plan", Handler: handleCors(app, users.GetInvestmentPlanOptionsHandler)},

		application.Route{Alias: "get_user_kyc_profile", Method: http.MethodGet, Path: "/user/profile/kyc", Handler: protectedRoute(app, users.GetKycTiers)},
		application.Route{Alias: "get_user_kyc_profile", Method: http.MethodPost, Path: "/user/profile/kyc", Handler: protectedRoute(app, users.UpdateInvestmentPlan)},
		application.Route{Alias: "get_user_kyc_profile", Method: http.MethodOptions, Path: "/user/profile/kyc", Handler: handleCors(app, users.GetKycTiers)},

		application.Route{Alias: "kyc_tier_step", Method: http.MethodGet, Path: "/user/profile/kyc/tier/:tier/step/:step", Handler: protectedRoute(app, users.GetKycTierStep)},
		application.Route{Alias: "kyc_tier_step", Method: http.MethodPost, Path: "/user/profile/kyc/tier/:tier/step/:step", Handler: protectedRoute(app, users.UpdateKycTierStep)},
		application.Route{Alias: "kyc_tier_step", Method: http.MethodOptions, Path: "/user/profile/kyc/tier/:tier/step/:step", Handler: handleCors(app, users.GetKycTierStep)},

		application.Route{Alias: "s3_notification", Method: http.MethodPost, Path: "/events/s3", Handler: defaultRoute(app, events.S3EventsHandler)},

		application.Route{Alias: "check_oauth_login_request", Method: http.MethodGet, Path: "/oauth/requests/:type", Handler: defaultRoute(app, oauth.GetOAuthLoginRequest)},
		application.Route{Alias: "check_oauth_login_request", Method: http.MethodOptions, Path: "/oauth/requests/:type", Handler: handleCors(app, oauth.GetOAuthLoginRequest)},

		application.Route{Alias: "start_oauth_flow", Method: http.MethodGet, Path: "/oauth/flow/:flow", Handler: defaultRoute(app, oauth.StartOAuthFlow)},
		application.Route{Alias: "start_oauth_flow", Method: http.MethodOptions, Path: "/oauth/flow/:flow", Handler: handleCors(app, oauth.StartOAuthFlow)},

		application.Route{Alias: "process_consent", Method: http.MethodPost, Path: "/oauth/consent", Handler: defaultRoute(app, oauth.ProcessUserConsent)},
		application.Route{Alias: "process_consent", Method: http.MethodOptions, Path: "/oauth/consent", Handler: handleCors(app, oauth.ProcessUserConsent)},

		application.Route{Alias: "oauth_code", Method: http.MethodGet, Path: "/oauth/callback", Handler: defaultRoute(app, oauth.ProcessOAuthCallback)},
		application.Route{Alias: "oauth_code", Method: http.MethodOptions, Path: "/oauth/callback", Handler: handleCors(app, oauth.ProcessOAuthCallback)},

		application.Route{Alias: "order_eth_price", Method: http.MethodGet, Path: "/admin/order/price", Handler: adminRoute(app, orderPrice.GetOrderEthPriceHandler)},
		application.Route{Alias: "order_eth_price", Method: http.MethodOptions, Path: "/admin/order/price", Handler: handleCors(app, orderPrice.GetOrderEthPriceHandler)},

		application.Route{Alias: "user_change_password", Method: http.MethodPost, Path: "/user/change_password", Handler: protectedRoute(app, changePassword.ChangePasswordHandler)},
		application.Route{Alias: "user_change_password", Method: http.MethodOptions, Path: "/user/change_password", Handler: handleCors(app, changePassword.ChangePasswordHandler)},

		application.Route{Alias: "user_2fa_setup", Method: http.MethodPost, Path: "/user/2fa", Handler: protectedRoute(app, google2fa.EnableGoogle2FAHandler)},
		application.Route{Alias: "user_2fa_setup", Method: http.MethodOptions, Path: "/user/2fa", Handler: handleCors(app, google2fa.EnableGoogle2FAHandler)},

		application.Route{Alias: "user_2fa_disable", Method: http.MethodPost, Path: "/user/2fa/disable", Handler: protectedRoute(app, google2fa.DisableGoogle2FaHandler)},
		application.Route{Alias: "user_2fa_disable", Method: http.MethodOptions, Path: "/user/2fa/disable", Handler: handleCors(app, google2fa.DisableGoogle2FaHandler)},

		application.Route{Alias: "user_2fa_complete", Method: http.MethodPost, Path: "/user/2fa/complete", Handler: protectedRoute(app, google2fa.CompleteGoogle2FaHandler)},
		application.Route{Alias: "user_2fa_complete", Method: http.MethodOptions, Path: "/user/2fa/complete", Handler: handleCors(app, google2fa.CompleteGoogle2FaHandler)},

		application.Route{Alias: "id_verification_result", Method: http.MethodPost, Path: "/user/kyc/verification/:user_id/id", Handler: defaultRoute(app, kyc.VerificationUserHandler)},
		application.Route{Alias: "id_verification_result", Method: http.MethodOptions, Path: "/user/kyc/verification/:user_id/id", Handler: handleCors(app, kyc.VerificationUserHandler)},

		application.Route{Alias: "id_verification_url", Method: http.MethodGet, Path: "/user/kyc/media/id/:image_type", Handler: protectedRoute(app, users.GetGenerateUrlHandler)},
		application.Route{Alias: "id_verification_url", Method: http.MethodOptions, Path: "/user/kyc/media/id/:image_type", Handler: handleCors(app, users.GetGenerateUrlHandler)},

		application.Route{Alias: "btcs_flow_event", Method: http.MethodPost, Path: "/user/kyc/flow/:user_id", Handler: defaultRoute(app, kyc.BTCSFlowHandler)},
		application.Route{Alias: "btcs_flow_event", Method: http.MethodOptions, Path: "/user/kyc/flow/:user_id", Handler: protectedRoute(app, kyc.BTCSFlowHandler)},

		application.Route{Alias: "reupload_image_task", Method: http.MethodPost, Path: "/user/kyc/crm/reupload_image_task", Handler: basicAuth(app, users.CustomerImageReUploadRequestHandler)},
		application.Route{Alias: "tier_changes", Method: http.MethodPost, Path: "/user/kyc/crm/tier_changes", Handler: basicAuth(app, pipeDrive.TierAchievedHandler)},

		application.Route{Alias: "access_recovery", Method: http.MethodPost, Path: "/user/access_recovery", Handler: defaultRoute(app, recoveryPassword.RecoveryHandler)},
		application.Route{Alias: "access_recovery", Method: http.MethodOptions, Path: "/user/access_recovery", Handler: handleCors(app, recoveryPassword.RecoveryHandler)},

		application.Route{Alias: "access_recovery_activation", Method: http.MethodGet, Path: "/user/access_recovery_activation", Handler: defaultRoute(app, recoveryPassword.RecoveryFromEmailHandler)},
		application.Route{Alias: "access_recovery_activation", Method: http.MethodOptions, Path: "/user/access_recovery_activation", Handler: handleCors(app, recoveryPassword.RecoveryFromEmailHandler)},

		application.Route{Alias: "access_recovery_new_password", Method: http.MethodPost, Path: "/user/access_recovery_new_password", Handler: defaultRoute(app, recoveryPassword.NewPasswordHandler)},
		application.Route{Alias: "access_recovery_new_password", Method: http.MethodOptions, Path: "/user/access_recovery_new_password", Handler: handleCors(app, recoveryPassword.NewPasswordHandler)},

		application.Route{Alias: "verify_recovery_token", Method: http.MethodPost, Path: "/user/access_recovery_activation/check", Handler: defaultRoute(app, recoveryPassword.VerifyRecoveryTokenHandler)},
		application.Route{Alias: "verify_recovery_token", Method: http.MethodOptions, Path: "/user/access_recovery_activation/check", Handler: handleCors(app, recoveryPassword.VerifyRecoveryTokenHandler)},

		application.Route{Alias: "match_simulation", Method: http.MethodPost, Path: "/match_simulation", Handler: protectedRoute(app, matchSimulator.MatchSimulationHandler)},
		application.Route{Alias: "match_simulation", Method: http.MethodOptions, Path: "/match_simulation", Handler: handleCors(app, matchSimulator.MatchSimulationHandler)},

		application.Route{Alias: "submit_success_image_validation", Method: http.MethodPatch, Path: "/user/kyc/verification/:user_id/id/:image_type", Handler: defaultRoute(app, kyc.SubmitSuccessImageValidationHandler)},
		application.Route{Alias: "submit_success_image_validation", Method: http.MethodDelete, Path: "/user/kyc/verification/:user_id/id/:image_type", Handler: defaultRoute(app, kyc.SubmitInvalidImageValidationHandler)},
		application.Route{Alias: "submit_success_image_validation", Method: http.MethodOptions, Path: "/user/kyc/verification/:user_id/id/:image_type", Handler: handleCors(app, kyc.SubmitSuccessImageValidationHandler)},

		application.Route{Alias: "ws", Method: http.MethodGet, Path: "/ws", Handler: defaultRoute(app, ws.HandlerWSServer)},

		application.Route{Alias: "health_check", Method: http.MethodGet, Path: "/healthz", Handler: handlers.ApplicationHandler(func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusOK) })},

		application.Route{Alias: "dashboard_main_chart", Method: http.MethodGet, Path: "/dashboard/main_chart", Handler: protectedRoute(app, dashboard.MainChartHandler)},
		application.Route{Alias: "dashboard_main_chart", Method: http.MethodOptions, Path: "/dashboard/main_chart", Handler: handleCors(app, dashboard.MainChartHandler)},

		application.Route{Alias: "dashboard_proposal_queue_estimation", Method: http.MethodGet, Path: "/dashboard/proposal/queue_estimation", Handler: protectedRoute(app, dashboard.ProposalQueueEstimation)},
		application.Route{Alias: "dashboard_proposal_queue_estimation", Method: http.MethodOptions, Path: "/dashboard/proposal/queue_estimation", Handler: handleCors(app, dashboard.ProposalQueueEstimation)},

		application.Route{Alias: "user_exchange_rate", Method: http.MethodGet, Path: "/user/exchange", Handler: protectedRoute(app, exchange.ExchangeHandler)},
		application.Route{Alias: "user_exchange_rate", Method: http.MethodOptions, Path: "/user/exchange", Handler: handleCors(app, exchange.ExchangeHandler)},

		application.Route{Alias: "user_order_exchange_rate", Method: http.MethodGet, Path: "/order/rate", Handler: protectedRoute(app, exchange.CalculateExchangeRateHandler)},
		application.Route{Alias: "user_order_exchange_rate", Method: http.MethodOptions, Path: "/order/rate", Handler: handleCors(app, exchange.CalculateExchangeRateHandler)},
	}

	for _, route := range routes {
		router.Handler(route.Method, route.Path, route.Handler)
	}

	app.MountRoutes(routes)

	return router
}
