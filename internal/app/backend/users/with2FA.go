package users

import (
	"github.com/dgryski/dgoogauth"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
)

func with2FA(app *application.Application, authenticateData *AuthenticateData) (bool, error) {

	user, err := app.Repo.User.GetUserByEmail(authenticateData.Email)
	if err != nil {
		log.Debug().Err(err).Msg(err.Error())
		return false, err
	}

	if !user.Google2FAEnabled {
		return true, nil
	}

	otpc := &dgoogauth.OTPConfig{
		Secret:      user.Google2FASecret,
		WindowSize:  3,
		HotpCounter: 0,
	}

	ok, err := otpc.Authenticate(authenticateData.Google2FAPass)
	if err != nil {
		log.Debug().Err(err).Msg(err.Error())
		return false, err
	}

	log.Debug().Msgf("Google returns OK: %v", ok)

	return ok, nil
}
