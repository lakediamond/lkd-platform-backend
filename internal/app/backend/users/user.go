package users

import (
	"errors"
	"strconv"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/crypto"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func createUser(app *application.Application, registerData *RegisterData, CFIPAddress, CFIPCountry string, spanID uint64) (errs []error) {

	encryptedPass := crypto.EncodePass(registerData.Password)

	errs = ValidateData(registerData)
	if len(errs) > 0 {
		var user models.User
		user.FirstName = registerData.FirstName
		user.LastName = registerData.LastName
		user.EthAddress = registerData.EthAddress
		user.Email = registerData.Email
		user.BirthDate = registerData.BirthDate
		accountActivity.AccountCreationFailure(app.Repo, &user, CFIPAddress, CFIPCountry, errs)
		return errs
	}

	if app.EthClient.Internal.IsContract(common.HexToAddress(registerData.EthAddress)) {
		errs = append(errs, errors.New("address is a contract"))
		return errs
	}

	registerData.EthAddress = strings.ToLower(registerData.EthAddress)

	isOwner := app.Repo.SubOwner.IsSubOwner(registerData.EthAddress)

	var role models.Role

	if isOwner {
		role = models.AdminRole
	} else {
		role = models.UserRole
	}

	user := models.User{
		FirstName:      registerData.FirstName,
		LastName:       registerData.LastName,
		Email:          registerData.Email,
		BirthDate:      registerData.BirthDate,
		Password:       encryptedPass,
		EthAddress:     registerData.EthAddress,
		Role:           role,
		InvestmentPlan: registerData.InvestmentPlan,
	}

	if err := app.Repo.User.CreateNewUser(&user); err != nil {
		errs = append(errs, err)
	}

	if errs != nil {
		accountActivity.AccountCreationFailure(app.Repo, &user, CFIPAddress, CFIPCountry, errs)
		return errs
	}

	err := createEventCronEmailVerification(app, &user, registerData.LoginChallenge, spanID)
	if err != nil {
		errs = append(errs, err)
		accountActivity.AccountCreationFailure(app.Repo, &user, CFIPAddress, CFIPCountry, errs)
		return errs
	}

	accountActivity.AccountCreated(app.Repo, &user, CFIPAddress, CFIPCountry)

	return nil
}

func authenticate(app *application.Application, data AuthenticateData) error {
	email, err := verifyEmail(data.Email)
	if err != nil {
		log.Debug().Msg("invalid email")
	}

	user, err := app.Repo.User.GetUserByEmail(email)
	if err != nil {
		return err
	}

	if user.Locked {
		return errors.New("user locked")
	}

	if !user.EmailVerified {
		return errors.New("email not verified")
	}

	if !crypto.Verify(data.Password, user.Password) {
		return errors.New("wrong password")
	}

	return nil
}

func registerFlags(app *application.Application, user *models.User) error {

	kycTier, err := app.Repo.KYC.GetTierByCode(user, 0)
	if err != nil {
		return err
	}

	kycTierFlag := models.KYCTierFlag{
		KYCTierID: kycTier.ID,
		Code:      app.PipeDrive.GetPerson().EmailUserVerificationValue,
		Value:     "True",
	}

	err = app.Repo.KYC.CreateTierFlag(&kycTierFlag)
	if err != nil {
		return err
	}

	bd, err := strconv.ParseInt(strings.Replace(user.BirthDate, "-", "", -1), 10, 32)
	if err != nil {
		return err
	}

	t, err := strconv.ParseInt(time.Now().Format("20060102"), 10, 32)
	if err != nil {
		return nil
	}

	kycTierFlag = *new(models.KYCTierFlag)
	u := uuid.NewV4()

	kycTierFlag = models.KYCTierFlag{
		ID:        &u,
		KYCTierID: kycTier.ID,
		Code:      app.PipeDrive.GetPerson().AgeOver18VerifiedValue,
	}

	kycTierFlag.ID = &u
	kycTierFlag.KYCTierID = kycTier.ID
	kycTierFlag.Code = app.PipeDrive.GetPerson().AgeOver18VerifiedValue
	if t-bd < 180000 {
		kycTierFlag.Value = "False"
	} else {
		kycTierFlag.Value = "True"
	}

	err = app.Repo.KYC.CreateTierFlag(&kycTierFlag)
	if err != nil {
		return err
	}

	log.Debug().Msg("Finish creating AgeOver18VerifiedValue flag")

	return nil
}
