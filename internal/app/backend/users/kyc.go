package users

import (
	"errors"
	"fmt"
	"lkd-platform-backend/internal/app/backend/users/kyc"
	"strconv"

	"github.com/fourcube/goiban"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/app/backend/users/pipeDrive"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

type (
	ErrRecordNotFound struct {
		ErrBase
	}
)

var (
	errRecordNotFound = ErrRecordNotFound{ErrBaseNew("record not found")}
)

type KYCService struct {
	app  *application.Application
	repo *repo.Repo
}

func NewKYCService(app *application.Application) *KYCService {
	return &KYCService{app, app.Repo}
}

func (ks *KYCService) createKYCApplication(user *models.User) error {
	return nil
}

func (ks *KYCService) getKYCPlans(user *models.User) ([]models.KYCInvestmentPlan, error) {
	plan, err := ks.repo.KYC.GetPlans(user)
	if err != nil {
		return nil, err
	}

	return plan, nil
}

func (ks *KYCService) getKYCTiers(user *models.User) ([]models.KYCTier, error) {
	tiers, err := ks.repo.KYC.GetTiers(user)
	if err != nil {
		return nil, err
	}

	return tiers, nil
}

func (ks *KYCService) getKYCTierStep(user *models.User, tierCode int, stepCode string) (*models.KYCTierStep, error) {
	step, err := ks.repo.KYC.GetTierStep(user, tierCode, stepCode)
	if err != nil {
		return nil, err
	}

	//contentItem := &models.KYCTierStepContentItem{}
	dicItems, err := ks.repo.KYC.GetDictionaryCountryPhoneCodeRecords()
	if err != nil {
		return nil, err
	}

	switch step.Code {
	case models.PhoneVerificationStep:
		for _, item := range step.ContentItems {
			if item.Code == "country_code" {
				for _, option := range dicItems {
					item.Options = append(item.Options, option)
				}
			}
		}
	case models.CommonInfo:
		for _, item := range step.ContentItems {
			if item.Code == "reg_address_country" {
				for _, option := range dicItems {
					item.Options = append(item.Options, option)
				}
			}
		}
	}

	return step, nil
}

func (ks *KYCService) getUserKYCSummary(user *models.User) (models.KYCSummary, error) {
	result := models.KYCSummary{}
	if user.KYCInvestmentPlan == nil {
		return result, nil
	}

	investmentPlan := models.KYCInvestmentPlan{}

	tier, err := ks.repo.KYC.GetUserTierByStatus(user, []models.KYCTierStatus{models.TierGranted})
	if err != nil {
		switch err.Error() {
		case errRecordNotFound.Error():
		default:
			return result, err
		}
	}

	if tier.ID != nil {
		investmentPlan, err = ks.repo.KYC.GetPlanByCode(tier.Code)
		if err != nil {
			return result, err
		}

		result.Achieved = &models.KYCSummaryEntry{
			ID:          tier.Code,
			Description: fmt.Sprintf("Tier %d (%s)", tier.Code, investmentPlan.Description),
		}
	}

	tier, err = ks.repo.KYC.GetUserTierByStatus(user, []models.KYCTierStatus{models.TierOpened, models.TierRework, models.TierSubmitted})
	if err != nil {
		switch err.Error() {
		case errRecordNotFound.Error():
		default:
			return result, err
		}
	}

	if tier.ID != nil {
		investmentPlan, err = ks.repo.KYC.GetPlanByCode(tier.Code)
		if err != nil {
			return result, err
		}

		result.Ongoing = &models.KYCSummaryEntry{
			ID:          tier.Code,
			Description: fmt.Sprintf("Tier %d (%s)", tier.Code, investmentPlan.Description),
		}
	}

	tier, err = ks.repo.KYC.GetUserTierByStatus(user, []models.KYCTierStatus{models.TierLocked})
	if err != nil {
		switch err.Error() {
		case errRecordNotFound.Error():
		default:
			return result, err
		}
	}

	if tier.ID != nil {
		investmentPlan, err = ks.repo.KYC.GetPlanByCode(tier.Code)
		if err != nil {
			return result, err
		}

		result.Upcoming = &models.KYCSummaryEntry{
			ID:          tier.Code,
			Description: fmt.Sprintf("Tier %d (%s)", tier.Code, investmentPlan.Description),
		}
	}

	return result, nil
}

func (ks *KYCService) isValidIBAN(commonInfo *models.StepCommonInfoForm) (bool, []error) {
	iban := goiban.ParseToIban(commonInfo.BankIBAN)
	if iban == nil {
		return false, []error{fmt.Errorf("failed to parse given value %s into IBAN", commonInfo.BankIBAN)}
	}

	result := iban.Validate()
	if !result.Valid {
		var validationErrors []error
		for _, msg := range result.Messages {
			validationErrors = append(validationErrors, errors.New(msg))
		}
		return false, validationErrors
	}

	return true, nil
}

func (ks *KYCService) isValidContributionDetails(commonInfo *models.StepCommonInfoForm) (bool, error) {
	if commonInfo.ContributionOption != models.OptFiat {
		return true, nil
	}

	if len(commonInfo.BankName) == 0 {
		return false, errors.New("Bank name is mandatory in case of fiat contribution")
	}
	if len(commonInfo.BankIBAN) == 0 {
		return false, errors.New("IBAN is mandatory in case of fiat contribution")
	}
	if len(commonInfo.BankAddress) == 0 {
		return false, errors.New("Bank address is mandatory in case of fiat contribution")
	}

	return true, nil
}

func (ks *KYCService) updateKYCTierStepCommonInfo(app *application.Application, user *models.User, commonInfo *models.StepCommonInfoForm) error {
	if contribDetailsValid, err := ks.isValidContributionDetails(commonInfo); !contribDetailsValid {
		return err
	}

	err := kyc.CreateKYCCustomer(app, user)
	if err != nil {
		log.Error().Err(err).Msg("kyc CreateKYCCustomer error")
		return err
	}

	err = ks.repo.KYC.UpdateTierStepCommonInfo(user, commonInfo)
	if err != nil {
		log.Error().Err(err).Msg("repo UpdateTierStepCommonInfo error")
		return err
	}

	err = ks.repo.KYC.UnlockTierStep(user, models.Tier0, models.IdentityScans)
	if err != nil {
		log.Error().Err(err).Msg("repo UnlockTierStep error")
		return err
	}

	err = pipeDrive.CreateNewPerson(app, user, commonInfo)
	if err != nil {
		log.Error().Err(err).Msg("pipeDrive CreateNewPerson error")
		return err
	}

	return nil
}

func (ks *KYCService) updateKYCTierStepComplianceReview(app *application.Application, user *models.User) error {
	return ks.repo.KYC.UpdateTierStepComplianceReview(user)
}

func (ks *KYCService) updateKYCTierStep(app *application.Application, user *models.User, tierCode int, stepCode string, data interface{}) error {
	updates := map[string]string{}
	var err error

	switch tierCode {
	case models.Tier0:
		if stepCode == models.PhoneVerificationStep {
			switch data.(type) {
			case CreatePhoneVerificationForm:
				pv := data.(CreatePhoneVerificationForm)
				updates = map[string]string{
					"phone_number": pv.Phone,
					"country_code": pv.CountryCode,
				}
			case struct{ Verified bool }:
				pv := data.(struct{ Verified bool })
				updates = map[string]string{
					"phone_verified": strconv.FormatBool(pv.Verified),
				}
			default:
			}

			err = ks.repo.KYC.UpdateTierStep(user, tierCode, stepCode, updates)
			if err != nil {
				return err
			}
		}
	case models.Tier1:
		if err = ks.repo.KYC.UpdateTierStepComplianceReview(user); err != nil {
			log.Error().Err(err).Msg("UpdateTierStepComplianceReview error")
		}
	default:
	}

	return nil
}
