package users

import (
	"errors"
	"fmt"
	"regexp"
	"unicode"

	"github.com/ethereum/go-ethereum/common"
	"github.com/goware/emailx"
)

func ValidateData(registerData *RegisterData) (errs []error) {
	email, err := verifyEmail(registerData.Email)
	if err != nil {
		errs = append(errs, err)
	}
	registerData.Email = email

	err = verifyHash(registerData.EthAddress)
	if err != nil {
		errs = append(errs, err)
	}

	err = verifyName(registerData.FirstName)
	if err != nil {
		errs = append(errs, fmt.Errorf("firstname: %s", err.Error()))
	}

	err = verifyName(registerData.LastName)
	if err != nil {
		errs = append(errs, fmt.Errorf("lastname: %s", err.Error()))
	}

	err = verifyBirthDate(registerData.BirthDate)
	if err != nil {
		errs = append(errs, err)
	}

	return errs
}

func verifyEmail(email string) (string, error) {
	email = emailx.Normalize(email)

	err := emailx.Validate(email)
	if err != nil {
		if err == emailx.ErrInvalidFormat {
			return "", errors.New("email: wrong format")
		}

		if err == emailx.ErrUnresolvableHost {
			return "", errors.New("email: unresolvable host")
		}
	}

	return email, nil
}

func verifyHash(hash string) error {
	if !common.IsHexAddress(hash) {
		return errors.New("invalid ethereum address")
	}

	return nil
}

func verifyName(s string) error {
	if len(s) == 0 {
		return errors.New("invalid")
	}

	for _, s := range s {
		dash := "-"
		dashRune := ([]rune(dash))[0]
		switch {
		case unicode.IsLetter(s) || unicode.IsUpper(s) || unicode.IsSpace(s) || s == dashRune:

		default:
			return errors.New("invalid")
		}
	}
	return nil
}

func verifyBirthDate(s string) error {
	re := regexp.MustCompile("((19|20)\\d\\d)-(0?[1-9]|1[012])-([0-2][0-9]|3[0]|3[1])")
	if !re.MatchString(s) {
		return errors.New("birth_date: invalid")
	}

	return nil
}
