package users

import (
	"fmt"
	"time"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

type (
	ErrPhoneVerificationToManyRequests struct {
		ErrBase
	}
	ErrPhoneVerificationAlreadyExists struct {
		ErrBase
	}
	ErrPhoneVerificationNotFound struct {
		ErrBase
	}
	ErrPhoneVerificationInvalid struct {
		ErrBase
	}
	ErrPhoneVerificationSMSGatewayProblem struct {
		ErrBase
	}
	ErrBase struct {
		message string
	}
)

func (e ErrBase) Error() string {
	return e.message
}

func ErrBaseNew(m string) ErrBase {
	return ErrBase{m}
}

var (
	errPhoneVerificationNotFound          = ErrPhoneVerificationNotFound{ErrBaseNew("There no active phone verification at the moment")}
	errPhoneVerificationAlreadyExists     = ErrPhoneVerificationAlreadyExists{ErrBaseNew("This phone number has been already submitted for verification")}
	errPhoneVerificationToManyRequests    = ErrPhoneVerificationToManyRequests{ErrBaseNew("Requests limit reached for given phone number")}
	errPhoneVerificationSMSGatewayProblem = ErrPhoneVerificationSMSGatewayProblem{ErrBaseNew("Failure on SMS service provider side")}
	errPhoneVerificationInvalid           = ErrPhoneVerificationInvalid{ErrBaseNew("Code Invalid")}
)

type PhoneVerificationService struct {
	app  *application.Application
	repo *repo.Repo
}

func NewPhoneVerificationService(app *application.Application) *PhoneVerificationService {
	return &PhoneVerificationService{app, app.Repo}
}

//Validate phone number
func (pvs *PhoneVerificationService) IsValidFormat(app *application.Application, phoneValue string) (bool, error) {
	return app.PhoneNumberValidator.ValidateNumber(phoneValue)
}

//start new phone verification
func (pvs *PhoneVerificationService) CreatePhoneVerificationService(user *models.User, userInput CreatePhoneVerificationForm) error {
	alreadyExists, err := pvs.repo.PhoneVerification.Exists(userInput.Phone, []models.PVStatus{models.PVNew, models.PVVerified})
	if err != nil {
		return err
	}
	if alreadyExists {
		return errPhoneVerificationAlreadyExists
	}
	count, err := pvs.repo.PhoneVerification.CheckVerificationCount(user)
	if err != nil {
		return err
	}
	err = pvs.repo.PhoneVerification.CancelPrevious(user, userInput.Phone)
	if err != nil {
		return err
	}
	if count > (pvs.app.Config.MaxVerificationPerDay - 1) {
		return errPhoneVerificationToManyRequests
	}

	pv, err := pvs.repo.PhoneVerification.CreatePhoneVerification(user, userInput.Phone, userInput.CountryCode)
	if err != nil {
		return err
	}

	eventCron := &models.Worker{
		UserID:              user.ID,
		Event:               models.EventSendSMS,
		State:               models.StateNew,
		PhoneVerificationID: pv.ID,
		CreatedAt:           time.Now(),
	}

	err = pvs.app.Repo.KYC.CreateWorker(eventCron)
	if err != nil {
		return err
	}

	return nil
}

//resend validation code
func (pvs *PhoneVerificationService) ResendPhoneVerificationService(user *models.User, phone string) error {
	pv, err := pvs.repo.PhoneVerification.GetVerificationByUser(user)

	if err != nil {
		return err
	}

	log.Debug().Msgf("=== Attempts %d ===", pv.Attempts)
	if pv.Attempts > (pvs.app.Config.MaxVerificationAttempts - 1) {
		return errPhoneVerificationToManyRequests
	}

	eventCron := &models.Worker{
		UserID:              user.ID,
		Event:               models.EventSendSMS,
		State:               models.StateNew,
		PhoneVerificationID: pv.ID,
		CreatedAt:           time.Now(),
	}

	err = pvs.app.Repo.KYC.CreateWorker(eventCron)
	if err != nil {
		return err
	}

	pv.Attempts++
	err = pvs.repo.DB.Save(&pv).Error
	if err != nil {
		return err
	}
	return nil
}

//verify phone number
func (pvs *PhoneVerificationService) VerifyPhoneService(app *application.Application, user models.User, code int) error {
	pv, err := pvs.repo.PhoneVerification.GetVerificationByUser(&user)

	if err != nil {
		return err
	}

	log.Debug().Msgf("=== Attempts %d ===", pv.Attempts)
	if pv.Attempts > (pvs.app.Config.MaxVerificationAttempts - 1) {
		return errPhoneVerificationToManyRequests
	}

	ok, err := pvs.repo.PhoneVerification.Verify(&pv, code)
	if err != nil {
		return err
	}

	if !ok {
		return errPhoneVerificationInvalid
	}

	return nil
}

//complete phone verificaiton
func (pvs *PhoneVerificationService) AfterVerifyPhoneService(app *application.Application, user models.User) error {
	kycTier, err := app.Repo.KYC.GetTierByCode(&user, models.Tier0)
	if err != nil {
		return err
	}

	phoneVerification, err := app.Repo.PhoneVerification.GetVerifiedUserPhone(&user)
	if err != nil {
		return err
	}

	var kycTierFlag models.KYCTierFlag
	kycTierFlag.KYCTierID = kycTier.ID
	kycTierFlag.Code = app.PipeDrive.GetPerson().TelephoneUserVerificationValue

	if phoneVerification.Status != "VERIFIED" {
		kycTierFlag.Value = "False"
	} else {
		kycTierFlag.Value = "True"
	}

	err = app.Repo.KYC.CreateTierFlag(&kycTierFlag)
	if err != nil {
		return err
	}

	kycTierFlag = *new(models.KYCTierFlag)
	kycTierFlag.KYCTierID = kycTier.ID
	kycTierFlag.Code = app.PipeDrive.GetDeal().TelephoneFromAllowedCountryValue

	kycCountryBlacklist, err := app.Repo.KYC.GetCountryBlacklistByCode(phoneVerification.CountryCode)
	if err == nil {
		kycTierFlag.Value = fmt.Sprintf("Failed; %s", kycCountryBlacklist.Name)
	} else {
		kycTierFlag.Value = "Passed"
	}

	err = app.Repo.KYC.CreateTierFlag(&kycTierFlag)
	if err != nil {
		return err
	}

	return nil
}
