package pipeDrive

import (
	"fmt"
	"strings"

	"github.com/pkg/errors"

	"github.com/Jeffail/gabs"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/pipeDrive"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//CustomerImageReUploadService service for reupload customer images
func CustomerImageReUploadService(app *application.Application, customerImageReUploadRequestInput *models.CustomerImageReUploadRequestInput) ([]string, error) {
	var processedImages []string

	user, err := app.Repo.User.GetUserByDealID(customerImageReUploadRequestInput.Current.DealID)
	if err != nil {
		return processedImages, err
	}

	str := strings.Replace(customerImageReUploadRequestInput.Current.Subject, " ", "", -1)
	taskSubjectItems := strings.Split(str, ",")

	for _, taskSubjectItem := range taskSubjectItems {
		imageMetainfo, storageErr := app.Storage.GetImageMetaInfo(&user, taskSubjectItem)
		if storageErr != nil {
			return processedImages, err
		}

		if imageMetainfo != nil {
			if err = app.Storage.InvalidateImage(&user, imageMetainfo); err != nil {
				return processedImages, err
			}
			processedImages = append(processedImages, taskSubjectItem)
		}
	}

	if len(processedImages) > 0 {
		if err = app.Repo.KYC.UnlockTierStep(&user, models.Tier0, models.IdentityScans); err != nil {
			return processedImages, err
		}

		if err = app.Repo.KYC.SetReworkTier(&user, models.Tier0); err != nil {
			return processedImages, err
		}
	}

	return processedImages, nil
}

//SetActivityDone Set true value into "done" activity column
func SetActivityDone(crm pipeDrive.ClientInterface, customerImageReUploadRequestInput *models.CustomerImageReUploadRequestInput, processedImages []string) error {
	jsonObj := gabs.New()
	jsonObj.Set(customerImageReUploadRequestInput.Current.ID, "id")
	jsonObj.Set("1", "done")
	jsonObj.Set(fmt.Sprintf("Number of images processed - %d", len(processedImages)), "note")

	_, err := crm.UpdateRecord(
		"activities",
		uint(customerImageReUploadRequestInput.Current.ID),
		jsonObj.Bytes(),
		"application/json")
	if err != nil {
		return errors.Wrapf(err, "Cannot update activity, id: %v. Reason", customerImageReUploadRequestInput.Current.ID)
	}

	return nil
}
