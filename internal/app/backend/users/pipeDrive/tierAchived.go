package pipeDrive

import (
	"net/http"
	"time"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

//TierAchievedInput expected input of handler for tier achieved event
type TierAchievedInput struct {
	Current struct {
		Active                                   bool        `json:"active"`
		ActivitiesCount                          int         `json:"activities_count"`
		AddTime                                  string      `json:"add_time"`
		Bfc7bdb2fc0b40acf49edbd363277f65397fd3a5 interface{} `json:"bfc7bdb2fc0b40acf49edbd363277f65397fd3a5"`
		CcEmail                                  string      `json:"cc_email"`
		CloseTime                                interface{} `json:"close_time"`
		CreatorUserID                            int         `json:"creator_user_id"`
		Currency                                 string      `json:"currency"`
		Deleted                                  bool        `json:"deleted"`
		DoneActivitiesCount                      int         `json:"done_activities_count"`
		EmailMessagesCount                       int         `json:"email_messages_count"`
		ExpectedCloseDate                        interface{} `json:"expected_close_date"`
		FilesCount                               int         `json:"files_count"`
		FirstWonTime                             interface{} `json:"first_won_time"`
		FollowersCount                           int         `json:"followers_count"`
		FormattedValue                           string      `json:"formatted_value"`
		FormattedWeightedValue                   string      `json:"formatted_weighted_value"`
		ID                                       int         `json:"id"`
		LastActivityDate                         string      `json:"last_activity_date"`
		LastActivityID                           int         `json:"last_activity_id"`
		LastIncomingMailTime                     interface{} `json:"last_incoming_mail_time"`
		LastOutgoingMailTime                     interface{} `json:"last_outgoing_mail_time"`
		LostReason                               interface{} `json:"lost_reason"`
		LostTime                                 interface{} `json:"lost_time"`
		NextActivityDate                         interface{} `json:"next_activity_date"`
		NextActivityDuration                     interface{} `json:"next_activity_duration"`
		NextActivityID                           interface{} `json:"next_activity_id"`
		NextActivityNote                         interface{} `json:"next_activity_note"`
		NextActivitySubject                      interface{} `json:"next_activity_subject"`
		NextActivityTime                         interface{} `json:"next_activity_time"`
		NextActivityType                         interface{} `json:"next_activity_type"`
		NotesCount                               int         `json:"notes_count"`
		OrgHidden                                bool        `json:"org_hidden"`
		OrgID                                    int         `json:"org_id"`
		OrgName                                  string      `json:"org_name"`
		OwnerName                                string      `json:"owner_name"`
		ParticipantsCount                        int         `json:"participants_count"`
		PersonHidden                             bool        `json:"person_hidden"`
		PersonID                                 int         `json:"person_id"`
		PersonName                               string      `json:"person_name"`
		PipelineID                               int         `json:"pipeline_id"`
		Probability                              interface{} `json:"probability"`
		ProductsCount                            int         `json:"products_count"`
		ReferenceActivitiesCount                 int         `json:"reference_activities_count"`
		RottenTime                               string      `json:"rotten_time"`
		StageChangeTime                          string      `json:"stage_change_time"`
		StageID                                  int         `json:"stage_id"`
		StageOrderNr                             int         `json:"stage_order_nr"`
		Status                                   string      `json:"status"`
		Title                                    string      `json:"title"`
		UndoneActivitiesCount                    int         `json:"undone_activities_count"`
		UpdateTime                               string      `json:"update_time"`
		UserID                                   int         `json:"user_id"`
		Value                                    int         `json:"value"`
		VisibleTo                                string      `json:"visible_to"`
		WeightedValue                            int         `json:"weighted_value"`
		WeightedValueCurrency                    string      `json:"weighted_value_currency"`
		WonTime                                  interface{} `json:"won_time"`
	} `json:"current"`
	Event          string `json:"event"`
	MatchesFilters struct {
		Current []interface{} `json:"current"`
	} `json:"matches_filters"`
	Meta struct {
		Action         string `json:"action"`
		CompanyID      int    `json:"company_id"`
		Host           string `json:"host"`
		ID             int    `json:"id"`
		IsBulkUpdate   bool   `json:"is_bulk_update"`
		MatchesFilters struct {
			Current []interface{} `json:"current"`
		} `json:"matches_filters"`
		Object               string `json:"object"`
		PermittedUserIds     []int  `json:"permitted_user_ids"`
		PipedriveServiceName bool   `json:"pipedrive_service_name"`
		Timestamp            int    `json:"timestamp"`
		TimestampMicro       int    `json:"timestamp_micro"`
		TransPending         bool   `json:"trans_pending"`
		UserID               int    `json:"user_id"`
		V                    int    `json:"v"`
		WebhookID            string `json:"webhook_id"`
	} `json:"meta"`
	Previous struct {
		Active                   bool        `json:"active"`
		ActivitiesCount          int         `json:"activities_count"`
		AddTime                  string      `json:"add_time"`
		CcEmail                  string      `json:"cc_email"`
		CloseTime                interface{} `json:"close_time"`
		CreatorUserID            int         `json:"creator_user_id"`
		Currency                 string      `json:"currency"`
		Deleted                  bool        `json:"deleted"`
		DoneActivitiesCount      int         `json:"done_activities_count"`
		EmailMessagesCount       int         `json:"email_messages_count"`
		ExpectedCloseDate        interface{} `json:"expected_close_date"`
		FilesCount               int         `json:"files_count"`
		FirstWonTime             interface{} `json:"first_won_time"`
		FollowersCount           int         `json:"followers_count"`
		FormattedValue           string      `json:"formatted_value"`
		FormattedWeightedValue   string      `json:"formatted_weighted_value"`
		ID                       int         `json:"id"`
		LastActivityDate         string      `json:"last_activity_date"`
		LastActivityID           int         `json:"last_activity_id"`
		LastIncomingMailTime     interface{} `json:"last_incoming_mail_time"`
		LastOutgoingMailTime     interface{} `json:"last_outgoing_mail_time"`
		LostReason               interface{} `json:"lost_reason"`
		LostTime                 interface{} `json:"lost_time"`
		NextActivityDate         interface{} `json:"next_activity_date"`
		NextActivityDuration     interface{} `json:"next_activity_duration"`
		NextActivityID           interface{} `json:"next_activity_id"`
		NextActivityNote         interface{} `json:"next_activity_note"`
		NextActivitySubject      interface{} `json:"next_activity_subject"`
		NextActivityTime         interface{} `json:"next_activity_time"`
		NextActivityType         interface{} `json:"next_activity_type"`
		NotesCount               int         `json:"notes_count"`
		OrgHidden                bool        `json:"org_hidden"`
		OrgID                    int         `json:"org_id"`
		OrgName                  string      `json:"org_name"`
		OwnerName                string      `json:"owner_name"`
		ParticipantsCount        int         `json:"participants_count"`
		PersonHidden             bool        `json:"person_hidden"`
		PersonID                 int         `json:"person_id"`
		PersonName               string      `json:"person_name"`
		PipelineID               int         `json:"pipeline_id"`
		Probability              interface{} `json:"probability"`
		ProductsCount            int         `json:"products_count"`
		ReferenceActivitiesCount int         `json:"reference_activities_count"`
		RottenTime               string      `json:"rotten_time"`
		StageChangeTime          string      `json:"stage_change_time"`
		StageID                  int         `json:"stage_id"`
		StageOrderNr             int         `json:"stage_order_nr"`
		Status                   string      `json:"status"`
		Title                    string      `json:"title"`
		UndoneActivitiesCount    int         `json:"undone_activities_count"`
		UpdateTime               string      `json:"update_time"`
		UserID                   int         `json:"user_id"`
		Value                    int         `json:"value"`
		VisibleTo                string      `json:"visible_to"`
		WeightedValue            int         `json:"weighted_value"`
		WeightedValueCurrency    string      `json:"weighted_value_currency"`
		WonTime                  interface{} `json:"won_time"`
	} `json:"previous"`
	Retry int `json:"retry"`
	V     int `json:"v"`
}

//TierAchievedHandler handler for tier achieved event
var TierAchievedHandler = handlers.ApplicationHandler(tierAchieved)

func tierAchieved(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)

	var tierAchievedInput TierAchievedInput

	err := httputil.ParseBody(w, r.Body, &tierAchievedInput)
	if err != nil {
		return
	}

	defer r.Body.Close()

	err = tierAchievedService(app, &tierAchievedInput)
	if err != nil {
		log.Error().Err(err).Msg("tierAchievedService error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func tierAchievedService(app *application.Application, tierAchievedInput *TierAchievedInput) error {
	if tierAchievedInput.Meta.Action != "updated" ||
		tierAchievedInput.Meta.Object != "deal" ||
		tierAchievedInput.Current.Deleted != false ||
		tierAchievedInput.Current.PipelineID != 2 {
		return nil
	}

	if tierAchievedInput.Current.Active == true &&
		tierAchievedInput.Current.StageID > tierAchievedInput.Previous.StageID {

		err := nextTierAchieved(app, tierAchievedInput)
		if err != nil {
			log.Error().Err(err).Msg("nextTierAchieved error")
			return err
		}
	}

	if tierAchievedInput.Current.Active == false &&
		tierAchievedInput.Current.Status == "won" {

		err := tierWonAchieved(app, tierAchievedInput)
		if err != nil {
			log.Error().Err(err).Msg("tierWonAchieved error")
			return err
		}
	}

	if tierAchievedInput.Current.Active == false &&
		tierAchievedInput.Current.Status == "lost" {

		err := tierRejected(app, tierAchievedInput)
		if err != nil {
			log.Error().Err(err).Msg("tierRejected error")
			return err
		}
	}

	return nil
}

func nextTierAchieved(app *application.Application, tierAchievedInput *TierAchievedInput) error {

	user, err := app.Repo.User.GetUserByDealID(tierAchievedInput.Current.ID)
	if err != nil {
		return err
	}

	tierGranted, err := app.Repo.KYC.GetTierByStageID(&user, tierAchievedInput.Previous.StageID)
	if err != nil {
		return err
	}

	err = app.Repo.KYC.UpdateTierStatus(&user, tierGranted.Code, models.TierGranted)
	if err != nil {
		return err
	}

	err = app.Repo.KYC.UnlockTierByCRMStageID(&user, tierAchievedInput.Current.StageID)
	if err != nil {
		return err
	}

	plan, err := app.Repo.KYC.GetPlanByCode(tierGranted.Code)
	if err != nil {
		return err
	}

	if err = app.Repo.User.UpdateTargetTier(&user, plan); err != nil {
		return err
	}

	eventCron := &models.Worker{
		UserID:    user.ID,
		Event:     models.EventAddToWhitelist,
		State:     models.StateNew,
		CreatedAt: time.Now(),
	}

	err = app.Repo.KYC.CreateWorker(eventCron)
	if err != nil {
		return err
	}

	return nil
}

func tierWonAchieved(app *application.Application, tierAchievedInput *TierAchievedInput) error {
	user, err := app.Repo.User.GetUserByDealID(tierAchievedInput.Current.ID)
	if err != nil {
		return err
	}

	tierGranted, err := app.Repo.KYC.GetTierByStageID(&user, tierAchievedInput.Previous.StageID)
	if err != nil {
		return err
	}

	err = app.Repo.KYC.UpdateTierStatus(&user, tierGranted.Code, models.TierGranted)
	if err != nil {
		return err
	}

	if user.KYCInvestmentPlan.Code > tierGranted.Code {
		err = app.Repo.KYC.UnlockTierByCode(&user, tierGranted.Code+1)
		if err != nil {
			return err
		}
	}

	plan, err := app.Repo.KYC.GetPlanByCode(tierGranted.Code)
	if err != nil {
		return err
	}

	if err = app.Repo.User.UpdateTargetTier(&user, plan); err != nil {
		return err
	}

	eventCron := &models.Worker{
		UserID:    user.ID,
		Event:     models.EventAddToWhitelist,
		State:     models.StateNew,
		CreatedAt: time.Now(),
	}

	err = app.Repo.KYC.CreateWorker(eventCron)
	if err != nil {
		return err
	}

	return nil
}

func tierRejected(app *application.Application, tierAchievedInput *TierAchievedInput) error {
	user, err := app.Repo.User.GetUserByDealID(tierAchievedInput.Current.ID)
	if err != nil {
		return err
	}

	tierRejected, err := app.Repo.KYC.GetTierByStageID(&user, tierAchievedInput.Previous.StageID)
	if err != nil {
		return err
	}
	tierRejected.Status = models.TierRejected

	err = app.Repo.KYC.UpdateTierByModel(&tierRejected)
	if err != nil {
		return err
	}

	return nil
}

//
//func addAddressToWhiteList(app *application.Application, address string, maxTokens *big.Int) error {
//	io, err := ioContract.NewIoContract(common.HexToAddress(app.Config.IOContractAddress), app.EthClient)
//	if err != nil {
//		return err
//	}
//
//	key, err := crypto.HexToECDSA(app.Config.EthereumPrivateKey)
//	if err != nil {
//		log.Error().Err(err).Msg("invalid ethereum private key")
//		return err
//	}
//
//	auth := bind.NewKeyedTransactor(key)
//
//	_, err = io.AddAddressToWhitelist(auth, common.HexToAddress(address), maxTokens)
//	if err != nil {
//		return err
//	}
//
//	return nil
//}
