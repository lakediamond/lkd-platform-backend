package pipeDrive

import (
	"fmt"
	"time"

	"github.com/Jeffail/gabs"
	"github.com/pkg/errors"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/pipeDrive"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//CreateNewPerson creating new person in CRM
func CreateNewPerson(app *application.Application, user *models.User, commonInfo *models.StepCommonInfoForm) error {

	verification, err := app.Repo.PhoneVerification.GetVerifiedUserPhone(user)
	if err != nil {
		return errors.New("cannot set phone")
	}

	_, err = app.Repo.KYC.GetTierFlag(user, 0)
	if err != nil {
		return err
	}

	eventCron := &models.Worker{
		UserID:    user.ID,
		Event:     models.EventCreateNewPerson,
		State:     models.StateNew,
		Phone:     verification.Phone,
		CreatedAt: time.Now(),
	}

	errDb := app.Repo.KYC.CreateWorker(eventCron)
	if errDb != nil {
		return errDb
	}

	return nil
}

func SendClientCreateNewPerson(app *application.Application, user *models.User, commonInfo *models.StepCommonInfoForm) error {

	CRMPersonID, err := duplicatePersonCheck(app, user)
	if err != nil {
		return err
	}

	kycTierFlags, err := app.Repo.KYC.GetTierFlag(user, 0)
	if err != nil {
		return err
	}

	jsonObj := gabs.New()
	jsonObj.Set(fmt.Sprintf("%s %s", user.LastName, user.FirstName), "name")
	jsonObj.Set(2, "org_id")
	jsonObj.Set(user.Email, "email")
	jsonObj.Set(commonInfo.Phone, "phone")
	jsonObj.Set(3, "visible_to")

	modelAddr := fmt.Sprintf(
		"%s , %s , %s, %s, %s",
		commonInfo.RegAddressCountry,
		commonInfo.RegAddressZIP,
		commonInfo.RegAddressCity,
		commonInfo.RegAddressDetails,
		commonInfo.RegAddressDetailsExt)

	personKeys := app.PipeDrive.GetPerson()

	jsonObj.Set(modelAddr, personKeys.ResidentialAddressValue)
	jsonObj.Set(user.FirstName, personKeys.FirstNameValue)
	jsonObj.Set(user.LastName, personKeys.LastNameValue)
	jsonObj.Set(user.EthAddress, personKeys.EthereumAddressValue)
	jsonObj.Set(user.BirthDate, personKeys.BirthDateValue)
	jsonObj.Set(commonInfo.BankName, personKeys.BankNameValue)
	jsonObj.Set(commonInfo.BankAddress, personKeys.BankAddressValue)
	jsonObj.Set(commonInfo.BankIBAN, personKeys.BankIbanValue)

	for _, kycTierFlag := range kycTierFlags {
		if pipeDrive.ContainsCRMKeyValue(personKeys, kycTierFlag.Code) {
			jsonObj.Set(kycTierFlag.Value, kycTierFlag.Code)
		}
	}

	if CRMPersonID != 0 {
		err = updatePerson(app, user, jsonObj)
		if err != nil {
			return err
		}
		return nil
	}

	err = createPerson(app, user, jsonObj)
	if err != nil {
		return err
	}

	return nil
}

func duplicatePersonCheck(app *application.Application, user *models.User) (uint, error) {
	res, err := app.PipeDrive.FindPersonsByEmail(user.Email)
	if res == nil || len(res) == 0 {
		return 0, nil
	}

	if err != nil {
		return 0, errors.Wrapf(err, "Search request not successful, user id %v. Reason", user.ID)
	}

	user.CRMPersonID = uint(res[0].ID)
	err = app.Repo.User.UpdateUser(user)
	if err != nil {
		return 0, err
	}

	return user.CRMPersonID, nil
}

func updatePerson(app *application.Application, user *models.User, jsonObj *gabs.Container) error {
	_, err := app.PipeDrive.UpdateRecord("persons", user.CRMPersonID, jsonObj.Bytes(), "application/json")
	if err != nil {
		return errors.Wrapf(err, "Cannot update person, userID: %v. Reason", user.ID)
	}

	return nil
}

func createPerson(app *application.Application, user *models.User, jsonObj *gabs.Container) error {
	id, err := app.PipeDrive.CreateRecord("persons", jsonObj.Bytes(), "application/json")
	if err != nil {
		return errors.Wrapf(err, "Cannot create new person, userID: %v. Reason", user.ID)
	}

	user.CRMPersonID = id
	err = app.Repo.User.UpdateUser(user)
	if err != nil {
		return err
	}

	return nil
}
