package pipeDrive

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"strconv"
	"strings"
	"time"

	"github.com/Jeffail/gabs"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/pipeDrive"
	"lkd-platform-backend/internal/pkg/repo/kyc"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//UpdateCustomerCRMDeal updates or creates if doesn't exist CRM deal for customer
func UpdateCustomerCRMDeal(app *application.Application, user *models.User) error {

	jsonObj, err := getDealJSONBody(app.Repo.KYC, app.PipeDrive.GetDeal(), user)
	if err != nil {
		return err
	}

	var id uint
	if user.CRMDealID == 0 {
		id, err = app.PipeDrive.CreateRecord("deals", jsonObj.Bytes(), "application/json")
	} else {
		id, err = app.PipeDrive.UpdateRecord("deals", uint(user.CRMDealID), jsonObj.Bytes(), "application/json")
	}
	if err != nil {
		return err
	}

	user.CRMDealID = uint64(id)
	err = app.Repo.User.UpdateUserCRMDeal(user)
	if err != nil {
		return err
	}

	err = uploadIDScanImages(app, user)
	if err != nil {
		return err
	}

	return nil
}

func getDealJSONBody(kycRepo kyc.Repository, dealSettings *pipeDrive.Deal, user *models.User) (*gabs.Container, error) {
	crmPipelineStage, err := getCRMPipelineStage(kycRepo, user, dealSettings)
	if err != nil {
		return nil, err
	}

	if user.KYCInvestmentPlan.Code == 0 && crmPipelineStage == dealSettings.KYCTier1StageID {
		err = kycRepo.UnlockTierByCode(user, models.Tier1)
		if err != nil {
			return nil, err
		}
	}

	jsonObj := gabs.New()

	title := fmt.Sprintf("%s %s - LD-KYC-%s", user.LastName, user.FirstName, time.Now().Format("20060102150405"))
	jsonObj.Set(title, "title")
	jsonObj.Set(user.CRMPersonID, "person_id")
	jsonObj.Set(2, "org_id")
	jsonObj.Set(crmPipelineStage, "stage_id")
	jsonObj.Set("open", "status")
	jsonObj.Set(3, "visible_to")

	kycInvestmentPlan, err := kycRepo.GetPlanByCode(user.InvestmentPlan)
	if err != nil {
		return nil, err
	}
	jsonObj.Set(kycInvestmentPlan.MaxLimit, "value")

	kycTierFlags, err := kycRepo.GetTierFlag(user, 0)
	if err != nil {
		return nil, err
	}

	for _, kycTierFlag := range kycTierFlags {
		if pipeDrive.ContainsCRMKeyValue(dealSettings, kycTierFlag.Code) {
			jsonObj.Set(kycTierFlag.Value, kycTierFlag.Code)
		}
	}

	return jsonObj, nil
}

func getCRMPipelineStage(kycRepo kyc.Repository, user *models.User, dealSettings *pipeDrive.Deal) (int, error) {
	rate, err := kycRepo.GetTierFailureRate(user, models.Tier0)
	if err != nil {
		return 0, err
	}

	var crmPipelineStage int
	switch rate {
	case 0:
		if user.KYCInvestmentPlan.Code == 0 {
			crmPipelineStage = dealSettings.KYCTier0StageID
		} else {
			crmPipelineStage = dealSettings.KYCTier1StageID
		}
	case 1:
		failureFlags, err := kycRepo.GetTierFailedFlags(user, models.Tier0)
		if err != nil {
			return 0, err
		}

		crmPipelineStage = failureFlags[0].CRMPipelineFailureStage
	default:
		crmPipelineStage = dealSettings.MultipleFailuresStage
	}

	return crmPipelineStage, nil
}

func uploadIDScanImages(app *application.Application, user *models.User) error {
	images, err := app.Storage.GetKYCImages(user)
	if err != nil {
		return err
	}

	for _, image := range images {
		err := addMinioPhoto(app, user, strconv.Itoa(int(user.CRMDealID)), image.Name)
		if err != nil {
			log.Error().Err(err).Msg("addMinioPhoto error")
		}
	}

	return nil
}

func addMinioPhoto(app *application.Application, user *models.User, dealID, objName string) error {

	//sending file to deal
	imageName := strings.Split(objName, "/")[2]
	imageNameParts := strings.Split(imageName, ".")

	//downloading file from minio client
	imageReader, err := app.Storage.GetImageReader(user, imageName)
	if err != nil {
		return err
	}

	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)

	fileName := fmt.Sprintf("%s-%s.%s", imageNameParts[0], time.Now().Format("2006-01-02 15:04:05"), imageNameParts[1])
	fileWriter, err := bodyWriter.CreateFormFile("file", fileName)
	if err != nil {
		return err
	}

	_, err = io.Copy(fileWriter, imageReader)
	if err != nil {
		return err
	}

	contentType := bodyWriter.FormDataContentType()

	bodyWriter.WriteField("deal_id", dealID)
	bodyWriter.Close()

	id, err := app.PipeDrive.CreateRecord("files", bodyBuf.Bytes(), contentType)
	if err != nil {
		return errors.WithMessage(err, "Cannot add new file to CRM")
	}

	err = app.Repo.KYC.UpdateMediaItemCRMFileID(user, strings.Split(imageName, ".")[0], uint64(id))
	if err != nil {
		return err
	}

	return nil
}
