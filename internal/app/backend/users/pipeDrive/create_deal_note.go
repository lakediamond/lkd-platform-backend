package pipeDrive

import (
	"encoding/json"

	"github.com/Jeffail/gabs"

	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"github.com/tidwall/pretty"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//CreateDealNote create new note for deal record in PipeDrive CRM
func CreateDealNote(app *application.Application, btcsResult *models.VerificationResult, userID *uuid.UUID) error {

	user, err := app.Repo.User.GetUserByID(userID)
	if err != nil {
		return err
	}

	screeningBytes, err := json.Marshal(btcsResult.VerifiedNameSanctionsScreen)
	if err != nil {
		return err
	}

	content := string(pretty.Pretty(screeningBytes))

	jsonObj := gabs.New()
	jsonObj.Set(user.CRMDealID, "deal_id")
	jsonObj.Set(content, "content")

	_, err = app.PipeDrive.CreateRecord("notes", jsonObj.Bytes(), "application/json")
	if err != nil {
		return errors.Wrapf(err, "Cannot create new deal note, userID: %v. Reason", user.ID)
	}
	return nil
}
