package users_test

import (
	"fmt"
	"net/http/httptest"
	"testing"

	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/app/backend/users"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
)

type PhoneVerificationSuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
	AuthToken  string
}

func (suite *PhoneVerificationSuite) SetupTestSuite() {

}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_PhoneVerificationSuite(t *testing.T) {
	suite.Run(t, new(PhoneVerificationSuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *PhoneVerificationSuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()

	//key, _ := crypto.HexToECDSA("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
	//app.EthClient = etherPkg.GetMockEthClient(key)

	priv.Migration(app.Repo.DB)
	suite.App = app
	suite.MockServer = mockServer

	log.Debug().Msg("DELETE FROM kyc_tier_step;")
	err := suite.App.Repo.DB.Exec("DELETE FROM kyc_tier_step;").Error
	if err != nil {
		panic(err)
	}

	log.Debug().Msg("DELETE FROM kyc_tier;")
	err = suite.App.Repo.DB.Exec("DELETE FROM kyc_tier;").Error
	if err != nil {
		panic(err)
	}

	log.Debug().Msg("DELETE FROM phone_verifications;")
	err = suite.App.Repo.DB.Exec("DELETE FROM phone_verifications;").Error
	if err != nil {
		panic(err)
	}

	log.Debug().Msg("DELETE FROM users;")
	err = suite.App.Repo.DB.Exec("DELETE FROM users;").Error

	if err != nil {
		panic(err)
	}
}

func (suite *PhoneVerificationSuite) Test_PhoneNumberValidation() {
	pvs := users.NewPhoneVerificationService(suite.App)

	sucess, err := pvs.IsValidFormat(suite.App, "123456")

	suite.Equal(true, sucess)
	suite.Equal(nil, err)
}

func (suite *PhoneVerificationSuite) Test_CreateVerification() {
	id := uuid.NewV4()
	_ = suite.App.Repo.User.CreateNewUser(&models.User{
		ID:         &id,
		FirstName:  "John",
		EthAddress: "0xaeB0920be125eB72e071B1357A5c95B52D8afc65",
		BirthDate:  "1990-09-04",
		Email:      "JohnSmithTest@gmail.com",
		LastName:   "Smith",
		Password:   "TestP@ssw0rd",
	})
	user, _ := suite.App.Repo.User.GetUserByID(&id)

	pvs := users.NewPhoneVerificationService(suite.App)
	createPVForm := &users.CreatePhoneVerificationForm{Phone: "+1111111111111", CountryCode: "1"}
	pvs.CreatePhoneVerificationService(&user, *createPVForm)

}

func (suite *PhoneVerificationSuite) Test_CreateVerificationLimit() {
	id := uuid.NewV4()
	_ = suite.App.Repo.User.CreateNewUser(&models.User{
		ID:         &id,
		FirstName:  "John",
		EthAddress: "0xaeB0920be125eB72e071B1357A5c95B52D8afc65",
		BirthDate:  "1990-09-04",
		Email:      "JohnSmithTest@gmail.com",
		LastName:   "Smith",
		Password:   "TestP@ssw0rd",
	})
	user, _ := suite.App.Repo.User.GetUserByID(&id)

	pvs := users.NewPhoneVerificationService(suite.App)
	for i := 0; i < 5; i++ {
		createPVForm := &users.CreatePhoneVerificationForm{Phone: fmt.Sprintf("+111111111111%d", i), CountryCode: "1"}
		pvs.CreatePhoneVerificationService(&user, *createPVForm)
	}
	createPVForm := &users.CreatePhoneVerificationForm{Phone: "+1111111111119", CountryCode: "1"}
	err := pvs.CreatePhoneVerificationService(&user, *createPVForm)

	suite.Equal("Requests limit reached for given phone number", err.Error())
}

func (suite *PhoneVerificationSuite) Test_VerifyPhoneVerification() {
	phone := "+1111111111111"
	id := uuid.NewV4()
	_ = suite.App.Repo.User.CreateNewUser(&models.User{
		ID:         &id,
		FirstName:  "John",
		EthAddress: "0xaeB0920be125eB72e071B1357A5c95B52D8afc65",
		BirthDate:  "1990-09-04",
		Email:      "JohnSmithTest@gmail.com",
		LastName:   "Smith",
		Password:   "TestP@ssw0rd",
	})
	user, _ := suite.App.Repo.User.GetUserByID(&id)

	suite.App.Repo.KYC.CreateTemplateRecords(&user)

	pvs := users.NewPhoneVerificationService(suite.App)
	createPVForm := &users.CreatePhoneVerificationForm{Phone: phone, CountryCode: "1"}
	pvs.CreatePhoneVerificationService(&user, *createPVForm)

	pv, _ := suite.App.Repo.PhoneVerification.GetVerificationByPhone(phone)

	err := pvs.VerifyPhoneService(suite.App, user, pv.Code)
	suite.Equal(nil, err)
}

func (suite *PhoneVerificationSuite) Test_VerifyPhoneVerificationLimit() {

	phone := "+1111111111111"
	id := uuid.NewV4()
	_ = suite.App.Repo.User.CreateNewUser(&models.User{
		ID:         &id,
		FirstName:  "John",
		EthAddress: "0xaeB0920be125eB72e071B1357A5c95B52D8afc65",
		BirthDate:  "1990-09-04",
		Email:      "JohnSmithTest@gmail.com",
		LastName:   "Smith",
		Password:   "TestP@ssw0rd",
	})
	user, _ := suite.App.Repo.User.GetUserByID(&id)

	pvs := users.NewPhoneVerificationService(suite.App)
	createPVForm := &users.CreatePhoneVerificationForm{Phone: phone, CountryCode: "1"}
	pvs.CreatePhoneVerificationService(&user, *createPVForm)

	var err error
	for i := 0; i <= 5; i++ {
		err = pvs.VerifyPhoneService(suite.App, user, 1111)
	}
	suite.Equal("Requests limit reached for given phone number", err.Error())
}

func (suite *PhoneVerificationSuite) Test_ResendPhoneVerification() {
	phone := "+1111111111111"
	id := uuid.NewV4()
	_ = suite.App.Repo.User.CreateNewUser(&models.User{
		ID:         &id,
		FirstName:  "John",
		EthAddress: "0xaeB0920be125eB72e071B1357A5c95B52D8afc65",
		BirthDate:  "1990-09-04",
		Email:      "JohnSmithTest@gmail.com",
		LastName:   "Smith",
		Password:   "TestP@ssw0rd",
	})
	user, _ := suite.App.Repo.User.GetUserByID(&id)

	pvs := users.NewPhoneVerificationService(suite.App)
	createPVForm := &users.CreatePhoneVerificationForm{Phone: phone, CountryCode: "1"}
	pvs.CreatePhoneVerificationService(&user, *createPVForm)

	err := pvs.ResendPhoneVerificationService(&user, phone)
	suite.Equal(nil, err)
}
