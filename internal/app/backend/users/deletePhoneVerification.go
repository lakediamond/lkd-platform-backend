package users

import (
	"net/http"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

var DeletePhoneVerificationHandler = handlers.ApplicationHandler(deletePhoneVerification)

func deletePhoneVerification(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)

	user := application.GetUser(r)

	err := deletePhoneVerificationService(app.Repo, &user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func deletePhoneVerificationService(repo *repo.Repo, user *models.User) error {

	ts, err := repo.KYC.GetTierStep(user, 0, "phone_verification")
	if err != nil {
		return err
	}

	return repo.KYC.DeletePhoneVerification(ts)
}
