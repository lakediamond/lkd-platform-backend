package users

import (
	"time"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

func limitAllowedAuth(app *application.Application, email, ipAddress string) error {

	lock, err := app.Repo.IPLock.GetLock(email, ipAddress)
	if err == nil {
		if lock.TimeTo.After(time.Now()) {
			return errors.New("account locked to " + lock.TimeTo.String())
		}
	}

	subTime := time.Now().Add(time.Duration(-app.Config.AuthRetries.TimeFrame) * time.Second)

	accountActivities, err := app.Repo.AccountActivity.LimitAllowedAuth(email, ipAddress, subTime)
	if err != nil {
		log.Error().Err(err).Msg("get account activities error")
		return err
	}

	if len(accountActivities) >= int(app.Config.AuthRetries.LimitRetries) {
		var lock models.IPLock
		lock.Email = email
		lock.IPAddress = ipAddress
		lock.TimeTo = time.Now().Add(time.Duration(app.Config.AuthRetries.LockTime) * time.Second)
		lock.Reason = app.Config.AuthRetries.LockReason

		app.Repo.IPLock.CreateNewLock(&lock)

		return errors.New(app.Config.AuthRetries.LockReason)
	}

	return nil
}
