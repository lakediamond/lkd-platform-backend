package users

import (
	"errors"
	"net/http"

	"github.com/ory/hydra/sdk/go/hydra/swagger"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

//AuthenticateData JSON request body to authentication endpoint
type AuthenticateData struct {
	Email         string `json:"email"`
	Password      string `json:"password"`
	ChallengeCode string `json:"login_challenge"`
	Google2FAPass string `json:"google_2_fa_pass"`
}

//AuthenticateAccountHandler handling user authentication endpoint
var AuthenticateAccountHandler = handlers.ApplicationHandler(authenticateAccount)

func authenticateAccount(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	app := application.GetApplicationContext(r)

	ipConfig := httputil.GetIPDetails(r)

	var authenticateData AuthenticateData
	err := httputil.ParseBody(w, r.Body, &authenticateData)
	if err != nil {
		return
	}

	defer r.Body.Close()

	err = limitAllowedAuth(app, authenticateData.Email, ipConfig.Address)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		httputil.WriteErrorMsg(w, err)
		return
	}

	if err := authenticate(app, authenticateData); err != nil {
		log.Debug().Err(err).Msg("can't authorize user")
		if err.Error() == "email not verified" {
			err = errors.New("login failed; Email needs to be verified first")
		} else {
			err = errors.New("login failed; Invalid userID or password")
		}

		accountActivity.UserAuthorizationFailure(app.Repo, authenticateData.Email, ipConfig.Address, ipConfig.Country, err)
		w.WriteHeader(http.StatusForbidden)
		httputil.WriteErrorMsg(w, err)
		return
	}

	ok, err := with2FA(app, &authenticateData)
	if err != nil {
		accountActivity.UserAuthorizationFailure(app.Repo, authenticateData.Email, ipConfig.Address, ipConfig.Country, errors.New("2fa is required"))
		httputil.RemoveCookies(w)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	if !ok {
		err := errors.New("invalid 2fa code")
		accountActivity.UserAuthorizationFailure(app.Repo, authenticateData.Email, ipConfig.Address, ipConfig.Country, err)
		httputil.RemoveCookies(w)
		w.WriteHeader(http.StatusUnauthorized)
		httputil.WriteErrorMsg(w, err)
		return
	}

	accountActivity.UserAuthorization(app.Repo, authenticateData.Email, ipConfig.Address, ipConfig.Country)

	loginRequest, _, err := app.IAMClient.GetOAuthClient().AdminApi.GetLoginRequest(authenticateData.ChallengeCode)
	if err != nil {
		log.Debug().Err(err).Msg("app.IAMClient GetLoginRequest error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	swaggerAcceptLoginRequest := swagger.AcceptLoginRequest{
		Subject:     authenticateData.Email,
		Remember:    !loginRequest.Skip,
		RememberFor: app.IAMClient.GetLoginRememberFor(),
	}

	log.Info().Msgf("app.IAMClient AcceptLoginRequest params: %s, %+v", authenticateData.ChallengeCode, swaggerAcceptLoginRequest)

	completedRequest, _, err := app.IAMClient.GetOAuthClient().AdminApi.AcceptLoginRequest(authenticateData.ChallengeCode, swaggerAcceptLoginRequest)
	if err != nil {
		log.Debug().Err(err).Msg("app.IAMClient AcceptLoginRequest error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Location", completedRequest.RedirectTo)
	w.WriteHeader(http.StatusOK)
}
