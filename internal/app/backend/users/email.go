package users

import (
	"errors"
	"fmt"
	"net/url"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/mailjet/mailjet-apiv3-go"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	mailjetClient "lkd-platform-backend/internal/pkg/mailjet"
	"lkd-platform-backend/internal/pkg/oauth"
	"lkd-platform-backend/internal/pkg/repo/models"
)

type (
	ErrEmailAlreadyVerified struct {
		ErrBase
	}
)

var (
	errEmailAlreadyVerified = ErrEmailAlreadyVerified{ErrBaseNew("user email has been already verified")}
)

func createEventCronEmailVerification(app *application.Application, user *models.User, loginChallenge string, spanID uint64) error {

	eventCron := &models.Worker{
		UserID:         user.ID,
		Event:          models.EventCreateEmailVerification,
		State:          models.StateNew,
		LoginChallenge: loginChallenge,
		CreatedAt:      time.Now(),
		SpanID:         spanID,
	}

	errDb := app.Repo.KYC.CreateWorker(eventCron)
	if errDb != nil {
		return errDb
	}

	return nil
}

func SendVerificationEmail(app *application.Application, user *models.User, loginChallenge string, spanID uint64) error {
	redirectURL, err := getRedirectURL(app.IAMClient, loginChallenge)
	if err != nil {
		return err
	}

	token := createEmailToken(app.Config.JwtSalt, user.Email, redirectURL, spanID)

	cfgTemplate := app.Mailjet.GetConfigTemplate("verification")

	callbackUrl, err := GenerateCallbackUrl(cfgTemplate.Variables["url"].(string),
		map[string]string{"token": token})

	if err != nil {
		return err
	}

	cfgTemplate.Variables["url"] = callbackUrl.String()
	cfgTemplate.Variables["firstname"] = user.FirstName
	cfgTemplate.Variables["lastname"] = user.LastName

	cfgTemplate.ToName = fmt.Sprintf("%s %s", user.FirstName, user.LastName)
	cfgTemplate.ToEmail = user.Email

	message, err := mailjetClient.BuildMessageByConfig(cfgTemplate)
	if err != nil {
		return err
	}

	messagesInfo := []mailjet.InfoMessagesV31{*message}

	err = app.Mailjet.MailjetSendEmail(messagesInfo)
	if err != nil {
		return err
	}

	return nil
}

func GenerateCallbackUrl(templateUrl string, paramValues map[string]string) (*url.URL, error) {
	urlParsed, err := url.Parse(templateUrl)

	if err != nil {
		return nil, err
	}

	urlQueryParams := urlParsed.Query()
	for key, value := range paramValues {
		urlQueryParams.Set(key, value)
	}
	urlParsed.RawQuery = urlQueryParams.Encode()

	log.Debug().Msg("email verification url " + urlParsed.String())

	return urlParsed, nil
}

func getRedirectURL(iamClient oauth.IdentityManagementClient, challenge string) (string, error) {
	loginRequest, _, err := iamClient.GetOAuthClient().AdminApi.GetLoginRequest(challenge)
	if err != nil {
		log.Debug().Err(err).Msgf("cant get hydra client config by challenge: %s", challenge)
		return "", err
	}
	if len(loginRequest.Client.ClientUri) == 0 {
		return "", errors.New("hydra client redirect URI not found")
	}

	return loginRequest.Client.ClientUri, nil
}

func createEmailToken(jwtSalt, email string, redirectURL string, spanID uint64) string {

	expire := time.Now().UTC().Add(24 * time.Hour).Unix()

	claims := &jwt.MapClaims{
		"exp":         expire,
		"email":       email,
		"redirectURL": redirectURL,
		"spanID":      strconv.FormatUint(spanID, 10),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	ss, _ := token.SignedString([]byte(jwtSalt))

	log.Debug().Msgf("Created new email token for: %s, token: %s", email, ss)
	return ss
}

func getJwtTokenValidClaims(salt string, tokenValue string) (jwt.MapClaims, error) {
	claims := jwt.MapClaims{}

	token, err := jwt.ParseWithClaims(tokenValue, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(salt), nil
	})
	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, errors.New("invalid token")
	}

	if !claims.VerifyExpiresAt(time.Now().UTC().Unix(), true) {
		return nil, errors.New("expired token")
	}
	return claims, nil
}
