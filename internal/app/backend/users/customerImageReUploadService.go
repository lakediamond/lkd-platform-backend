package users

import (
	"fmt"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/app/backend/users/kyc"
	"lkd-platform-backend/internal/app/backend/users/pipeDrive"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func customerImageReUploadService(app *application.Application, customerImageReUploadRequestInput *models.CustomerImageReUploadRequestInput) error {
	if customerImageReUploadRequestInput.Meta.Action == "added" &&
		customerImageReUploadRequestInput.Meta.Object == "activity" &&
		customerImageReUploadRequestInput.Current.Type == "task" {

		user, err := app.Repo.User.GetUserByDealID(customerImageReUploadRequestInput.Current.DealID)
		if err != nil {
			return err
		}

		processedImages, err := pipeDrive.CustomerImageReUploadService(app, customerImageReUploadRequestInput)
		if err != nil {
			log.Error().Err(err).Msg("customerImageReUploadService error")
			return err
		}

		for _, image := range processedImages {
			if err = kyc.ResetMediaUploadStatus(app, &user, image); err != nil {
				log.Error().Err(err).Msg(fmt.Sprintf("kyc.ResetMediaUploadStatus error for %s", image))
				// return err
			}

			if err = app.Repo.KYC.UpdateMediaItemStatus(&user, image, models.MediaItemInvalid); err != nil {
				log.Error().Err(err).Msg(fmt.Sprintf("UpdateMediaItemStatus error for %s", processedImages[0]))
				// return err
			}
		}

		if err = pipeDrive.SetActivityDone(app.PipeDrive, customerImageReUploadRequestInput, processedImages); err != nil {
			log.Error().Err(err).Msg(fmt.Sprintf("setActivityDone error for %v", customerImageReUploadRequestInput))
			return err
		}
	}

	return nil
}
