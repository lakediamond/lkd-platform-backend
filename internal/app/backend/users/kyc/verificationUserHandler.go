package kyc

import (
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

//VerificationUserHandler handling user verification endpoint
var VerificationUserHandler = handlers.ApplicationHandler(verificationUser)

func verificationUser(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)

	routeParams := httprouter.ParamsFromContext(r.Context())
	id := routeParams.ByName("user_id")

	userID, err := uuid.FromString(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	user, err := app.Repo.User.GetUserByID(&userID)
	if err != nil {
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	var verificationResult models.VerificationResult
	err = httputil.ParseBody(w, r.Body, &verificationResult)
	if err != nil {
		return
	}

	defer r.Body.Close()

	if verificationResult.VerificationStatus == "PROCESSING_ERROR" {
		err := errors.New("invalid request verification status for this route")
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, err)
		return
	}

	verificationResult.UserID = &userID

	verificationResultID, err := app.Repo.VerificationResult.CreateByID(&verificationResult)
	if err != nil {
		log.Error().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = CreateKYCPostIdentityVerificationFlags(app, &verificationResult)
	if err != nil {
		log.Error().Err(err).Msg("createKYCPostIdentityVerificationFlags error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	eventCron := &models.Worker{
		UserID:    user.ID,
		Event:     models.EventCreateUpdateCRMDeal,
		State:     models.StateNew,
		CreatedAt: time.Now(),
	}

	errDb := app.Repo.KYC.CreateWorker(eventCron)
	if errDb != nil {
		log.Error().Err(err).Msg("createWorker::[EventCreateUpdateCRMDeal] error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(verificationResult.VerifiedNameSanctionsScreen.Matches) > 0 {
		eventCron := &models.Worker{
			UserID:                  user.ID,
			Event:                   models.EventCreateCRMDealNote,
			State:                   models.StateNew,
			CrmVerificationResultID: verificationResultID,
			CreatedAt:               time.Now(),
		}

		errDb := app.Repo.KYC.CreateWorker(eventCron)
		if errDb != nil {
			log.Error().Err(err).Msg("createWorker::[EventCreateCRMDealNote] error")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	w.WriteHeader(http.StatusCreated)
}
