package kyc

import (
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func submitSuccessImageValidationService(repo *repo.Repo, user *models.User, imageType string) error {
	return repo.KYC.UpdateMediaItemStatus(user, imageType, models.MediaItemValid)
}

func submitFailedImageValidationService(repo *repo.Repo, user *models.User, imageType string) error {

	step, err := repo.KYC.GetTierStep(user, 0, "identity_scans")
	if err != nil {
		return err
	}

	stepItem, err := repo.KYC.GetTierStepContentItemByCode(step, imageType)
	if err != nil {
		return err
	}

	stepItem.Status = "INVALID"

	err = repo.KYC.UpdateTierStepContentItem(&stepItem)
	if err != nil {
		return err
	}

	return nil
}
