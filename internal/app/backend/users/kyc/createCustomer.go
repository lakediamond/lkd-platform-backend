package kyc

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
)

type CreateKYCCustomerRequestData struct {
	ID          *uuid.UUID `json:"id"`
	FirstName   string     `json:"first_name"`
	LastName    string     `json:"last_name"`
	Email       string     `json:"email"`
	Phone       []string   `json:"phone"`
	EthAddress  string     `json:"eth_address"`
	DoB         string     `json:"dob"`
	CRMPersonID uint       `json:"crm_person_id"`
}

func CreateKYCCustomer(app *application.Application, user *models.User) error {
	url := fmt.Sprintf("%s/customer", app.Config.KYCRootURL)

	createKYCCustomerRequestData := CreateKYCCustomerRequestData{user.ID,
		user.FirstName,
		user.LastName,
		user.Email,
		nil,
		user.EthAddress,
		user.BirthDate,
		user.CRMPersonID,
	}

	phoneVerification, err := app.Repo.PhoneVerification.GetVerifiedUserPhone(user)
	if err != nil {
		return err
	}

	createKYCCustomerRequestData.Phone = []string{phoneVerification.Phone}

	data, _ := json.Marshal(createKYCCustomerRequestData)

	r := bytes.NewReader(data)
	resp, err := http.Post(url, "application/json", r)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		return errors.New(fmt.Sprintf("KYC returns %d code", resp.StatusCode))
	}

	return nil
}
