package kyc

import (
	"strings"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func handleProcessingError(app *application.Application, userID *uuid.UUID, verificationResult *models.VerificationResult) error {
	splitter := strings.Split(verificationResult.IdentityVerification, "is invalid, ")
	if len(splitter) != 2 {
		return errors.New("invalid verificationResult")
	}

	log.Debug().Msgf("original data from kyc ", verificationResult.IdentityVerification)
	imageName := splitter[1]
	user, err := app.Repo.User.GetUserByID(userID)

	log.Debug().Msgf("image name ", imageName)
	imageMetaInfo, err := app.Storage.GetImageMetaInfo(&user, imageName)
	if err != nil {
		return err
	}

	log.Debug().Msgf("image metaInfo ", imageMetaInfo)

	if err = app.Storage.InvalidateImage(&user, imageMetaInfo); err != nil {
		return err
	}

	return nil
}
