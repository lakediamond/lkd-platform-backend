package kyc

import (
	"fmt"
	"net/http"

	"github.com/pkg/errors"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func ResetMediaUploadStatus(app *application.Application, user *models.User, mediaType models.KYCMediaItemType) error {
	url := fmt.Sprintf("%s/customer/%s/media/%s", app.Config.KYCRootURL, user.ID.String(), mediaType)

	client := &http.Client{}
	req, err := http.NewRequest("PATCH", url, nil)
	if err != nil {
		return err
	}

	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusAccepted {
		return errors.New(fmt.Sprintf("ResetMediaUploadStatus returns %d response", resp.StatusCode))
	}

	return nil
}
