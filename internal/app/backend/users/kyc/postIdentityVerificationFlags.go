package kyc

import (
	"encoding/json"
	"fmt"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//CreateKYCPostIdentityVerificationFlags creating KYC post identity verification flags
func CreateKYCPostIdentityVerificationFlags(app *application.Application, verificationResult *models.VerificationResult) error {
	user, err := app.Repo.User.GetUserByID(verificationResult.UserID)
	if err != nil {
		return err
	}

	kycTier, err := app.Repo.KYC.GetTierByCode(&user, models.Tier0)
	if err != nil {
		return err
	}

	err = app.Repo.KYC.DeleteTierFlags(&user, models.Tier0, app.PipeDrive.GetRelatedKYCFlags())
	if err != nil {
		return err
	}

	var kycTierFlag models.KYCTierFlag
	kycTierFlag.KYCTierID = kycTier.ID
	kycTierFlag.Code = app.PipeDrive.GetDeal().ResidentialAddressFromAllowedCountryValue

	var kycTierStep models.KYCTierStep
	for _, kTS := range kycTier.Steps {
		if kTS.Code == models.CommonInfo {
			kycTierStep = kTS
			break
		}
	}

	kycTierStepContentItem, err := app.Repo.KYC.GetTierStepContentItemByCode(&kycTierStep, "reg_address_country")
	if err != nil {
		log.Error().Err(err).Msg("GetTierStepContentItemByCode error")
	}

	countryBlacklist, err := app.Repo.KYC.GetCountryBlacklistByCode(kycTierStepContentItem.Value)
	if err != nil {
		kycTierFlag.Value = "Passed"
	} else {
		kycTierFlag.Value = fmt.Sprintf("Failed ; %s", countryBlacklist.Name)
	}

	err = app.Repo.KYC.CreateTierFlag(&kycTierFlag)
	if err != nil {
		return err
	}

	var kycTierFlag2 models.KYCTierFlag
	kycTierFlag2.KYCTierID = kycTier.ID
	kycTierFlag2.Code = app.PipeDrive.GetDeal().IDDocumentFromAllowedCountryValue
	kycTierFlag2.CRMPipelineFailureStage = app.PipeDrive.GetDeal().IDDocumentFromAllowedCountryFailureStage

	countryBlacklist, err = app.Repo.KYC.GetCountryBlacklistByCode(verificationResult.IDCountry)
	if err != nil {
		kycTierFlag2.Value = "Passed"
	} else {
		kycTierFlag2.Value = fmt.Sprintf("Failed ; %s", countryBlacklist.Name)
	}

	err = app.Repo.KYC.CreateTierFlag(&kycTierFlag2)
	if err != nil {
		return err
	}

	var kycTierFlag3 models.KYCTierFlag
	kycTierFlag3.KYCTierID = kycTier.ID
	kycTierFlag3.Code = app.PipeDrive.GetDeal().IDDocumentBitaccessVerificationValue
	kycTierFlag3.CRMPipelineFailureStage = app.PipeDrive.GetDeal().IDDocumentBitaccessVerificationFailureStage

	if verificationResult.IDScanStatus == "SUCCESS" &&
		verificationResult.IDCheckSignature == "OK" &&
		verificationResult.IDCheckSecurityFeatures == "OK" &&
		verificationResult.IDCheckMicroprint == "OK" &&
		verificationResult.IDCheckMRZCode == "OK" &&
		verificationResult.IDCheckHologram == "OK" &&
		verificationResult.IDCheckDocumentValidation == "OK" &&
		verificationResult.IDCheckDataPositions == "OK" {
		kycTierFlag3.Value = "Passed"
	} else {
		kycTierFlag3.Value = fmt.Sprintf("Failed ; %s %v", verificationResult.VerificationStatus,
			map[string]string{
				"IDScanStatus":              verificationResult.IDScanStatus,
				"IDCheckSignature":          verificationResult.IDCheckSignature,
				"IDCheckSecurityFeatures":   verificationResult.IDCheckSecurityFeatures,
				"IDCheckMicroprint":         verificationResult.IDCheckMicroprint,
				"IDCheckMRZCode":            verificationResult.IDCheckMRZCode,
				"IDCheckHologram":           verificationResult.IDCheckHologram,
				"IDCheckDocumentValidation": verificationResult.IDCheckDocumentValidation,
				"IDCheckDataPositions":      verificationResult.IDCheckDataPositions})

		log.Debug().Msgf("verif status: %+s", kycTierFlag3.Value)
	}

	err = app.Repo.KYC.CreateTierFlag(&kycTierFlag3)
	if err != nil {
		return err
	}

	var kycTierFlag4 models.KYCTierFlag
	kycTierFlag4.KYCTierID = kycTier.ID
	kycTierFlag4.Code = app.PipeDrive.GetDeal().IPConnectionFromAllowedCountryValue

	data, err := app.Repo.KYC.GetAllBlacklistedUserCountries(user.Email)
	if err != nil {
		return err
	}
	if len(data) == 0 {
		kycTierFlag4.Value = "Passed"
	} else {
		kycTierFlag4.Value = fmt.Sprintf("Failed ; %s", data[0].Name)
	}

	err = app.Repo.KYC.CreateTierFlag(&kycTierFlag4)
	if err != nil {
		return err
	}

	var kycTierFlag5 models.KYCTierFlag
	kycTierFlag5.KYCTierID = kycTier.ID
	kycTierFlag5.Code = app.PipeDrive.GetDeal().SelfieBitaccessVerificationValue
	kycTierFlag5.CRMPipelineFailureStage = app.PipeDrive.GetDeal().SelfieBitaccessVerificationFailureStage

	var selfieBtcsResult models.BtcsSelfieValidationResult
	if err = json.Unmarshal([]byte(verificationResult.IdentityVerification), &selfieBtcsResult); err != nil {
		return err
	}
	if selfieBtcsResult.Validity && selfieBtcsResult.Similarity == "MATCH" {
		kycTierFlag5.Value = "Passed"
	} else {
		kycTierFlag5.Value = fmt.Sprintf("Failed ; %s", selfieBtcsResult.Reason)
	}

	err = app.Repo.KYC.CreateTierFlag(&kycTierFlag5)
	if err != nil {
		return err
	}

	var kycTierFlag6 models.KYCTierFlag
	kycTierFlag6.KYCTierID = kycTier.ID
	kycTierFlag6.Code = app.PipeDrive.GetDeal().NameSanctionScreeningValue

	nameScreeningMatches := len(verificationResult.VerifiedNameSanctionsScreen.Matches)
	if nameScreeningMatches == 0 {
		kycTierFlag6.Value = "Passed"
	} else {
		kycTierFlag6.Value = fmt.Sprintf("Failed ; %d matches found, see deal notes", nameScreeningMatches)
	}

	err = app.Repo.KYC.CreateTierFlag(&kycTierFlag6)
	if err != nil {
		return err
	}

	return nil
}
