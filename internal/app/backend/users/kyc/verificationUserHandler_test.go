package kyc_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
)

type VerificationUserHandlerSuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
}

func (suite *VerificationUserHandlerSuite) SetupTestSuite() {

}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_VerificationUserHandlerSuite(t *testing.T) {
	suite.Run(t, new(VerificationUserHandlerSuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *VerificationUserHandlerSuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()

	priv.Migration(app.Repo.DB)
	suite.App = app
	suite.MockServer = mockServer

	suite.App.Repo.DB.Exec("DELETE FROM users;")
	suite.App.Repo.DB.Exec("DELETE FROM verification_results;")

	app.S3 = application.InitS3()

}

func (suite *VerificationUserHandlerSuite) AfterTest() {
	suite.App.Repo.DB.Exec("DELETE FROM users;")
	suite.App.Repo.DB.Exec("DELETE FROM verification_results;")
}

func (suite *VerificationUserHandlerSuite) TestInvalidUserID() {
	client := &http.Client{}
	req, _ := http.NewRequest("POST", suite.MockServer.URL+"/user/kyc/verification/user_id/id", nil)
	req.Header.Add("Content-Type", "application/json")

	resp, _ := client.Do(req)
	suite.Equal(http.StatusBadRequest, resp.StatusCode)
}

func (suite *VerificationUserHandlerSuite) TestUserIDNotExists() {
	client := &http.Client{}
	req, _ := http.NewRequest("POST", suite.MockServer.URL+"/user/kyc/verification/550e8400-e29b-41d4-a716-446655440000/id", nil)

	req.Header.Add("Content-Type", "application/json")

	resp, _ := client.Do(req)
	suite.Equal(http.StatusUnprocessableEntity, resp.StatusCode)
}

func (suite *VerificationUserHandlerSuite) TestEmptyData() {
	//Create new user
	var user models.User
	user.Email = "1@2.com"
	suite.App.Repo.User.CreateNewUser(&user)

	client := &http.Client{}
	req, _ := http.NewRequest("POST", suite.MockServer.URL+"/user/kyc/verification/"+user.ID.String()+"/id", nil)

	req.Header.Add("Content-Type", "application/json")

	resp, _ := client.Do(req)
	suite.Equal(http.StatusUnprocessableEntity, resp.StatusCode)
}

func (suite *VerificationUserHandlerSuite) TestCorrectData() {
	//Create new user
	var user models.User
	user.Email = "1@2.com"
	suite.App.Repo.User.CreateNewUser(&user)

	var jsonStr = []byte(`{
  "callBackType": "NETVERIFYID",
  "callbackDate": "2018-09-10T17:06:26.195Z",
  "callback_url": "https://url-provided-earlier.com/uuid",
  "firstAttemptDate": "2018-09-10T17:04:06.209Z",
  "idCheckDataPositions": "OK",
  "idCheckDocumentValidation": "OK",
  "idCheckHologram": "OK",
  "idCheckMRZcode": "OK",
  "idCheckMicroprint": "OK",
  "idCheckSecurityFeatures": "OK",
  "idCheckSignature": "OK",
  "idCountry": "NZL",
  "idDob": "1978-05-31",
  "idExpiry": "2028-08-31",
  "idFirstName": "Hank",
  "idLastName": "Bitaccess",
  "idNumber": "LM123456",
  "idScanImage": "https://url.com/front",
  "idScanImageFace": "https://url.com/face",
  "idScanSource": "API",
  "idScanStatus": "SUCCESS",
  "idType": "PASSPORT",
  "identityVerification": "{\"similarity\":\"MATCH\",\"validity\":true}",
  "jumioIdScanReference": "xxxx-1c48743d12aa",
  "merchantIdScanReference": "5689962313809920",
  "personalNumber": "N/A",
  "transactionDate": "2018-09-10T17:04:06.210Z",
  "verificationStatus": "APPROVED_VERIFIED"
}`)

	var data map[string]json.RawMessage
	err := json.Unmarshal(jsonStr, &data)
	if err != nil {
		fmt.Println(err)
	}

	body, _ := json.Marshal(data)

	client := &http.Client{}
	req, _ := http.NewRequest("POST", suite.MockServer.URL+"/user/kyc/verification/"+user.ID.String()+"/id", bytes.NewReader(body))

	req.Header.Add("Content-Type", "application/json")

	resp, _ := client.Do(req)
	suite.Equal(http.StatusCreated, resp.StatusCode)
}

func (suite *VerificationUserHandlerSuite) TestProcessingErrorInvalidVerificationResult() {
	//Create new user
	var user models.User
	user.Email = "1@2.com"
	suite.App.Repo.User.CreateNewUser(&user)

	var jsonStr = []byte(`{
  "callBackType": "NETVERIFYID",
  "callbackDate": "2018-09-10T17:06:26.195Z",
  "callback_url": "https://url-provided-earlier.com/uuid",
  "firstAttemptDate": "2018-09-10T17:04:06.209Z",
  "idCheckDataPositions": "OK",
  "idCheckDocumentValidation": "OK",
  "idCheckHologram": "OK",
  "idCheckMRZcode": "OK",
  "idCheckMicroprint": "OK",
  "idCheckSecurityFeatures": "OK",
  "idCheckSignature": "OK",
  "idCountry": "NZL",
  "idDob": "1978-05-31",
  "idExpiry": "2028-08-31",
  "idFirstName": "Hank",
  "idLastName": "Bitaccess",
  "idNumber": "LM123456",
  "idScanImage": "https://url.com/front",
  "idScanImageFace": "https://url.com/face",
  "idScanSource": "API",
  "idScanStatus": "SUCCESS",
  "idType": "PASSPORT",
  "identityVerification": "{\"similarity\":\"MATCH\",\"validity\":true}",
  "jumioIdScanReference": "xxxx-1c48743d12aa",
  "merchantIdScanReference": "5689962313809920",
  "personalNumber": "N/A",
  "transactionDate": "2018-09-10T17:04:06.210Z",
  "verificationStatus": "PROCESSING_ERROR"
}`)

	var data map[string]json.RawMessage
	err := json.Unmarshal(jsonStr, &data)
	if err != nil {
		fmt.Println(err)
	}

	body, _ := json.Marshal(data)

	client := &http.Client{}
	req, _ := http.NewRequest("POST", suite.MockServer.URL+"/user/kyc/verification/"+user.ID.String()+"/id", bytes.NewReader(body))

	req.Header.Add("Content-Type", "application/json")

	resp, _ := client.Do(req)
	suite.Equal(http.StatusInternalServerError, resp.StatusCode)
}

func (suite *VerificationUserHandlerSuite) TestProcessingErrorS3Error() {
	//Create new user
	var user models.User
	user.Email = "1@2.com"
	suite.App.Repo.User.CreateNewUser(&user)

	var jsonStr = []byte(`{
  "callBackType": "NETVERIFYID",
  "callbackDate": "2018-09-10T17:06:26.195Z",
  "callback_url": "https://url-provided-earlier.com/uuid",
  "firstAttemptDate": "2018-09-10T17:04:06.209Z",
  "idCheckDataPositions": "OK",
  "idCheckDocumentValidation": "OK",
  "idCheckHologram": "OK",
  "idCheckMRZcode": "OK",
  "idCheckMicroprint": "OK",
  "idCheckSecurityFeatures": "OK",
  "idCheckSignature": "OK",
  "idCountry": "NZL",
  "idDob": "1978-05-31",
  "idExpiry": "2028-08-31",
  "idFirstName": "Hank",
  "idLastName": "Bitaccess",
  "idNumber": "LM123456",
  "idScanImage": "https://url.com/front",
  "idScanImageFace": "https://url.com/face",
  "idScanSource": "API",
  "idScanStatus": "SUCCESS",
  "idType": "PASSPORT",
  "identityVerification": "{face_image is invalid, file_name}",
  "jumioIdScanReference": "xxxx-1c48743d12aa",
  "merchantIdScanReference": "5689962313809920",
  "personalNumber": "N/A",
  "transactionDate": "2018-09-10T17:04:06.210Z",
  "verificationStatus": "PROCESSING_ERROR"
}`)

	var data map[string]json.RawMessage
	err := json.Unmarshal(jsonStr, &data)
	if err != nil {
		fmt.Println(err)
	}

	body, _ := json.Marshal(data)

	client := &http.Client{}
	req, _ := http.NewRequest("POST", suite.MockServer.URL+"/user/kyc/verification/"+user.ID.String()+"/id", bytes.NewReader(body))

	req.Header.Add("Content-Type", "application/json")

	resp, _ := client.Do(req)
	suite.Equal(http.StatusInternalServerError, resp.StatusCode)
}
