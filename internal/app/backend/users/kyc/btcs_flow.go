package kyc

import (
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func updateKYCProgress(app *application.Application, userID *uuid.UUID) error {

	user, err := app.Repo.User.GetUserByID(userID)
	if err != nil {
		return err
	}

	err = app.Repo.KYC.CompleteTierStepIdentityScans(&user)
	if err != nil {
		return err
	}

	err = app.Repo.KYC.UpdateTierStatus(&user, models.Tier0, models.TierSubmitted)
	if err != nil {
		return err
	}

	if user.KYCInvestmentPlan.Code == models.Tier1 {
		err = app.Repo.KYC.UnlockTierByCode(&user, models.Tier1)
		if err != nil {
			return err
		}
	}

	return nil
}
