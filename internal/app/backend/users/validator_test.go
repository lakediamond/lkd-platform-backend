package users_test

import (
	"testing"

	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/app/backend/users"
)

type ValidateUserSuite struct {
	suite.Suite
}

func Test_ValidateUserSuite(t *testing.T) {
	suite.Run(t, new(ValidateUserSuite))
}

//All methods that begin with "Test" are run as tests within a
//suite.

func (suite *ValidateUserSuite) TestValidationCorrectData() {
	userData := getValidRegisterData()
	errors := users.ValidateData(userData)
	suite.Equal(0, len(errors))
}

func (suite *ValidateUserSuite) TestValidationInvalidFirstName() {
	userData := getValidRegisterData()
	userData.FirstName = "Test1"
	errors := users.ValidateData(userData)
	suite.Equal(1, len(errors))
	suite.Equal("firstname: invalid", errors[0].Error())
}

func (suite *ValidateUserSuite) TestValidationInvalidLastName() {
	userData := getValidRegisterData()
	userData.LastName = "Test1"
	errors := users.ValidateData(userData)
	suite.Equal(1, len(errors))
	suite.Equal("lastname: invalid", errors[0].Error())
}

func (suite *ValidateUserSuite) TestValidationInvalidBirthDate() {
	userData := getValidRegisterData()
	userData.BirthDate = "7782-81-08"
	errors := users.ValidateData(userData)
	suite.Equal(1, len(errors))
	suite.Equal("birth_date: invalid", errors[0].Error())
}

func (suite *ValidateUserSuite) TestValidationInvalidEthAddress() {
	userData := getValidRegisterData()
	userData.EthAddress = "0x8498"
	errors := users.ValidateData(userData)
	suite.Equal(1, len(errors))
	suite.Equal("invalid ethereum address", errors[0].Error())
}

func (suite *ValidateUserSuite) TestValidationInvalidEmail() {
	userData := getValidRegisterData()
	userData.Email = "JohnSmithTestgmail.com"
	errors := users.ValidateData(userData)
	suite.Equal(1, len(errors))
	suite.Equal("email: wrong format", errors[0].Error())
}

func (suite *ValidateUserSuite) TestValidationInvalidPasswordVariationAndLength() {
	userData := getValidRegisterData()
	userData.Password = "test"
	errors := users.ValidateData(userData)
	suite.Equal(2, len(errors))
	suite.Equal("password: too short", errors[0].Error())
	suite.Equal("password: too short password or too few characters variation", errors[1].Error())
}

func (suite *ValidateUserSuite) TestValidationInvalidPasswordVariationOrLength() {
	userData := getValidRegisterData()
	userData.Password = "testLowVariation"
	errors := users.ValidateData(userData)
	suite.Equal(1, len(errors))
	suite.Equal("password: too short password or too few characters variation", errors[0].Error())
}

func (suite *ValidateUserSuite) TestValidationInvalidPasswordVariation() {
	userData := getValidRegisterData()
	userData.Password = "testlowvariationinpassword"
	errors := users.ValidateData(userData)
	suite.Equal(1, len(errors))
	suite.Equal("password: too few characters variation", errors[0].Error())
}

func getValidRegisterData() *users.RegisterData {
	return &users.RegisterData{
		FirstName:      "John",
		EthAddress:     "0xaeB0920be125eB72e071B1357A5c95B52D8afc65",
		BirthDate:      "1990-09-04",
		Email:          "JohnSmithTest@gmail.com",
		LastName:       "Smith",
		LoginChallenge: "1",
		Password:       "TestP@ssw0rd",
	}
}
