package users

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
)

var GetGenerateUrlHandler = handlers.ApplicationHandler(gcGetGenerateUrl)

func gcGetGenerateUrl(w http.ResponseWriter, r *http.Request) {

	routeParams := httprouter.ParamsFromContext(r.Context())
	imageType := routeParams.ByName("image_type")

	app := application.GetApplicationContext(r)
	user := application.GetUser(r)

	exists, err := app.GC.ExistsImage(user.ID.String(), imageType)
	if err != nil {
		log.Error().Err(err).Msg("ExistsImage error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if exists {
		w.WriteHeader(http.StatusNotModified)
		return
	}

	content := r.Header.Get("response-content-disposition")

	lines := strings.Split(strings.ToLower(content), "filename=")
	if len(lines) != 2 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	presignedURL, err := app.GC.GeneratePresignedURL(user.ID.String(), lines[1])
	if err != nil {
		log.Error().Err(err).Msg("getGenerateUrlService error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = app.Repo.KYC.UpdateMediaItemStatus(&user, imageType, "")
	log.Debug().Msgf("Successfully generated presigned URL ", presignedURL)

	resp, _ := json.Marshal(map[string]string{"url": presignedURL})

	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}
