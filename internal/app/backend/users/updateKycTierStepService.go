package users

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/rs/zerolog/log"

	usersErr "lkd-platform-backend/internal/app/backend/users/errors"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func updateKycTierStepService(app *application.Application, user *models.User, body []byte, tierCode int, step string) usersErr.Errors {
	ks := NewKYCService(app)
	usersErrors := usersErr.Errors{}
	var commonInfo models.StepCommonInfoForm

	switch fmt.Sprintf("%d-%s", tierCode, step) {
	case fmt.Sprintf("%d-%s", models.Tier0, models.CommonInfo):
		if err := json.Unmarshal(body, &commonInfo); err != nil {
			log.Debug().Err(err).Msg("invalid data format " + models.CommonInfo)
			usersErrors.AddError(errors.New("invalid data format"))
			return usersErrors
		}

		log.Debug().Msgf("step common info form: %v", commonInfo)

		if commonInfo.BankIBAN != "" {
			if isIBANValid, validationFailures := ks.isValidIBAN(&commonInfo); !isIBANValid {
				usersErrors.AddErrors(validationFailures)

				log.Debug().Errs("validationFailures", usersErrors.GetErrors()).Msg(usersErrors.Error())
				return usersErrors
			}
		}

		if err := ks.updateKYCTierStepCommonInfo(app, user, &commonInfo); err != nil {
			log.Debug().Err(err).Msg("failure during execution " + models.CommonInfo)
			usersErrors.AddError(errors.New("invalid data format"))
			return usersErrors
		}
	case fmt.Sprintf("%d-%s", models.Tier1, models.ComplianceReview):
		var complianceReview models.StepComplianceReviewForm
		if err := json.Unmarshal(body, &complianceReview); err != nil {
			log.Debug().Err(err).Msg("invalid data format " + models.ComplianceReview)
			usersErrors.AddError(errors.New("invalid data format"))
			return usersErrors
		}

		if err := ks.updateKYCTierStepComplianceReview(app, user); err != nil {
			log.Debug().Err(err).Msg("failure during execution " + models.ComplianceReview)
			usersErrors.AddError(errors.New("invalid data format"))
			return usersErrors
		}
	}

	return usersErrors
}
