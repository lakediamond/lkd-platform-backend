package errors

import (
	"strings"
)

type ErrInterface interface {
	IsErrors() bool
	AddError(item error)
	AddErrors(items []error)
	GetErrors() []error
}

type Errors struct {
	errors []error
}

func (err *Errors) IsErrors() bool {
	if len(err.errors) == 0 {
		return false
	}

	return true
}

func (err *Errors) AddError(item error) {
	err.errors = append(err.errors, item)
}

func (err *Errors) AddErrors(items []error) {
	err.errors = items
}

func (err *Errors) GetErrors() []error {
	return err.errors
}

func (err *Errors) Error() string {
	errSlice := make([]string, 0)

	for key, value := range err.errors {
		errSlice[key] = value.Error()
	}

	return strings.Join(errSlice, ",")
}
