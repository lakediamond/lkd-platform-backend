package users

import (
	"net/http"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

type UserLogoutInput struct {
	Email string `json:"email"`
}

//UserLogoutHandler handling user logout
var UserLogoutHandler = handlers.ApplicationHandler(userLogout)

func userLogout(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	app := application.GetApplicationContext(r)

	//remove session cookies
	httputil.RemoveCookies(w)

	user := application.GetUser(r)

	if err := app.IAMClient.Logout(user.Email, ""); err != nil {
		log.Debug().Err(err).Msg("failed to call Hydra logout API")
	}

	w.Header().Set("Location", app.IAMClient.GetClientLogoutRedirectURL())
	w.WriteHeader(http.StatusOK)

}
