package users

import (
	"encoding/json"
	"errors"
	"fmt"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/app/backend/users/kyc"
	"lkd-platform-backend/internal/pkg/accountActivity"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

var CustomerImageReUploadRequestHandler = handlers.ApplicationHandler(customerImageReUpload)

func customerImageReUpload(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)

	var customerImageReUploadRequestInput models.CustomerImageReUploadRequestInput

	err := httputil.ParseBody(w, r.Body, &customerImageReUploadRequestInput)
	if err != nil {
		return
	}
	defer r.Body.Close()

	err = customerImageReUploadService(app, &customerImageReUploadRequestInput)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

//GetUserProfile returns user profile information
var GetUserProfile = handlers.ApplicationHandler(getUserProfile)

func getUserProfile(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	app := application.GetApplicationContext(r)
	user := application.GetUser(r)

	ks := NewKYCService(app)
	kycSummary, err := ks.getUserKYCSummary(&user)
	user.KYCSummary = kycSummary

	respData, err := json.Marshal(user)
	if err != nil {
		log.Debug().Msg(err.Error())
		w.WriteHeader(http.StatusForbidden)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(respData)
	return
}

//ConfirmEmailHandler handling email confirmation endpoint
var ConfirmEmailHandler = handlers.ApplicationHandler(confirmEmail)

func confirmEmail(w http.ResponseWriter, r *http.Request) {
	log.Debug().Msg("confirming email")
	//to prevent built-in Redirect behaviour
	w.Header().Set("Content-Type", "")

	app := application.GetApplicationContext(r)

	frontentURL := app.Config.LkdPlatformFrontendDNS

	keys, ok := r.URL.Query()["token"]
	if !ok || len(keys[0]) < 1 {
		log.Debug().Msg("Url Param 'token' are missing or invalid")
		redirectToURL(frontentURL, w, r, map[string]string{"email_verified": "false"})
		return
	}

	claims, err := getJwtTokenValidClaims(app.Config.JwtSalt, keys[0])
	if err != nil {
		log.Debug().Err(err).Msg("JWT claims parse error")
		redirectToURL(frontentURL, w, r, map[string]string{"email_verified": "false"})
	}

	email := claims["email"].(string)
	spanID := claims["spanID"].(string)

	var sID uint64
	sID, _ = strconv.ParseUint(spanID, 10, 64)

	span := tracer.StartSpan("web.request",
		tracer.ResourceName(r.RequestURI),
		tracer.WithSpanID(sID))

	user, err := app.Repo.User.GetUserByEmail(email)
	if err != nil {
		span.Finish(tracer.WithError(err))
		log.Debug().Err(err).Msgf("User with email %s not found", email)
		redirectToURL(frontentURL, w, r, map[string]string{"email_verified": "false"})
	}

	if !user.EmailVerified {
		user.EmailVerified = true
		err = app.Repo.User.UpdateUser(&user)
		if err != nil {
			log.Debug().Err(err).Msgf("Cannot set EmailVerified = true for user with email %s", email)
			span.Finish(tracer.WithError(err))
			redirectToURL(frontentURL, w, r, map[string]string{"email_verified": "false"})
		}

		err = app.Repo.KYC.CreateTemplateRecords(&user, app.PipeDrive.GetDeal())
		if err != nil {
			log.Error().Err(err).Msg("CreateTemplateRecords error")
			span.Finish(tracer.WithError(err))
			http.Redirect(w, r, frontentURL.String(), http.StatusFound)
			return
		}

		err = registerFlags(app, &user)
		if err != nil {
			log.Error().Err(err).Msg("RegisterFlags error")
			span.Finish(tracer.WithError(err))
			http.Redirect(w, r, frontentURL.String(), http.StatusFound)
			return
		}
	} else {
		err = errors.New(errEmailAlreadyVerified.Error())
		span.Finish(tracer.WithError(err))
		log.Debug().Err(err).Msgf("user email: %s", email)
	}

	redirectRawURL := claims["redirectURL"].(string)
	redirectURL, err := url.Parse(redirectRawURL)
	if err != nil {
		span.Finish(tracer.WithError(err))
		log.Error().Err(err).Msg("Cant parse redirectURL from jwt token")
		http.Redirect(w, r, frontentURL.String(), http.StatusFound)
		return
	}

	span.Finish()
	redirectToURL(redirectURL, w, r, map[string]string{})
}

func redirectToURL(frontendUrl *url.URL, w http.ResponseWriter, r *http.Request, params map[string]string) {
	urlQueryParams := frontendUrl.Query()

	for key, value := range params {
		urlQueryParams.Set(key, value)
	}
	frontendUrl.RawQuery = urlQueryParams.Encode()
	http.Redirect(w, r, frontendUrl.String(), http.StatusFound)
}

//LockUserHandler handling ban user request
var LockUserHandler = handlers.ApplicationHandler(lockUser)

func lockUser(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)
	err := lockAccount(app.Repo, r.Header.Get("email"))

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	log.Debug().Msgf("Locking user %s , from admin %s", r.Header.Get("email"), r.Header.Get("user_email"))

	w.WriteHeader(http.StatusOK)
}

// ValidatePhoneFormatHandler validates given value against Twilio phone lookup API
var ValidatePhoneFormatHandler = handlers.ApplicationHandler(validatePhoneFormatHandler)

func validatePhoneFormatHandler(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)

	queryValues := r.URL.Query()
	phoneValue := queryValues.Get("phone")

	if len(phoneValue) == 0 {
		err := errors.New("phone number is invalid or empty")
		log.Debug().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusUnprocessableEntity)
		httputil.WriteErrorMsg(w, err)
		return
	}

	pvs := NewPhoneVerificationService(app)
	if isValid, err := pvs.IsValidFormat(app, phoneValue); !isValid {
		err := fmt.Errorf("phone number format is not invalid. 3rd party response - %s", err.Error())
		log.Debug().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusUnprocessableEntity)
		httputil.WriteErrorMsg(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

type CreatePhoneVerificationForm struct {
	Phone       string `json:"phone"`
	CountryCode string `json:"country_code"`
}

// CreatePhoneVerificationHandler handling new phone verification
var CreatePhoneVerificationHandler = handlers.ApplicationHandler(createPhoneVerification)

func createPhoneVerification(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)

	var createPVForm CreatePhoneVerificationForm

	err := httputil.ParseBody(w, r.Body, &createPVForm)
	if err != nil {
		return
	}

	defer r.Body.Close()

	user := application.GetUser(r)

	pvs := NewPhoneVerificationService(app)
	err = pvs.CreatePhoneVerificationService(&user, createPVForm)

	if err != nil {
		var status int
		switch err.(type) {
		case ErrPhoneVerificationToManyRequests:
			status = http.StatusTooManyRequests
		case ErrPhoneVerificationAlreadyExists:
			status = http.StatusConflict
		default:
			log.Debug().Msg(err.Error())
			status = http.StatusInternalServerError
		}
		w.WriteHeader(status)
		httputil.WriteErrorMsg(w, err)
		return
	}

	ks := NewKYCService(app)
	if err = ks.updateKYCTierStep(app, &user, models.Tier0, models.PhoneVerificationStep, createPVForm); err != nil {
		log.Debug().Err(err).Msg("failed to update related KYC tier step")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, errors.New("failed to update related KYC tier step"))
		return
	}

	w.WriteHeader(http.StatusCreated)
}

type verifyPhoneForm struct {
	Code int `json:"code,string" required:"true"`
}

// VerifyPhoneHandler handling new phone verification
var VerifyPhoneHandler = handlers.ApplicationHandler(verifyPhone)

func verifyPhone(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)

	var verifyPVForm verifyPhoneForm
	err := httputil.ParseBody(w, r.Body, &verifyPVForm)
	if err != nil {
		return
	}

	defer r.Body.Close()

	user := application.GetUser(r)

	validator, _ := regexp.Compile("^[0-9]*$")

	ok := validator.Find([]byte(strconv.Itoa(verifyPVForm.Code)))

	if len(ok) == 0 {
		err = errors.New("invalid data format")
		log.Debug().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusUnprocessableEntity)
		httputil.WriteErrorMsg(w, err)
		return
	}

	pvs := NewPhoneVerificationService(app)
	err = pvs.VerifyPhoneService(app, user, verifyPVForm.Code)

	if err != nil {
		var status int
		switch err.(type) {
		case ErrPhoneVerificationToManyRequests:
			status = http.StatusTooManyRequests
		case ErrPhoneVerificationNotFound:
			status = http.StatusNotFound
		case ErrPhoneVerificationInvalid:
			status = http.StatusConflict
		default:
			log.Debug().Msg(err.Error())
			status = http.StatusInternalServerError
		}
		w.WriteHeader(status)
		httputil.WriteErrorMsg(w, err)
		return
	}

	err = pvs.AfterVerifyPhoneService(app, user)
	if err != nil {
		log.Error().Err(err).Msg("AfterVerifyPhoneService error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	ks := NewKYCService(app)
	if err = ks.updateKYCTierStep(app, &user, models.Tier0, models.PhoneVerificationStep, struct{ Verified bool }{Verified: true}); err != nil {
		log.Debug().Err(err).Msg("Failed to update related KYC tier step")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, errors.New("failed to update related KYC tier step"))
		return
	}

	w.WriteHeader(http.StatusOK)
}

// ResendPhoneVerificationHandler handling new phone verification
var ResendPhoneVerificationHandler = handlers.ApplicationHandler(resendPhoneVerification)

func resendPhoneVerification(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)

	var createPVForm CreatePhoneVerificationForm
	err := httputil.ParseBody(w, r.Body, &createPVForm)
	if err != nil {
		return
	}

	defer r.Body.Close()

	user := application.GetUser(r)

	pvs := NewPhoneVerificationService(app)
	err = pvs.ResendPhoneVerificationService(&user, createPVForm.Phone)
	if err != nil {
		w.WriteHeader(http.StatusTooManyRequests)
		httputil.WriteErrorMsg(w, err)
		return
	}

	ks := NewKYCService(app)
	if err = ks.updateKYCTierStep(app, &user, models.Tier0, models.PhoneVerificationStep, createPVForm); err != nil {
		log.Debug().Err(err).Msg("Failed to update related KYC tier step")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, errors.New("failed to update related KYC tier step"))
		return
	}

	w.WriteHeader(http.StatusOK)
}

// UpdateKycTierStep updates tier step with user inputs
var UpdateKycTierStep = handlers.ApplicationHandler(updateKycTierStep)

func updateKycTierStep(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)
	user := application.GetUser(r)
	routeParams := httprouter.ParamsFromContext(r.Context())

	step := routeParams.ByName("step")
	tierRouteVar := routeParams.ByName("tier")

	tierCode, err := strconv.Atoi(tierRouteVar)
	if err != nil || len(step) == 0 {
		w.WriteHeader(http.StatusForbidden)
		httputil.WriteErrorMsg(w, errors.New("invalid route params"))
		return
	}

	body := httputil.ReadBody(r.Body)
	defer r.Body.Close()

	errUser := updateKycTierStepService(app, &user, body, tierCode, step)
	if errUser.IsErrors() == true {
		w.WriteHeader(http.StatusUnprocessableEntity)
		httputil.WriteMultiplyErrorMsg(w, errUser.GetErrors())
		return
	}

	w.WriteHeader(http.StatusOK)
}

// GetKycTierStep returns details on a certain step of specific KYC tier
var GetKycTierStep = handlers.ApplicationHandler(getKycTierStep)

func getKycTierStep(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)
	user := application.GetUser(r)

	routeParams := httprouter.ParamsFromContext(r.Context())
	tierCodeRouteVar := routeParams.ByName("tier")

	tierCode, err := strconv.Atoi(tierCodeRouteVar)
	if err != nil {
		log.Debug().Err(err).Msg("invalid tier code")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	stepCode := routeParams.ByName("step")
	ks := NewKYCService(app)

	step, err := ks.getKYCTierStep(&user, tierCode, stepCode)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot get step details from db")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	resp, _ := json.Marshal(step)
	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}

type updateInvestmentPlanInputData struct {
	InvestmentPlan int `json:"investment_plan"`
}

type UpdateInvestmentPlanInput struct {
	InvestmentPlan int `json:"investment_plan"`
}

// GetKycTiers returns details on specific KYC tier including nested steps
var UpdateInvestmentPlan = handlers.ApplicationHandler(updateInvestmentPlan)

func updateInvestmentPlan(w http.ResponseWriter, r *http.Request) {
	var payload updateInvestmentPlanInputData

	app := application.GetApplicationContext(r)
	user := application.GetUser(r)

	err := httputil.ParseBody(w, r.Body, &payload)
	if err != nil {
		return
	}
	defer r.Body.Close()

	if !user.IsValidTier(payload.InvestmentPlan) {
		err := errors.New("required parameter `investment_plan` is missing or invalid")
		log.Debug().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusUnprocessableEntity)
		httputil.WriteErrorMsg(w, err)
		return
	}

	plan, err := app.Repo.KYC.GetPlanByCode(payload.InvestmentPlan)
	if err != nil {
		err := errors.New(fmt.Sprintf("failed to get investment plan for request - %v", payload))
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	if user.KYCInvestmentPlan == nil {
		if err = app.Repo.User.UpdateTargetTier(&user, plan); err != nil {
			err = fmt.Errorf("failed to set target tier on user record - %s", err.Error())
			log.Debug().Err(err).Msg(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			httputil.WriteErrorMsg(w, err)
			return
		}
		if err = app.Repo.KYC.UnlockTierByCode(&user, plan.Code); err != nil {
			err = fmt.Errorf("failed to unlock target tier for user - %s", err.Error())
			log.Debug().Err(err).Msg(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			httputil.WriteErrorMsg(w, err)
			return
		}

		w.WriteHeader(http.StatusOK)
		return
	}

	log.Info().Msgf("user plan %v, new plan %v", user.KYCInvestmentPlan, plan)

	if user.KYCInvestmentPlan.Code == plan.Code {
		w.WriteHeader(http.StatusOK)
		return
	}

	if err = app.Repo.User.UpdateTargetTier(&user, plan); err != nil {
		err = fmt.Errorf("failed to set target tier on user record - %s", err.Error())
		log.Debug().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	if err = app.Repo.KYC.UnlockTierByCode(&user, plan.Code); err != nil {
		err = fmt.Errorf("failed to unlock target tier for user - %s", err.Error())
		log.Debug().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	if verificationResult, err := app.Repo.VerificationResult.GetLastResultRecord(&user); err != nil {
		log.Warn().Msgf("There was no previous verification records for user ID - %s", user.ID.String())
	} else {
		if err = kyc.CreateKYCPostIdentityVerificationFlags(app, &verificationResult); err != nil {
			log.Error().Err(err).Msg("CreateKYCPostIdentityVerificationFlags error")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	if user.CRMDealID == 0 {
		w.WriteHeader(http.StatusOK)
		return
	}

	eventCron := &models.Worker{
		UserID:    user.ID,
		Event:     models.EventCreateUpdateCRMDeal,
		State:     models.StateNew,
		CreatedAt: time.Now(),
	}

	errDb := app.Repo.KYC.CreateWorker(eventCron)
	if errDb != nil {
		log.Error().Err(err).Msg("createWorker::[EventCreateUpdateCRMDeal] error")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, fmt.Errorf("update CRM deal for customer - %s", err.Error()))
		return
	}

	w.WriteHeader(http.StatusOK)
}

// GetKycTiers returns details on specific KYC tier including nested steps
var GetKycTiers = handlers.ApplicationHandler(getKycTiers)

func getKycTiers(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)
	user := application.GetUser(r)

	ks := NewKYCService(app)

	tiers, err := ks.getKYCTiers(&user)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot get tiers")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	resp, _ := json.Marshal(tiers)
	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}

var GetInvestmentPlanOptionsHandler = handlers.ApplicationHandler(getInvestmentPlanOptionsHandler)

func getInvestmentPlanOptionsHandler(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)

	ks := NewKYCService(app)
	user := application.GetUser(r)

	plan, err := ks.getKYCPlans(&user)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot get dictionary `tier_investment_plan`")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	resp, _ := json.Marshal(plan)
	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}

//SetBlockchainProviderHandler returns expected investment buckets for customer to select from
var SetBlockchainProviderHandler = handlers.ApplicationHandler(setBlockchainProvider)

const (
	ConnectionProviderMetamask = "METAMASK"
	ConnectionProviderLedger   = "LEDGER"
)

type blockchainProvider struct {
	ConnType string `json:"blockchain_connection_type"`
}

func (bp blockchainProvider) isValid() bool {
	return bp.ConnType == ConnectionProviderMetamask || bp.ConnType == ConnectionProviderLedger
}

func setBlockchainProvider(w http.ResponseWriter, r *http.Request) {
	provider := blockchainProvider{}
	err := httputil.ParseBody(w, r.Body, &provider)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot parse request body")
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, err)
		return
	}

	if !provider.isValid() {
		log.Debug().Err(err).Msg("Invalid connection provider")
		w.WriteHeader(http.StatusUnprocessableEntity)
		httputil.WriteErrorMsg(w, err)
		return
	}

	app := application.GetApplicationContext(r)
	user := application.GetUser(r)

	err = app.Repo.User.UpdateBlockchainProvider(user.ID, provider.ConnType)
	if err != nil {
		accountActivity.SetBlockchainProviderFailure(app.Repo, user.Email, provider.ConnType, err)
		log.Debug().Err(err).Msg("Cannot set new value")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	accountActivity.SetBlockchainProvider(app.Repo, user.Email, provider.ConnType)

	w.WriteHeader(http.StatusOK)
}
