package admins

import (
	"net/http"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"

	"github.com/rs/zerolog/log"
)

//GetAllAdminsHandler handling get all subOwners request
var GetAllAdminsHandler = handlers.ApplicationHandler(getAllAdmins)

func getAllAdmins(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)

	admins, err := getAdmins(app.Repo.SubOwner)
	if err != nil {
		log.Debug().Err(err).Msg("Get all admins error")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	respData, err := httputil.MarshalStructure(w, admins)
	if err != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(respData)
}
