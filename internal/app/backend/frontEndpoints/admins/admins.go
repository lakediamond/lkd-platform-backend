package admins

import (
	"lkd-platform-backend/internal/pkg/repo"
)

func getAdmins(repo repo.SubOwnerRepository) ([]repo.Admin, error) {

	admins, err := repo.GetAllSubOwnersJoinUsers()
	if err != nil {
		return admins, err
	}

	return admins, nil

}
