package exchange

import (
	"errors"
	"lkd-platform-backend/internal/pkg/repo/models"
	"math"
	"os"
	"strconv"

	"lkd-platform-backend/internal/pkg/repo"
)

type ExchangeInterface interface {
	calculateRate(ethAmount float64, rate float64) error
}

type BaseExchangeRate struct {
	ConvertedAmount float64 `json:"converted_amount"`
	Currency        string  `json:"currency"`
}

func (ber *BaseExchangeRate) calculateRate(ethAmount float64, rate float64) error {
	ber.ConvertedAmount = math.RoundToEven((ethAmount*rate)*100) / 100
	ber.Currency = "CHF"

	return nil
}

type CalcExchangeRate struct {
	BaseExchangeRate
	RiskIncluded        float64 `json:"risk_included"`
	RiskAmount          float64 `json:"risk_amount"`
	DeviationAllowedMin float64 `json:"deviation_allowed_min"`
	DeviationAllowedMax float64 `json:"deviation_allowed_max"`
}

func (cer *CalcExchangeRate) calculateRate(ethAmount float64, rate float64) error {

	riskIncludedEnv, ok := os.LookupEnv("API_EXCHANGE_RISK_RATE")
	if !ok {
		return errors.New("can not find API_EXCHANGE_RISK_RATE")
	}

	riskInclude, err := strconv.ParseFloat(riskIncludedEnv, 64)
	if err != nil {
		return err
	}

	deviationEnv, ok := os.LookupEnv("API_EXCHANGE_DEVIATION_RATE")
	if !ok {
		return errors.New("can not find API_EXCHANGE_DEVIATION_RATE")
	}

	deviation, err := strconv.ParseFloat(deviationEnv, 64)
	if err != nil {
		return err
	}

	calcAmount := (ethAmount * rate) + ((ethAmount * rate) * (riskInclude / 100))
	deviationAllowedMin := calcAmount - calcAmount*(deviation/100)
	deviationAllowedMax := calcAmount + calcAmount*(deviation/100)

	cer.ConvertedAmount = math.RoundToEven(calcAmount*100) / 100
	cer.Currency = "CHF"
	cer.RiskIncluded = math.RoundToEven((riskInclude/100)*100) / 100
	cer.RiskAmount = math.RoundToEven(((ethAmount*rate)*(riskInclude/100))*100) / 100
	cer.DeviationAllowedMin = math.RoundToEven(deviationAllowedMin*100) / 100
	cer.DeviationAllowedMax = math.RoundToEven(deviationAllowedMax*100) / 100

	return nil
}

// Exchange calculate
func getCalcExchange(repo *repo.Repo, ethAmount float64, riskIncluded bool) (interface{}, error) {
	var calcExchangeRate CalcExchangeRate
	var baseExchangeRate BaseExchangeRate

	exchangeEthEur, err := repo.Exchange.GetExchangeRateByCode(models.RATE_CODE_ETH_EUR)
	if err != nil {
		return nil, err
	}

	exchangeEurChr, err := repo.Exchange.GetExchangeRateByCode(models.RATE_CODE_EUR_CHF)
	if err != nil {
		return nil, err
	}

	rate := exchangeEthEur.Rate * exchangeEurChr.Rate

	if riskIncluded {
		err := calcExchangeRate.calculateRate(ethAmount, rate)
		if err != nil {
			return nil, err
		}

		return calcExchangeRate, nil
	}

	_ = baseExchangeRate.calculateRate(ethAmount, rate)
	return baseExchangeRate, nil
}
