package exchange

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"lkd-platform-backend/pkg/httputil"
	"net/http"
	"strconv"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
)

// Get exchange rate used currency code
var ExchangeHandler = handlers.ApplicationHandler(getExchangeData)

func getExchangeData(w http.ResponseWriter, r *http.Request) {
	const codeKey = "code"
	code, ok := r.URL.Query()[codeKey]

	if !ok || len(code[0]) < 1 {
		errorMessage := fmt.Errorf("url Param %s is missing", codeKey)
		log.Info().Msg(errorMessage.Error())
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	app := application.GetApplicationContext(r)

	exchangeData, err := app.Repo.Exchange.GetExchangeRateByCode(code[0])
	if err != nil {
		errorMessage := errors.New("can`t find exchange rate")
		log.Error().Err(err).Msg(errorMessage.Error())
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	result, err := json.Marshal(exchangeData)
	if err != nil {
		errorMessage := errors.New("can`t serialize exchange data")
		log.Error().Err(err).Msg(errorMessage.Error())
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(result)
}

// Exchange calculate rate
var CalculateExchangeRateHandler = handlers.ApplicationHandler(getCalculateExchangeData)

func getCalculateExchangeData(w http.ResponseWriter, r *http.Request) {
	var riskIncluded bool
	const ethAmountKey = "ethAmount"
	const riskIncludedKey = "riskIncluded"

	queryValues := r.URL.Query()

	ethAmount := queryValues.Get(ethAmountKey)
	if ethAmount == "" {
		errorMessage := fmt.Errorf("url Param %s is missing", ethAmountKey)
		log.Info().Msg(errorMessage.Error())
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	floatEthAmount, err := strconv.ParseFloat(ethAmount, 64)
	if err != nil {
		ethAmountError := fmt.Errorf("can not validate format %s", ethAmountKey)
		log.Error().Err(err).Msg("Exchange ethAmount errors")
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, ethAmountError)
		return
	}

	boolRiskIncluded := queryValues.Get(riskIncludedKey)

	riskIncluded = true
	if boolRiskIncluded != "true" {
		riskIncluded = false
	}

	app := application.GetApplicationContext(r)
	response, err := getCalcExchange(app.Repo, floatEthAmount, riskIncluded)
	if err != nil {
		errorMessage := errors.New("can`t calculate exchange rate")
		log.Error().Err(err).Msg(errorMessage.Error())
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	result, err := json.Marshal(response)
	if err != nil {
		errorMessage := errors.New("can`t serialize exchange data")
		log.Error().Err(err).Msg(errorMessage.Error())
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(result)
}
