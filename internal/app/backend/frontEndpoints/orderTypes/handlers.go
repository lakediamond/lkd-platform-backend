package orderTypes

import (
	"encoding/json"
	"net/http"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
)

//GetAllOrderTypesHandler handling get all order types request
var GetAllOrderTypesHandler = handlers.ApplicationHandler(getAllOrderTypes)

func getAllOrderTypes(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)

	orderTypes, err := getOrderTypes(app.Repo.OrderTypes)
	if err != nil {
		log.Debug().Err(err).Msg("Get all order types error")
		//w.WriteHeader(http.StatusInternalServerError)
		return
	}

	respData, _ := json.Marshal(orderTypes)

	w.WriteHeader(http.StatusOK)
	w.Write(respData)
}
