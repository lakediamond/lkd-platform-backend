package orderTypes_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
	"lkd-platform-backend/pkg/httputil"
)

type FrontEndpointsOrderTypeSuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
	User       *models.User
}

func (suite *FrontEndpointsOrderTypeSuite) SetupTestSuite() {

}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_FrontEndpointsOrderTypeSuite(t *testing.T) {
	suite.Run(t, new(FrontEndpointsOrderTypeSuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *FrontEndpointsOrderTypeSuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()

	priv.Migration(app.Repo.DB)
	suite.App = app
	suite.MockServer = mockServer

	suite.App.Repo.DB.Exec("DELETE FROM users;")
	suite.App.Repo.DB.Exec("DELETE FROM order_types;")

	var user models.User
	user.Email = "romuch@test.com"
	user.EthAddress = "0x040334a07f601350cAFAc8AbE5d9E21968d31521"
	user.Role = "admin"

	suite.App.Repo.User.CreateNewUser(&user)

	suite.User = &user
}

func (suite *FrontEndpointsOrderTypeSuite) AfterTest() {
	suite.App.Repo.DB.Exec("DELETE FROM users;")
	suite.App.Repo.DB.Exec("DELETE FROM order_types;")
}

func (suite *FrontEndpointsOrderTypeSuite) TestZeroOrderTypesData() {

	u, _ := url.Parse(suite.MockServer.URL + "/admin/order_type")

	client := &http.Client{}
	req, _ := http.NewRequest("GET", u.String(), nil)
	req.Header.Add("user_email", suite.User.Email)

	resp, _ := client.Do(req)

	suite.Equal(http.StatusOK, resp.StatusCode)

	var orderTypes []models.OrderType

	b := httputil.ReadBody(resp.Body)
	defer resp.Body.Close()

	json.Unmarshal(b, &orderTypes)

	suite.Equal(0, len(orderTypes))
}

func (suite *FrontEndpointsOrderTypeSuite) TestOneOrderTypeData() {

	u, _ := url.Parse(suite.MockServer.URL + "/admin/order_type")

	orderType := &models.OrderType{ID: 1, Name: "Test"}
	suite.App.Repo.OrderTypes.SaveOrderType(orderType)

	client := &http.Client{}
	req, _ := http.NewRequest("GET", u.String(), nil)
	req.Header.Add("user_email", suite.User.Email)

	resp, error := client.Do(req)

	suite.Equal(nil, error)

	suite.Equal(http.StatusOK, resp.StatusCode)

	var orderTypes []models.OrderType

	b := httputil.ReadBody(resp.Body)
	defer resp.Body.Close()

	json.Unmarshal(b, &orderTypes)

	suite.Equal(1, len(orderTypes))
	suite.Equal(orderType.ID, orderTypes[0].ID)
	suite.Equal(orderType.Name, orderTypes[0].Name)
}
