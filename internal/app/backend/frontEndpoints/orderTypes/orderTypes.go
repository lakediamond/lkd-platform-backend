package orderTypes

import (
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func getOrderTypes(repo repo.OrderTypesRepository) ([]models.OrderType, error) {

	var orderTypes []models.OrderType

	orderTypes, err := repo.GetAllOrderTypes()
	if err != nil {
		return orderTypes, err
	}

	return orderTypes, nil
}
