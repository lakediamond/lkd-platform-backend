package dashboard

import (
	"encoding/json"
	"errors"
	"fmt"
	"lkd-platform-backend/pkg/httputil"
	"math/big"
	"net/http"
	"strconv"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
)

var MainChartHandler = handlers.ApplicationHandler(getMainChartData)

func getMainChartData(w http.ResponseWriter, r *http.Request) {
	const daysToLoadValueKey = "daysToLoad"
	daysToLoadValue, ok := r.URL.Query()[daysToLoadValueKey]

	if !ok || len(daysToLoadValue[0]) < 1 {
		errorMessage := fmt.Errorf("url Param %s is missing", daysToLoadValueKey)
		log.Info().Msg(errorMessage.Error())
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	daysToLoad, err := strconv.Atoi(daysToLoadValue[0])
	if err != nil {
		errorMessage := fmt.Errorf("url Param %s must be a number", daysToLoadValueKey)
		log.Info().Err(err).Msg(errorMessage.Error())
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	app := application.GetApplicationContext(r)

	mainChartData, err := loadMainChartData(app.Repo.Dashboard, daysToLoad)
	if err != nil && mainChartData == nil {
		errorMessage := errors.New("can`t load main chart data")
		log.Error().Err(err).Msg(errorMessage.Error())
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	result, err := json.Marshal(mainChartData)
	if err != nil {
		errorMessage := errors.New("can`t serialize main chart data")
		log.Error().Err(err).Msg(errorMessage.Error())
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(result)
}

var ProposalQueueEstimation = handlers.ApplicationHandler(getProposalQueueEstimation)

func getProposalQueueEstimation(w http.ResponseWriter, r *http.Request) {

	const priceValueKey = "price"
	priceValue, ok := r.URL.Query()[priceValueKey]

	if !ok || len(priceValue[0]) < 1 {
		errorMessage := fmt.Errorf("url Param %s is missing", priceValueKey)
		log.Info().Msg(errorMessage.Error())
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	price, ok := (new(big.Int)).SetString(priceValue[0], 10)
	if !ok {
		errorMessage := fmt.Errorf("url Param %s must be a number", priceValueKey)
		log.Info().Msg(errorMessage.Error())
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	app := application.GetApplicationContext(r)
	loadProposalQueueEstimationResponse, err := loadProposalQueueEstimation(app.Repo.Dashboard, price)
	if err != nil {
		errorMessage := errors.New("can`t load proposal queue estimation")
		log.Error().Err(err).Msg(errorMessage.Error())
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	result, err := json.Marshal(loadProposalQueueEstimationResponse)
	if err != nil {
		errorMessage := errors.New("can`t serialize load proposal queue estimation")
		log.Error().Err(err).Msg(errorMessage.Error())
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(result)

}
