package dashboard

import (
	"lkd-platform-backend/internal/pkg/repo"
	"math/big"

	"github.com/shopspring/decimal"
)

func loadMainChartData(repo repo.DashboardRepository, daysToLoad int) (*mainChartData, error) {
	dataRows := make([]mainChartDataRow, 0)
	data := &mainChartData{}

	rows, err := repo.GetMainChartRows(daysToLoad)
	if err != nil && rows == nil {
		return nil, err
	}

	for _, row := range rows {
		dataRows = append(dataRows, mainChartDataRow{
			MinValue:           row.MinValue,
			MaxValue:           row.MaxValue,
			OpenProposalVolume: row.OpenProposalVolume,
			PastOrderVolume:    row.PastOrderVolume,
		})
	}

	data.Rows = dataRows

	lastConversionPrice, err := repo.GetLastPriceConversion(daysToLoad)
	if err != nil && lastConversionPrice == nil {
		return nil, err
	}

	data.LastPriceConversion = lastConversionPrice

	avgConversionPrice, err := repo.GetAveragePriceConversion(daysToLoad)
	if err != nil && avgConversionPrice == nil {
		return nil, err
	}

	data.AveragePriceConversion = avgConversionPrice.Round(0)

	return data, nil
}

type mainChartData struct {
	AveragePriceConversion decimal.Decimal    `json:"average_price_conversion"`
	LastPriceConversion    *decimal.Decimal   `json:"last_price_conversion"`
	Rows                   []mainChartDataRow `json:"rows"`
}

type mainChartDataRow struct {
	MinValue           decimal.Decimal `json:"minValue"`
	MaxValue           decimal.Decimal `json:"maxValue"`
	OpenProposalVolume uint            `json:"proposalsTokenAmount"`
	PastOrderVolume    uint            `json:"ordersTokenAmount"`
}

func loadProposalQueueEstimation(repo repo.DashboardRepository, price *big.Int) (proposalQueueEstimation, error) {
	var proposalQueueEstimation proposalQueueEstimation

	TokenAmount, ProposalsAmount := repo.GetProposalQueueEstimation(price)
	proposalQueueEstimation.TokensAmount = TokenAmount
	proposalQueueEstimation.ProposalsAmount = ProposalsAmount

	if proposalQueueEstimation.TokensAmount == nil {
		value, _ := decimal.NewFromString("0")
		proposalQueueEstimation.TokensAmount = &value
	}

	if proposalQueueEstimation.ProposalsAmount == nil {
		value, _ := decimal.NewFromString("0")
		proposalQueueEstimation.ProposalsAmount = &value
	}

	return proposalQueueEstimation, nil
}

type proposalQueueEstimation struct {
	TokensAmount    *decimal.Decimal `json:"upfront_queue_tokens_volume"`
	ProposalsAmount *decimal.Decimal `json:"upfront_queue_proposals_nb"`
}
