package matchSimulator

import (
	"errors"
	"math/big"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	. "lkd-platform-backend/internal/pkg/errs"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

type OrderData struct {
	Rest           *big.Int        `json:"rest"`
	Order          models.Order    `json:"order"`
	Investors      []Investor      `json:"investors"`
	CHFCoefficient decimal.Decimal `json:"chf_coefficient"`
}

type Investor struct {
	EthAddress string     `json:"address"`
	Proposals  []Proposal `json:"proposals"`
	Banned     bool       `json:"banned"`
}

type Proposal struct {
	ID        uint            `json:"id"`
	Price     decimal.Decimal `json:"price"`
	Tokens    decimal.Decimal `json:"tokens"`
	Eth       decimal.Decimal `json:"eth"`
	CreatedAt time.Time       `json:"created_at"`
}

func MatchSimulationService(repo *repo.Repo, orderData OrderData) (OrderData, error) {
	log.Print("Matcher simulation starts")

	log.Print(orderData)

	var ids []*big.Int
	var tokenValues []*big.Int
	var etherValues []*big.Int

	orderAmountBuf := orderData.Order.TokenAmount

	rest := orderData.Order.EthAmount

	var orderResp OrderData
	orderResp.Order = orderData.Order
	dec := decimal.New(1, 18)

	if orderData.Order.EthAmount.IsZero() {
		return orderData, errors.New("order eth amount = 0, dividing by zero")
	}
	coefficient := (orderData.Order.Price.Mul(orderData.Order.TokenAmount).Mul(dec).Div(orderData.Order.EthAmount)).Ceil()

	log.Debug().Msgf("chfCoefficient: %s", coefficient.String())

	orderResp.CHFCoefficient = coefficient.Div(dec)

	proposals, err := repo.Proposal.GetAllProposalsToMatchSimulate(orderData.Order.Price)
	if err != nil {
		log.Debug().Err(err).Msg(ErrDBError)
		return orderData, err
	}

	var tokenBuf = decimal.New(0, 0)

	for _, proposal := range proposals {

		//Check if exists
		index := -1
		for i, investor := range orderResp.Investors {
			if proposal.CreatedBy == investor.EthAddress {
				index = i
				break
			}
		}

		if index == -1 {
			orderResp.Investors = append(orderResp.Investors, Investor{proposal.CreatedBy, []Proposal{}, false})
			index = len(orderResp.Investors) - 1
		}
		//Check if exists finish

		//Check for ban
		for _, investor := range orderData.Investors {
			if orderResp.Investors[index].EthAddress == investor.EthAddress && investor.Banned {
				orderResp.Investors[index].Banned = true
				break
			}
		}

		//Add new proposal
		orderResp.Investors[index].Proposals = append(orderResp.Investors[index].Proposals, Proposal{proposal.ID,
			proposal.Price,
			decimal.New(0, 0), decimal.New(0, 0), proposal.CreatedOn})

		propIndex := len(orderResp.Investors[index].Proposals) - 1

		if proposal.Banned || orderResp.Investors[index].Banned {
			continue
		}
		//Check for ban finish

		// x<y
		ids = append(ids, big.NewInt(int64(proposal.BlockchainID)))

		if orderAmountBuf.Cmp(proposal.Amount) == -1 {
			x, _ := new(big.Int).SetString(orderAmountBuf.String(), 10)

			tokenValues = append(tokenValues, x)
			tokenBuf = tokenBuf.Add(orderAmountBuf)

			orderResp.Investors[index].Proposals[propIndex].Tokens = orderAmountBuf

			valueBuf := (orderAmountBuf.Mul(proposal.Price).Ceil().Mul(dec).Div(coefficient)).Floor()

			x, _ = new(big.Int).SetString(valueBuf.String(), 10)

			etherValues = append(etherValues, x)
			orderResp.Investors[index].Proposals[propIndex].Eth = valueBuf

			rest = rest.Sub(valueBuf)

			break
		} else {
			x, _ := new(big.Int).SetString(proposal.Amount.String(), 10)

			tokenValues = append(tokenValues, x)
			tokenBuf = tokenBuf.Add(proposal.Amount)

			orderAmountBuf = orderAmountBuf.Sub(proposal.Amount)
			orderResp.Investors[index].Proposals[propIndex].Tokens = proposal.Amount

			valueBuf := (proposal.Amount.Mul(proposal.Price).Mul(dec).Div(coefficient)).Floor()
			orderResp.Investors[index].Proposals[propIndex].Eth = valueBuf

			x, _ = new(big.Int).SetString(valueBuf.String(), 10)

			etherValues = append(etherValues, x)

			rest = rest.Sub(valueBuf)
		}

		if rest.Equal(decimal.New(0, 0)) || tokenBuf.Cmp(orderData.Order.TokenAmount) == 0 {
			break
		}
	}

	x, _ := new(big.Int).SetString(rest.String(), 10)
	orderResp.Rest = x
	log.Debug().Msgf("proposal ids: %v", ids)
	log.Debug().Msgf("token values: %v", tokenValues)
	log.Debug().Msgf("ether values: %v", etherValues)
	log.Debug().Msgf("rest: %d", x)

	log.Print("matcher simulation finished")

	return orderResp, nil
}
