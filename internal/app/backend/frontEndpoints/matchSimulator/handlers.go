package matchSimulator

import (
	"encoding/json"
	"net/http"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

var MatchSimulationHandler = handlers.ApplicationHandler(MatchSimulation)

func MatchSimulation(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)

	var orderData OrderData

	err := httputil.ParseBody(w, r.Body, &orderData)
	if err != nil {
		return
	}
	defer r.Body.Close()

	data, err := MatchSimulationService(app.Repo, orderData)
	if err != nil {
		log.Error().Err(err).Msg("MatchSimulationService error")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	respData, _ := json.Marshal(data)

	w.WriteHeader(http.StatusOK)
	w.Write(respData)
}
