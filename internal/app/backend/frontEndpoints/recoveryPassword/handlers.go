package recoveryPassword

import (
	"net/http"
	"net/url"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

const (
	noUser             string = "No user record found with provided email address"
	emailNotVerified   string = "User record with provided email address still requires verification"
	invalidAccessToken string = "Invalid access recovery token"
)

type Input struct {
	Email string `json:"email"`
}

var RecoveryHandler = handlers.ApplicationHandler(recoveryPassword)

func recoveryPassword(w http.ResponseWriter, r *http.Request) {
	var recoveryPasswordInput Input

	err := httputil.ParseBody(w, r.Body, &recoveryPasswordInput)
	if err != nil {
		return
	}

	defer r.Body.Close()

	app := application.GetApplicationContext(r)

	user, err := app.Repo.User.GetUserByEmail(recoveryPasswordInput.Email)
	if err != nil {
		err := errors.New(noUser)
		w.WriteHeader(http.StatusUnprocessableEntity)
		httputil.WriteErrorMsg(w, err)
		return
	}

	if !user.EmailVerified {
		err := errors.New(emailNotVerified)
		w.WriteHeader(http.StatusNotAcceptable)
		httputil.WriteErrorMsg(w, err)
		return
	}

	err = recoveryPasswordService(app, &user)
	if err != nil {
		log.Error().Err(err).Msg("recoveryPasswordService error")
		w.WriteHeader(http.StatusInternalServerError)
		httputil.WriteErrorMsg(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

var RecoveryFromEmailHandler = handlers.ApplicationHandler(recoveryPasswordFromEmail)

func recoveryPasswordFromEmail(w http.ResponseWriter, r *http.Request) {
	//to prevent built-in Redirect behaviour
	w.Header().Set("Content-Type", "")

	app := application.GetApplicationContext(r)

	httputil.RemoveCookies(w)

	urlParsed, _ := url.Parse(app.Config.LkdPlatformFrontendRecoveryPasswordUrl)
	keys, ok := r.URL.Query()["token"]
	if !ok {
		http.Redirect(w, r, urlParsed.String(), http.StatusFound)
		return
	}

	urlQueryParams := urlParsed.Query()
	urlQueryParams.Set("token", keys[0])
	urlParsed.RawQuery = urlQueryParams.Encode()

	user, accessRecoveryRequest, err := verifyRestoreToken(app, keys[0])
	if err != nil {
		log.Debug().Err(err).Msg(invalidAccessToken)
		http.Redirect(w, r, urlParsed.String(), http.StatusFound)
		return
	}

	app.IAMClient.Logout(user.Email, "")

	err = recoveryPasswordFromEmailService(app.Repo, &user, &accessRecoveryRequest)
	if err != nil {
		http.Redirect(w, r, urlParsed.String(), http.StatusFound)
		return
	}

	http.Redirect(w, r, urlParsed.String(), http.StatusFound)
}

type NewPasswordInput struct {
	Password string `json:"password"`
	Token    string `json:"token"`
}

var NewPasswordHandler = handlers.ApplicationHandler(newPassword)

func newPassword(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var passwordInput NewPasswordInput

	err := httputil.ParseBody(w, r.Body, &passwordInput)
	if err != nil {
		return
	}

	defer r.Body.Close()

	app := application.GetApplicationContext(r)

	user, accessRecoveryRequest, err := verifyRestoreToken(app, passwordInput.Token)
	if err != nil {
		err := errors.New(invalidAccessToken)
		w.WriteHeader(http.StatusUnprocessableEntity)
		httputil.WriteErrorMsg(w, err)
		return
	}

	err = newPasswordService(app.Repo, &user, &accessRecoveryRequest, passwordInput.Password)
	if err != nil {
		log.Error().Err(err).Msg("newPasswordService error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

type VerifyRecoveryTokenInput struct {
	Token string `json:"token"`
}

var VerifyRecoveryTokenHandler = handlers.ApplicationHandler(verifyRecoveryToken)

func verifyRecoveryToken(w http.ResponseWriter, r *http.Request) {
	var verifyRecoveryTokenInput VerifyRecoveryTokenInput

	err := httputil.ParseBody(w, r.Body, &verifyRecoveryTokenInput)
	if err != nil {
		return
	}

	defer r.Body.Close()

	app := application.GetApplicationContext(r)

	_, _, err = verifyRestoreToken(app, verifyRecoveryTokenInput.Token)
	if err != nil {
		log.Debug().Err(err).Msg(invalidAccessToken)
		w.WriteHeader(http.StatusUnprocessableEntity)
		httputil.WriteErrorMsg(w, errors.New(invalidAccessToken))
		return
	}

	w.WriteHeader(http.StatusOK)
}
