package orderPrice

import (
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
)

func getOrderEthPriceService(tokensStr string, tokenPriceStr string, exchangeRateStr string) (*EtherPriceResponse, error) {

	var ethExchangeRateResponse EtherPriceResponse

	tokens, err := decimal.NewFromString(tokensStr)
	if err != nil {
		return nil, err
	}

	tokenPrice, err := decimal.NewFromString(tokenPriceStr)
	if err != nil {
		return nil, err
	}

	exchangeRate, err := decimal.NewFromString(exchangeRateStr)
	if err != nil {
		return nil, err
	}

	if exchangeRate.Equal(decimal.New(0, 0)) {
		return nil, errors.New("dividing by zero")
	}

	if tokens.IsNegative() || tokenPrice.IsNegative() || exchangeRate.IsNegative() {
		return nil, errors.New("invalid data")
	}

	ethExchangeRateResponse.EtherPrice = tokenPrice.Mul(tokens).Div(exchangeRate)

	return &ethExchangeRateResponse, nil
}
