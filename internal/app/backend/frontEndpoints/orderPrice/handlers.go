package orderPrice

import (
	"encoding/json"
	"net/http"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

//EtherPriceResponse is a response for get ether price rate endpoint
type EtherPriceResponse struct {
	EtherPrice decimal.Decimal `json:"ether_price"`
}

//GetOrderEthPriceHandler handling get order eth exchange rate request
var GetOrderEthPriceHandler = handlers.ApplicationHandler(getOrderEthPrice)

func getOrderEthPrice(w http.ResponseWriter, r *http.Request) {

	tokens := r.URL.Query()["tokens"]
	tokenPrice := r.URL.Query()["tokenPrice"]
	exchangeRate := r.URL.Query()["exchangeRate"]

	if len(tokens) != 1 || len(tokenPrice) != 1 || len(exchangeRate) != 1 {
		err := errors.New("invalid input data")
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, err)
		return
	}

	result, err := getOrderEthPriceService(tokens[0], tokenPrice[0], exchangeRate[0])
	if err != nil {
		log.Debug().Err(err).Msg("getOrderEthExchangeRateService error")
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, err)
		return
	}

	respData, _ := json.Marshal(result)

	w.WriteHeader(http.StatusOK)
	w.Write(respData)
}
