package orderPrice_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/app/backend/frontEndpoints/orderPrice"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
	"lkd-platform-backend/pkg/httputil"
)

type FrontEndpointsOrderPriceSuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
	User       *models.User
}

func (suite *FrontEndpointsOrderPriceSuite) SetupTestSuite() {

}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_FrontEndpointsOrderPriceSuite(t *testing.T) {
	suite.Run(t, new(FrontEndpointsOrderPriceSuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *FrontEndpointsOrderPriceSuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()

	priv.Migration(app.Repo.DB)
	suite.App = app
	suite.MockServer = mockServer

	suite.App.Repo.DB.Exec("DELETE FROM users;")

	var user models.User
	user.Email = "romuch@test.com"
	user.EthAddress = "0x040334a07f601350cAFAc8AbE5d9E21968d31521"
	user.Role = "admin"

	suite.App.Repo.User.CreateNewUser(&user)

	suite.User = &user
}

func (suite *FrontEndpointsOrderPriceSuite) AfterTest() {
	suite.App.Repo.DB.Exec("DELETE FROM users;")
}

func (suite *FrontEndpointsOrderPriceSuite) TestCorrectData() {

	u, _ := url.Parse(suite.MockServer.URL + "/admin/order/price")

	q := u.Query()

	q.Set("tokens", "100")
	q.Set("tokenPrice", "200")
	q.Set("exchangeRate", "300")

	u.RawQuery = q.Encode()

	client := &http.Client{}
	req, _ := http.NewRequest("GET", u.String(), nil)
	req.Header.Add("user_email", suite.User.Email)

	resp, _ := client.Do(req)

	suite.Equal(http.StatusOK, resp.StatusCode)

	var ethExchangeRate orderPrice.EtherPriceResponse

	b := httputil.ReadBody(resp.Body)
	defer resp.Body.Close()

	json.Unmarshal(b, &ethExchangeRate)

	suite.Equal(decimal.New(100, 0).Mul(decimal.New(200, 0)).Div(decimal.New(300, 0)).String(), ethExchangeRate.EtherPrice.String())
}

func (suite *FrontEndpointsOrderPriceSuite) TestNoExchangeRateInput() {

	u, _ := url.Parse(suite.MockServer.URL + "/admin/order/price")

	q := u.Query()

	q.Set("tokens", "100")
	q.Set("tokenPrice", "200")

	u.RawQuery = q.Encode()

	client := &http.Client{}
	req, _ := http.NewRequest("GET", u.String(), nil)
	req.Header.Add("user_email", suite.User.Email)

	resp, _ := client.Do(req)

	suite.Equal(http.StatusBadRequest, resp.StatusCode)
}

func (suite *FrontEndpointsOrderPriceSuite) TestNoTokensInput() {
	u, _ := url.Parse(suite.MockServer.URL + "/admin/order/price")

	q := u.Query()

	q.Set("tokenPrice", "200")
	q.Set("exchangeRate", "300")

	u.RawQuery = q.Encode()

	client := &http.Client{}
	req, _ := http.NewRequest("GET", u.String(), nil)
	req.Header.Add("user_email", suite.User.Email)

	resp, _ := client.Do(req)

	suite.Equal(http.StatusBadRequest, resp.StatusCode)
}

func (suite *FrontEndpointsOrderPriceSuite) TestNoTokenPriceInput() {
	u, _ := url.Parse(suite.MockServer.URL + "/admin/order/price")

	q := u.Query()

	q.Set("tokens", "100")
	q.Set("exchangeRate", "300")

	u.RawQuery = q.Encode()

	client := &http.Client{}
	req, _ := http.NewRequest("GET", u.String(), nil)
	req.Header.Add("user_email", suite.User.Email)

	resp, _ := client.Do(req)

	suite.Equal(http.StatusBadRequest, resp.StatusCode)
}

func (suite *FrontEndpointsOrderPriceSuite) TestInvalidData() {
	u, _ := url.Parse(suite.MockServer.URL + "/admin/order/price")

	q := u.Query()

	q.Set("tokens", "-100")
	q.Set("tokenPrice", "200")
	q.Set("exchangeRate", "300")

	u.RawQuery = q.Encode()

	client := &http.Client{}
	req, _ := http.NewRequest("GET", u.String(), nil)
	req.Header.Add("user_email", suite.User.Email)

	resp, _ := client.Do(req)

	suite.Equal(http.StatusBadRequest, resp.StatusCode)
}

func (suite *FrontEndpointsOrderPriceSuite) TestZeroExchangeRate() {
	u, _ := url.Parse(suite.MockServer.URL + "/admin/order/price")

	q := u.Query()

	q.Set("tokens", "100")
	q.Set("tokenPrice", "200")
	q.Set("exchangeRate", "0")

	u.RawQuery = q.Encode()

	client := &http.Client{}
	req, _ := http.NewRequest("GET", u.String(), nil)
	req.Header.Add("user_email", suite.User.Email)

	resp, _ := client.Do(req)

	suite.Equal(http.StatusBadRequest, resp.StatusCode)
}
