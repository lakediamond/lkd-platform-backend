package changePassword

import (
	"net/http"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

type InputData struct {
	OldPassword   string `json:"old_password"`
	NewPassword   string `json:"new_password"`
	Google2FaPass string `json:"google_2_fa_pass"`
}

//ChangePasswordHandler handling change password request
var ChangePasswordHandler = handlers.ApplicationHandler(changePassword)

func changePassword(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)

	var passwordInputData InputData
	err := httputil.ParseBody(w, r.Body, &passwordInputData)
	if err != nil {
		return
	}

	defer r.Body.Close()

	user := application.GetUser(r)

	err = changePasswordService(app.Repo, &user, &passwordInputData)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		httputil.WriteErrorMsg(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}
