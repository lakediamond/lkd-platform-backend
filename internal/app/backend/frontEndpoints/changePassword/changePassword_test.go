package changePassword_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/app/backend/frontEndpoints/changePassword"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/crypto"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
)

type FrontEndpointsChangePasswordSuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
}

func (suite *FrontEndpointsChangePasswordSuite) SetupTestSuite() {

}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_FrontEndpointsChangePasswordSuite(t *testing.T) {
	suite.Run(t, new(FrontEndpointsChangePasswordSuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *FrontEndpointsChangePasswordSuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()

	priv.Migration(app.Repo.DB)
	suite.App = app
	suite.MockServer = mockServer

	suite.App.Repo.DB.Exec("DELETE FROM users;")
}

func (suite *FrontEndpointsChangePasswordSuite) AfterTest() {
	suite.App.Repo.DB.Exec("DELETE FROM users;")
}

func (suite *FrontEndpointsChangePasswordSuite) TestInvalidOldPassword() {

	var user models.User
	user.Email = "romuch@test.com"
	user.EthAddress = "0x040334a07f601350cAFAc8AbE5d9E21968d31521"
	user.Role = "admin"
	password := "1QAZ2WSX3edc"
	user.Password = crypto.EncodePass(password)

	suite.App.Repo.User.CreateNewUser(&user)

	var changePasswordInputData changePassword.ChangePasswordInputData
	changePasswordInputData.OldPassword = "1QAZ2WSX3edc1"
	changePasswordInputData.NewPassword = "1QAZ2WSX3edc1f12"

	b, _ := json.Marshal(changePasswordInputData)

	client := &http.Client{}
	req, _ := http.NewRequest("POST", suite.MockServer.URL+"/user/change_password", bytes.NewReader(b))

	req.Header.Add("user_email", user.Email)
	resp, err := client.Do(req)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	suite.Equal(http.StatusForbidden, resp.StatusCode)
}

func (suite *FrontEndpointsChangePasswordSuite) TestInvalidNewPassword() {

	var user models.User
	user.Email = "romuch@test.com"
	user.EthAddress = "0x040334a07f601350cAFAc8AbE5d9E21968d31521"
	user.Role = "admin"
	password := "1QAZ2WSX3edc"
	user.Password = crypto.EncodePass(password)

	suite.App.Repo.User.CreateNewUser(&user)

	var changePasswordInputData changePassword.ChangePasswordInputData
	changePasswordInputData.OldPassword = password
	changePasswordInputData.NewPassword = "1QAZ"

	b, _ := json.Marshal(changePasswordInputData)

	client := &http.Client{}
	req, _ := http.NewRequest("POST", suite.MockServer.URL+"/user/change_password", bytes.NewReader(b))

	req.Header.Add("user_email", user.Email)
	resp, err := client.Do(req)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	suite.Equal(http.StatusForbidden, resp.StatusCode)
}

func (suite *FrontEndpointsChangePasswordSuite) TestValidChange() {

	var user models.User
	user.Email = "romuch@test.com"
	user.EthAddress = "0x040334a07f601350cAFAc8AbE5d9E21968d31521"
	user.Role = "admin"
	password := "1QAZ2WSX3edc"
	user.Password = crypto.EncodePass(password)

	suite.App.Repo.User.CreateNewUser(&user)

	var changePasswordInputData changePassword.ChangePasswordInputData
	changePasswordInputData.OldPassword = password
	changePasswordInputData.NewPassword = "1QAZ2WSX3edc1f12"

	b, _ := json.Marshal(changePasswordInputData)

	client := &http.Client{}
	req, _ := http.NewRequest("POST", suite.MockServer.URL+"/user/change_password", bytes.NewReader(b))

	req.Header.Add("user_email", user.Email)
	resp, err := client.Do(req)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	suite.Equal(http.StatusOK, resp.StatusCode)
}
