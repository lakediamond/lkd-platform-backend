package changePassword

import (
	"github.com/pkg/errors"

	"lkd-platform-backend/internal/app/backend/google2fa"
	"lkd-platform-backend/internal/pkg/crypto"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func changePasswordService(repo *repo.Repo, user *models.User, inputData *InputData) error {

	if !crypto.Verify(inputData.OldPassword, user.Password) {
		return errors.New("invalid old password")
	}

	if user.Google2FAEnabled && !google2fa.Check2FA(user, inputData.Google2FaPass) {
		return errors.New("invalid 2FA code")
	}

	newPass := crypto.EncodePass(inputData.NewPassword)
	user.Password = newPass

	err := repo.User.UpdateUser(user)
	if err != nil {
		return err
	}

	return nil
}
