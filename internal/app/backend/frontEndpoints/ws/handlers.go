package ws

import (
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
	"net/http"
)

var HandlerWSServer = handlers.ApplicationHandler(getHandlerWSServer)

func getHandlerWSServer(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)
	app.WSClient.ServeHTTP(w, r)
}
