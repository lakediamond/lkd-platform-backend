package oauth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
	"net/http"
	"strconv"
	"strings"

	"github.com/julienschmidt/httprouter"
	"github.com/ory/hydra/sdk/go/hydra/swagger"
	"github.com/ory/x/cmdx"
	"github.com/ory/x/randx"
	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"
	"golang.org/x/oauth2"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/oauth"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

const (
	errTokenRefresh            = "failed to refresh access token"
	errInvalidFlow             = "invalid flow type provided, only Code flow is enabled"
	errNoHeader                = "login_challenge URL query parameter not found or empty"
	errInvalidConsent          = "invalid consent outcome detected, one of (approve|decline) expected"
	errInvalidRequest          = "invalid OAuth2 request type provided, one of (login|consent) expected"
	errHeaderNotFound          = "oauth_scope header not found"
	errReadScopes              = "failed to read scopes meta info from database"
	errInvalidStateToken       = "state token received doesn't match one defined"
	errInvalidAuthCode         = "invalid auth code provided"
	errIntrospect              = "failed to get token details"
	errNoTokenAtSessionCookie  = "failed to get token from session cookie"
	errTokenPresentButInactive = "OAuth token found but reported as inactive or failed at refresh call to Hydra"
)

//StartOAuthCodeFlow returns OAuth login request details
var StartOAuthFlow = handlers.ApplicationHandler(startOAuthFlow)

func startOAuthFlow(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	app := application.GetApplicationContext(r)
	iamClient := app.IAMClient
	routeParams := httprouter.ParamsFromContext(r.Context())

	span := tracer.StartSpan("web.request", tracer.ResourceName(r.RequestURI))
	defer span.Finish()

	w.Header().Set("Spanid", strconv.FormatUint(uint64(span.Context().SpanID()), 10))

	oauthToken, err := application.GetOAuthToken(r)
	if err != nil {
		log.Debug().Err(err).Msg(err.Error())
	}

	err = application.UpdateSessionWithOAuthToken(w, r, oauthToken)
	if err != nil {
		log.Debug().Err(err).Msg(err.Error())
	}

	if oauthToken.Valid() {
		w.Header().Set("Location", fmt.Sprintf("%s/profile/overview", iamClient.GetClientRedirectURL()))
		w.WriteHeader(http.StatusOK)
		return
	}

	log.Info().Msg("No session cookie found, starting new Auth Code flow")
	nonce, err := randx.RuneSequence(24, randx.AlphaLower)
	cmdx.Must(err, "Could not generate random state: %s", err)

	if routeParams.ByName("flow") != "code" {
		w.WriteHeader(http.StatusForbidden)
		httputil.WriteErrorMsg(w, errors.New(errInvalidFlow))
		return
	}

	var authURL = iamClient.OAuth2Config().AuthCodeURL(
		iamClient.GetStateToken(),
		oauth2.SetAuthURLParam("audience", strings.Join(app.IAMClient.GetAuthTokenAudience(), "+")),
		oauth2.SetAuthURLParam("nonce", string(nonce)),
		oauth2.SetAuthURLParam("prompt", strings.Join(make([]string, 0), "+")),
		oauth2.SetAuthURLParam("max_age", strconv.Itoa(0)),
		oauth2.SetAuthURLParam("scopes", strings.Join(iamClient.OAuth2Config().Scopes, ",")),
	)
	log.Info().Msgf("Auth flow URL generated - %s", authURL)
	w.Header().Set("Location", authURL)
	w.WriteHeader(http.StatusOK)
}

func isOAuthFlowRestartRequired(err error) bool {
	if strings.Contains(err.Error(), "[recovery]") {
		return true
	}
	return false
}

//GetOAuthLoginRequest returns OAuth login request details
var GetOAuthLoginRequest = handlers.ApplicationHandler(getOAuthLoginRequest)

func getOAuthLoginRequest(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	routeParams := httprouter.ParamsFromContext(r.Context())
	queryValues := r.URL.Query()
	app := application.GetApplicationContext(r)
	referer := r.Header.Get("referer")

	switch routeParams.ByName("type") {
	case "login":
		challengeCode := queryValues.Get("login_challenge")

		if challengeCode == "" {
			err := errors.New(errNoHeader)
			log.Debug().Err(err).Msg(errNoHeader)
			w.WriteHeader(http.StatusInternalServerError)
			httputil.WriteErrorMsg(w, err)
			return
		}

		loginRequest, _, err := app.IAMClient.GetOAuthClient().AdminApi.GetLoginRequest(challengeCode)
		if err != nil {
			log.Debug().Err(err).Msg(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		log.Print(fmt.Sprintf("referer header value - %s", referer))
		if strings.HasPrefix(loginRequest.Client.ClientId, models.OAuthICOAdminClient) && len(queryValues.Get("email_verified")) == 0 {
			log.Debug().Msg("Auth flow initiated by ICO Admin detected. Checking for sign-up flow")

			if len(loginRequest.RequestedAccessTokenAudience) >= 1 {
				if loginRequest.RequestedAccessTokenAudience[0] == "sign-up" {
					w.Header().Set("Location", fmt.Sprintf("%s/register?login_challenge=%s", app.IAMClient.GetClientRedirectURL(), loginRequest.Challenge))
					w.WriteHeader(http.StatusOK)
					return
				}
			}
		}

		var completedRequest *swagger.CompletedRequest
		if loginRequest.Skip {
			completedRequest, _, err = app.IAMClient.GetOAuthClient().AdminApi.AcceptLoginRequest(loginRequest.Challenge, swagger.AcceptLoginRequest{
				Subject:     loginRequest.Subject,
				Remember:    !loginRequest.Skip,
				RememberFor: app.IAMClient.GetLoginRememberFor(),
			})

			if err != nil {
				log.Debug().Err(err).Msg(err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			w.Header().Set("Location", completedRequest.RedirectTo)
			w.WriteHeader(http.StatusOK)
			return
		}

		w.WriteHeader(http.StatusNoContent)
		return
	case "consent":
		challengeCode := queryValues.Get("consent_challenge")

		if challengeCode == "" {
			err := errors.New(errNoHeader)
			log.Debug().Err(err).Msg(errNoHeader)
			w.WriteHeader(http.StatusInternalServerError)
			httputil.WriteErrorMsg(w, err)
			return
		}

		consentRequest, _, err := app.IAMClient.GetOAuthClient().AdminApi.GetConsentRequest(challengeCode)
		if err != nil {
			log.Debug().Err(err).Msg(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		if consentRequest.Skip {
			completedRequest, _, err := app.IAMClient.GetOAuthClient().AdminApi.AcceptConsentRequest(challengeCode, swagger.AcceptConsentRequest{
				GrantScope: consentRequest.RequestedScope,
				Remember:   false,
			})
			if err != nil {
				log.Debug().Err(err).Msg(err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			w.Header().Set("Location", completedRequest.RedirectTo)
			w.WriteHeader(http.StatusOK)
			return
		}

		w.WriteHeader(http.StatusNoContent)
		return
	default:
		err := errors.New(errInvalidRequest)
		log.Info().Err(err).Msg(errInvalidRequest)
		w.WriteHeader(http.StatusForbidden)
		httputil.WriteErrorMsg(w, err)
		return
	}
}

//ProcessUserConsent handles consent options made by user
var ProcessUserConsent = handlers.ApplicationHandler(processUserConsent)

func processUserConsent(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	app := application.GetApplicationContext(r)

	var userConsent oauth.UserConsent

	err := httputil.ParseBody(w, r.Body, &userConsent)
	if err != nil {
		return
	}

	defer r.Body.Close()

	consentRequest, _, err := app.IAMClient.GetOAuthClient().AdminApi.GetConsentRequest(userConsent.Challenge)
	if err != nil {
		log.Debug().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	completedRequest, _, err := app.IAMClient.GetOAuthClient().AdminApi.AcceptConsentRequest(userConsent.Challenge, swagger.AcceptConsentRequest{
		GrantScope:  consentRequest.RequestedScope,
		Remember:    !consentRequest.Skip,
		RememberFor: app.IAMClient.GetConsentRememberFor(),
	})

	if err != nil {
		err := errors.New(errInvalidConsent)
		log.Debug().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusForbidden)
		httputil.WriteErrorMsg(w, err)
		return
	}

	w.Header().Set("Location", completedRequest.RedirectTo)
	return

}

//ProcessOAuthCallback handles final callback from oauth server to exchange auth code for access token
var ProcessOAuthCallback = handlers.ApplicationHandler(processOAuthCallback)

func processOAuthCallback(w http.ResponseWriter, r *http.Request) {
	queryValues := r.URL.Query()
	authCode := queryValues.Get("code")
	authStateToken := queryValues.Get("state")
	app := application.GetApplicationContext(r)

	iamClient := app.IAMClient

	if iamClient.GetStateToken() != authStateToken {
		err := errors.New(errInvalidStateToken)
		log.Debug().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusForbidden)
		httputil.WriteErrorMsg(w, err)
		return
	}

	if authCode == "" {
		err := errors.New(errInvalidAuthCode)
		log.Debug().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusForbidden)
		httputil.WriteErrorMsg(w, err)
		return
	}

	oauthToken, err := iamClient.OAuth2Config().Exchange(context.Background(), authCode)
	if err != nil {
		log.Debug().Err(err).Msg(err.Error())
		w.WriteHeader(http.StatusForbidden)
		httputil.WriteErrorMsg(w, err)
		return
	}

	tokenDetails, _, introspectErr := app.IAMClient.GetOAuthClient().AdminApi.IntrospectOAuth2Token(oauthToken.AccessToken, "")
	if introspectErr != nil {
		err := errors.New(errIntrospect)
		log.Debug().Err(err).Msg(errIntrospect)
		w.WriteHeader(http.StatusForbidden)
		httputil.WriteErrorMsg(w, err)
		return
	}

	if tokenDetails.Active {
		if err = application.UpdateSessionWithOAuthToken(w, r, oauthToken); err != nil {
			log.Debug().Err(err).Msg(err.Error())
			w.WriteHeader(http.StatusForbidden)
			httputil.WriteErrorMsg(w, err)
			return
		}

		var user models.User
		if user, err = app.Repo.User.GetUserByEmail(tokenDetails.Sub); err != nil {
			log.Debug().Err(err).Msg(err.Error())
			w.WriteHeader(http.StatusForbidden)
			httputil.WriteErrorMsg(w, fmt.Errorf("failed to get session from context %s", err.Error()))
			return
		}

		serializedOAuthToken, err := json.Marshal(oauthToken)
		if err != nil {
			log.Debug().Err(err).Msg("can`t serialize oauth token")
			w.WriteHeader(http.StatusInternalServerError)
			httputil.WriteErrorMsg(w, fmt.Errorf("can`t serialize oauth token"))
			return
		}

		connectionId := uuid.NewV4()
		authTokenRecord := &models.AuthToken{
			ID:              &connectionId,
			UserId:          user.ID,
			SerializedToken: string(serializedOAuthToken),
			Description:     fmt.Sprintf("User agent: %s, ip: %s", r.UserAgent(), r.RemoteAddr),
		}
		if err = app.Repo.AuthToken.Create(authTokenRecord); err != nil {
			log.Debug().Err(err).Msg("error while saving auth token in db")
			w.WriteHeader(http.StatusInternalServerError)
			httputil.WriteErrorMsg(w, fmt.Errorf("error while saving auth token in db"))
			return
		}

		if err = application.SetSessionConnectionId(w, r, connectionId.String()); err != nil {
			log.Debug().Err(err).Msg(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			httputil.WriteErrorMsg(w, err)
			return
		}

		log.Debug().Msgf("Session updated with token %s", oauthToken)
	}

	w.Header().Set("Location", fmt.Sprintf("%s/profile/overview", iamClient.GetClientRedirectURL()))
	w.WriteHeader(http.StatusOK)
}
