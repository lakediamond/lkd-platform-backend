package orders

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"lkd-platform-backend/internal/pkg/pagination"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/internal/pkg/repo/response"
	"time"

	"github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/repo"
)

// TO DO deprecated
func getOrders(repo *repo.Repo, user models.User) ([]OrderResponse, error) {
	var orderResponses []OrderResponse

	orders, err := repo.Order.GetOrdersDetails()
	if err != nil {
		return orderResponses, err
	}

	orderResponses = combineOrderResponse(orders, user)
	return orderResponses, nil
}

func getOrdersPagination(repo *repo.Repo, user models.User, filters interface{}, orderBy map[string]string, offset int, limit int) (pagination.Paginator, error) {

	var ordersResponsesResponses []response.OrderDetailsResponse
	var orderResponses []OrderResponse
	var ordersPagination pagination.Paginator

	ordersPagination = repo.Order.GetOrdersDetailsPagination(filters, orderBy, offset, limit)
	sliceInterface := ordersPagination.ConvertingToSlice()
	for _, item := range sliceInterface {
		val, ok := item.(response.OrderDetailsResponseDB)
		if ok {
			var orderResponsesResponse response.OrderDetailsResponse
			var investments []response.OrderDetailsInvestmentsResponse

			if val.Matches != nil {
				err := json.Unmarshal([]byte(val.Matches), &investments)
				if err != nil {
					log.Error().Err(err).Msg("getOrdersPagination::Matches")
				}
			}

			orderResponsesResponse.ID = val.ID
			orderResponsesResponse.TokenAmount = val.TokenAmount
			orderResponsesResponse.CreatedBy = val.CreatedBy
			orderResponsesResponse.CreatedOn = val.CreatedOn
			orderResponsesResponse.Type = val.Type
			orderResponsesResponse.Metadata = val.Metadata
			orderResponsesResponse.TokenPrice = val.TokenPrice
			orderResponsesResponse.EthAmount = val.EthAmount
			orderResponsesResponse.FilledRate = val.FilledRate
			orderResponsesResponse.TxHash = val.TxHash
			orderResponsesResponse.Investments = investments

			ordersResponsesResponses = append(ordersResponsesResponses, orderResponsesResponse)
		}
	}

	orderResponses = combineOrderDetailsResponse(ordersResponsesResponses, user)
	ordersPagination.Records = orderResponses

	return ordersPagination, nil
}

func getOrdersCumulativeData(repo *repo.Repo, daysToLoad int) ([]repo.OrdersCumulativeDataDB, error) {
	return repo.Order.GetOrdersCumulativeData(daysToLoad)
}

func combineOrderDetailsResponse(ordersResponsesResponses []response.OrderDetailsResponse, user models.User) []OrderResponse {
	orderResponses := make([]OrderResponse, 0)

	for _, ordersResponsesResponse := range ordersResponsesResponses {
		var orderResponse OrderResponse

		orderResponse.ID = ordersResponsesResponse.ID
		orderResponse.CreatedOn = ordersResponsesResponse.CreatedOn
		orderResponse.CreatedBy = ordersResponsesResponse.CreatedBy
		orderResponse.Type = ordersResponsesResponse.Type
		orderResponse.Metadata = ordersResponsesResponse.Metadata
		orderResponse.TokenPrice = ordersResponsesResponse.TokenPrice
		orderResponse.TokensAmount = ordersResponsesResponse.TokenAmount
		orderResponse.EthAmount = ordersResponsesResponse.EthAmount
		orderResponse.FilledRate = ordersResponsesResponse.FilledRate
		orderResponse.TxHash = ordersResponsesResponse.TxHash

		for _, orderInvestments := range ordersResponsesResponse.Investments {
			var investment Investment

			if orderInvestments.TxHash != "" {
				investment.ID = orderInvestments.InvestmentID
				investment.ProposalID = orderInvestments.ProposalID
				investment.TxHash = orderInvestments.TxHash
				investment.TokensInvested = orderInvestments.TokensInvested
				investment.EthReceived = orderInvestments.EthReceived
				investment.Price = orderInvestments.Price
				investment.CreatedOn = orderInvestments.CreatedOn
				if orderInvestments.EthAccount != "" {
					if user.Role == models.AdminRole {
						investment.FirstName = orderInvestments.FirstName
						investment.LastName = orderInvestments.LastName
						investment.Email = orderInvestments.Email
					} else if user.Role != models.AdminRole && user.EthAddress == orderInvestments.EthAccount {
						investment.FirstName = orderInvestments.FirstName
						investment.LastName = orderInvestments.LastName
						investment.Email = orderInvestments.Email
					}

					investment.EthAccount = orderInvestments.EthAccount
				}
			}

			if orderInvestments.TxHash != "" {
				orderResponse.Investments = append(orderResponse.Investments, investment)
			}
		}

		orderResponses = append(orderResponses, orderResponse)
	}

	return orderResponses
}

// TO DO deprecated
func combineOrderResponse(orderResponsesDB []response.OrderResponseDB, user models.User) []OrderResponse {

	var orderResponses []OrderResponse

	for _, orderResponseDB := range orderResponsesDB {

		var orderResponse OrderResponse
		var existsID = -1

		for i, orderResponseIter := range orderResponses {
			if orderResponseIter.ID == orderResponseDB.ID {
				orderResponse = orderResponseIter
				existsID = i
				break
			}
		}

		//if not set yet
		if orderResponse.Metadata == "" {
			orderResponse.ID = orderResponseDB.ID
			orderResponse.CreatedOn = orderResponseDB.CreatedOn
			orderResponse.CreatedBy = orderResponseDB.CreatedBy
			orderResponse.Type = orderResponseDB.Type
			orderResponse.Metadata = orderResponseDB.Metadata
			orderResponse.TokenPrice = orderResponseDB.TokenPrice
			orderResponse.TokensAmount = orderResponseDB.TokenAmount
			orderResponse.EthAmount = orderResponseDB.EthAmount
			orderResponse.FilledRate = orderResponseDB.FilledRate
		}

		var investment Investment

		if orderResponseDB.TxHash != "" {
			investment.ID = orderResponseDB.InvestmentID
			investment.TxHash = orderResponseDB.TxHash
			investment.TokensInvested = orderResponseDB.TokensInvested
			investment.EthReceived = orderResponseDB.EthReceived
			if orderResponseDB.EthAccount != "" {
				if user.Role == models.AdminRole {
					investment.FirstName = orderResponseDB.FirstName
					investment.LastName = orderResponseDB.LastName
					investment.Email = orderResponseDB.Email
				} else if user.Role != models.AdminRole && user.EthAddress == orderResponseDB.EthAccount {
					investment.FirstName = orderResponseDB.FirstName
					investment.LastName = orderResponseDB.LastName
					investment.Email = orderResponseDB.Email
				}

				investment.EthAccount = orderResponseDB.EthAccount
			}
		}

		if orderResponseDB.TxHash != "" {
			orderResponse.Investments = append(orderResponse.Investments, investment)
		}

		if existsID == -1 {
			orderResponses = append(orderResponses, orderResponse)
		} else {
			orderResponses[existsID] = orderResponse
		}

	}

	return orderResponses
}

//OrderResponse contains response to getOrders request
type OrderResponse struct {
	ID           uint            `json:"id"`
	CreatedBy    string          `json:"created_by"`
	CreatedOn    time.Time       `json:"created_on"`
	Type         string          `json:"type"`
	Metadata     string          `json:"metadata"`
	TokenPrice   decimal.Decimal `json:"token_price"`
	TokensAmount decimal.Decimal `json:"tokens_amount"`
	EthAmount    decimal.Decimal `json:"eth_amount"`
	FilledRate   decimal.Decimal `json:"filled_rate"`
	TxHash       string          `json:"tx_hash"`
	Investments  []Investment    `json:"investments"`
}

//Investment contains information about investor
type Investment struct {
	ID             *uuid.UUID      `json:"id"`
	ProposalID     int             `json:"proposal_id,omitempty"`
	TxHash         string          `json:"tx_hash"`
	TokensInvested decimal.Decimal `json:"tokens_invested"`
	EthReceived    decimal.Decimal `json:"eth_received"`
	FirstName      string          `json:"first_name"`
	LastName       string          `json:"last_name"`
	Email          string          `json:"email"`
	EthAccount     string          `json:"eth_account"`
	Price          decimal.Decimal `json:"price"`
	CreatedOn      string          `json:"created_on"`
}
