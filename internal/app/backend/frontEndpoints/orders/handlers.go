package orders

import (
	"encoding/json"
	"fmt"
	"lkd-platform-backend/internal/pkg/pagination"
	"net/http"
	"strconv"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
	"lkd-platform-backend/pkg/httputil"
)

//GetAllOrdersHandler handling get all orders request
var GetPaginationAllOrdersHandler = handlers.ApplicationHandler(getPaginationAllOrders)

func getPaginationAllOrders(w http.ResponseWriter, r *http.Request) {
	var offset int
	var limit int
	var field string
	var sort string

	app := application.GetApplicationContext(r)
	currentUser := application.GetUser(r)

	queryValues := r.URL.Query()
	offsetQuery := queryValues.Get("page")
	if offsetQuery == "" {
		offset = pagination.DEFAULT_PAGING_OFFSET
	} else {
		offset, _ = strconv.Atoi(offsetQuery)
	}

	limitQuery := queryValues.Get("limit")
	if limitQuery == "" {
		limit = pagination.DEFAULT_PAGING_LIMIT
	} else {
		limit, _ = strconv.Atoi(limitQuery)
	}

	orderBy := make(map[string]string, 0)

	field = queryValues.Get("field")
	if field == "" {
		field = pagination.DEFAULT_PAGING_FIELD
	}

	sort = queryValues.Get("sort")
	orderBy[field] = pagination.SortingType(sort)

	// init filters
	var filterOrderDetails pagination.FilterOrderDetails
	filters, err := pagination.FilterParseQuery(&filterOrderDetails, r.URL.Query())
	if err != nil {
		log.Debug().Err(err).Msgf("FilterParseQuery error")
	}

	orders, err := getOrdersPagination(app.Repo, currentUser, filters, orderBy, offset, limit)
	if err != nil {
		log.Debug().Err(err).Msg("Get all orders error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	respData, _ := json.Marshal(orders)

	w.WriteHeader(http.StatusOK)
	w.Write(respData)
}

// TO DO deprecated
//GetAllOrdersHandler handling get all orders request
var GetAllOrdersHandler = handlers.ApplicationHandler(getAllOrders)

// TO DO deprecated
func getAllOrders(w http.ResponseWriter, r *http.Request) {
	app := application.GetApplicationContext(r)
	currentUser := application.GetUser(r)

	orders, err := getOrders(app.Repo, currentUser)
	if err != nil {
		log.Debug().Err(err).Msg("Get all orders error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	respData, _ := json.Marshal(orders)

	w.WriteHeader(http.StatusOK)
	w.Write(respData)
}

//GetAllOrdersHandler handling get all orders request
var GetOrdersCumulativeDataHandler = handlers.ApplicationHandler(getOrdersCumulativeDataByDays)

func getOrdersCumulativeDataByDays(w http.ResponseWriter, r *http.Request) {
	const daysToLoadValueKey = "daysToLoad"
	daysToLoadValue, ok := r.URL.Query()["daysToLoad"]

	if !ok || len(daysToLoadValue[0]) < 1 {
		errorMessage := fmt.Errorf("url Param %s is missing", daysToLoadValueKey)
		log.Info().Msg(errorMessage.Error())
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	app := application.GetApplicationContext(r)

	daysToLoad, err := strconv.Atoi(daysToLoadValue[0])

	if err != nil {
		errorMessage := fmt.Errorf("url Param %s must be a number", daysToLoadValueKey)
		log.Info().Err(err).Msg(errorMessage.Error())
		w.WriteHeader(http.StatusBadRequest)
		httputil.WriteErrorMsg(w, errorMessage)
		return
	}

	ordersCumulativeData, err := getOrdersCumulativeData(app.Repo, daysToLoad)
	if err != nil {
		log.Error().Err(err).Msg("Get orders cumulative data error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	respData, _ := json.Marshal(ordersCumulativeData)
	w.WriteHeader(http.StatusOK)
	w.Write(respData)
}
