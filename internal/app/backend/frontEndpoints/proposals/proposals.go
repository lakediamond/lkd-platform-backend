package proposals

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"
	"lkd-platform-backend/internal/pkg/repo/response"
	"math/big"
	"time"

	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"

	"lkd-platform-backend/internal/pkg/pagination"
)

type OwnerListResponse struct {
	Owners []string `json:"owners"`
}

type ProposalResponse struct {
	ID                    uint            `json:"id"`
	TokensAmount          *big.Int        `json:"remaining_tokens_amount"`
	CreatedOn             time.Time       `json:"created_on"`
	Price                 *big.Int        `json:"price"`
	WithdrawnOn           *time.Time      `json:"withdrawn_on"`
	WithdrawnTokensAmount *big.Int        `json:"withdrawn_tokens_amount"`
	UpfrontQueueVolume    decimal.Decimal `json:"upfront_queue_volume,omitempty"`
	OwnerAddress          string          `json:"owner_address"`
	Status                models.Status   `json:"status"`
	OriginalTokens        decimal.Decimal `json:"original_tokens_amount"`
	TxHash                string          `json:"tx_hash"`
	WithdrawTxHash        string          `json:"cancellation_tx_hash"`

	Investments []response.ProposalInvestmentsResponse `json:"investments,omitempty"`
}

func getOwners(repo repo.ProposalRepository) (OwnerListResponse, error) {

	var owners OwnerListResponse

	proposals, err := repo.GetOwnerProposal()
	if err != nil {
		return owners, err
	}

	owners.Owners = make([]string, 0)
	for _, proposal := range proposals {
		owners.Owners = append(owners.Owners, proposal.CreatedBy)
	}

	return owners, nil
}

func getProposals(repo repo.ProposalRepository) ([]ProposalResponse, error) {

	var proposalResponses []ProposalResponse

	proposals, err := repo.GetAllProposals()
	if err != nil {
		return proposalResponses, err
	}

	proposalResponses, err = combineProposalResponse(proposals)
	if err != nil {
		return proposalResponses, err
	}

	return proposalResponses, nil
}

func getUserProposals(repo repo.ProposalRepository, filters interface{}, orderBy map[string]string, offset int, limit int) (pagination.Paginator, error) {

	proposalsResponse := make([]ProposalResponse, 0)
	var proposalPagination pagination.Paginator
	proposalPagination = repo.GetAllUserProposalsPagination(filters, orderBy, offset, limit)

	sliceInterface := proposalPagination.ConvertingToSlice()
	for _, item := range sliceInterface {
		val, ok := item.(response.ProposalResponseDB)
		if ok {
			var proposalResponse ProposalResponse
			var investments []response.ProposalInvestmentsResponse

			if val.Matches != nil {
				err := json.Unmarshal([]byte(val.Matches), &investments)
				if err != nil {
					log.Error().Err(err).Msg("getUserProposals::Matches")
				}
			}

			proposalResponse.ID = val.BlockchainID
			proposalResponse.TokensAmount = val.Amount.Coefficient()
			proposalResponse.CreatedOn = val.CreatedOn
			proposalResponse.WithdrawnOn = val.WithdrawnOn
			proposalResponse.WithdrawnTokensAmount = val.WithdrawnAmount.Coefficient()
			proposalResponse.OwnerAddress = val.CreatedBy
			proposalResponse.Price = val.Price.Coefficient()
			proposalResponse.UpfrontQueueVolume = val.UpfrontQueueVolume
			proposalResponse.Status = val.Status
			proposalResponse.TxHash = val.TxHash
			proposalResponse.OriginalTokens = val.OriginalTokens
			proposalResponse.WithdrawTxHash = val.WithdrawalTxHash
			proposalResponse.Investments = investments

			proposalsResponse = append(proposalsResponse, proposalResponse)
		}
	}

	proposalPagination.Records = proposalsResponse

	return proposalPagination, nil
}

func getProposalPositionHint(repo repo.ProposalRepository, price *big.Int) (uint, error) {
	return repo.GetPositionHint(price)
}

func combineProposalResponse(proposals []models.Proposal) ([]ProposalResponse, error) {
	var proposalResponses []ProposalResponse

	for _, proposal := range proposals {
		proposalResponse := ProposalResponse{
			ID:                    proposal.BlockchainID,
			TokensAmount:          proposal.Amount.Coefficient(),
			CreatedOn:             proposal.CreatedOn,
			WithdrawnOn:           proposal.WithdrawnOn,
			WithdrawnTokensAmount: proposal.WithdrawnAmount.Coefficient(),
			OwnerAddress:          proposal.CreatedBy,
			Price:                 proposal.Price.Coefficient(),
			UpfrontQueueVolume:    proposal.UpfrontQueueVolume,
		}
		proposalResponses = append(proposalResponses, proposalResponse)
	}

	return proposalResponses, nil
}
