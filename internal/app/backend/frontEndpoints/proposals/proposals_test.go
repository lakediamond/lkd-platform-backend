package proposals_test

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/rs/zerolog/log"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
)

type FrontEndpointsProposalsSuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
}

func (suite *FrontEndpointsProposalsSuite) SetupTestSuite() {

}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_FrontEndpointsSuite(t *testing.T) {
	suite.Run(t, new(FrontEndpointsProposalsSuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *FrontEndpointsProposalsSuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()

	priv.Migration(app.Repo.DB)
	suite.App = app
	suite.MockServer = mockServer

	suite.App.Repo.DB.Exec("DELETE FROM users;")
	suite.App.Repo.DB.Exec("DELETE FROM order_types;")
	suite.App.Repo.DB.Exec("DELETE FROM proposals;")
	suite.App.Repo.DB.Exec("DELETE FROM orders;")
	suite.App.Repo.DB.Exec("DELETE FROM sub_owners;")
}

func (suite *FrontEndpointsProposalsSuite) AfterTest() {
	suite.App.Repo.DB.Exec("DELETE FROM users;")
	suite.App.Repo.DB.Exec("DELETE FROM order_types;")
	suite.App.Repo.DB.Exec("DELETE FROM proposals;")
	suite.App.Repo.DB.Exec("DELETE FROM orders;")
	suite.App.Repo.DB.Exec("DELETE FROM sub_owners;")
}

func (suite *FrontEndpointsProposalsSuite) TestGetAllProposals() {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", suite.MockServer.URL+"/user/proposal", nil)

	resp, _ := client.Do(req)
	suite.Equal(http.StatusUnauthorized, resp.StatusCode)

	//create new user
	currencyEventPayload := map[string]interface{}{
		"first_name": "roman",
		"last_name":  "holovay",
		"email":      "romuch@test.com",
		"password":   "32jignui#u3U8BYW3",
		"hash":       "0xaeB0920be125eB72e071B1357A5c95B52D8afc12",
	}
	tmp, _ := json.Marshal(currencyEventPayload)
	requestData := bytes.NewReader(tmp)
	resp, _ = http.Post(suite.MockServer.URL+suite.App.FindRoute("register_account"), "application/json", requestData)
	suite.Equal(http.StatusCreated, resp.StatusCode, "Response")

	user, err := suite.App.Repo.User.GetUserByEmail("romuch@test.com")
	if err != nil {
		log.Fatal().Msg(err.Error())
	}
	user.EmailVerified = true
	suite.App.Repo.User.UpdateUser(&user)

	//authorizing
	currencyEventPayload = map[string]interface{}{
		"email":    "Romuch@test.com",
		"password": "32jignui#u3U8BYW3",
	}
	tmp, _ = json.Marshal(currencyEventPayload)
	requestData = bytes.NewReader(tmp)
	resp, _ = http.Post(suite.MockServer.URL+suite.App.FindRoute("authenticate_account"), "application/json", requestData)
	suite.Equal(http.StatusOK, resp.StatusCode, "Response")

	tokenString := resp.Header.Get("auth_token")

	user.Role = models.AdminRole
	suite.App.Repo.User.UpdateUser(&user)

	client = &http.Client{}
	req, _ = http.NewRequest("GET", suite.MockServer.URL+"/user/proposal", nil)
	req.Header.Add("Authorization", "bearer "+tokenString)

	resp, _ = client.Do(req)
	suite.Equal(http.StatusOK, resp.StatusCode)
	r, _ := ioutil.ReadAll(resp.Body)

	var proposals []models.Proposal

	json.Unmarshal(r, &proposals)

	suite.Equal(0, len(proposals))

	var proposal models.Proposal
	proposal.CreatedBy = "0xaeB0920be125eB72e071B1357A5c95B52D8afc12"
	proposal.Status = models.Active
	proposal.Amount = decimal.New(100, 0)
	proposal.Price = decimal.New(200, 0)
	proposal.BlockchainID = 0

	suite.App.Repo.Proposal.UpdateProposal(&proposal)

	client = &http.Client{}
	req, _ = http.NewRequest("GET", suite.MockServer.URL+"/user/proposal", nil)
	req.Header.Add("Authorization", "bearer "+tokenString)

	resp, _ = client.Do(req)
	suite.Equal(http.StatusOK, resp.StatusCode)
	r, _ = ioutil.ReadAll(resp.Body)

	json.Unmarshal(r, &proposals)

	suite.Equal("0xaeB0920be125eB72e071B1357A5c95B52D8afc12", proposals[0].CreatedBy)
	suite.Equal(models.Active, proposals[0].Status)
	suite.Equal(uint(100), proposals[0].Amount)
	suite.Equal(uint(200), proposals[0].Price)
	suite.Equal(uint(0), proposals[0].BlockchainID)
}
