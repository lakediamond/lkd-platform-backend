package proposals

import (
	"encoding/json"
	"lkd-platform-backend/internal/pkg/pagination"
	"math/big"
	"net/http"
	"strconv"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/pkg/handlers"
)

//GetOwnersProposalHandler handling get all owner proposal request
var GetOwnersProposalHandler = handlers.ApplicationHandler(getOwnersProposal)

func getOwnersProposal(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)

	proposals, err := getOwners(app.Repo.Proposal)
	if err != nil {
		log.Debug().Err(err).Msg("Get proposal owner error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	respData, _ := json.Marshal(proposals)

	w.WriteHeader(http.StatusOK)
	w.Write(respData)
}

//GetAllProposalsHandler handling get all proposals request
var GetAllProposalsHandler = handlers.ApplicationHandler(getAllProposals)

func getAllProposals(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)

	proposals, err := getProposals(app.Repo.Proposal)
	if err != nil {
		log.Debug().Err(err).Msg("Get all proposals error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	respData, _ := json.Marshal(proposals)

	w.WriteHeader(http.StatusOK)
	w.Write(respData)
}

//getUserProposalsHandler handling get all proposals request
var GetAllUserProposalsHandler = handlers.ApplicationHandler(getAllUserProposals)

func getAllUserProposals(w http.ResponseWriter, r *http.Request) {
	var offset int
	var limit int
	var field string
	var sort string

	app := application.GetApplicationContext(r)

	queryValues := r.URL.Query()
	offsetQuery := queryValues.Get("page")
	if offsetQuery == "" {
		offset = pagination.DEFAULT_PAGING_OFFSET
	} else {
		offset, _ = strconv.Atoi(offsetQuery)
	}

	limitQuery := queryValues.Get("limit")
	if limitQuery == "" {
		limit = pagination.DEFAULT_PAGING_LIMIT
	} else {
		limit, _ = strconv.Atoi(limitQuery)
	}

	orderBy := make(map[string]string, 0)

	field = queryValues.Get("field")
	if field == "" {
		field = pagination.DEFAULT_PAGING_FIELD
	}

	sort = queryValues.Get("sort")
	sortType := pagination.SortingType(sort)

	orderBy[field] = sortType

	// init filters
	var filterProposals pagination.FilterProposals
	filters, err := pagination.FilterParseQuery(&filterProposals, r.URL.Query())
	if err != nil {
		log.Debug().Err(err).Msgf("FilterParseQuery error")
	}

	paginationProposals, err := getUserProposals(app.Repo.Proposal, filters, orderBy, offset, limit)
	if err != nil {
		log.Debug().Err(err).Msg("Get all proposals error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	respData, _ := json.Marshal(&paginationProposals)

	w.WriteHeader(http.StatusOK)
	w.Write(respData)
}

//getUserProposalsHandler handling get all proposals request
var GetNewProposalPositionHint = handlers.ApplicationHandler(getNewProposalPositionHint)

func getNewProposalPositionHint(w http.ResponseWriter, r *http.Request) {

	app := application.GetApplicationContext(r)

	prices, ok := r.URL.Query()["tokenPrice"]

	if !ok || len(prices[0]) < 1 {
		errorMessage := "Parameter \"tokenPrice\" not found or incorrect"
		log.Debug().Msg(errorMessage)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errorMessage))
		return
	}

	price, success := new(big.Int).SetString(prices[0], 10)
	if !success {
		errorMessage := "Can`t parse parameter \"tokenPrice\""
		log.Debug().Msg(errorMessage)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errorMessage))
		return
	}

	previousProposalId, err := getProposalPositionHint(app.Repo.Proposal, price)
	if err != nil {
		log.Debug().Err(err).Msg("Get proposal position hint error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	result := struct {
		StartPosition uint `json:"startPosition"`
	}{
		StartPosition: previousProposalId,
	}
	respData, _ := json.Marshal(&result)

	w.WriteHeader(http.StatusOK)
	w.Write(respData)
}
