package exchange

import (
	"errors"
	"fmt"
)

//BaseResponse base CRM API response
type BaseResponse struct {
	Success bool   `json:"success"`
	Error   string `json:"error"`
}

type DataFixerError struct {
	Code int    `json:"code"`
	Type string `json:"type"`
	Info string `json:"info"`
}

func (dfe *DataFixerError) Error() error {
	return errors.New(fmt.Sprintf("Code: %d, Type: %s, Info: %s", dfe.Code, dfe.Type, dfe.Info))
}

type DataFixer struct {
	Success   bool   `json:"success"`
	Timestamp int    `json:"timestamp"`
	Base      string `json:"base"`
	Date      string `json:"date"`
	Rates     struct {
		CHF float64 `json:"CHF"`
	} `json:"rates"`
	Error DataFixerError `json:"error"`
}

type DataBitsTamp struct {
	High      string `json:"high"`
	Last      string `json:"last"`
	Timestamp string `json:"timestamp"`
	Bid       string `json:"bid"`
	Vwap      string `json:"vwap"`
	Volume    string `json:"volume"`
	Low       string `json:"low"`
	Ask       string `json:"ask"`
	Open      string `json:"open"`
}
