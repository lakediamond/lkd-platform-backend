package exchange

type ClientInterface interface {
	GetBitsTampExchangeRate(code string) (DataBitsTamp, error)
	GetFixerExchangeRate(base string, symbols []string) (DataFixer, error)
}
