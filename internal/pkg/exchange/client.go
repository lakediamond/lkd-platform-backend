package exchange

import (
	"bytes"
	"encoding/json"
	"fmt"
	"lkd-platform-backend/pkg/environment"
	"net/http"
	"net/url"
	"strings"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/pkg/httputil"
)

type FixerSetting struct {
	AccessKey string
}

type Client struct {
	FixerSetting
	BaseURL string
}

func InitBitsTampClient() *Client {
	env := environment.GetEnv()
	bitstampUrl, ok := env["API_EXCHANGE_BITSTAMP_URL"]
	if !ok {
		log.Debug().Msg("$API_EXCHANGE_BITSTAMP_URL value can not be emty")
	}

	return &Client{BaseURL: bitstampUrl}
}

func InitFixerClient() *Client {
	env := environment.GetEnv()

	fixerUrl, ok := env["API_EXCHANGE_FIXER_URL"]
	if !ok {
		log.Debug().Msg("$API_EXCHANGE_FIXER_URL value can not be emty")
	}

	accessKey, ok := env["API_EXCHANGE_FIXER_ACCESS_KEY"]
	if !ok {
		log.Debug().Msg("$API_EXCHANGE_FIXER_ACCESS_KEY value can not be emty")
	}

	return &Client{
		BaseURL:      fixerUrl,
		FixerSetting: FixerSetting{AccessKey: accessKey}}
}

// https://www.bitstamp.net/api/v2/ticker/etheur/
func (c *Client) GetBitsTampExchangeRate(code string) (DataBitsTamp, error) {

	var response DataBitsTamp

	u, err := c.combineURL("ticker/"+code, nil)
	if err != nil {
		return response, err
	}

	err = c.sendRequest(http.MethodGet, u, []byte{}, "application/json", &response)
	if err != nil {
		return response, err
	}

	return response, nil
}

func (c *Client) GetFixerExchangeRate(base string, symbols []string) (DataFixer, error) {

	var response DataFixer

	params := make(map[string]string, 0)
	params["base"] = base
	params["symbols"] = strings.Join(symbols, ",")
	params["access_key"] = c.FixerSetting.AccessKey

	u, err := c.combineURL("latest", params)
	if err != nil {
		return response, err
	}

	err = c.sendRequest(http.MethodGet, u, []byte{}, "application/json", &response)
	if err != nil {
		return response, err
	}

	if !response.Success {
		return response, response.Error.Error()
	}

	return response, nil
}

func (c *Client) combineURL(path string, params map[string]string) (string, error) {
	u := fmt.Sprintf("%s/%s", c.BaseURL, path)

	urlParsed, err := url.Parse(u)
	if err != nil {
		return "", err
	}

	if params == nil {
		params = map[string]string{}
	}

	urlQueryParams := urlParsed.Query()
	for key, value := range params {
		urlQueryParams.Add(key, value)
	}

	urlParsed.RawQuery = urlQueryParams.Encode()

	return urlParsed.String(), nil
}

func (c *Client) sendRequest(method string, u string, body []byte, contentType string, respData interface{}) error {
	if body == nil {
		body = []byte{}
	}
	r := bytes.NewReader(body)

	client := &http.Client{}
	req, err := http.NewRequest(method, u, r)
	if err != nil {
		return err
	}

	if len(contentType) > 0 {
		req.Header.Set("Content-Type", contentType)
	}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	respBody := httputil.ReadBody(resp.Body)
	defer resp.Body.Close()

	err = json.Unmarshal(respBody, respData)
	if err != nil {
		return err
	}
	return nil
}
