package mailjet

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/pkg/environment"
)

//NewMailjetClient return new mailjet client instance
func NewClient(isSandbox bool) *Sender {
	e := environment.GetEnv()

	var emailConfigs map[string]*EmailConfig

	err := json.Unmarshal([]byte(getValue(e, "MAILJET_EMAIL_CONFIGS")), &emailConfigs)
	if err != nil {
		log.Panic().Msg("invalid MAILJET_EMAIL_CONFIGS")
	}

	err = validateRequiredTemplates(emailConfigs)
	if err != nil {
		log.Panic().Msg(err.Error())
	}

	publicKey := getValue(e, "MAILJET_PUBLIC_KEY")
	privateKey := getValue(e, "MAILJET_PRIVATE_KEY")
	c := NewSender(publicKey, privateKey, emailConfigs, isSandbox)
	return c
}

func validateRequiredTemplates(configs map[string]*EmailConfig) error {
	for _, value := range []string{"verification", "recovery"} {
		config := configs[value]

		if config == nil {
			return errors.New(value + " email config not found in MAILJET_EMAIL_CONFIGS")
		}

		if config.TemplateID == 0 {
			return fmt.Errorf(value + " email config \"templateID\" is invalid in MAILJET_EMAIL_CONFIGS")
		}

		if len(config.FromEmail) == 0 {
			return fmt.Errorf(value + " email config \"fromEmail\" is invalid in MAILJET_EMAIL_CONFIGS")
		}

		if len(config.FromName) == 0 {
			return fmt.Errorf(value + " email config \"fromName\" is invalid in MAILJET_EMAIL_CONFIGS")
		}

		if len(config.Variables["url"].(string)) == 0 {
			return fmt.Errorf(value + " email config \"variables[\"url\"]\" is invalid in MAILJET_EMAIL_CONFIGS")
		}
	}
	return nil
}

func getValue(env map[string]string, key string) string {
	value := env[key]
	if len(value) == 0 {
		log.Panic().Msg("invalid " + key)
	}
	return value
}
