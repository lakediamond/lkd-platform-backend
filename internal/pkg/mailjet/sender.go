package mailjet

import (
	"github.com/mailjet/mailjet-apiv3-go"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

//Sender mailjet sender config
type Sender struct {
	publicKey            string
	privateKey           string
	sandboxMode          bool
	emailConfigTemplates map[string]*EmailConfig
}

//NewSender create new sender instance
func NewSender(publicKey string, privateKey string, emailConfigs map[string]*EmailConfig, sandboxMode bool) *Sender {
	return &Sender{
		privateKey:           privateKey,
		publicKey:            publicKey,
		sandboxMode:          sandboxMode,
		emailConfigTemplates: emailConfigs,
	}
}

//GetConfigTemplate
func (s *Sender) GetConfigTemplate(name string) *EmailConfig {
	return s.emailConfigTemplates[name].GetCopy()
}

//BuildMessageByConfig
func BuildMessageByConfig(config *EmailConfig) (*mailjet.InfoMessagesV31, error) {
	if len(config.FromEmail) == 0 {
		return nil, errors.New("From email is empty")
	}

	if len(config.ToEmail) == 0 {
		return nil, errors.New("ToEmail email is empty")
	}

	if config.TemplateID == 0 {
		return nil, errors.New("TemplateID is not set")
	}

	return &mailjet.InfoMessagesV31{
		From: &mailjet.RecipientV31{
			Email: config.FromEmail,
			Name:  config.FromName,
		},
		To: &mailjet.RecipientsV31{
			mailjet.RecipientV31{
				Email: config.ToEmail,
				Name:  config.ToName,
			},
		},
		TemplateID:       config.TemplateID,
		TemplateLanguage: true,
		Variables:        config.Variables,
	}, nil
}

func (s *Sender) MailjetSendEmail(messagesInfo []mailjet.InfoMessagesV31) error {
	res, err := s.Send(messagesInfo)
	if err != nil {
		log.Error().Err(err).Msg("Mailjet response error")
		return err
	}
	log.Debug().Msgf("Data: %+v\n", res)

	return nil
}

//Send
func (s *Sender) Send(message []mailjet.InfoMessagesV31) (*mailjet.ResultsV31, error) {
	mailjetClient := mailjet.NewMailjetClient(s.publicKey, s.privateKey)

	messages := mailjet.MessagesV31{Info: message, SandBoxMode: s.sandboxMode}

	return mailjetClient.SendMailV31(&messages)
}
