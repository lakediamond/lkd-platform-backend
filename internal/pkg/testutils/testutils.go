package testutils

import (
	"net/http/httptest"

	"github.com/julienschmidt/httprouter"

	"lkd-platform-backend/internal/app/backend/httpserver"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/oauth"
	"lkd-platform-backend/internal/pkg/pipeDrive"
)

//TODO make it works for tests
// NewHTTPMock return mocked application, router and server for tests
func NewHTTPMock() (*application.Application, *httprouter.Router, *httptest.Server) {
	app := application.NewApplication()
	app.IAMClient = oauth.GetIAMClient()
	app.Config.AuthRetries = application.InitAuthRetries()
	app.Mailjet = application.NewMailjetClient(true)
	app.S3 = application.InitS3()
	app.PipeDrive = pipeDrive.InitDataMock()

	app.SMSSender = application.InitSMSSender(&app.Config)
	app.PhoneNumberValidator = application.InitValidator(&app.Config)

	router := httpserver.NewHTTPRouter(app)
	mockServer := httptest.NewServer(router)
	return app, router, mockServer
}
