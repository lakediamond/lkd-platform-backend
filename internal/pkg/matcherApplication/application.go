package matcherApplication

import (
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/pkg/environment"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// Config as application Config type
type Config struct {
	Env      string
	LogLevel string
}

type Application struct {
	Config    Config
	EthClient *etherPkg.PlatformEthereumApplication
	Repo      *repo.Repo
}

// NewApplication return application instance
func NewMatcherApplication() *Application {
	app := Application{}

	app.Config = compileConfig()

	app.SetLoggerLevel()

	return &app
}

func compileConfig() Config {
	myEnv := environment.GetEnv()
	return Config{
		Env:      myEnv["ENV"],
		LogLevel: myEnv["LOG_LEVEL"],
	}
}

//SetLoggerLevel setting logger level
func (app *Application) SetLoggerLevel() {
	level, err := zerolog.ParseLevel(app.Config.LogLevel)
	if err != nil {
		level = zerolog.DebugLevel
	}
	zerolog.SetGlobalLevel(level)
	log.Info().Msgf("Log level set to %s", level.String())
}
