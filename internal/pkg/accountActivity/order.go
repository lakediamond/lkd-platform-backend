package accountActivity

import (
	"fmt"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//OrderAdded store new record into account_activities table with unique type
func OrderAdded(repo *repo.Repo, order *models.Order) {
	var activity models.AccountActivity
	activity.Type = "IO_CONTRACT_ORDER_CREATED"

	user, _ := repo.User.GetUserByEthAddress(order.CreatedBy)

	coefficient := order.Price.Mul(order.TokenAmount).Div(order.EthAmount)

	content := fmt.Sprintf(`{"id": %d, "eth_account": "%s", "email": "%s", "tokens_amount": %s, "tokens_price": %s, "eth_amount": %s, "exchange_rate": %s}`,
		order.BlockchainID, order.CreatedBy, user.Email, order.TokenAmount.String(), order.Price.String(), order.EthAmount.String(), coefficient.String())

	activity.Content = content

	err := repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot save order added record")
	}
}

func OrderMatched(repo *repo.Repo, order *models.Order) {
	var activity models.AccountActivity
	activity.Type = "IO_CONTRACT_ORDER_MATCHED"

	matches, err := repo.Matches.GetAllMatchesByOrderID(int(order.BlockchainID))
	if err != nil {
		log.Error().Err(err).Msg("db error")
	}

	var addresses []string
	var tokens []string
	var ether []string

	for i, match := range matches {
		proposal, err := repo.Proposal.GetProposalByBlockchainID(match.ProposalBlockchainID)
		if err != nil {
			log.Error().Err(err).Msg("db error")
		}

		if i != len(matches)-1 {
			addresses = append(addresses, "\""+proposal.CreatedBy+"\",")
			tokens = append(tokens, match.Tokens.String()+",")
			ether = append(ether, match.Ether.String()+",")
		} else {
			addresses = append(addresses, "\""+proposal.CreatedBy+"\"")
			tokens = append(tokens, match.Tokens.String())
			ether = append(ether, match.Ether.String())
		}

	}

	content := fmt.Sprintf(`{"id": %d, "distributed_addresses": %v, "contributed_tokens": %v, "distributed_eth": %v, "rest": %s}`,
		order.BlockchainID, addresses, tokens, ether, order.EthAmountLeft.String())

	log.Debug().Msgf("content: %v", content)

	activity.Content = content

	err = repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot save order closed record")
	}
}
