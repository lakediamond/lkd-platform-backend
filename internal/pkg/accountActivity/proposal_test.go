package accountActivity_test

import (
	"context"
	"crypto/ecdsa"
	"encoding/json"
	"math/big"
	"net/http/httptest"
	"testing"

	"github.com/ethereum/go-ethereum/core/types"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/app/matcherBackend"
	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/ioContract"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/storageContract"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/tokenContract"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
)

type ProposalActivitySuite struct {
	suite.Suite
	App         *application.Application
	MockServer  *httptest.Server
	PrivateKey1 *ecdsa.PrivateKey
	Address1    common.Address
	Auth        *bind.TransactOpts

	Token   *tokenContract.Token
	Storage *storageContract.Storage
	IO      *ioContract.IO

	EthMock *etherPkg.EthMock

	StorageAddress common.Address
}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_ProposalActivitySuite(t *testing.T) {
	suite.Run(t, new(ProposalActivitySuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *ProposalActivitySuite) SetupTest() {
	app, _, _ := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()

	ethMock, privKey := etherPkg.GetMockEthClient()

	suite.EthMock = ethMock

	app.EthClient = ethMock
	app.MatcherData = etherPkg.InitializeMatcherData()

	auth := bind.NewKeyedTransactor(privKey)

	suite.Auth = auth
	suite.Address1 = crypto.PubkeyToAddress(privKey.PublicKey)

	app.Repo.DB.Exec("DELETE FROM users;")
	app.Repo.DB.Exec("DELETE FROM jwt_tokens;")
	app.Repo.DB.Exec("DELETE FROM orders;")
	app.Repo.DB.Exec("DELETE FROM proposals;")
	app.Repo.DB.Exec("DELETE FROM support_data;")
	app.Repo.DB.Exec("DELETE FROM order_types;")
	app.Repo.DB.Exec("DELETE FROM account_activities;")

	tokenAddress, _, token, err := tokenContract.DeployToken(auth, ethMock)
	if err != nil {
		log.Debug().Err(err).Msg("DeployToken error")
	}

	suite.EthMock.Commit()

	storageAddress, _, storage, err := storageContract.DeployStorage(auth, ethMock, tokenAddress)
	if err != nil {
		log.Debug().Err(err).Msg("DeployStorage error")
	}

	suite.EthMock.Commit()

	ioAddress, _, io, err := ioContract.DeployIO(auth, ethMock, storageAddress, suite.Address1)
	if err != nil {
		log.Debug().Err(err).Msg("DeployIo error")
	}

	suite.EthMock.Commit()

	_, err = storage.SetContractManager(auth, ioAddress)
	if err != nil {
		log.Debug().Err(err).Msg("SetContractManager error")
	}

	suite.EthMock.Commit()

	app.MatcherData.StorageAddress = storageAddress.String()
	app.MatcherData.LakeDiamond = ioAddress.String()

	suite.Token = token
	suite.Storage = storage
	suite.IO = io

	suite.StorageAddress = storageAddress

	suite.App = app
	suite.PrivateKey1 = privKey

	priv.Migration(app.Repo.DB)
	go matcherBackend.ListenEvents(app)
	go matcherBackend.ListenSubOwnerEvents(app)
}

func (suite *ProposalActivitySuite) TestAccountActivityProposalCreated() {
	key, _ := crypto.GenerateKey()
	userAddress := crypto.PubkeyToAddress(key.PublicKey)

	userAuth := bind.NewKeyedTransactor(key)

	nonce, err := suite.EthMock.PendingNonceAt(context.Background(), suite.Address1)
	if err != nil {
		log.Fatal()
	}

	tx := types.NewTransaction(nonce, userAddress, big.NewInt(100000000000000000), 50000000, big.NewInt(100), nil)

	signedTx, err := types.SignTx(tx, types.HomesteadSigner{}, suite.PrivateKey1)
	if err != nil {
		log.Fatal()
	}

	suite.EthMock.SendTransaction(context.Background(), signedTx)

	suite.EthMock.Commit()

	_, err = suite.Token.Transfer(suite.Auth, userAddress, big.NewInt(10000))
	if err != nil {
		log.Debug().Msg(err.Error())
	}
	suite.EthMock.Commit()

	_, err = suite.Token.Approve(userAuth, suite.StorageAddress, big.NewInt(10000))
	if err != nil {
		log.Debug().Msg(err.Error())
	}
	suite.EthMock.Commit()

	_, err = suite.IO.CreateProposal(userAuth, big.NewInt(100), big.NewInt(200))
	if err != nil {
		log.Debug().Msg(err.Error())
	}

	suite.EthMock.Commit()

	for i := 0; i < 20; i++ {
		suite.Token.Approve(suite.Auth, suite.Address1, big.NewInt(100))
		suite.EthMock.Commit()
	}

	proposal, err := suite.App.Repo.Proposal.GetProposalByBlockchainID(0)
	if err != nil {
		log.Fatal()
	}

	activities, _ := suite.App.Repo.AccountActivity.GetAllActivities()

	suite.Assert().Equal("IO_CONTRACT_INVESTMENT_PROPOSAL_CREATED", activities[0].Type)

	var objmap map[string]*json.RawMessage

	var buf string
	var bufInt uint

	json.Unmarshal([]byte(activities[0].Content), &objmap)

	json.Unmarshal(*objmap["id"], &bufInt)
	suite.Assert().Equal(proposal.BlockchainID, bufInt)

	json.Unmarshal(*objmap["eth_account"], &buf)
	suite.Assert().Equal(proposal.CreatedBy, buf)

	json.Unmarshal(*objmap["email"], &buf)
	suite.Assert().Equal("", buf)

	json.Unmarshal(*objmap["tokens_amount"], &bufInt)
	suite.Assert().Equal(proposal.Amount.Coefficient(), big.NewInt(int64(bufInt)))

	json.Unmarshal(*objmap["token_price"], &bufInt)
	suite.Assert().Equal(proposal.Price.Coefficient(), big.NewInt(int64(bufInt)))
}

func (suite *ProposalActivitySuite) TestAccountActivityProposalWithdrawn() {
	key, _ := crypto.GenerateKey()
	userAddress := crypto.PubkeyToAddress(key.PublicKey)

	userAuth := bind.NewKeyedTransactor(key)

	nonce, err := suite.EthMock.PendingNonceAt(context.Background(), suite.Address1)
	if err != nil {
		log.Fatal()
	}

	tx := types.NewTransaction(nonce, userAddress, big.NewInt(100000000000000000), 50000000, big.NewInt(100), nil)

	signedTx, err := types.SignTx(tx, types.HomesteadSigner{}, suite.PrivateKey1)
	if err != nil {
		log.Fatal()
	}

	suite.EthMock.SendTransaction(context.Background(), signedTx)

	suite.EthMock.Commit()

	_, err = suite.Token.Transfer(suite.Auth, userAddress, big.NewInt(10000))
	if err != nil {
		log.Debug().Msg(err.Error())
	}
	suite.EthMock.Commit()

	_, err = suite.Token.Approve(userAuth, suite.StorageAddress, big.NewInt(10000))
	if err != nil {
		log.Debug().Msg(err.Error())
	}
	suite.EthMock.Commit()

	_, err = suite.IO.CreateProposal(userAuth, big.NewInt(100), big.NewInt(200))
	if err != nil {
		log.Debug().Msg(err.Error())
	}

	suite.EthMock.Commit()

	for i := 0; i < 20; i++ {
		suite.Token.Approve(suite.Auth, suite.Address1, big.NewInt(100))
		suite.EthMock.Commit()
	}

	_, err = suite.IO.WithdrawProposal(userAuth, big.NewInt(0))
	if err != nil {
		log.Debug().Msg(err.Error())
	}

	suite.EthMock.Commit()
	for i := 0; i < 20; i++ {
		suite.Token.Approve(suite.Auth, suite.Address1, big.NewInt(100))
		suite.EthMock.Commit()
	}

	activities, _ := suite.App.Repo.AccountActivity.GetAllActivities()

	suite.Assert().Equal("IO_CONTRACT_INVESTMENT_PROPOSAL_WITHDRAWN", activities[1].Type)

	proposal, err := suite.App.Repo.Proposal.GetProposalByBlockchainID(0)
	if err != nil {
		log.Fatal()
	}

	var objmap map[string]*json.RawMessage

	var buf string
	var bufInt uint

	json.Unmarshal([]byte(activities[1].Content), &objmap)

	json.Unmarshal(*objmap["id"], &bufInt)
	suite.Assert().Equal(proposal.BlockchainID, bufInt)

	json.Unmarshal(*objmap["eth_account"], &buf)
	suite.Assert().Equal(proposal.CreatedBy, buf)

	json.Unmarshal(*objmap["email"], &buf)
	suite.Assert().Equal("", buf)

	json.Unmarshal(*objmap["tokens_amount"], &bufInt)
	suite.Assert().Equal(proposal.Amount.Coefficient(), big.NewInt(int64(bufInt)))
}
