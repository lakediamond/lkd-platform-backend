package accountActivity

import (
	"fmt"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//ProposalCreated store new record into account_activities table with unique type
func ProposalCreated(repo *repo.Repo, proposal *models.Proposal) {
	var activity models.AccountActivity
	activity.Type = "IO_CONTRACT_INVESTMENT_PROPOSAL_CREATED"

	user, _ := repo.User.GetUserByEthAddress(proposal.CreatedBy)

	content := fmt.Sprintf(`{"id": %d, "eth_account": "%s", "email": "%s", "tokens_amount": %d, "token_price": %d}`,
		proposal.BlockchainID, proposal.CreatedBy, user.Email, proposal.Amount.Coefficient(), proposal.Price.Coefficient())

	activity.Content = content

	err := repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot save proposal created record")
	}
}

//ProposalWithdrawn store new record into account_activities table with unique type
func ProposalWithdrawn(repo *repo.Repo, proposal *models.Proposal) {
	var activity models.AccountActivity
	activity.Type = "IO_CONTRACT_INVESTMENT_PROPOSAL_WITHDRAWN"

	user, _ := repo.User.GetUserByEthAddress(proposal.CreatedBy)

	content := fmt.Sprintf(`{"id": %d, "eth_account": "%s", "email": "%s", "tokens_amount": %d}`,
		proposal.BlockchainID, proposal.CreatedBy, user.Email, proposal.Amount.Coefficient())

	activity.Content = content

	err := repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot save proposal withdrawn record")
	}
}

//ProposalClosed store new record into account_activities table with unique type
func ProposalClosed(repo *repo.Repo, proposal *models.Proposal) {
	var activity models.AccountActivity
	activity.Type = "IO_CONTRACT_INVESTMENT_PROPOSAL_CLOSED"

	user, _ := repo.User.GetUserByEthAddress(proposal.CreatedBy)

	content := fmt.Sprintf(`{"id": %d, "eth_account": "%s", "email": "%s"}`,
		proposal.BlockchainID, proposal.CreatedBy, user.Email)

	activity.Content = content

	err := repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot save proposal closed record")
	}
}
