package accountActivity

import (
	"fmt"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//UserAuthorization store new record into account_activities table with unique type
func SetBlockchainProvider(repo *repo.Repo, email string, value string) {
	var activity models.AccountActivity
	activity.Type = "SET_BLOCKCHAIN_PROVIDER"

	content := fmt.Sprintf(
		`{"email": "%s", "value": "%s"}`,
		email,
		value)

	activity.Content = content

	err := repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("cannot save user authorization record")
	}
}

//UserAuthorizationFailure store new record into account_activities table with unique type
func SetBlockchainProviderFailure(repo *repo.Repo, email string, value string, err error) {
	var activity models.AccountActivity
	activity.Type = "SET_BLOCKCHAIN_PROVIDER_FAILURE"

	content := fmt.Sprintf(`{"email": "%s", "value": "%s", "error": "%s"}`,
		email,
		value,
		err.Error())

	activity.Content = content

	err = repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("cannot save user authorization failure record")
	}
}
