package accountActivity_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
)

type RegistrationActivitySuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
}

func (suite *RegistrationActivitySuite) SetupTestSuite() {

}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_RegistrationActivitySuite(t *testing.T) {
	suite.Run(t, new(RegistrationActivitySuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *RegistrationActivitySuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()
	app.EthClient, _ = etherPkg.GetMockEthClient()

	priv.Migration(app.Repo.DB)
	suite.App = app
	suite.MockServer = mockServer

	suite.App.Repo.DB.Exec("DELETE FROM users;")
	suite.App.Repo.DB.Exec("DELETE FROM jwt_tokens;")
	suite.App.Repo.DB.Exec("DELETE FROM account_activities;")
}

func (suite *RegistrationActivitySuite) TestAccountActivityCreateUserPositive() {
	currencyEventPayload := map[string]interface{}{
		"first_name": "roman",
		"last_name":  "holovay",
		"email":      "romuch@gmail.com",
		"password":   "32jignui#u3U8BYW3",
		"hash":       "0xaeB0920be125eB72e071B1357A5c95B52D8afc22",
	}
	tmp, _ := json.Marshal(currencyEventPayload)
	requestData := bytes.NewReader(tmp)
	http.Post(suite.MockServer.URL+suite.App.FindRoute("register_account"), "application/json", requestData)

	activities, _ := suite.App.Repo.AccountActivity.GetAllActivities()

	suite.Assert().Equal("USER_SIGNUP", activities[0].Type)

	var objmap map[string]*json.RawMessage
	json.Unmarshal([]byte(activities[0].Content), &objmap)

	var buf string

	json.Unmarshal(*objmap["first_name"], &buf)
	suite.Assert().Equal("roman", buf)

	json.Unmarshal(*objmap["last_name"], &buf)
	suite.Assert().Equal("holovay", buf)

	json.Unmarshal(*objmap["email"], &buf)
	suite.Assert().Equal("romuch@gmail.com", buf)

	json.Unmarshal(*objmap["eth_address"], &buf)
	suite.Assert().Equal(strings.ToLower("0xaeB0920be125eB72e071B1357A5c95B52D8afc22"), buf)

}

func (suite *RegistrationActivitySuite) TestAccountActivityCreateUserValidationFailed() {

	//validation failed
	currencyEventPayload := map[string]interface{}{
		"first_name": "roman",
		"last_name":  "holovay",
		"email":      "romuch@gmail.com",
		"password":   "32jignui#u3U8BYW3",
		"hash":       "0xaeB0920be125eB72e071B1357A5c95B52D8afc",
	}
	tmp, _ := json.Marshal(currencyEventPayload)
	requestData := bytes.NewReader(tmp)
	http.Post(suite.MockServer.URL+suite.App.FindRoute("register_account"), "application/json", requestData)

	activities, _ := suite.App.Repo.AccountActivity.GetAllActivities()

	suite.Assert().Equal("USER_SIGNUP_FAILURE", activities[0].Type)

	var objmap map[string]*json.RawMessage
	json.Unmarshal([]byte(activities[0].Content), &objmap)

	var buf string

	json.Unmarshal(*objmap["first_name"], &buf)
	suite.Assert().Equal("roman", buf)

	json.Unmarshal(*objmap["last_name"], &buf)
	suite.Assert().Equal("holovay", buf)

	json.Unmarshal(*objmap["email"], &buf)
	suite.Assert().Equal("romuch@gmail.com", buf)

	json.Unmarshal(*objmap["eth_address"], &buf)
	suite.Assert().Equal("0xaeB0920be125eB72e071B1357A5c95B52D8afc", buf)

	json.Unmarshal(*objmap["error_message"], &buf)
	suite.Assert().Equal(strings.ToLower("hash:invalid ethereum address"), buf)

}

func (suite *RegistrationActivitySuite) TestAccountActivityCreateUserDuplicateUser() {
	currencyEventPayload := map[string]interface{}{
		"first_name": "roman",
		"last_name":  "holovay",
		"email":      "romuch@gmail.com",
		"password":   "32jignui#u3U8BYW3",
		"hash":       "0xaeB0920be125eB72e071B1357A5c95B52D8afc22",
	}
	tmp, _ := json.Marshal(currencyEventPayload)
	requestData := bytes.NewReader(tmp)
	http.Post(suite.MockServer.URL+suite.App.FindRoute("register_account"), "application/json", requestData)

	currencyEventPayload = map[string]interface{}{
		"first_name": "roman",
		"last_name":  "holovay",
		"email":      "romuch@gmail.com",
		"password":   "32jignui#u3U8BYW3",
		"hash":       "0xaeB0920be125eB72e071B1357A5c95B52D8afc22",
	}
	tmp, _ = json.Marshal(currencyEventPayload)
	requestData = bytes.NewReader(tmp)
	http.Post(suite.MockServer.URL+suite.App.FindRoute("register_account"), "application/json", requestData)

	activities, _ := suite.App.Repo.AccountActivity.GetAllActivities()

	suite.Assert().Equal("USER_SIGNUP_FAILURE", activities[1].Type)

	var objmap map[string]*json.RawMessage
	json.Unmarshal([]byte(activities[1].Content), &objmap)

	var buf string

	json.Unmarshal(*objmap["first_name"], &buf)
	suite.Assert().Equal("roman", buf)

	json.Unmarshal(*objmap["last_name"], &buf)
	suite.Assert().Equal("holovay", buf)

	json.Unmarshal(*objmap["email"], &buf)
	suite.Assert().Equal("romuch@gmail.com", buf)

	json.Unmarshal(*objmap["eth_address"], &buf)
	suite.Assert().Equal(strings.ToLower("0xaeB0920be125eB72e071B1357A5c95B52D8afc22"), buf)

	json.Unmarshal(*objmap["error_message"], &buf)
	suite.Assert().Equal("db error:User account with such email address already exists", buf)
}
