package accountActivity

import (
	"fmt"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//SubOwnerAdded store new record into account_activities table with unique type
func SubOwnerAdded(repo *repo.Repo, ethAddress string) {
	var activity models.AccountActivity
	activity.Type = "IO_CONTRACT_SUBOWNER_ADDED"

	content := fmt.Sprintf(`{"eth_address": "%s"}`,
		ethAddress)

	activity.Content = content

	err := repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("cannot save subOwner added record")
	}
}

//SubOwnerRemoved store new record into account_activities table with unique type
func SubOwnerRemoved(repo *repo.Repo, ethAddress string) {
	var activity models.AccountActivity
	activity.Type = "IO_CONTRACT_SUBOWNER_REMOVED"

	content := fmt.Sprintf(`{"eth_address": "%s"}`,
		ethAddress)

	activity.Content = content

	err := repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("cannot save subOwner removed record")
	}
}
