package accountActivity

import (
	"fmt"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//UserAuthorization store new record into account_activities table with unique type
func UserAuthorization(repo *repo.Repo, email string, CFIPAddress, CFIPCountry string) {
	var activity models.AccountActivity
	activity.Type = "USER_AUTHORIZATION"
	activity.CFIPAddress = CFIPAddress
	activity.CFIPCountry = CFIPCountry

	content := fmt.Sprintf(`{"email": "%s"}`,
		email)

	activity.Content = content

	err := repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot save user authorization record")
	}
}

//UserAuthorizationFailure store new record into account_activities table with unique type
func UserAuthorizationFailure(repo *repo.Repo, email, CFIPAddress, CFIPCountry string, err error) {
	var activity models.AccountActivity
	activity.Type = "USER_SIGNUP_FAILURE"
	activity.CFIPAddress = CFIPAddress
	activity.CFIPCountry = CFIPCountry

	content := fmt.Sprintf(`{"email": "%s", "error": "%s"}`,
		email, err.Error())
	activity.Content = content

	err = repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot save user authorization failure record")
	}
}
