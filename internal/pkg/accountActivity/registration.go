package accountActivity

import (
	"fmt"
	"strings"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//AccountCreated store new record into account_activities table with unique type
func AccountCreated(repo *repo.Repo, user *models.User, CFIPAddress, CFIPCountry string) {
	var activity models.AccountActivity
	activity.Type = "USER_SIGNUP"
	activity.CFIPAddress = CFIPAddress
	activity.CFIPCountry = CFIPCountry

	content := fmt.Sprintf(`{"first_name": "%s", "last_name": "%s", "email": "%s", "eth_address": "%s"}`,
		user.FirstName, user.LastName, user.Email, user.EthAddress)

	activity.Content = content

	err := repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot save account creation record")
	}
}

//AccountCreationFailure store new record into account_activities table with unique type
func AccountCreationFailure(repo *repo.Repo, user *models.User, CFIPAddress, CFIPCountry string, errs []error) {
	var activity models.AccountActivity
	activity.Type = "USER_SIGNUP_FAILURE"
	activity.CFIPAddress = CFIPAddress
	activity.CFIPCountry = CFIPCountry

	var res []string
	for _, err := range errs {
		res = append(res, err.Error())
	}

	content := fmt.Sprintf(`{"error_message": "%s", "first_name": "%s", "last_name": "%s", "email": "%s", "eth_address": "%s"}`,
		strings.Join(res, ", "), user.FirstName, user.LastName, user.Email, user.EthAddress)

	activity.Content = content

	err := repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot save account creation failure record")
	}
}
