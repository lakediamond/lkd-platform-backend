package accountActivity_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"

	"lkd-platform-backend/internal/pkg/application"
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/priv"
	"lkd-platform-backend/internal/pkg/testutils"
)

type AuthorizationActivitySuite struct {
	suite.Suite
	App        *application.Application
	MockServer *httptest.Server
}

func (suite *AuthorizationActivitySuite) SetupTestSuite() {

}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func Test_AuthorizationActivitySuite(t *testing.T) {
	suite.Run(t, new(AuthorizationActivitySuite))
}

// Make sure that VariableThatShouldStartAtFive is set to five
// before each test
func (suite *AuthorizationActivitySuite) SetupTest() {
	app, _, mockServer := testutils.NewHTTPMock()
	app.Repo = repo.GetDbMockClient()
	app.EthClient, _ = etherPkg.GetMockEthClient()

	priv.Migration(app.Repo.DB)
	suite.App = app
	suite.MockServer = mockServer

	suite.App.Repo.DB.Exec("DELETE FROM users;")
	suite.App.Repo.DB.Exec("DELETE FROM jwt_tokens;")
	suite.App.Repo.DB.Exec("DELETE FROM account_activities;")
}

func (suite *AuthorizationActivitySuite) TestAccountActivityAuthorizationPositive() {
	currencyEventPayload := map[string]interface{}{
		"first_name": "roman",
		"last_name":  "holovay",
		"email":      "romuch@gmail.com",
		"password":   "32jignui#u3U8BYW3",
		"hash":       "0xaeB0920be125eB72e071B1357A5c95B52D8afc22",
	}
	tmp, _ := json.Marshal(currencyEventPayload)
	requestData := bytes.NewReader(tmp)
	http.Post(suite.MockServer.URL+suite.App.FindRoute("register_account"), "application/json", requestData)

	user, _ := suite.App.Repo.User.GetUserByEmail("romuch@gmail.com")
	user.EmailVerified = true
	suite.App.Repo.User.UpdateUser(&user)

	currencyEventPayload = map[string]interface{}{
		"email":    "romuch@gmail.com",
		"password": "32jignui#u3U8BYW3",
	}
	tmp, _ = json.Marshal(currencyEventPayload)
	requestData = bytes.NewReader(tmp)
	http.Post(suite.MockServer.URL+suite.App.FindRoute("authenticate_account"), "application/json", requestData)

	activities, _ := suite.App.Repo.AccountActivity.GetAllActivities()

	suite.Assert().Equal("USER_AUTHORIZATION", activities[1].Type)

	var objmap map[string]*json.RawMessage
	json.Unmarshal([]byte(activities[1].Content), &objmap)

	var buf string

	json.Unmarshal(*objmap["email"], &buf)
	suite.Assert().Equal("romuch@gmail.com", buf)

}

func (suite *AuthorizationActivitySuite) TestAccountActivityAuthorizationNegative() {
	currencyEventPayload := map[string]interface{}{
		"first_name": "roman",
		"last_name":  "holovay",
		"email":      "romuch@test.com",
		"password":   "32jignui#u3U8BYW3",
		"hash":       "0xaeB0920be125eB72e071B1357A5c95B52D8afc22",
	}
	tmp, _ := json.Marshal(currencyEventPayload)
	requestData := bytes.NewReader(tmp)
	http.Post(suite.MockServer.URL+suite.App.FindRoute("register_account"), "application/json", requestData)

	user, _ := suite.App.Repo.User.GetUserByEmail("romuch@gmail.com")
	user.EmailVerified = true
	suite.App.Repo.User.UpdateUser(&user)

	currencyEventPayload = map[string]interface{}{
		"email":    "romuch2@test.com",
		"password": "32jignui#u3U8BYW3",
	}
	tmp, _ = json.Marshal(currencyEventPayload)
	requestData = bytes.NewReader(tmp)
	http.Post(suite.MockServer.URL+suite.App.FindRoute("authenticate_account"), "application/json", requestData)

	activities, _ := suite.App.Repo.AccountActivity.GetAllActivities()

	suite.Assert().Equal("USER_SIGNUP_FAILURE", activities[1].Type)

	var objmap map[string]*json.RawMessage
	json.Unmarshal([]byte(activities[1].Content), &objmap)

	var buf string

	json.Unmarshal(*objmap["email"], &buf)
	suite.Assert().Equal("romuch2@test.com", buf)

	json.Unmarshal(*objmap["error"], &buf)
	suite.Assert().Equal("login failed; Invalid userID or password", buf)
}
