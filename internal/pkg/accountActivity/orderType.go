package accountActivity

import (
	"fmt"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/repo/models"
)

//OrderTypeAdded store new record into account_activities table with unique type
func OrderTypeAdded(repo *repo.Repo, orderType *models.OrderType) {
	var activity models.AccountActivity
	activity.Type = "IO_CONTRACT_ORDER_TYPE_ADDED"

	content := fmt.Sprintf(`{"id": %d, "description": "%s"}`,
		orderType.ID, orderType.Name)

	activity.Content = content

	err := repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot save order type added record")
	}
}

//OrderTypeUpdated store new record into account_activities table with unique type
func OrderTypeUpdated(repo *repo.Repo, orderType *models.OrderType) {
	var activity models.AccountActivity
	activity.Type = "IO_CONTRACT_ORDER_TYPE_UPDATED"

	content := fmt.Sprintf(`{"id": %d, "description": "%s"}`,
		orderType.ID, orderType.Name)

	activity.Content = content

	err := repo.AccountActivity.NewRecord(&activity)
	if err != nil {
		log.Debug().Err(err).Msg("Cannot save order type added record")
	}
}
