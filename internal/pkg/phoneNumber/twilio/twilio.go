package twilio

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"github.com/rs/zerolog/log"

	"lkd-platform-backend/pkg/httputil"
)

//Validator phone number validation. Implementation for twilio client
type Validator struct {
	APIRoot    string
	AccountSID string
	Secret     string
}

//NewValidator create new instance of twilio phone number validator client
func NewValidator(apiRoot string, accountSID string, secret string) *Validator {
	return &Validator{
		APIRoot:    apiRoot,
		AccountSID: accountSID,
		Secret:     secret,
	}
}

//ValidateNumber validate phone number
func (config *Validator) ValidateNumber(phoneNumber string) (bool, error) {
	var twilioResponse map[string]interface{}

	apiRouteParsed, _ := url.Parse(config.APIRoot)
	fullURL := fmt.Sprintf("%s/PhoneNumbers/%s", apiRouteParsed, phoneNumber)

	client := &http.Client{}
	req, err := http.NewRequest("GET", fullURL, nil)
	if err != nil {
		return false, err
	}

	req.SetBasicAuth(config.AccountSID, config.Secret)
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return false, err
	}

	if resp.StatusCode != http.StatusOK {
		return false, fmt.Errorf("invalid phone format, 3rd party response status- %d", resp.StatusCode)
	}

	defer resp.Body.Close()
	data := httputil.ReadBody(resp.Body)
	err = json.Unmarshal(data, &twilioResponse)
	if err != nil {
		return false, err
	}

	log.Debug().Msg(fmt.Sprintf("3rd party detailed answer - %s", twilioResponse))

	return true, nil
}
