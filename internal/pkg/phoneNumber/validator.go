package phoneNumber

//Validator interface for phone number validator client
type Validator interface {
	ValidateNumber(phoneNumber string) (bool, error)
}
