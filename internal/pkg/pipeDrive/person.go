package pipeDrive

//Person contains information about CRM person keys
type Person struct {
	LastNameValue                  string `crmKey:"(LD_CF_1)"`
	FirstNameValue                 string `crmKey:"(LD_CF_2)"`
	BirthDateValue                 string `crmKey:"(LD_CF_3)"`
	ResidentialAddressValue        string `crmKey:"(LD_CF_5)"`
	EthereumAddressValue           string `crmKey:"(LD_CF_8)"`
	BankNameValue                  string `crmKey:"(LD_CF_9)"`
	BankAddressValue               string `crmKey:"(LD_CF_10)"`
	BankIbanValue                  string `crmKey:"(LD_CF_11)"`
	AgeOver18VerifiedValue         string `crmKey:"(LD_FL_2)"`
	EmailUserVerificationValue     string `crmKey:"(LD_FL_3)"`
	TelephoneUserVerificationValue string `crmKey:"(LD_FL_4)"`
}
