package pipeDrive

//BaseResponse base CRM API response
type BaseResponse struct {
	Success bool   `json:"success"`
	Error   string `json:"error"`
}

//CreateResponse is a create response which receiving from CRM
type CreateResponse struct {
	BaseResponse
	CreateResponseData CreateResponseData `json:"data"`
}

//CreateResponseData is a response data from CreateResponse
type CreateResponseData struct {
	ID uint `json:"id"`
}

type configResponse struct {
	BaseResponse
	AdditionalData struct {
		Pagination struct {
			Limit                 int  `json:"limit"`
			MoreItemsInCollection bool `json:"more_items_in_collection"`
			Start                 int  `json:"start"`
		} `json:"pagination"`
	} `json:"additional_data"`
	Data []struct {
		ActiveFlag         bool        `json:"active_flag"`
		AddTime            string      `json:"add_time"`
		AddVisibleFlag     bool        `json:"add_visible_flag"`
		BulkEditAllowed    bool        `json:"bulk_edit_allowed"`
		DetailsVisibleFlag bool        `json:"details_visible_flag"`
		EditFlag           bool        `json:"edit_flag"`
		FieldType          string      `json:"field_type"`
		FilteringAllowed   bool        `json:"filtering_allowed"`
		ID                 int         `json:"id"`
		ImportantFlag      bool        `json:"important_flag"`
		IndexVisibleFlag   bool        `json:"index_visible_flag"`
		Key                string      `json:"key"`
		Link               string      `json:"link"`
		Name               string      `json:"name"`
		OrderNr            int         `json:"order_nr"`
		PicklistData       interface{} `json:"picklist_data"`
		SearchableFlag     bool        `json:"searchable_flag"`
		SortableFlag       bool        `json:"sortable_flag"`
		UpdateTime         string      `json:"update_time"`
		UseField           string      `json:"use_field"`
	} `json:"data"`
}

//PersonSearchResponse is a response from persons/find
type PersonSearchResponse struct {
	BaseResponse
	Data []PersonSearchData `json:"data"`
}

//PersonSearchData is a found persons data
type PersonSearchData struct {
	Email     string  `json:"email"`
	ID        int     `json:"id"`
	Name      string  `json:"name"`
	OrgID     int     `json:"org_id"`
	OrgName   string  `json:"org_name"`
	Phone     string  `json:"phone"`
	Picture   Picture `json:"picture"`
	VisibleTo string  `json:"visible_to"`
}

//Picture is a picture info
type Picture struct {
	Height int    `json:"height"`
	ID     int    `json:"id"`
	URL    string `json:"url"`
	Width  int    `json:"width"`
}
