package pipeDrive

//DataInitializerMock is a mock structure
type DataInitializerMock struct {
	Person *Person
	Deal   *Deal
}

var currentMaxID uint = 2

//InitDataMock initializing mock data
func InitDataMock() *DataInitializerMock {
	return &DataInitializerMock{
		Deal: &Deal{
			TelephoneFromAllowedCountryValue:               "TelephoneFromAllowedCountryValue",
			NameSanctionScreeningValue:                     "NameSanctionScreeningValue",
			ResidentialAddressFromAllowedCountryValue:      "ResidencialAddressFromAllowedCountryValue",
			IDDocumentBitaccessVerificationValue:           "IDDocumentBitaccessVerificationValue",
			IDDocumentBitaccessVerificationFailureStage:    20,
			IDDocumentFromAllowedCountryValue:              "IDDocumentFromAllowedCountryValue",
			IDDocumentFromAllowedCountryFailureStage:       24,
			SelfieBitaccessVerificationValue:               "SelfieBitaccessVerificationValue",
			SelfieBitaccessVerificationFailureStage:        22,
			EthereumAddressScorechainScreeningValue:        "EthereumAddressScorechainScreeningValue",
			EthereumAddressScorechainScreeningFailureStage: 21,
			IPConnectionFromAllowedCountryValue:            "IPConnectionFromAllowedCountryValue",
			MultipleFailuresStage:                          26,
		},
		Person: &Person{
			LastNameValue:                  "LastNameValue",
			FirstNameValue:                 "FirstNameValue",
			BirthDateValue:                 "BirthDateValue",
			ResidentialAddressValue:        "ResidentialAddressValue",
			EthereumAddressValue:           "EthereumAddressValue",
			BankNameValue:                  "BankNameValue",
			BankAddressValue:               "BankAddressValue",
			AgeOver18VerifiedValue:         "AgeOver18VerifiedValue",
			EmailUserVerificationValue:     "EmailUserVerificationValue",
			TelephoneUserVerificationValue: "TelephoneUserVerificationValue",
		},
	}
}

//GetRelatedKYCFlags returns all CRM custom flags
func (d *DataInitializerMock) GetRelatedKYCFlags() []string {
	return []string{d.Person.AgeOver18VerifiedValue, d.Person.EmailUserVerificationValue, d.Person.TelephoneUserVerificationValue, d.Deal.TelephoneFromAllowedCountryValue}
}

//GetPerson return CRM Person integration settings
func (d *DataInitializerMock) GetPerson() *Person {
	return d.Person
}

//GetDeal return CRM Deal integration settings
func (d *DataInitializerMock) GetDeal() *Deal {
	return d.Deal
}

//UpdateRecord mock for update record in CRM
func (d *DataInitializerMock) UpdateRecord(entityName string, id uint, reqBody []byte, contentType string) (uint, error) {
	currentMaxID++
	return currentMaxID, nil
}

//CreateRecord mock for create record in CRM
func (d *DataInitializerMock) CreateRecord(entityName string, reqBody []byte, contentType string) (uint, error) {
	return currentMaxID, nil
}

//FindPersonsByEmail mock for find persons by email in CRM
func (d *DataInitializerMock) FindPersonsByEmail(email string) ([]PersonSearchData, error) {
	if email == "test@test.com" {
		return []PersonSearchData{
			{ID: 1},
		}, nil
	}
	return []PersonSearchData{}, nil
}
