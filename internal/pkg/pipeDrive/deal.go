package pipeDrive

//Deal contains information about CRM deal keys
type Deal struct {
	TelephoneFromAllowedCountryValue               string `crmKey:"(LD_FL_4)"`
	NameSanctionScreeningValue                     string `crmKey:"(LD_FL_1)"`
	ResidentialAddressFromAllowedCountryValue      string `crmKey:"(LD_FL_5)"`
	IDDocumentBitaccessVerificationValue           string `crmKey:"(LD_FL_6_a)"`
	IDDocumentBitaccessVerificationFailureStage    int
	IDDocumentFromAllowedCountryValue              string `crmKey:"(LD_FL_6_b)"`
	IDDocumentFromAllowedCountryFailureStage       int
	SelfieBitaccessVerificationValue               string `crmKey:"(LD_FL_7)"`
	SelfieBitaccessVerificationFailureStage        int
	EthereumAddressScorechainScreeningValue        string `crmKey:"(LD_FL_8)"`
	EthereumAddressScorechainScreeningFailureStage int
	IPConnectionFromAllowedCountryValue            string `crmKey:"(LD_FL_9)"`
	MultipleFailuresStage                          int
	KYCTier0StageID                                int
	KYCTier1StageID                                int
	KYCTier2StageID                                int
	KYCTier3StageID                                int
}
