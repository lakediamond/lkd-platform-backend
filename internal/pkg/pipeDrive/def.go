package pipeDrive

import "reflect"

//ClientInterface Interface to get CRM integration settings
type ClientInterface interface {
	GetRelatedKYCFlags() []string
	GetPerson() *Person
	GetDeal() *Deal
	UpdateRecord(entityName string, id uint, reqBody []byte, contentType string) (uint, error)
	CreateRecord(entityName string, reqBody []byte, contentType string) (uint, error)
	FindPersonsByEmail(email string) ([]PersonSearchData, error)
}

const (
	crmKey = "crmKey"
)

//ContainsCRMKeyValue check if person columns contain crm value
func ContainsCRMKeyValue(s interface{}, value string) bool {
	val := reflect.ValueOf(s).Elem()

	for i := 0; i < val.NumField(); i++ {
		key := val.Type().Field(i).Tag.Get(crmKey)
		if len(key) != 0 && val.Field(i).String() == value {
			return true
		}
	}

	return false
}
