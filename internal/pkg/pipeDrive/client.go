package pipeDrive

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"reflect"
	"strings"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/pkg/httputil"
)

//Client contains all CRM data information
type Client struct {
	BaseURL string
	Token   string
	Person  *Person
	Deal    *Deal
}

//InitData initializing new CRM data
func InitData(baseURL string, token string) *Client {
	var d Client

	d.BaseURL = baseURL
	d.Token = token

	d.initPerson()
	d.initDeal()

	return &d
}

//GetRelatedKYCFlags returns all CRM custom flags
func (c *Client) GetRelatedKYCFlags() []string {
	return []string{
		c.Person.AgeOver18VerifiedValue,
		c.Person.EmailUserVerificationValue,
		c.Person.TelephoneUserVerificationValue,
		c.Deal.TelephoneFromAllowedCountryValue,
	}
}

//GetPerson return CRM Person integration settings
func (c *Client) GetPerson() *Person {
	return c.Person
}

//GetDeal return CRM Deal integration settings
func (c *Client) GetDeal() *Deal {
	return c.Deal
}

//CreateRecord create new record in CRM
func (c *Client) CreateRecord(entityName string, reqBody []byte, contentType string) (uint, error) {
	var response CreateResponse

	u, err := c.combineURL(entityName, c.getDefaultParametersMap())
	if err != nil {
		return 0, err
	}

	err = c.sendRequest(http.MethodPost, u, reqBody, contentType, &response)
	if err != nil {
		return 0, err
	}

	if !response.Success {
		return 0, errors.New(response.Error)
	}

	return response.CreateResponseData.ID, nil
}

//UpdateRecord in CRM
func (c *Client) UpdateRecord(entityName string, id uint, reqBody []byte, contentType string) (uint, error) {
	var response CreateResponse
	path := fmt.Sprintf("%s/%d", entityName, id)

	u, err := c.combineURL(path, c.getDefaultParametersMap())
	if err != nil {
		return 0, err
	}

	err = c.sendRequest(http.MethodPut, u, reqBody, contentType, &response)
	if err != nil {
		return 0, err
	}

	if !response.Success {
		return 0, errors.New(response.Error)
	}

	return response.CreateResponseData.ID, nil
}

//FindPersonsByEmail in CRM
func (c *Client) FindPersonsByEmail(email string) ([]PersonSearchData, error) {
	var response PersonSearchResponse

	params := c.getDefaultParametersMap()

	params["term"] = email
	params["start"] = "0"
	params["search_by_email"] = "1"

	u, err := c.combineURL("persons/find", params)
	if err != nil {
		return nil, err
	}

	err = c.sendRequest(http.MethodGet, u, []byte{}, "application/json", &response)
	if err != nil {
		return nil, err
	}

	if !response.Success {
		return nil, errors.New(response.Error)
	}

	return response.Data, nil
}

func (c *Client) initPerson() {
	var person Person

	u, err := c.combineURL("personFields", c.getDefaultParametersMap())
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	val := reflect.ValueOf(&person).Elem()
	err = c.loadColumnsConfig(u, &val)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	c.Person = &person
}

func (c *Client) initDeal() {
	var deal Deal

	u, err := c.combineURL("dealFields", c.getDefaultParametersMap())
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	val := reflect.ValueOf(&deal).Elem()
	err = c.loadColumnsConfig(u, &val)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}

	c.Deal = &deal
}

func (c *Client) loadColumnsConfig(crmURL string, val *reflect.Value) error {
	configResponse := &configResponse{}
	err := c.sendRequest(http.MethodGet, crmURL, []byte{}, "", &configResponse)
	if err != nil {
		return err
	}

	for _, crmDataResp := range configResponse.Data {
		for i := 0; i < val.NumField(); i++ {
			field := val.Field(i)
			typeField := val.Type().Field(i)

			key := typeField.Tag.Get(crmKey)
			if len(key) != 0 && strings.Contains(crmDataResp.Name, key) {
				field.SetString(crmDataResp.Key)
				continue
			}
		}
	}

	err = checkForEmptyCRMFields(val)
	if err != nil {
		log.Panic().Err(err).Msg(err.Error())
	}
	return nil
}

func checkForEmptyCRMFields(val *reflect.Value) error {
	for i := 0; i < val.NumField(); i++ {
		field := val.Field(i)
		typeField := val.Type().Field(i)

		key := typeField.Tag.Get(crmKey)

		if len(key) != 0 && len(field.String()) == 0 {
			return errors.New("empty " + typeField.Name)
		}
	}
	return nil
}

func (c *Client) combineURL(path string, params map[string]string) (string, error) {
	u := fmt.Sprintf("%s/%s", c.BaseURL, path)

	urlParsed, err := url.Parse(u)
	if err != nil {
		return "", err
	}

	if params == nil {
		params = map[string]string{}
	}

	urlQueryParams := urlParsed.Query()
	for key, value := range params {
		urlQueryParams.Add(key, value)
	}

	urlParsed.RawQuery = urlQueryParams.Encode()

	return urlParsed.String(), nil
}

func (c *Client) sendRequest(method string, u string, body []byte, contentType string, respData interface{}) error {
	if body == nil {
		body = []byte{}
	}
	r := bytes.NewReader(body)

	client := &http.Client{}
	req, err := http.NewRequest(method, u, r)
	if err != nil {
		return err
	}

	if len(body) < 2000 {
		log.Debug().Msgf("Request data from CRM. URL: %s, method: %s, body: %s", u, method, string(body))
	} else {
		log.Debug().Msgf("Request data from CRM. URL: %s, method: %s, body: %s...", u, method, string(body[:2000]))
	}

	if len(contentType) > 0 {
		req.Header.Set("Content-Type", contentType)
	}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	respBody := httputil.ReadBody(resp.Body)
	defer resp.Body.Close()

	if len(respBody) < 2000 {
		log.Debug().Msgf("Response from CRM address: %s. Body: %s", u, string(respBody))
	} else {
		log.Debug().Msgf("Response from CRM address: %s. Body: %s...", u, string(respBody[:2000]))
	}

	err = json.Unmarshal(respBody, respData)
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) getDefaultParametersMap() map[string]string {
	return map[string]string{
		"api_token": c.Token,
	}
}
