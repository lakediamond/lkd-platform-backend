package pagination

import (
	"math"
	"reflect"

	"github.com/jinzhu/gorm"
)

var (
	DEFAULT_PAGING_OFFSET = 0
	DEFAULT_PAGING_LIMIT  = 10
	DEFAULT_PAGING_FIELD  = "id"
	SORTING_PAGING_TYPES  = []string{"desc", "des", "asc"}
)

type PaginatorInterface interface {
	ConvertingToSlice() []interface{}
}

// Param
type Param struct {
	DB           *gorm.DB
	Page         int
	Limit        int
	OrderBy      []string
	ShowSQL      bool
	NotUsedModel bool
	DBCount      *gorm.DB
}

// Paginator
type Paginator struct {
	TotalRecord int         `json:"total_record"`
	TotalPage   int         `json:"total_page"`
	Records     interface{} `json:"records"`
	Offset      int         `json:"offset"`
	Limit       int         `json:"limit"`
	Page        int         `json:"page"`
	PrevPage    int         `json:"prev_page"`
	NextPage    int         `json:"next_page"`
}

func (p *Paginator) ConvertingToSlice() []interface{} {
	var slice []interface{}

	switch reflect.TypeOf(p.Records).Kind() {
	case reflect.Ptr:
		val := reflect.ValueOf(p.Records)

		if val.Kind() == reflect.Interface && !val.IsNil() {
			elm := val.Elem()
			if elm.Kind() == reflect.Ptr && !elm.IsNil() && elm.Elem().Kind() == reflect.Ptr {
				val = elm
			}
		}
		if val.Kind() == reflect.Ptr {
			val = val.Elem()
		}

		for i := 0; i < val.Len(); i++ {
			slice = append(slice, val.Index(i).Interface())
		}
	}

	return slice
}

// Paging
func Paging(p *Param, result interface{}) Paginator {
	db := p.DB

	if p.ShowSQL {
		db = db.Debug()
	}
	if p.Page < 1 {
		p.Page = 1
	}
	if p.Limit == 0 {
		p.Limit = 10
	}
	if len(p.OrderBy) > 0 {
		for _, o := range p.OrderBy {
			db = db.Order(o)
		}
	}

	done := make(chan bool, 1)
	var paginator Paginator
	var count int
	var offset int

	go countRecords(db, result, done, &count, p.NotUsedModel, p.DBCount)

	//done <- true
	if p.Page == 1 {
		offset = 0
	} else {
		offset = (p.Page - 1) * p.Limit
	}

	db.Limit(p.Limit).Offset(offset).Find(result)
	<-done

	paginator.TotalRecord = count
	paginator.Records = result
	paginator.Page = p.Page

	paginator.Offset = offset
	paginator.Limit = p.Limit
	paginator.TotalPage = int(math.Ceil(float64(count) / float64(p.Limit)))

	if p.Page > 1 {
		paginator.PrevPage = p.Page - 1
	} else {
		paginator.PrevPage = p.Page
	}

	if p.Page == paginator.TotalPage {
		paginator.NextPage = p.Page
	} else {
		paginator.NextPage = p.Page + 1
	}
	return paginator
}

func countRecords(db *gorm.DB, anyType interface{}, done chan bool, count *int, notUsedModel bool, dbCount *gorm.DB) {
	if notUsedModel == true && dbCount != nil {
		dbCount.Count(count)
	} else {
		db.Model(anyType).Count(count)
	}

	done <- true
}
