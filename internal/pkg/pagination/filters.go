package pagination

import (
	"encoding/json"
	"net/url"
	"reflect"
	"strings"

	"github.com/rs/zerolog/log"
)

const tagName = "field"

type FilterOrderDetails struct {
	ID          []string `json:"id" field:"ord.blockchain_id"`
	Type        []string `json:"type" field:"ordt.name"`
	CreatedOn   []string `json:"created_on" field:"ord.created_on"`
	CreatedBy   []string `json:"created_by" field:"ord.created_by"`
	Metadata    []string `json:"metadata" field:"ord.metadata"`
	Price       []string `json:"token_price" field:"ord.price"`
	TokenAmount []string `json:"tokens_amount" field:"ord.token_amount"`
	EthAmount   []string `json:"eth_amount" field:"ord.eth_amount"`
	TxHash      []string `json:"tx_hash" field:"ord.tx_hash"`
}

type FilterProposals struct {
	ID                    []string `json:"id" field:"p.blockchain_id"`
	RemainingTokensAmount []string `json:"remaining_tokens_amount" field:"p.balance"`
	CreatedOn             []string `json:"created_on" field:"p.created_on"`
	Price                 []string `json:"price" field:"p.price"`
	WithdrawnOn           []string `json:"withdrawn_on" field:"p.withdrawn_on"`
	WithdrawnTokensAmount []string `json:"withdrawn_tokens_amount" field:"p.withdrawn_amount"`
	OwnerAddress          []string `json:"owner_address" field:"p.created_by"`
	Status                []string `json:"status" field:"p.status"`
	OriginalTokensAmount  []string `json:"original_tokens_amount" field:"p.original_tokens"`
	TxHash                []string `json:"tx_hash" field:"p.tx_hash"`
}

func FilterParseQuery(object interface{}, values url.Values) (interface{}, error) {
	data, err := json.Marshal(values)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &object)
	if err != nil {
		return nil, err
	}

	return object, nil
}

func QueryWhere(object interface{}) string {
	queryWhere := "true"
	valRef := reflect.ValueOf(object)
	if object != nil && valRef.Kind() == reflect.Ptr {
		val := valRef.Elem()
		for i := 0; i < val.NumField(); i++ {
			field := val.Type().Field(i)
			tag := field.Tag.Get(tagName)
			if tag == "" {
				log.Info().Msgf("%T tag(%s) can not be empty", object, tagName)
				continue
			}

			value := val.Field(i).Interface()
			switch value.(type) {
			case []string:
				var eqValues []string
				for _, item := range value.([]string) {
					item = strings.ToLower(item)
					eqValues = append(eqValues, "'"+item+"'")
				}

				if len(eqValues) == 1 {
					queryWhere += " AND " + tag + " = " + eqValues[0]
				} else if len(eqValues) > 1 {
					queryWhere += " AND " + tag + " IN(" + strings.Join(eqValues, ",") + ")"
				}
			case string:
				if len(value.(string)) > 0 {
					queryWhere += " AND " + tag + " = '" + strings.ToLower(value.(string)) + "'"
				}
			}
		}
	}

	return queryWhere
}
