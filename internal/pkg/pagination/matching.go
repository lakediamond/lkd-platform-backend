package pagination

import (
	"strings"
)

func SortingType(value string) string {
	for _, item := range SORTING_PAGING_TYPES {
		if item == strings.ToLower(value) {
			if item == "des" {
				return "desc"
			}

			return item
		}
	}

	return "desc"
}

func MatchingFieldProposal(field string) (string, bool) {
	fields := make(map[string]string, 0)

	fields["id"] = "blockchain_id"
	fields["original_tokens_amount"] = "original_tokens"
	fields["created_on"] = "created_on"
	fields["price"] = "price"
	fields["status"] = "status"
	fields["withdrawn_on"] = "withdrawn_on"
	fields["withdrawn_tokens_amount"] = "withdrawn_amount"
	fields["upfront_queue_volume"] = "upfront_queue_volume"
	fields["tx_hash"] = "tx_hash"
	fields["remaining_tokens_amount"] = "balance"

	if value, ok := fields[field]; ok {
		return value, true
	}

	return "", false
}

func MatchingOrderFieldAllOrders(field string) (string, bool) {
	fields := make(map[string]string, 0)

	fields["id"] = "ord.id"
	fields["created_by"] = "ord.created_by"
	fields["created_on"] = "ord.created_on"
	fields["type"] = "ordt.name"
	fields["metadata"] = "ord.metadata"
	fields["token_price"] = "ord.price"
	fields["tokens_amount"] = "ord.token_amount"
	fields["eth_amount"] = "ord.eth_amount"
	fields["tx_hash"] = "ord.tx_hash"

	if value, ok := fields[field]; ok {
		return value, true
	}

	return "ord.id", false
}
