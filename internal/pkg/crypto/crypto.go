package crypto

import (
	"context"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io"
	"strconv"

	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/argon2"

	cloudkms "cloud.google.com/go/kms/apiv1"
	kmspb "google.golang.org/genproto/googleapis/cloud/kms/v1"
)

//Data contains all needed for Aragon2 crypto
type Data struct {
	Salt    []byte
	Time    uint32
	Memory  uint32
	Threads uint8
	KeyLen  uint32
}

var data Data

//InitData initializing crypto data
func InitData(salt, time, memory, threads, keyLen string) {
	if data.Salt != nil {
		return
	}

	data.Salt = []byte(salt)

	timeBuf, err := strconv.ParseUint(time, 10, 32)
	if err != nil {
		log.Error().Err(err).Msg("cannot convert CRYPTO_ITERATIONS")
	}
	data.Time = uint32(timeBuf)

	memoryBuf, err := strconv.ParseUint(memory, 10, 32)
	if err != nil {
		log.Error().Err(err).Msg("cannot convert CRYPTO_MEMORY")
	}
	data.Memory = uint32(memoryBuf)

	threadsBuf, err := strconv.ParseUint(threads, 10, 32)
	if err != nil {
		log.Error().Err(err).Msg("cannot convert CRYPTO_THREADS")
	}
	data.Threads = uint8(threadsBuf)

	keyLenBuf, err := strconv.ParseUint(keyLen, 10, 32)
	if err != nil {
		log.Error().Err(err).Msg("cannot convert CRYPTO_KEYLEN")
	}
	data.KeyLen = uint32(keyLenBuf)
}

//EncodePass encode string to Aragon2 string
func EncodePass(password string) string {
	key := argon2.IDKey([]byte(password), data.Salt, data.Time, data.Memory, data.Threads, data.KeyLen)

	return base64.StdEncoding.EncodeToString(key)
}

//Verify checks a difference between string and encoded string, returns true if equals
func Verify(password, encodePath string) bool {
	if EncodePass(password) == encodePath {
		return true
	}
	return false
}

func createHash(key string) string {
	hasher := md5.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}

func Encrypt(data []byte, password string) []byte {
	block, _ := aes.NewCipher([]byte(createHash(password)))
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err.Error())
	}
	ciphertext := gcm.Seal(nonce, nonce, data, nil)
	return ciphertext
}

func Decrypt(data []byte, password string) []byte {
	key := []byte(createHash(password))
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	nonceSize := gcm.NonceSize()
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		panic(err.Error())
	}
	return plaintext
}

// DecryptRSA will attempt to decrypt a given ciphertext with an 'RSA_DECRYPT_OAEP_2048_SHA256' private key.stored on Cloud KMS
// example keyName: "projects/PROJECT_ID/locations/global/keyRings/RING_ID/cryptoKeys/KEY_ID/cryptoKeyVersions/1"
func DecryptRSA(keyName string, ciphertext []byte) ([]byte, error) {
	ctx := context.Background()
	client, err := cloudkms.NewKeyManagementClient(ctx)
	if err != nil {
		return nil, err
	}

	// Build the request.
	req := kmspb.AsymmetricDecryptRequest{
		Name:       keyName,
		Ciphertext: ciphertext,
	}

	// Call the API.
	response, err := client.AsymmetricDecrypt(ctx, &req)
	if err != nil {
		return nil, fmt.Errorf("decryption request failed: %+v", err)
	}

	return response.Plaintext, nil
}
