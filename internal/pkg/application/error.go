package application

const (
	ErrNotValidEntity               = "Invalid entity_type"
	ErrGetCtxModel                  = "Failed to get model from context"
	ErrUserNotFound                 = "Can't find user with given email & password"
	ErrUserAlreadyExists            = "User with such email already exists"
	ErrUserCreation                 = "Failed to create user record"
	ErrAuthFailed                   = "Authorization failed"
	ErrUserProfileDecode            = "Failed to decode user profile record"
	ErrNoAuthHeader                 = "No auth header provided"
	ErrInvalidAuthToken             = "Invalid auth token"
	ErrRequestPayloadParse          = "Failed to parse request payload"
	ErrValidationSchemaResourceLoad = "Failed to load validation schema embedded resource file"
	ErrValidationSchemaLoad         = "Failed to load validation schema"
	ErrValidationFailed             = "Validation failed"
)
