package application

import (
	_context "context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
	"github.com/ory/hydra/sdk/go/hydra/swagger"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"
	"golang.org/x/oauth2"

	"lkd-platform-backend/internal/pkg/repo/models"
)

//GetApplicationContext returns application from http request
func GetApplicationContext(r *http.Request) *Application {
	return r.Context().Value(applicationKey).(*Application)
}

//GetRequestParams returns params from http request context
func GetRequestParams(r *http.Request) httprouter.Params {
	return httprouter.ParamsFromContext(r.Context())
}

//HandlingContext contains needed handling context
type HandlingContext struct {
	Configuration *Application
}

func SetUser(r *http.Request, user models.User) {
	context.Set(r, "user", user)
}

func GetUser(r *http.Request) models.User {
	return context.Get(r, "user").(models.User)
}

func UpdateSessionWithOAuthToken(w http.ResponseWriter, r *http.Request, token *oauth2.Token) error {
	app := GetApplicationContext(r)

	session, err := app.IAMClient.GetCookies().Get(r, "lkd-platform")
	if err != nil {
		log.Debug().Err(err).Msg("Failed to get session from request")
		return err
	}

	if token != nil {
		session.Values["oauth-token"] = token
		if err = session.Save(r, w); err != nil {
			log.Debug().Err(err).Msg("Failed to update session with OAuth token value")
			return err
		}
	}

	return nil
}

func SetSessionConnectionId(w http.ResponseWriter, r *http.Request, connectionId string) error {
	app := GetApplicationContext(r)

	session, err := app.IAMClient.GetCookies().Get(r, "lkd-platform")
	if err != nil {
		log.Debug().Err(err).Msg("Failed to get session from request")
		return err
	}

	if connectionId == "" {
		log.Debug().Err(err).Msg("Failed to update session connectionId not be empty")
		return errors.New("Failed to update session connectionId not be empty")
	}

	session.Values["connectionId"] = connectionId
	if err = session.Save(r, w); err != nil {
		log.Debug().Err(err).Msg("Failed to update session with OAuth token value")
		return err
	}

	return nil
}

func GetOAuthToken(r *http.Request) (*oauth2.Token, error) {
	app := GetApplicationContext(r)

	session, err := app.IAMClient.GetCookies().Get(r, "lkd-platform")
	if err != nil {
		log.Debug().Err(err).Msg("get session error")
		return nil, err
	}

	log.Debug().Msgf("session: %v", session)

	tokenSessionRecord := session.Values["oauth-token"]

	log.Debug().Msgf("tokenSessionRecord: %v", tokenSessionRecord)
	if tokenSessionRecord == nil {
		return nil, errors.New("Failed to get token from session cookie, [recovery]")
	}

	oauthToken := tokenSessionRecord.(*oauth2.Token)
	log.Debug().Msg(fmt.Sprintf("OAuth token from session, before refresh call - %v", oauthToken))

	logTokenDebugInfo(oauthToken, "token from cookie")

	if oauthToken.Valid() {
		return oauthToken, nil
	}

	connectionIdValue := session.Values["connectionId"]
	if connectionIdValue == nil {
		return nil, errors.New("connection id not found")
	}

	connectionId, err := uuid.FromString(connectionIdValue.(string))
	if err != nil {
		return nil, errors.New("cant parse connectionId")
	}

	authTokenLockedRecord, err := app.Repo.AuthToken.GetLockedRecord(&connectionId)
	if err != nil {
		return nil, errors.New("cant load oauth token from db")
	}
	defer app.Repo.AuthToken.UnlockRecord(authTokenLockedRecord)

	var dbToken oauth2.Token
	err = json.Unmarshal([]byte(authTokenLockedRecord.AuthToken.SerializedToken), &dbToken)
	if err != nil {
		return nil, errors.WithMessage(err, "cant deserialize token from db")
	}

	logTokenDebugInfo(&dbToken, "token from db")
	if dbToken.Valid() {
		return &dbToken, nil
	}

	newToken, err := RefreshOAuthToken(app, &dbToken)
	if err != nil {
		return nil, errors.WithMessage(err, "can`t refresh hydra token")
	}

	if !newToken.Valid() {
		return nil, fmt.Errorf("refresh call to Hydra for OAuth. Refreshed token %v is not valid, [recovery]", newToken)
	}

	serializedToken, err := json.Marshal(newToken)
	if err != nil {
		return nil, errors.WithMessage(err, "cant serialize new oauth token to save in db")
	}

	authTokenLockedRecord.AuthToken.SerializedToken = string(serializedToken)
	err = app.Repo.AuthToken.UpdateLockedRecord(authTokenLockedRecord)
	if err != nil {
		return nil, errors.WithMessage(err, "cant update new oauth token in db")
	}

	return newToken, nil
}

//RefreshOAuthToken refreshing Hydra oauth token
func RefreshOAuthToken(app *Application, token *oauth2.Token) (*oauth2.Token, error) {
	logTokenDebugInfo(token, "Old token")

	token, err := app.IAMClient.OAuth2Config().TokenSource(_context.TODO(), token).Token()

	if err != nil {
		log.Debug().Err(err).Msg("Token refresh")
		return nil, err
	}

	logTokenDebugInfo(token, "New token")

	return token, nil
}

func logTokenDebugInfo(token *oauth2.Token, msg string) {
	log.Debug().Str("AccessToken", token.AccessToken).Msg(msg)
	log.Debug().Str("RefreshToken", token.RefreshToken).Msg(msg)
	log.Debug().Str("Type", token.TokenType).Msg(msg)
	log.Debug().Time("Token expiration at", token.Expiry).Msg(msg)
	log.Debug().Bool("Token valid", token.Valid()).Msg(msg)
}

func SetTokenDetails(r *http.Request, oauthToken *oauth2.Token) (*swagger.OAuth2TokenIntrospection, error) {
	app := GetApplicationContext(r)
	oauthTokenDetails, _, err := app.IAMClient.GetOAuthClient().AdminApi.IntrospectOAuth2Token(oauthToken.AccessToken, "")
	if err != nil {
		return nil, err
	}

	context.Set(r, "tokenDetails", oauthTokenDetails)
	return oauthTokenDetails, nil
}

func GetTokenDetails(r *http.Request) *swagger.OAuth2TokenIntrospection {
	return context.Get(r, "tokenDetails").(*swagger.OAuth2TokenIntrospection)
}
