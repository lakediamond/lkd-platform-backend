package application

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/crypto"
	"lkd-platform-backend/internal/pkg/etherPkg"
	"lkd-platform-backend/internal/pkg/googleCloud"
	"lkd-platform-backend/internal/pkg/mailjet"
	"lkd-platform-backend/internal/pkg/oauth"
	"lkd-platform-backend/internal/pkg/phoneNumber"
	"lkd-platform-backend/internal/pkg/pipeDrive"
	"lkd-platform-backend/internal/pkg/repo"
	"lkd-platform-backend/internal/pkg/sms"
	"lkd-platform-backend/pkg/environment"

	"lkd-platform-backend/pkg/ws/ws-server"
)

type CronConfig struct {
	ProjectId         string
	TopicName         string
	ConcurrencyWorker int
	DurationTasks     int64
}

// Config as application Config type
type Config struct {
	Env       string
	LogLevel  string
	Port      string
	JwtSalt   string
	JwtExpTTL int

	ErrorsMap          map[string]int
	CORSStatusCode     int
	CORSAllowedHeaders string
	CORSAllowedOrigins string
	CORSAllowedMethods string

	LkdPlatformFrontendDNS                 *url.URL
	LkdPlatformFrontendRecoveryPasswordUrl string
	IOContractAddress                      string
	EthereumPrivateKey                     string

	MaxVerificationAttempts int
	MaxVerificationPerDay   int

	KYCRootURL  string
	AuthRetries *AuthRetries

	AuthUser     string
	AuthPassword string

	CRON *CronConfig
}

//Application is a main structure. It contains all needed variables
type Application struct {
	Config Config

	Repo                 *repo.Repo
	Mailjet              *mailjet.Sender
	Storage              *Storage
	PipeDrive            pipeDrive.ClientInterface
	EthClient            *etherPkg.PlatformEthereumApplication
	routes               Routes
	IAMClient            oauth.IdentityManagementClient
	SMSSender            sms.Sender
	PhoneNumberValidator phoneNumber.Validator
	GC                   *googleCloud.GCApplication
	WSClient             *ws_server.Server
}

type key int

const (
	applicationKey key = 0
)

//Route contains route variables
type Route struct {
	Alias   string
	Method  string
	Path    string
	Handler http.Handler
}

// Routes represents list of routes
type Routes []Route

func NewCronConfig(projectId string, topicName string, concurrencyWorker int, durationTasks int64) *CronConfig {
	return &CronConfig{
		ProjectId:         projectId,
		TopicName:         topicName,
		ConcurrencyWorker: concurrencyWorker,
		DurationTasks:     durationTasks,
	}
}

// NewApplication return application instance
func NewApplication() *Application {
	config := compileConfig()
	app := Application{Config: config}
	app.SetLoggerLevel()

	return &app
}

//WithApplicationContext store context into each http request
func (app *Application) WithApplicationContext(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), applicationKey, app)
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}

//MountRoutes mounting all needed application routes
func (app *Application) MountRoutes(routes Routes) {
	app.routes = routes
}

//FindRoute returns http path to requested alias
func (app *Application) FindRoute(alias string) string {
	var found string
	var supportedAliases []string
	for _, route := range app.routes {
		supportedAliases = append(supportedAliases, route.Alias)
		if route.Alias == alias {
			found = route.Path
		}
	}
	if found == "" {
		panic(fmt.Errorf("route with alias %s not found. Supported aliases: %s", alias, supportedAliases))
	}
	return found
}

func compileConfig() Config {
	myEnv := environment.GetEnv()

	crypto.InitData(
		myEnv["CRYPTO_SALT"],
		myEnv["CRYPTO_ITERATIONS"],
		myEnv["CRYPTO_MEMORY"],
		myEnv["CRYPTO_PARALLELISM"],
		myEnv["CRYPTO_KEYLEN"],
	)

	statusCode, _ := strconv.ParseInt(myEnv["CORSStatusCode"], 10, 32)
	maxVerificationAttempts, _ := strconv.ParseInt(myEnv["MAX_VERIFCATION_ATTEMPS"], 10, 32)
	maxVerificationPerDay, _ := strconv.ParseInt(myEnv["MAX_VERFICATIONS_PER_DAY"], 10, 32)

	authRecoveryTTLMin, err := strconv.Atoi(myEnv["AUTH_RECOVERY_TTL_MIN"])
	if err != nil {
		log.Fatal().Err(err).Msg("$AUTH_RECOVERY_TTL_MIN can not validate format")
	}

	lkdPlatformFrontendURL, err := url.Parse(myEnv["LKD_PLATFORM_FRONTEND_DNS"])
	if err != nil {
		log.Fatal().Err(err).Msg("Cant parse LkdPlatformFrontendDNS url")
	}

	return Config{
		Env:                                    myEnv["ENV"],
		LogLevel:                               myEnv["LOG_LEVEL"],
		Port:                                   myEnv["PORT"],
		LkdPlatformFrontendDNS:                 lkdPlatformFrontendURL,
		LkdPlatformFrontendRecoveryPasswordUrl: myEnv["LKD_PLATFORM_FRONTEND_RECOVERY_PASSWORD_URL"],

		JwtSalt:            myEnv["JWT_SALT"],
		JwtExpTTL:          authRecoveryTTLMin,
		CORSAllowedHeaders: myEnv["CORSAllowedHeaders"],
		CORSAllowedOrigins: myEnv["CORSAllowedOrigins"],
		CORSAllowedMethods: myEnv["CORSAllowedMethods"],
		CORSStatusCode:     int(statusCode),

		IOContractAddress:  myEnv["ETHEREUM_IO_CONTRACT_ADDRESS"],
		EthereumPrivateKey: myEnv["ETHEREUM_PRIVATE_KEY"],

		MaxVerificationAttempts: int(maxVerificationAttempts),
		MaxVerificationPerDay:   int(maxVerificationPerDay),

		KYCRootURL: myEnv["KYC_ROOT_URL"],

		AuthUser:     myEnv["BASIC_AUTH_USERNAME"],
		AuthPassword: myEnv["BASIC_AUTH_PASSWORD"],
	}
}

func (app *Application) initErrorsMap() {
	app.Config.ErrorsMap = make(map[string]int)
	app.Config.ErrorsMap[ErrUserNotFound] = http.StatusNotFound
	app.Config.ErrorsMap[ErrAuthFailed] = http.StatusUnauthorized
	app.Config.ErrorsMap[ErrNoAuthHeader] = http.StatusUnauthorized
	app.Config.ErrorsMap[ErrInvalidAuthToken] = http.StatusUnauthorized
}

//SetLoggerLevel setting logger level
func (app *Application) SetLoggerLevel() {
	level, err := zerolog.ParseLevel(app.Config.LogLevel)
	if err != nil {
		level = zerolog.DebugLevel
	}
	zerolog.SetGlobalLevel(level)
	log.Info().Msgf("Log level set to %s", level.String())
}
