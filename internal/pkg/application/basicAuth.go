package application

import (
	"net/http"
)

func (app *Application) BasicAuth(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user, pass, _ := r.BasicAuth()
		if !checkAuth(app, user, pass) {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		h.ServeHTTP(w, r)
	})
}

func checkAuth(app *Application, user, password string) bool {
	if user != app.Config.AuthUser || password != app.Config.AuthPassword {
		return false
	}

	return true
}
