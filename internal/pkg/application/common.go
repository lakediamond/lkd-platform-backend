package application

import (
	"crypto/rand"
	"encoding/base64"
	"net/http"

	"github.com/gorilla/context"
)

func getRequestMeta(r *http.Request) (string, string) {
	if v, ok := context.GetOk(r, nil); ok {
		if id, ok := v.(string); ok {
			return id, r.URL.String()
		}
	}
	return "", r.URL.String()
}

// RequestID Generates unique request_id for each request received
func RequestID(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		b := make([]byte, 8)
		rand.Read(b)
		base64ID := base64.URLEncoding.EncodeToString(b)
		context.Set(r, nil, base64ID)
		next.ServeHTTP(w, r)
		// Clear the context at the end of the request lifetime
		context.Clear(r)
	}
	return http.HandlerFunc(fn)
}
func (app *Application) HandleCORS(w http.ResponseWriter, r *http.Request) {
	app.HandleAPIResponse(r, w, http.StatusCreated, nil)
}

// CORS handles CORS specifics
func (app *Application) CORS(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", app.Config.CORSAllowedOrigins)
		w.Header().Set("Access-Control-Expose-Headers", "Location")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Methods", app.Config.CORSAllowedMethods)
		w.Header().Set("Access-Control-Allow-Headers", app.Config.CORSAllowedHeaders)
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}
