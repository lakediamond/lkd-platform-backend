package application

import (
	"context"
	"encoding/base64"
	"fmt"
	"lkd-platform-backend/internal/pkg/repo/models"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"

	"cloud.google.com/go/storage"
	"golang.org/x/oauth2/google"
	iam "google.golang.org/api/iam/v1"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

const (
	GCP_IAM_API_ROOT     = "https://iam.googleapis.com"
	GCP_STORAGE_API_ROOT = "storage.googleapis.com"
	GCP_OAUTH_IAM_SCOPE  = "https://www.googleapis.com/auth/iam"
)

type Storage struct {
	gcpProjectID           string
	gcpServiceAccountEmail string
	kycBucket              *storage.BucketHandle
	kycBucketName          string
	Client                 *storage.Client
}

func NewStorage(deploymentProjectID, kycBucketName string) *Storage {
	ctx := context.Background()

	cr, err := google.FindDefaultCredentials(ctx, storage.ScopeReadWrite)
	if err != nil {
		log.Fatal().Err(err).Msg(err.Error())
	}

	cl, err := storage.NewClient(ctx, option.WithCredentials(cr))
	if err != nil {
		log.Fatal().Err(err).Msg(err.Error())
	}

	bucket := cl.Bucket(kycBucketName)
	return &Storage{
		gcpProjectID:           deploymentProjectID,
		gcpServiceAccountEmail: strings.Split(deploymentProjectID, "/")[3],
		kycBucket:              bucket,
		kycBucketName:          kycBucketName,
		Client:                 cl,
	}
}

func signedURLTemplate(objectPath string, expiresAt int64) string {
	imageName := strings.Split(objectPath, "/")[2]
	imageNameTokens := strings.Split(imageName, ".")
	if imageNameTokens[1] == "jpg" {
		imageNameTokens[1] = "jpeg"
	}

	log.Debug().Msgf("content type - image/%s", imageNameTokens[1])
	return fmt.Sprintf("%s\n%s\n%s\n%d\n%s\n%s",
		http.MethodPut,
		"",
		fmt.Sprintf("image/%s", imageNameTokens[1]),
		expiresAt,
		"",
		objectPath)
}

func (s *Storage) NewSignedURL(user *models.User, imageName string) (string, error) {
	ctx := context.Background()
	imageFullPath := fmt.Sprintf("%s/identity-scans/%s", user.ID.String(), imageName)
	expiresAt := time.Now().UTC().Add(30 * time.Minute).Unix()
	digest := signedURLTemplate(imageFullPath, expiresAt)
	log.Debug().Msgf("Digest to be signed - ", digest)

	httpClient, err := google.DefaultClient(ctx, GCP_OAUTH_IAM_SCOPE)
	if err != nil {
		log.Error().Msgf("failed to get default http client - %s", err.Error())
		return "", err
	}

	iamBaseService, err := iam.New(httpClient)
	if err != nil {
		log.Error().Msgf("failed to get base IAM service - %s", err.Error())
		return "", err
	}
	iamBaseService.BasePath = GCP_IAM_API_ROOT

	log.Debug().Msgf("iamBaseService: %v", iamBaseService)

	digestInBase64 := base64.URLEncoding.EncodeToString([]byte(digest))
	iamProjectServiceAccountService := iam.NewProjectsServiceAccountsService(iamBaseService)

	signBlobRequest := &iam.SignBlobRequest{
		BytesToSign: digestInBase64,
	}

	signBlobResponse, err := iamProjectServiceAccountService.SignBlob(s.gcpProjectID, signBlobRequest).Do()
	if err != nil {
		log.Error().Msgf("failed to call SignBlob - %v - %s", signBlobResponse, err.Error())
		return "", err
	}

	signedURL, err := url.Parse(fmt.Sprintf("https://%s/%s/%s", GCP_STORAGE_API_ROOT, s.kycBucketName, imageFullPath))
	if err != nil {
		return "", err
	}

	log.Debug().Msgf("signedURL: %s", signedURL.String())

	signedURLParams := signedURL.Query()
	signedURLParams.Set("GoogleAccessId", s.gcpServiceAccountEmail)
	signedURLParams.Set("Expires", strconv.FormatInt(expiresAt, 10))
	signedURLParams.Set("Signature", signBlobResponse.Signature)
	signedURL.RawQuery = signedURLParams.Encode()

	url := signedURL.String()
	url = strings.Replace(url, s.kycBucketName+"/", "", 1)
	url = strings.Replace(url, "https://", "https://"+s.kycBucketName+".", 1)

	return url, nil
}

func (s *Storage) GetKYCImages(user *models.User) ([]*storage.ObjectAttrs, error) {
	var items []*storage.ObjectAttrs

	query := &storage.Query{
		Prefix:    fmt.Sprintf("%s/identity-scans/", user.ID.String()),
		Delimiter: "/"}
	it := s.kycBucket.Objects(context.Background(), query)

	for {
		imageAttributes, err := it.Next()
		if err != nil {
			if err == iterator.Done {
				break
			}
			return nil, err
		}
		log.Debug().Msgf("imageAttributes.Name: %s", imageAttributes.Name)

		items = append(items, imageAttributes)
	}
	return items, nil
}

func (s *Storage) InvalidateImage(user *models.User, imageAttributes *storage.ObjectAttrs) error {
	srcObject := s.kycBucket.Object(imageAttributes.Name)
	dstObjectName := fmt.Sprintf("%s/identity-scans/invalid/%s-%s", user.ID.String(), time.Now().UTC().Format("2006-01-02 15:04:05"), strings.Split(imageAttributes.Name, "/")[2])

	log.Debug().Msgf("srcObject: %v, dstObjectName: %v", srcObject, dstObjectName)

	_, err := s.kycBucket.Object(dstObjectName).CopierFrom(srcObject).Run(context.Background())
	if err != nil {
		return err
	}

	err = srcObject.Delete(context.Background())
	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) GetImageReader(user *models.User, imageName string) (*storage.Reader, error) {
	obj := fmt.Sprintf("%s/identity-scans/%s", user.ID.String(), imageName)
	log.Debug().Msgf("Getting object from bucket: %s, object: %s", s.kycBucketName, obj)
	objectHandle := s.kycBucket.Object(obj)
	return objectHandle.NewReader(context.Background())
}

func (s *Storage) GetImageMetaInfo(user *models.User, imageName string) (*storage.ObjectAttrs, error) {
	query := &storage.Query{
		Prefix:    fmt.Sprintf("%s/identity-scans/", user.ID.String()),
		Delimiter: "/"}

	it := s.kycBucket.Objects(context.Background(), query)

	for {
		imageAttributes, err := it.Next()
		if err != nil {
			if err == iterator.Done {
				break
			}
			return nil, err
		}

		if strings.Contains(imageAttributes.Name, imageName) {
			return imageAttributes, nil
		}
	}
	return nil, nil
}
