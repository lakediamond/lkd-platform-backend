package application

import (
	"encoding/json"
	"net/http"
)

//HandleAPIResponse handling API response
func (app *Application) HandleAPIResponse(r *http.Request, rw http.ResponseWriter, statusCode int, result interface{}) {
	url, reqID := getRequestMeta(r)
	if statusCode == 0 {
		statusCode = http.StatusInternalServerError
	}
	apiResponseMarshaled, _ := json.Marshal(GetAPIResponseTemplate(statusCode, url, reqID, result))
	app.Write(r, rw, statusCode, apiResponseMarshaled)
}

//Write writing a response to http.ResponseWriter
func (app *Application) Write(r *http.Request, rw http.ResponseWriter, statusCode int, payload []byte) {
	rw.Header().Set("Content-Type", "application/json")
	if r.Method == "OPTIONS" {
		statusCode = app.Config.CORSStatusCode
	}
	rw.WriteHeader(statusCode)
	rw.Write(payload)
}
