package application

//APIResponse contains the whole API response
type APIResponse struct {
	*APIResponseMeta   `json:"meta"`
	*APIResponseData   `json:"data,omitempty"`
	*APIResponseErrors `json:"error,omitempty"`
}

//APIResponseMeta contains API response metadata
type APIResponseMeta struct {
	Url       string `json:"url"`
	Code      int    `json:"code"`
	RequestID string `json:"request_id"`
}

//APIResponseData contains API response data
type APIResponseData struct {
	Result interface{} `json:"result,omitempty"`
}

//APIResponseErrors contains API response errors
type APIResponseErrors struct {
	Type             string             `json:"type"`
	InvalidEntity    string             `json:"invalid_entity"`
	ValidationErrors []APIResponseError `json:"validation_errors,omitempty"`
}

//APIResponseError contains API response error
type APIResponseError struct {
	EntryType string                       `json:"entry_type"`
	Entry     string                       `json:"entry"`
	Rules     *[]APIResponseValidationRule `json:"rules,omitempty"`
}

//APIResponseValidationRule contains API response validation rule
type APIResponseValidationRule struct {
	Rule        string
	Params      string
	Description string
}

func getAPIMetaTemplate(statusCode int, url string, requestID string) *APIResponseMeta {
	return &APIResponseMeta{
		Url:       url,
		Code:      statusCode,
		RequestID: requestID,
	}
}

//GetAPIResponseTemplate returns basic API response template
func GetAPIResponseTemplate(statusCode int, url string, requestID string, result interface{}) *APIResponse {
	return &APIResponse{
		APIResponseMeta: getAPIMetaTemplate(statusCode, url, requestID),
		APIResponseData: &APIResponseData{
			Result: result,
		},
	}
}
