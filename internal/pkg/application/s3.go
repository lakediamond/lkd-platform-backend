package application

import (
	"github.com/minio/minio-go"
)

type S3 struct {
	BucketName      string
	HostInternalURL string
	HostExternalURL string
	AccessKey       string
	SecretKey       string
	SSL             bool
	InternalClient  *minio.Client
	ExternalClient  *minio.Client
}
