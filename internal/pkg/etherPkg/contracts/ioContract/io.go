// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package ioContract

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = abi.U256
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// IOABI is the input ABI used to generate the binding from.
const IOABI = "[{\"constant\":true,\"inputs\":[],\"name\":\"backendAddress\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"minProposalTokenAmount\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"subOwnerColumnKey\",\"outputs\":[{\"name\":\"\",\"type\":\"bytes32\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"whiteListKey\",\"outputs\":[{\"name\":\"\",\"type\":\"bytes32\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"isOwner\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"lakeDiamondStorage\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"name\":\"storageAddress\",\"type\":\"address\"},{\"name\":\"backend\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"subOwner\",\"type\":\"address\"}],\"name\":\"SubOwnerAdded\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"subOwner\",\"type\":\"address\"}],\"name\":\"SubOwnerRemoved\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"description\",\"type\":\"string\"}],\"name\":\"OrderTypeSet\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"from\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"price\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"tokenAmount\",\"type\":\"uint256\"}],\"name\":\"ProposalCreated\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"withdrownBy\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"tokenAmount\",\"type\":\"uint256\"}],\"name\":\"ProposalWithdrawn\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"orderId\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"tokenAmount\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"weiAmount\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"isActive\",\"type\":\"bool\"}],\"name\":\"ProposalCompleted\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"orderId\",\"type\":\"uint256\"},{\"indexed\":true,\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"typeId\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"price\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"weiAmount\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"tokenAmount\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"exchangeRate\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"metadata\",\"type\":\"string\"}],\"name\":\"OrderAdded\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":false,\"name\":\"rest\",\"type\":\"uint256\"}],\"name\":\"OrderProcessed\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"account\",\"type\":\"address\"}],\"name\":\"WhiteListAddressAdded\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"account\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"allowedTokenAmount\",\"type\":\"uint256\"}],\"name\":\"WhitelistedAmountChanged\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"account\",\"type\":\"address\"}],\"name\":\"WhiteListAddressRemoved\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"constant\":false,\"inputs\":[{\"name\":\"newAddress\",\"type\":\"address\"}],\"name\":\"transferTokensToNewLakeDiamondAddress\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"addresses\",\"type\":\"address[]\"}],\"name\":\"addSubOwners\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"addresses\",\"type\":\"address[]\"}],\"name\":\"removeSubOwners\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"typeId\",\"type\":\"uint256\"},{\"name\":\"metadata\",\"type\":\"string\"},{\"name\":\"price\",\"type\":\"uint256\"},{\"name\":\"tokenAmount\",\"type\":\"uint256\"},{\"name\":\"exchangeRate\",\"type\":\"uint256\"}],\"name\":\"createOrder\",\"outputs\":[],\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"account\",\"type\":\"address\"},{\"name\":\"allowedTokenAmount\",\"type\":\"uint256\"}],\"name\":\"addToWhitelist\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"accounts\",\"type\":\"address[]\"},{\"name\":\"allowedTokenAmounts\",\"type\":\"uint256[]\"}],\"name\":\"batchAddToWhitelist\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"account\",\"type\":\"address\"},{\"name\":\"allowedTokenAmount\",\"type\":\"uint256\"}],\"name\":\"increaseAllowance\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"account\",\"type\":\"address\"}],\"name\":\"removeFromWhitelist\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"price\",\"type\":\"uint256\"},{\"name\":\"tokenAmount\",\"type\":\"uint256\"},{\"name\":\"startPositionId\",\"type\":\"uint256\"}],\"name\":\"createProposal\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"id\",\"type\":\"uint256\"}],\"name\":\"withdrawProposal\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"id\",\"type\":\"uint256\"},{\"name\":\"description\",\"type\":\"string\"}],\"name\":\"setOrderType\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"newAddress\",\"type\":\"address\"}],\"name\":\"setBackendAddress\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"account\",\"type\":\"address\"}],\"name\":\"getAllowedTokenAmount\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"subOwnerAddress\",\"type\":\"address\"}],\"name\":\"getIsSubOwner\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"setMinProposalTokenAmount\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]"

// IO is an auto generated Go binding around an Ethereum contract.
type IO struct {
	IOCaller     // Read-only binding to the contract
	IOTransactor // Write-only binding to the contract
	IOFilterer   // Log filterer for contract events
}

// IOCaller is an auto generated read-only Go binding around an Ethereum contract.
type IOCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IOTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IOTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IOFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IOFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IOSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IOSession struct {
	Contract     *IO               // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IOCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IOCallerSession struct {
	Contract *IOCaller     // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// IOTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IOTransactorSession struct {
	Contract     *IOTransactor     // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IORaw is an auto generated low-level Go binding around an Ethereum contract.
type IORaw struct {
	Contract *IO // Generic contract binding to access the raw methods on
}

// IOCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IOCallerRaw struct {
	Contract *IOCaller // Generic read-only contract binding to access the raw methods on
}

// IOTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IOTransactorRaw struct {
	Contract *IOTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIO creates a new instance of IO, bound to a specific deployed contract.
func NewIO(address common.Address, backend bind.ContractBackend) (*IO, error) {
	contract, err := bindIO(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IO{IOCaller: IOCaller{contract: contract}, IOTransactor: IOTransactor{contract: contract}, IOFilterer: IOFilterer{contract: contract}}, nil
}

// NewIOCaller creates a new read-only instance of IO, bound to a specific deployed contract.
func NewIOCaller(address common.Address, caller bind.ContractCaller) (*IOCaller, error) {
	contract, err := bindIO(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IOCaller{contract: contract}, nil
}

// NewIOTransactor creates a new write-only instance of IO, bound to a specific deployed contract.
func NewIOTransactor(address common.Address, transactor bind.ContractTransactor) (*IOTransactor, error) {
	contract, err := bindIO(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IOTransactor{contract: contract}, nil
}

// NewIOFilterer creates a new log filterer instance of IO, bound to a specific deployed contract.
func NewIOFilterer(address common.Address, filterer bind.ContractFilterer) (*IOFilterer, error) {
	contract, err := bindIO(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IOFilterer{contract: contract}, nil
}

// bindIO binds a generic wrapper to an already deployed contract.
func bindIO(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IOABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IO *IORaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _IO.Contract.IOCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IO *IORaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IO.Contract.IOTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IO *IORaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IO.Contract.IOTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IO *IOCallerRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _IO.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IO *IOTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IO.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IO *IOTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IO.Contract.contract.Transact(opts, method, params...)
}

// BackendAddress is a free data retrieval call binding the contract method 0x0d72d57f.
//
// Solidity: function backendAddress() constant returns(address)
func (_IO *IOCaller) BackendAddress(opts *bind.CallOpts) (common.Address, error) {
	var (
		ret0 = new(common.Address)
	)
	out := ret0
	err := _IO.contract.Call(opts, out, "backendAddress")
	return *ret0, err
}

// BackendAddress is a free data retrieval call binding the contract method 0x0d72d57f.
//
// Solidity: function backendAddress() constant returns(address)
func (_IO *IOSession) BackendAddress() (common.Address, error) {
	return _IO.Contract.BackendAddress(&_IO.CallOpts)
}

// BackendAddress is a free data retrieval call binding the contract method 0x0d72d57f.
//
// Solidity: function backendAddress() constant returns(address)
func (_IO *IOCallerSession) BackendAddress() (common.Address, error) {
	return _IO.Contract.BackendAddress(&_IO.CallOpts)
}

// GetAllowedTokenAmount is a free data retrieval call binding the contract method 0xb891940f.
//
// Solidity: function getAllowedTokenAmount(account address) constant returns(uint256)
func (_IO *IOCaller) GetAllowedTokenAmount(opts *bind.CallOpts, account common.Address) (*big.Int, error) {
	var (
		ret0 = new(*big.Int)
	)
	out := ret0
	err := _IO.contract.Call(opts, out, "getAllowedTokenAmount", account)
	return *ret0, err
}

// GetAllowedTokenAmount is a free data retrieval call binding the contract method 0xb891940f.
//
// Solidity: function getAllowedTokenAmount(account address) constant returns(uint256)
func (_IO *IOSession) GetAllowedTokenAmount(account common.Address) (*big.Int, error) {
	return _IO.Contract.GetAllowedTokenAmount(&_IO.CallOpts, account)
}

// GetAllowedTokenAmount is a free data retrieval call binding the contract method 0xb891940f.
//
// Solidity: function getAllowedTokenAmount(account address) constant returns(uint256)
func (_IO *IOCallerSession) GetAllowedTokenAmount(account common.Address) (*big.Int, error) {
	return _IO.Contract.GetAllowedTokenAmount(&_IO.CallOpts, account)
}

// GetIsSubOwner is a free data retrieval call binding the contract method 0x4c9d2ceb.
//
// Solidity: function getIsSubOwner(subOwnerAddress address) constant returns(bool)
func (_IO *IOCaller) GetIsSubOwner(opts *bind.CallOpts, subOwnerAddress common.Address) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _IO.contract.Call(opts, out, "getIsSubOwner", subOwnerAddress)
	return *ret0, err
}

// GetIsSubOwner is a free data retrieval call binding the contract method 0x4c9d2ceb.
//
// Solidity: function getIsSubOwner(subOwnerAddress address) constant returns(bool)
func (_IO *IOSession) GetIsSubOwner(subOwnerAddress common.Address) (bool, error) {
	return _IO.Contract.GetIsSubOwner(&_IO.CallOpts, subOwnerAddress)
}

// GetIsSubOwner is a free data retrieval call binding the contract method 0x4c9d2ceb.
//
// Solidity: function getIsSubOwner(subOwnerAddress address) constant returns(bool)
func (_IO *IOCallerSession) GetIsSubOwner(subOwnerAddress common.Address) (bool, error) {
	return _IO.Contract.GetIsSubOwner(&_IO.CallOpts, subOwnerAddress)
}

// IsOwner is a free data retrieval call binding the contract method 0x8f32d59b.
//
// Solidity: function isOwner() constant returns(bool)
func (_IO *IOCaller) IsOwner(opts *bind.CallOpts) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _IO.contract.Call(opts, out, "isOwner")
	return *ret0, err
}

// IsOwner is a free data retrieval call binding the contract method 0x8f32d59b.
//
// Solidity: function isOwner() constant returns(bool)
func (_IO *IOSession) IsOwner() (bool, error) {
	return _IO.Contract.IsOwner(&_IO.CallOpts)
}

// IsOwner is a free data retrieval call binding the contract method 0x8f32d59b.
//
// Solidity: function isOwner() constant returns(bool)
func (_IO *IOCallerSession) IsOwner() (bool, error) {
	return _IO.Contract.IsOwner(&_IO.CallOpts)
}

// LakeDiamondStorage is a free data retrieval call binding the contract method 0xe851ddd2.
//
// Solidity: function lakeDiamondStorage() constant returns(address)
func (_IO *IOCaller) LakeDiamondStorage(opts *bind.CallOpts) (common.Address, error) {
	var (
		ret0 = new(common.Address)
	)
	out := ret0
	err := _IO.contract.Call(opts, out, "lakeDiamondStorage")
	return *ret0, err
}

// LakeDiamondStorage is a free data retrieval call binding the contract method 0xe851ddd2.
//
// Solidity: function lakeDiamondStorage() constant returns(address)
func (_IO *IOSession) LakeDiamondStorage() (common.Address, error) {
	return _IO.Contract.LakeDiamondStorage(&_IO.CallOpts)
}

// LakeDiamondStorage is a free data retrieval call binding the contract method 0xe851ddd2.
//
// Solidity: function lakeDiamondStorage() constant returns(address)
func (_IO *IOCallerSession) LakeDiamondStorage() (common.Address, error) {
	return _IO.Contract.LakeDiamondStorage(&_IO.CallOpts)
}

// MinProposalTokenAmount is a free data retrieval call binding the contract method 0x2fd3b5b7.
//
// Solidity: function minProposalTokenAmount() constant returns(uint256)
func (_IO *IOCaller) MinProposalTokenAmount(opts *bind.CallOpts) (*big.Int, error) {
	var (
		ret0 = new(*big.Int)
	)
	out := ret0
	err := _IO.contract.Call(opts, out, "minProposalTokenAmount")
	return *ret0, err
}

// MinProposalTokenAmount is a free data retrieval call binding the contract method 0x2fd3b5b7.
//
// Solidity: function minProposalTokenAmount() constant returns(uint256)
func (_IO *IOSession) MinProposalTokenAmount() (*big.Int, error) {
	return _IO.Contract.MinProposalTokenAmount(&_IO.CallOpts)
}

// MinProposalTokenAmount is a free data retrieval call binding the contract method 0x2fd3b5b7.
//
// Solidity: function minProposalTokenAmount() constant returns(uint256)
func (_IO *IOCallerSession) MinProposalTokenAmount() (*big.Int, error) {
	return _IO.Contract.MinProposalTokenAmount(&_IO.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() constant returns(address)
func (_IO *IOCaller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var (
		ret0 = new(common.Address)
	)
	out := ret0
	err := _IO.contract.Call(opts, out, "owner")
	return *ret0, err
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() constant returns(address)
func (_IO *IOSession) Owner() (common.Address, error) {
	return _IO.Contract.Owner(&_IO.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() constant returns(address)
func (_IO *IOCallerSession) Owner() (common.Address, error) {
	return _IO.Contract.Owner(&_IO.CallOpts)
}

// SubOwnerColumnKey is a free data retrieval call binding the contract method 0x30f4f8df.
//
// Solidity: function subOwnerColumnKey() constant returns(bytes32)
func (_IO *IOCaller) SubOwnerColumnKey(opts *bind.CallOpts) ([32]byte, error) {
	var (
		ret0 = new([32]byte)
	)
	out := ret0
	err := _IO.contract.Call(opts, out, "subOwnerColumnKey")
	return *ret0, err
}

// SubOwnerColumnKey is a free data retrieval call binding the contract method 0x30f4f8df.
//
// Solidity: function subOwnerColumnKey() constant returns(bytes32)
func (_IO *IOSession) SubOwnerColumnKey() ([32]byte, error) {
	return _IO.Contract.SubOwnerColumnKey(&_IO.CallOpts)
}

// SubOwnerColumnKey is a free data retrieval call binding the contract method 0x30f4f8df.
//
// Solidity: function subOwnerColumnKey() constant returns(bytes32)
func (_IO *IOCallerSession) SubOwnerColumnKey() ([32]byte, error) {
	return _IO.Contract.SubOwnerColumnKey(&_IO.CallOpts)
}

// WhiteListKey is a free data retrieval call binding the contract method 0x4bfae20d.
//
// Solidity: function whiteListKey() constant returns(bytes32)
func (_IO *IOCaller) WhiteListKey(opts *bind.CallOpts) ([32]byte, error) {
	var (
		ret0 = new([32]byte)
	)
	out := ret0
	err := _IO.contract.Call(opts, out, "whiteListKey")
	return *ret0, err
}

// WhiteListKey is a free data retrieval call binding the contract method 0x4bfae20d.
//
// Solidity: function whiteListKey() constant returns(bytes32)
func (_IO *IOSession) WhiteListKey() ([32]byte, error) {
	return _IO.Contract.WhiteListKey(&_IO.CallOpts)
}

// WhiteListKey is a free data retrieval call binding the contract method 0x4bfae20d.
//
// Solidity: function whiteListKey() constant returns(bytes32)
func (_IO *IOCallerSession) WhiteListKey() ([32]byte, error) {
	return _IO.Contract.WhiteListKey(&_IO.CallOpts)
}

// AddSubOwners is a paid mutator transaction binding the contract method 0x8d1fd1af.
//
// Solidity: function addSubOwners(addresses address[]) returns()
func (_IO *IOTransactor) AddSubOwners(opts *bind.TransactOpts, addresses []common.Address) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "addSubOwners", addresses)
}

// AddSubOwners is a paid mutator transaction binding the contract method 0x8d1fd1af.
//
// Solidity: function addSubOwners(addresses address[]) returns()
func (_IO *IOSession) AddSubOwners(addresses []common.Address) (*types.Transaction, error) {
	return _IO.Contract.AddSubOwners(&_IO.TransactOpts, addresses)
}

// AddSubOwners is a paid mutator transaction binding the contract method 0x8d1fd1af.
//
// Solidity: function addSubOwners(addresses address[]) returns()
func (_IO *IOTransactorSession) AddSubOwners(addresses []common.Address) (*types.Transaction, error) {
	return _IO.Contract.AddSubOwners(&_IO.TransactOpts, addresses)
}

// AddToWhitelist is a paid mutator transaction binding the contract method 0x214405fc.
//
// Solidity: function addToWhitelist(account address, allowedTokenAmount uint256) returns()
func (_IO *IOTransactor) AddToWhitelist(opts *bind.TransactOpts, account common.Address, allowedTokenAmount *big.Int) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "addToWhitelist", account, allowedTokenAmount)
}

// AddToWhitelist is a paid mutator transaction binding the contract method 0x214405fc.
//
// Solidity: function addToWhitelist(account address, allowedTokenAmount uint256) returns()
func (_IO *IOSession) AddToWhitelist(account common.Address, allowedTokenAmount *big.Int) (*types.Transaction, error) {
	return _IO.Contract.AddToWhitelist(&_IO.TransactOpts, account, allowedTokenAmount)
}

// AddToWhitelist is a paid mutator transaction binding the contract method 0x214405fc.
//
// Solidity: function addToWhitelist(account address, allowedTokenAmount uint256) returns()
func (_IO *IOTransactorSession) AddToWhitelist(account common.Address, allowedTokenAmount *big.Int) (*types.Transaction, error) {
	return _IO.Contract.AddToWhitelist(&_IO.TransactOpts, account, allowedTokenAmount)
}

// BatchAddToWhitelist is a paid mutator transaction binding the contract method 0xdb47ada2.
//
// Solidity: function batchAddToWhitelist(accounts address[], allowedTokenAmounts uint256[]) returns()
func (_IO *IOTransactor) BatchAddToWhitelist(opts *bind.TransactOpts, accounts []common.Address, allowedTokenAmounts []*big.Int) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "batchAddToWhitelist", accounts, allowedTokenAmounts)
}

// BatchAddToWhitelist is a paid mutator transaction binding the contract method 0xdb47ada2.
//
// Solidity: function batchAddToWhitelist(accounts address[], allowedTokenAmounts uint256[]) returns()
func (_IO *IOSession) BatchAddToWhitelist(accounts []common.Address, allowedTokenAmounts []*big.Int) (*types.Transaction, error) {
	return _IO.Contract.BatchAddToWhitelist(&_IO.TransactOpts, accounts, allowedTokenAmounts)
}

// BatchAddToWhitelist is a paid mutator transaction binding the contract method 0xdb47ada2.
//
// Solidity: function batchAddToWhitelist(accounts address[], allowedTokenAmounts uint256[]) returns()
func (_IO *IOTransactorSession) BatchAddToWhitelist(accounts []common.Address, allowedTokenAmounts []*big.Int) (*types.Transaction, error) {
	return _IO.Contract.BatchAddToWhitelist(&_IO.TransactOpts, accounts, allowedTokenAmounts)
}

// CreateOrder is a paid mutator transaction binding the contract method 0xd1cfca07.
//
// Solidity: function createOrder(typeId uint256, metadata string, price uint256, tokenAmount uint256, exchangeRate uint256) returns()
func (_IO *IOTransactor) CreateOrder(opts *bind.TransactOpts, typeId *big.Int, metadata string, price *big.Int, tokenAmount *big.Int, exchangeRate *big.Int) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "createOrder", typeId, metadata, price, tokenAmount, exchangeRate)
}

// CreateOrder is a paid mutator transaction binding the contract method 0xd1cfca07.
//
// Solidity: function createOrder(typeId uint256, metadata string, price uint256, tokenAmount uint256, exchangeRate uint256) returns()
func (_IO *IOSession) CreateOrder(typeId *big.Int, metadata string, price *big.Int, tokenAmount *big.Int, exchangeRate *big.Int) (*types.Transaction, error) {
	return _IO.Contract.CreateOrder(&_IO.TransactOpts, typeId, metadata, price, tokenAmount, exchangeRate)
}

// CreateOrder is a paid mutator transaction binding the contract method 0xd1cfca07.
//
// Solidity: function createOrder(typeId uint256, metadata string, price uint256, tokenAmount uint256, exchangeRate uint256) returns()
func (_IO *IOTransactorSession) CreateOrder(typeId *big.Int, metadata string, price *big.Int, tokenAmount *big.Int, exchangeRate *big.Int) (*types.Transaction, error) {
	return _IO.Contract.CreateOrder(&_IO.TransactOpts, typeId, metadata, price, tokenAmount, exchangeRate)
}

// CreateProposal is a paid mutator transaction binding the contract method 0x722c5176.
//
// Solidity: function createProposal(price uint256, tokenAmount uint256, startPositionId uint256) returns()
func (_IO *IOTransactor) CreateProposal(opts *bind.TransactOpts, price *big.Int, tokenAmount *big.Int, startPositionId *big.Int) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "createProposal", price, tokenAmount, startPositionId)
}

// CreateProposal is a paid mutator transaction binding the contract method 0x722c5176.
//
// Solidity: function createProposal(price uint256, tokenAmount uint256, startPositionId uint256) returns()
func (_IO *IOSession) CreateProposal(price *big.Int, tokenAmount *big.Int, startPositionId *big.Int) (*types.Transaction, error) {
	return _IO.Contract.CreateProposal(&_IO.TransactOpts, price, tokenAmount, startPositionId)
}

// CreateProposal is a paid mutator transaction binding the contract method 0x722c5176.
//
// Solidity: function createProposal(price uint256, tokenAmount uint256, startPositionId uint256) returns()
func (_IO *IOTransactorSession) CreateProposal(price *big.Int, tokenAmount *big.Int, startPositionId *big.Int) (*types.Transaction, error) {
	return _IO.Contract.CreateProposal(&_IO.TransactOpts, price, tokenAmount, startPositionId)
}

// IncreaseAllowance is a paid mutator transaction binding the contract method 0x39509351.
//
// Solidity: function increaseAllowance(account address, allowedTokenAmount uint256) returns()
func (_IO *IOTransactor) IncreaseAllowance(opts *bind.TransactOpts, account common.Address, allowedTokenAmount *big.Int) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "increaseAllowance", account, allowedTokenAmount)
}

// IncreaseAllowance is a paid mutator transaction binding the contract method 0x39509351.
//
// Solidity: function increaseAllowance(account address, allowedTokenAmount uint256) returns()
func (_IO *IOSession) IncreaseAllowance(account common.Address, allowedTokenAmount *big.Int) (*types.Transaction, error) {
	return _IO.Contract.IncreaseAllowance(&_IO.TransactOpts, account, allowedTokenAmount)
}

// IncreaseAllowance is a paid mutator transaction binding the contract method 0x39509351.
//
// Solidity: function increaseAllowance(account address, allowedTokenAmount uint256) returns()
func (_IO *IOTransactorSession) IncreaseAllowance(account common.Address, allowedTokenAmount *big.Int) (*types.Transaction, error) {
	return _IO.Contract.IncreaseAllowance(&_IO.TransactOpts, account, allowedTokenAmount)
}

// RemoveFromWhitelist is a paid mutator transaction binding the contract method 0x8ab1d681.
//
// Solidity: function removeFromWhitelist(account address) returns()
func (_IO *IOTransactor) RemoveFromWhitelist(opts *bind.TransactOpts, account common.Address) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "removeFromWhitelist", account)
}

// RemoveFromWhitelist is a paid mutator transaction binding the contract method 0x8ab1d681.
//
// Solidity: function removeFromWhitelist(account address) returns()
func (_IO *IOSession) RemoveFromWhitelist(account common.Address) (*types.Transaction, error) {
	return _IO.Contract.RemoveFromWhitelist(&_IO.TransactOpts, account)
}

// RemoveFromWhitelist is a paid mutator transaction binding the contract method 0x8ab1d681.
//
// Solidity: function removeFromWhitelist(account address) returns()
func (_IO *IOTransactorSession) RemoveFromWhitelist(account common.Address) (*types.Transaction, error) {
	return _IO.Contract.RemoveFromWhitelist(&_IO.TransactOpts, account)
}

// RemoveSubOwners is a paid mutator transaction binding the contract method 0xbf9a4067.
//
// Solidity: function removeSubOwners(addresses address[]) returns()
func (_IO *IOTransactor) RemoveSubOwners(opts *bind.TransactOpts, addresses []common.Address) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "removeSubOwners", addresses)
}

// RemoveSubOwners is a paid mutator transaction binding the contract method 0xbf9a4067.
//
// Solidity: function removeSubOwners(addresses address[]) returns()
func (_IO *IOSession) RemoveSubOwners(addresses []common.Address) (*types.Transaction, error) {
	return _IO.Contract.RemoveSubOwners(&_IO.TransactOpts, addresses)
}

// RemoveSubOwners is a paid mutator transaction binding the contract method 0xbf9a4067.
//
// Solidity: function removeSubOwners(addresses address[]) returns()
func (_IO *IOTransactorSession) RemoveSubOwners(addresses []common.Address) (*types.Transaction, error) {
	return _IO.Contract.RemoveSubOwners(&_IO.TransactOpts, addresses)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_IO *IOTransactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_IO *IOSession) RenounceOwnership() (*types.Transaction, error) {
	return _IO.Contract.RenounceOwnership(&_IO.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_IO *IOTransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _IO.Contract.RenounceOwnership(&_IO.TransactOpts)
}

// SetBackendAddress is a paid mutator transaction binding the contract method 0x1815ce7d.
//
// Solidity: function setBackendAddress(newAddress address) returns()
func (_IO *IOTransactor) SetBackendAddress(opts *bind.TransactOpts, newAddress common.Address) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "setBackendAddress", newAddress)
}

// SetBackendAddress is a paid mutator transaction binding the contract method 0x1815ce7d.
//
// Solidity: function setBackendAddress(newAddress address) returns()
func (_IO *IOSession) SetBackendAddress(newAddress common.Address) (*types.Transaction, error) {
	return _IO.Contract.SetBackendAddress(&_IO.TransactOpts, newAddress)
}

// SetBackendAddress is a paid mutator transaction binding the contract method 0x1815ce7d.
//
// Solidity: function setBackendAddress(newAddress address) returns()
func (_IO *IOTransactorSession) SetBackendAddress(newAddress common.Address) (*types.Transaction, error) {
	return _IO.Contract.SetBackendAddress(&_IO.TransactOpts, newAddress)
}

// SetMinProposalTokenAmount is a paid mutator transaction binding the contract method 0xe9ecdc8a.
//
// Solidity: function setMinProposalTokenAmount(value uint256) returns()
func (_IO *IOTransactor) SetMinProposalTokenAmount(opts *bind.TransactOpts, value *big.Int) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "setMinProposalTokenAmount", value)
}

// SetMinProposalTokenAmount is a paid mutator transaction binding the contract method 0xe9ecdc8a.
//
// Solidity: function setMinProposalTokenAmount(value uint256) returns()
func (_IO *IOSession) SetMinProposalTokenAmount(value *big.Int) (*types.Transaction, error) {
	return _IO.Contract.SetMinProposalTokenAmount(&_IO.TransactOpts, value)
}

// SetMinProposalTokenAmount is a paid mutator transaction binding the contract method 0xe9ecdc8a.
//
// Solidity: function setMinProposalTokenAmount(value uint256) returns()
func (_IO *IOTransactorSession) SetMinProposalTokenAmount(value *big.Int) (*types.Transaction, error) {
	return _IO.Contract.SetMinProposalTokenAmount(&_IO.TransactOpts, value)
}

// SetOrderType is a paid mutator transaction binding the contract method 0x72cda48a.
//
// Solidity: function setOrderType(id uint256, description string) returns()
func (_IO *IOTransactor) SetOrderType(opts *bind.TransactOpts, id *big.Int, description string) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "setOrderType", id, description)
}

// SetOrderType is a paid mutator transaction binding the contract method 0x72cda48a.
//
// Solidity: function setOrderType(id uint256, description string) returns()
func (_IO *IOSession) SetOrderType(id *big.Int, description string) (*types.Transaction, error) {
	return _IO.Contract.SetOrderType(&_IO.TransactOpts, id, description)
}

// SetOrderType is a paid mutator transaction binding the contract method 0x72cda48a.
//
// Solidity: function setOrderType(id uint256, description string) returns()
func (_IO *IOTransactorSession) SetOrderType(id *big.Int, description string) (*types.Transaction, error) {
	return _IO.Contract.SetOrderType(&_IO.TransactOpts, id, description)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(newOwner address) returns()
func (_IO *IOTransactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(newOwner address) returns()
func (_IO *IOSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _IO.Contract.TransferOwnership(&_IO.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(newOwner address) returns()
func (_IO *IOTransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _IO.Contract.TransferOwnership(&_IO.TransactOpts, newOwner)
}

// TransferTokensToNewLakeDiamondAddress is a paid mutator transaction binding the contract method 0x51908314.
//
// Solidity: function transferTokensToNewLakeDiamondAddress(newAddress address) returns()
func (_IO *IOTransactor) TransferTokensToNewLakeDiamondAddress(opts *bind.TransactOpts, newAddress common.Address) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "transferTokensToNewLakeDiamondAddress", newAddress)
}

// TransferTokensToNewLakeDiamondAddress is a paid mutator transaction binding the contract method 0x51908314.
//
// Solidity: function transferTokensToNewLakeDiamondAddress(newAddress address) returns()
func (_IO *IOSession) TransferTokensToNewLakeDiamondAddress(newAddress common.Address) (*types.Transaction, error) {
	return _IO.Contract.TransferTokensToNewLakeDiamondAddress(&_IO.TransactOpts, newAddress)
}

// TransferTokensToNewLakeDiamondAddress is a paid mutator transaction binding the contract method 0x51908314.
//
// Solidity: function transferTokensToNewLakeDiamondAddress(newAddress address) returns()
func (_IO *IOTransactorSession) TransferTokensToNewLakeDiamondAddress(newAddress common.Address) (*types.Transaction, error) {
	return _IO.Contract.TransferTokensToNewLakeDiamondAddress(&_IO.TransactOpts, newAddress)
}

// WithdrawProposal is a paid mutator transaction binding the contract method 0xe1f02ffa.
//
// Solidity: function withdrawProposal(id uint256) returns()
func (_IO *IOTransactor) WithdrawProposal(opts *bind.TransactOpts, id *big.Int) (*types.Transaction, error) {
	return _IO.contract.Transact(opts, "withdrawProposal", id)
}

// WithdrawProposal is a paid mutator transaction binding the contract method 0xe1f02ffa.
//
// Solidity: function withdrawProposal(id uint256) returns()
func (_IO *IOSession) WithdrawProposal(id *big.Int) (*types.Transaction, error) {
	return _IO.Contract.WithdrawProposal(&_IO.TransactOpts, id)
}

// WithdrawProposal is a paid mutator transaction binding the contract method 0xe1f02ffa.
//
// Solidity: function withdrawProposal(id uint256) returns()
func (_IO *IOTransactorSession) WithdrawProposal(id *big.Int) (*types.Transaction, error) {
	return _IO.Contract.WithdrawProposal(&_IO.TransactOpts, id)
}

// IOOrderAddedIterator is returned from FilterOrderAdded and is used to iterate over the raw logs and unpacked data for OrderAdded events raised by the IO contract.
type IOOrderAddedIterator struct {
	Event *IOOrderAdded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IOOrderAddedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IOOrderAdded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IOOrderAdded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IOOrderAddedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IOOrderAddedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IOOrderAdded represents a OrderAdded event raised by the IO contract.
type IOOrderAdded struct {
	OrderId      *big.Int
	From         common.Address
	TypeId       *big.Int
	Price        *big.Int
	WeiAmount    *big.Int
	TokenAmount  *big.Int
	ExchangeRate *big.Int
	Metadata     string
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterOrderAdded is a free log retrieval operation binding the contract event 0x7b1b15443d5c7d9804431b559ea2cc5e7aa69c8593fd2f09cfcef83a081b8e93.
//
// Solidity: e OrderAdded(orderId indexed uint256, from indexed address, typeId indexed uint256, price uint256, weiAmount uint256, tokenAmount uint256, exchangeRate uint256, metadata string)
func (_IO *IOFilterer) FilterOrderAdded(opts *bind.FilterOpts, orderId []*big.Int, from []common.Address, typeId []*big.Int) (*IOOrderAddedIterator, error) {

	var orderIdRule []interface{}
	for _, orderIdItem := range orderId {
		orderIdRule = append(orderIdRule, orderIdItem)
	}
	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var typeIdRule []interface{}
	for _, typeIdItem := range typeId {
		typeIdRule = append(typeIdRule, typeIdItem)
	}

	logs, sub, err := _IO.contract.FilterLogs(opts, "OrderAdded", orderIdRule, fromRule, typeIdRule)
	if err != nil {
		return nil, err
	}
	return &IOOrderAddedIterator{contract: _IO.contract, event: "OrderAdded", logs: logs, sub: sub}, nil
}

// WatchOrderAdded is a free log subscription operation binding the contract event 0x7b1b15443d5c7d9804431b559ea2cc5e7aa69c8593fd2f09cfcef83a081b8e93.
//
// Solidity: e OrderAdded(orderId indexed uint256, from indexed address, typeId indexed uint256, price uint256, weiAmount uint256, tokenAmount uint256, exchangeRate uint256, metadata string)
func (_IO *IOFilterer) WatchOrderAdded(opts *bind.WatchOpts, sink chan<- *IOOrderAdded, orderId []*big.Int, from []common.Address, typeId []*big.Int) (event.Subscription, error) {

	var orderIdRule []interface{}
	for _, orderIdItem := range orderId {
		orderIdRule = append(orderIdRule, orderIdItem)
	}
	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var typeIdRule []interface{}
	for _, typeIdItem := range typeId {
		typeIdRule = append(typeIdRule, typeIdItem)
	}

	logs, sub, err := _IO.contract.WatchLogs(opts, "OrderAdded", orderIdRule, fromRule, typeIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IOOrderAdded)
				if err := _IO.contract.UnpackLog(event, "OrderAdded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IOOrderProcessedIterator is returned from FilterOrderProcessed and is used to iterate over the raw logs and unpacked data for OrderProcessed events raised by the IO contract.
type IOOrderProcessedIterator struct {
	Event *IOOrderProcessed // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IOOrderProcessedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IOOrderProcessed)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IOOrderProcessed)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IOOrderProcessedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IOOrderProcessedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IOOrderProcessed represents a OrderProcessed event raised by the IO contract.
type IOOrderProcessed struct {
	Id   *big.Int
	Rest *big.Int
	Raw  types.Log // Blockchain specific contextual infos
}

// FilterOrderProcessed is a free log retrieval operation binding the contract event 0xa72954bf82a02979408df189cd39e5f8ced72a87fc687cbfb63201dcd0c95c32.
//
// Solidity: e OrderProcessed(id indexed uint256, rest uint256)
func (_IO *IOFilterer) FilterOrderProcessed(opts *bind.FilterOpts, id []*big.Int) (*IOOrderProcessedIterator, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _IO.contract.FilterLogs(opts, "OrderProcessed", idRule)
	if err != nil {
		return nil, err
	}
	return &IOOrderProcessedIterator{contract: _IO.contract, event: "OrderProcessed", logs: logs, sub: sub}, nil
}

// WatchOrderProcessed is a free log subscription operation binding the contract event 0xa72954bf82a02979408df189cd39e5f8ced72a87fc687cbfb63201dcd0c95c32.
//
// Solidity: e OrderProcessed(id indexed uint256, rest uint256)
func (_IO *IOFilterer) WatchOrderProcessed(opts *bind.WatchOpts, sink chan<- *IOOrderProcessed, id []*big.Int) (event.Subscription, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _IO.contract.WatchLogs(opts, "OrderProcessed", idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IOOrderProcessed)
				if err := _IO.contract.UnpackLog(event, "OrderProcessed", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IOOrderTypeSetIterator is returned from FilterOrderTypeSet and is used to iterate over the raw logs and unpacked data for OrderTypeSet events raised by the IO contract.
type IOOrderTypeSetIterator struct {
	Event *IOOrderTypeSet // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IOOrderTypeSetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IOOrderTypeSet)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IOOrderTypeSet)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IOOrderTypeSetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IOOrderTypeSetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IOOrderTypeSet represents a OrderTypeSet event raised by the IO contract.
type IOOrderTypeSet struct {
	Id          *big.Int
	Description string
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterOrderTypeSet is a free log retrieval operation binding the contract event 0xa7a0ef1a07f8a531f17e96e02556356cbe2a1476a005a418ca1f1b348685670b.
//
// Solidity: e OrderTypeSet(id indexed uint256, description string)
func (_IO *IOFilterer) FilterOrderTypeSet(opts *bind.FilterOpts, id []*big.Int) (*IOOrderTypeSetIterator, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _IO.contract.FilterLogs(opts, "OrderTypeSet", idRule)
	if err != nil {
		return nil, err
	}
	return &IOOrderTypeSetIterator{contract: _IO.contract, event: "OrderTypeSet", logs: logs, sub: sub}, nil
}

// WatchOrderTypeSet is a free log subscription operation binding the contract event 0xa7a0ef1a07f8a531f17e96e02556356cbe2a1476a005a418ca1f1b348685670b.
//
// Solidity: e OrderTypeSet(id indexed uint256, description string)
func (_IO *IOFilterer) WatchOrderTypeSet(opts *bind.WatchOpts, sink chan<- *IOOrderTypeSet, id []*big.Int) (event.Subscription, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}

	logs, sub, err := _IO.contract.WatchLogs(opts, "OrderTypeSet", idRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IOOrderTypeSet)
				if err := _IO.contract.UnpackLog(event, "OrderTypeSet", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IOOwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the IO contract.
type IOOwnershipTransferredIterator struct {
	Event *IOOwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IOOwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IOOwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IOOwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IOOwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IOOwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IOOwnershipTransferred represents a OwnershipTransferred event raised by the IO contract.
type IOOwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: e OwnershipTransferred(previousOwner indexed address, newOwner indexed address)
func (_IO *IOFilterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*IOOwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _IO.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &IOOwnershipTransferredIterator{contract: _IO.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: e OwnershipTransferred(previousOwner indexed address, newOwner indexed address)
func (_IO *IOFilterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *IOOwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _IO.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IOOwnershipTransferred)
				if err := _IO.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IOProposalCompletedIterator is returned from FilterProposalCompleted and is used to iterate over the raw logs and unpacked data for ProposalCompleted events raised by the IO contract.
type IOProposalCompletedIterator struct {
	Event *IOProposalCompleted // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IOProposalCompletedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IOProposalCompleted)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IOProposalCompleted)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IOProposalCompletedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IOProposalCompletedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IOProposalCompleted represents a ProposalCompleted event raised by the IO contract.
type IOProposalCompleted struct {
	Id          *big.Int
	OrderId     *big.Int
	TokenAmount *big.Int
	WeiAmount   *big.Int
	IsActive    bool
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterProposalCompleted is a free log retrieval operation binding the contract event 0x6a656d34dcc18dc84f88a839cf55a39e73969b663381896230bf831ca13c9ac8.
//
// Solidity: e ProposalCompleted(id indexed uint256, orderId indexed uint256, tokenAmount uint256, weiAmount uint256, isActive bool)
func (_IO *IOFilterer) FilterProposalCompleted(opts *bind.FilterOpts, id []*big.Int, orderId []*big.Int) (*IOProposalCompletedIterator, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}
	var orderIdRule []interface{}
	for _, orderIdItem := range orderId {
		orderIdRule = append(orderIdRule, orderIdItem)
	}

	logs, sub, err := _IO.contract.FilterLogs(opts, "ProposalCompleted", idRule, orderIdRule)
	if err != nil {
		return nil, err
	}
	return &IOProposalCompletedIterator{contract: _IO.contract, event: "ProposalCompleted", logs: logs, sub: sub}, nil
}

// WatchProposalCompleted is a free log subscription operation binding the contract event 0x6a656d34dcc18dc84f88a839cf55a39e73969b663381896230bf831ca13c9ac8.
//
// Solidity: e ProposalCompleted(id indexed uint256, orderId indexed uint256, tokenAmount uint256, weiAmount uint256, isActive bool)
func (_IO *IOFilterer) WatchProposalCompleted(opts *bind.WatchOpts, sink chan<- *IOProposalCompleted, id []*big.Int, orderId []*big.Int) (event.Subscription, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}
	var orderIdRule []interface{}
	for _, orderIdItem := range orderId {
		orderIdRule = append(orderIdRule, orderIdItem)
	}

	logs, sub, err := _IO.contract.WatchLogs(opts, "ProposalCompleted", idRule, orderIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IOProposalCompleted)
				if err := _IO.contract.UnpackLog(event, "ProposalCompleted", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IOProposalCreatedIterator is returned from FilterProposalCreated and is used to iterate over the raw logs and unpacked data for ProposalCreated events raised by the IO contract.
type IOProposalCreatedIterator struct {
	Event *IOProposalCreated // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IOProposalCreatedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IOProposalCreated)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IOProposalCreated)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IOProposalCreatedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IOProposalCreatedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IOProposalCreated represents a ProposalCreated event raised by the IO contract.
type IOProposalCreated struct {
	Id          *big.Int
	From        common.Address
	Price       *big.Int
	TokenAmount *big.Int
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterProposalCreated is a free log retrieval operation binding the contract event 0xd38d9a9a102af286aea1cbcadb8aab8446a90859626389db3710ed4fea4c2c39.
//
// Solidity: e ProposalCreated(id indexed uint256, from indexed address, price uint256, tokenAmount uint256)
func (_IO *IOFilterer) FilterProposalCreated(opts *bind.FilterOpts, id []*big.Int, from []common.Address) (*IOProposalCreatedIterator, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}
	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}

	logs, sub, err := _IO.contract.FilterLogs(opts, "ProposalCreated", idRule, fromRule)
	if err != nil {
		return nil, err
	}
	return &IOProposalCreatedIterator{contract: _IO.contract, event: "ProposalCreated", logs: logs, sub: sub}, nil
}

// WatchProposalCreated is a free log subscription operation binding the contract event 0xd38d9a9a102af286aea1cbcadb8aab8446a90859626389db3710ed4fea4c2c39.
//
// Solidity: e ProposalCreated(id indexed uint256, from indexed address, price uint256, tokenAmount uint256)
func (_IO *IOFilterer) WatchProposalCreated(opts *bind.WatchOpts, sink chan<- *IOProposalCreated, id []*big.Int, from []common.Address) (event.Subscription, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}
	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}

	logs, sub, err := _IO.contract.WatchLogs(opts, "ProposalCreated", idRule, fromRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IOProposalCreated)
				if err := _IO.contract.UnpackLog(event, "ProposalCreated", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IOProposalWithdrawnIterator is returned from FilterProposalWithdrawn and is used to iterate over the raw logs and unpacked data for ProposalWithdrawn events raised by the IO contract.
type IOProposalWithdrawnIterator struct {
	Event *IOProposalWithdrawn // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IOProposalWithdrawnIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IOProposalWithdrawn)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IOProposalWithdrawn)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IOProposalWithdrawnIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IOProposalWithdrawnIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IOProposalWithdrawn represents a ProposalWithdrawn event raised by the IO contract.
type IOProposalWithdrawn struct {
	Id          *big.Int
	WithdrownBy common.Address
	TokenAmount *big.Int
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterProposalWithdrawn is a free log retrieval operation binding the contract event 0x85fb970862c5a917ab511585db36caa4ecb3588fd06b1d89cf21b1a812cb58f9.
//
// Solidity: e ProposalWithdrawn(id indexed uint256, withdrownBy indexed address, tokenAmount uint256)
func (_IO *IOFilterer) FilterProposalWithdrawn(opts *bind.FilterOpts, id []*big.Int, withdrownBy []common.Address) (*IOProposalWithdrawnIterator, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}
	var withdrownByRule []interface{}
	for _, withdrownByItem := range withdrownBy {
		withdrownByRule = append(withdrownByRule, withdrownByItem)
	}

	logs, sub, err := _IO.contract.FilterLogs(opts, "ProposalWithdrawn", idRule, withdrownByRule)
	if err != nil {
		return nil, err
	}
	return &IOProposalWithdrawnIterator{contract: _IO.contract, event: "ProposalWithdrawn", logs: logs, sub: sub}, nil
}

// WatchProposalWithdrawn is a free log subscription operation binding the contract event 0x85fb970862c5a917ab511585db36caa4ecb3588fd06b1d89cf21b1a812cb58f9.
//
// Solidity: e ProposalWithdrawn(id indexed uint256, withdrownBy indexed address, tokenAmount uint256)
func (_IO *IOFilterer) WatchProposalWithdrawn(opts *bind.WatchOpts, sink chan<- *IOProposalWithdrawn, id []*big.Int, withdrownBy []common.Address) (event.Subscription, error) {

	var idRule []interface{}
	for _, idItem := range id {
		idRule = append(idRule, idItem)
	}
	var withdrownByRule []interface{}
	for _, withdrownByItem := range withdrownBy {
		withdrownByRule = append(withdrownByRule, withdrownByItem)
	}

	logs, sub, err := _IO.contract.WatchLogs(opts, "ProposalWithdrawn", idRule, withdrownByRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IOProposalWithdrawn)
				if err := _IO.contract.UnpackLog(event, "ProposalWithdrawn", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IOSubOwnerAddedIterator is returned from FilterSubOwnerAdded and is used to iterate over the raw logs and unpacked data for SubOwnerAdded events raised by the IO contract.
type IOSubOwnerAddedIterator struct {
	Event *IOSubOwnerAdded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IOSubOwnerAddedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IOSubOwnerAdded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IOSubOwnerAdded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IOSubOwnerAddedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IOSubOwnerAddedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IOSubOwnerAdded represents a SubOwnerAdded event raised by the IO contract.
type IOSubOwnerAdded struct {
	SubOwner common.Address
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterSubOwnerAdded is a free log retrieval operation binding the contract event 0xe3772d952738044f4ce6bd3d37f413339d207460a0fb78280c48d2704ab893c1.
//
// Solidity: e SubOwnerAdded(subOwner indexed address)
func (_IO *IOFilterer) FilterSubOwnerAdded(opts *bind.FilterOpts, subOwner []common.Address) (*IOSubOwnerAddedIterator, error) {

	var subOwnerRule []interface{}
	for _, subOwnerItem := range subOwner {
		subOwnerRule = append(subOwnerRule, subOwnerItem)
	}

	logs, sub, err := _IO.contract.FilterLogs(opts, "SubOwnerAdded", subOwnerRule)
	if err != nil {
		return nil, err
	}
	return &IOSubOwnerAddedIterator{contract: _IO.contract, event: "SubOwnerAdded", logs: logs, sub: sub}, nil
}

// WatchSubOwnerAdded is a free log subscription operation binding the contract event 0xe3772d952738044f4ce6bd3d37f413339d207460a0fb78280c48d2704ab893c1.
//
// Solidity: e SubOwnerAdded(subOwner indexed address)
func (_IO *IOFilterer) WatchSubOwnerAdded(opts *bind.WatchOpts, sink chan<- *IOSubOwnerAdded, subOwner []common.Address) (event.Subscription, error) {

	var subOwnerRule []interface{}
	for _, subOwnerItem := range subOwner {
		subOwnerRule = append(subOwnerRule, subOwnerItem)
	}

	logs, sub, err := _IO.contract.WatchLogs(opts, "SubOwnerAdded", subOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IOSubOwnerAdded)
				if err := _IO.contract.UnpackLog(event, "SubOwnerAdded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IOSubOwnerRemovedIterator is returned from FilterSubOwnerRemoved and is used to iterate over the raw logs and unpacked data for SubOwnerRemoved events raised by the IO contract.
type IOSubOwnerRemovedIterator struct {
	Event *IOSubOwnerRemoved // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IOSubOwnerRemovedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IOSubOwnerRemoved)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IOSubOwnerRemoved)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IOSubOwnerRemovedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IOSubOwnerRemovedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IOSubOwnerRemoved represents a SubOwnerRemoved event raised by the IO contract.
type IOSubOwnerRemoved struct {
	SubOwner common.Address
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterSubOwnerRemoved is a free log retrieval operation binding the contract event 0x02e64b25b4100bd9309507ffa9a49d4f50ffec109e41334dc504a4dec6525fc1.
//
// Solidity: e SubOwnerRemoved(subOwner indexed address)
func (_IO *IOFilterer) FilterSubOwnerRemoved(opts *bind.FilterOpts, subOwner []common.Address) (*IOSubOwnerRemovedIterator, error) {

	var subOwnerRule []interface{}
	for _, subOwnerItem := range subOwner {
		subOwnerRule = append(subOwnerRule, subOwnerItem)
	}

	logs, sub, err := _IO.contract.FilterLogs(opts, "SubOwnerRemoved", subOwnerRule)
	if err != nil {
		return nil, err
	}
	return &IOSubOwnerRemovedIterator{contract: _IO.contract, event: "SubOwnerRemoved", logs: logs, sub: sub}, nil
}

// WatchSubOwnerRemoved is a free log subscription operation binding the contract event 0x02e64b25b4100bd9309507ffa9a49d4f50ffec109e41334dc504a4dec6525fc1.
//
// Solidity: e SubOwnerRemoved(subOwner indexed address)
func (_IO *IOFilterer) WatchSubOwnerRemoved(opts *bind.WatchOpts, sink chan<- *IOSubOwnerRemoved, subOwner []common.Address) (event.Subscription, error) {

	var subOwnerRule []interface{}
	for _, subOwnerItem := range subOwner {
		subOwnerRule = append(subOwnerRule, subOwnerItem)
	}

	logs, sub, err := _IO.contract.WatchLogs(opts, "SubOwnerRemoved", subOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IOSubOwnerRemoved)
				if err := _IO.contract.UnpackLog(event, "SubOwnerRemoved", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IOWhiteListAddressAddedIterator is returned from FilterWhiteListAddressAdded and is used to iterate over the raw logs and unpacked data for WhiteListAddressAdded events raised by the IO contract.
type IOWhiteListAddressAddedIterator struct {
	Event *IOWhiteListAddressAdded // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IOWhiteListAddressAddedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IOWhiteListAddressAdded)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IOWhiteListAddressAdded)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IOWhiteListAddressAddedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IOWhiteListAddressAddedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IOWhiteListAddressAdded represents a WhiteListAddressAdded event raised by the IO contract.
type IOWhiteListAddressAdded struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterWhiteListAddressAdded is a free log retrieval operation binding the contract event 0x8bf610b504c9ff6e30ce933966c1de06b4a80a01807f405382a1c76e11961834.
//
// Solidity: e WhiteListAddressAdded(account indexed address)
func (_IO *IOFilterer) FilterWhiteListAddressAdded(opts *bind.FilterOpts, account []common.Address) (*IOWhiteListAddressAddedIterator, error) {

	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}

	logs, sub, err := _IO.contract.FilterLogs(opts, "WhiteListAddressAdded", accountRule)
	if err != nil {
		return nil, err
	}
	return &IOWhiteListAddressAddedIterator{contract: _IO.contract, event: "WhiteListAddressAdded", logs: logs, sub: sub}, nil
}

// WatchWhiteListAddressAdded is a free log subscription operation binding the contract event 0x8bf610b504c9ff6e30ce933966c1de06b4a80a01807f405382a1c76e11961834.
//
// Solidity: e WhiteListAddressAdded(account indexed address)
func (_IO *IOFilterer) WatchWhiteListAddressAdded(opts *bind.WatchOpts, sink chan<- *IOWhiteListAddressAdded, account []common.Address) (event.Subscription, error) {

	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}

	logs, sub, err := _IO.contract.WatchLogs(opts, "WhiteListAddressAdded", accountRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IOWhiteListAddressAdded)
				if err := _IO.contract.UnpackLog(event, "WhiteListAddressAdded", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IOWhiteListAddressRemovedIterator is returned from FilterWhiteListAddressRemoved and is used to iterate over the raw logs and unpacked data for WhiteListAddressRemoved events raised by the IO contract.
type IOWhiteListAddressRemovedIterator struct {
	Event *IOWhiteListAddressRemoved // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IOWhiteListAddressRemovedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IOWhiteListAddressRemoved)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IOWhiteListAddressRemoved)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IOWhiteListAddressRemovedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IOWhiteListAddressRemovedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IOWhiteListAddressRemoved represents a WhiteListAddressRemoved event raised by the IO contract.
type IOWhiteListAddressRemoved struct {
	Account common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterWhiteListAddressRemoved is a free log retrieval operation binding the contract event 0x66c8e0f3e7412cc43e517ae12160bd68cb03166e5777b9a053e2b88570eb7d3a.
//
// Solidity: e WhiteListAddressRemoved(account indexed address)
func (_IO *IOFilterer) FilterWhiteListAddressRemoved(opts *bind.FilterOpts, account []common.Address) (*IOWhiteListAddressRemovedIterator, error) {

	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}

	logs, sub, err := _IO.contract.FilterLogs(opts, "WhiteListAddressRemoved", accountRule)
	if err != nil {
		return nil, err
	}
	return &IOWhiteListAddressRemovedIterator{contract: _IO.contract, event: "WhiteListAddressRemoved", logs: logs, sub: sub}, nil
}

// WatchWhiteListAddressRemoved is a free log subscription operation binding the contract event 0x66c8e0f3e7412cc43e517ae12160bd68cb03166e5777b9a053e2b88570eb7d3a.
//
// Solidity: e WhiteListAddressRemoved(account indexed address)
func (_IO *IOFilterer) WatchWhiteListAddressRemoved(opts *bind.WatchOpts, sink chan<- *IOWhiteListAddressRemoved, account []common.Address) (event.Subscription, error) {

	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}

	logs, sub, err := _IO.contract.WatchLogs(opts, "WhiteListAddressRemoved", accountRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IOWhiteListAddressRemoved)
				if err := _IO.contract.UnpackLog(event, "WhiteListAddressRemoved", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// IOWhitelistedAmountChangedIterator is returned from FilterWhitelistedAmountChanged and is used to iterate over the raw logs and unpacked data for WhitelistedAmountChanged events raised by the IO contract.
type IOWhitelistedAmountChangedIterator struct {
	Event *IOWhitelistedAmountChanged // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IOWhitelistedAmountChangedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IOWhitelistedAmountChanged)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IOWhitelistedAmountChanged)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IOWhitelistedAmountChangedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IOWhitelistedAmountChangedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IOWhitelistedAmountChanged represents a WhitelistedAmountChanged event raised by the IO contract.
type IOWhitelistedAmountChanged struct {
	Account            common.Address
	AllowedTokenAmount *big.Int
	Raw                types.Log // Blockchain specific contextual infos
}

// FilterWhitelistedAmountChanged is a free log retrieval operation binding the contract event 0x34f0d25bbc4733a231921db30ccb7c99dd2131c67b6b91a78347e3980639a014.
//
// Solidity: e WhitelistedAmountChanged(account indexed address, allowedTokenAmount uint256)
func (_IO *IOFilterer) FilterWhitelistedAmountChanged(opts *bind.FilterOpts, account []common.Address) (*IOWhitelistedAmountChangedIterator, error) {

	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}

	logs, sub, err := _IO.contract.FilterLogs(opts, "WhitelistedAmountChanged", accountRule)
	if err != nil {
		return nil, err
	}
	return &IOWhitelistedAmountChangedIterator{contract: _IO.contract, event: "WhitelistedAmountChanged", logs: logs, sub: sub}, nil
}

// WatchWhitelistedAmountChanged is a free log subscription operation binding the contract event 0x34f0d25bbc4733a231921db30ccb7c99dd2131c67b6b91a78347e3980639a014.
//
// Solidity: e WhitelistedAmountChanged(account indexed address, allowedTokenAmount uint256)
func (_IO *IOFilterer) WatchWhitelistedAmountChanged(opts *bind.WatchOpts, sink chan<- *IOWhitelistedAmountChanged, account []common.Address) (event.Subscription, error) {

	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}

	logs, sub, err := _IO.contract.WatchLogs(opts, "WhitelistedAmountChanged", accountRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IOWhitelistedAmountChanged)
				if err := _IO.contract.UnpackLog(event, "WhitelistedAmountChanged", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}
