package etherPkg

import (
	"context"
	"crypto/ecdsa"
	"fmt"
	"io/ioutil"
	"math/big"
	"strconv"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/rs/zerolog/log"

	cr "lkd-platform-backend/internal/pkg/crypto"
	"lkd-platform-backend/internal/pkg/etherPkg/contracts/ioContract"
)

type PlatformEthereumApplicationInterface interface {
	AddAddressToWhiteList(address string, maxTokens *big.Int) error
	IsContract(address common.Address) bool
	GetBlockTimestamp(blockNumber *big.Int, retries uint) time.Time
	NewProposal(from *ecdsa.PrivateKey, price, amount *big.Int) (*types.Transaction, error)
	GetOwnerPrivateKey() (string, error)
}

type EthereumInternal struct {
	MatcherData *MatcherData
	PlatformEthereum
}

func (ethClient *EthereumInternal) AddAddressToWhiteList(address string, maxTokens *big.Int) error {
	io, err := ioContract.NewIO(common.HexToAddress(ethClient.MatcherData.IOContractAddress), ethClient.PlatformEthereum)
	if err != nil {
		return err
	}

	k, err := ethClient.GetOwnerPrivateKey()
	if err != nil {
		return err
	}

	key, err := crypto.HexToECDSA(k)
	if err != nil {
		log.Error().Err(err).Msg("invalid ethereum private key")
		return err
	}

	auth := bind.NewKeyedTransactor(key)

	_, err = io.AddToWhitelist(auth, common.HexToAddress(address), maxTokens)
	if err != nil {
		return err
	}

	k = ""
	key = nil

	return nil
}

func (ethClient *EthereumInternal) IsContract(address common.Address) bool {

	b, err := ethClient.CodeAt(context.Background(), address, nil)

	if err != nil {
		log.Error().Err(err).Msg("ethereum connection failed, reconnecting")
		ethClient.PlatformEthereum = GetEthClient(ethClient.MatcherData.RPCPort)
		ethClient.IsContract(address)
	}

	return len(b) > 0
}

func (ethClient *EthereumInternal) GetBlockTimestamp(blockNumber *big.Int, retries uint) time.Time {
	result := time.Time{}

	if retries > 10 {
		log.Error().Msg("ethereum block parsing error after 10 times, returning time.Now")
		result = time.Now()
		return result
	}

	block, err := ethClient.BlockByNumber(context.Background(), blockNumber)
	if err != nil {
		log.Error().Err(err).Msg("ethereum block parsing error, retrying")

		time.Sleep(5 * time.Second)
		return ethClient.GetBlockTimestamp(blockNumber, retries+1)
	}

	log.Print(block.Time().String())
	blockTimestamp, err := strconv.ParseInt(block.Time().String(), 10, 64)
	if err != nil {
		log.Error().Err(err).Msgf("invalid block timestamp: %s, wait 5 seconds and retry", block.Time().String())

		time.Sleep(5 * time.Second)
		return ethClient.GetBlockTimestamp(blockNumber, retries+1)
	}

	result = time.Unix(blockTimestamp, 0)
	return result
}

func (ethClient *EthereumInternal) NewProposal(from *ecdsa.PrivateKey, price, amount *big.Int) (*types.Transaction, error) {
	io, err := ioContract.NewIO(common.HexToAddress(ethClient.MatcherData.IOContractAddress), ethClient.PlatformEthereum)
	if err != nil {
		return nil, err
	}

	auth := bind.NewKeyedTransactor(from)

	tx, err := io.CreateProposal(auth, price, amount, big.NewInt(0))
	if err != nil {
		return nil, err
	}

	return tx, nil
}

func (ethClient *EthereumInternal) GetOwnerPrivateKey() (string, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return "", err
	}

	sep := strings.Split(ethClient.MatcherData.PrivateKeyStorageData, "/")
	if len(sep) != 2 {
		return "", fmt.Errorf("invalid ETHEREUM_PRIVATE_KEY_STORAGE_DATA")
	}

	rc, err := client.Bucket(sep[0]).Object(sep[1]).NewReader(ctx)
	if err != nil {
		return "", err
	}
	defer rc.Close()

	d, err := ioutil.ReadAll(rc)
	if err != nil {
		return "", err
	}

	b, err := cr.DecryptRSA(ethClient.MatcherData.PrivateKeyStorageKey, d)
	if err != nil {
		log.Error().Err(err).Msg("decryptRSA error")
		return "", err
	}

	resp := strings.Replace(string(b), "\n", "", -1)

	return resp, nil
}
