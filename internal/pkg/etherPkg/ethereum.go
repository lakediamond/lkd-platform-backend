package etherPkg

import (
	"time"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/rs/zerolog/log"
)

//MatcherData contains IO contracts and events for event listener
type MatcherData struct {
	StorageAddress string
	LakeDiamond    string

	NewOrder         string
	CloseOrder       string
	NewProposal      string
	WithdrawProposal string
	CompleteProposal string
	NewOrderType     string
	CompleteOrder    string
	BannedAddress    string

	NewSubOwner     string
	RemovedSubOwner string

	IOContractAddress string

	RPCPort string

	PrivateKeyStorageKey  string
	PrivateKeyStorageData string
}

type PlatformEthereumApplication struct {
	PlatformEthereum
	MatcherData *MatcherData
	Internal    PlatformEthereumApplicationInterface
}

type PlatformEthereum interface {
	ethereum.ChainReader
	bind.ContractBackend
}

func InitializePlatformEthereumApplication(md *MatcherData) *PlatformEthereumApplication {
	client := GetEthClient(md.RPCPort)
	return &PlatformEthereumApplication{PlatformEthereum: client,
		MatcherData: md,
		Internal: &EthereumInternal{
			MatcherData:      md,
			PlatformEthereum: client}}
}

//GetEthClient initializing and return ethereum rpc client
func GetEthClient(rpcPort string) PlatformEthereum {
	log.Info().Msg("Setting WS connection")
	ethClient, err := ethclient.Dial(rpcPort)
	if err != nil {
		log.Error().Err(err).Msg("Node connection broken. Reconnecting in 5 second")
		time.Sleep(5 * time.Second)
		return GetEthClient(rpcPort)
	}

	return ethClient
}
