package etherPkg

import (
	"context"
	"crypto/ecdsa"
	"math/big"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi/bind/backends"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"

	"lkd-platform-backend/pkg/environment"
)

type EthMock struct {
	*backends.SimulatedBackend
}

func (EthMock) BlockByHash(ctx context.Context, hash common.Hash) (*types.Block, error) {
	panic("implement me")
}

func (EthMock) BlockByNumber(ctx context.Context, number *big.Int) (*types.Block, error) {
	var header = types.Header{
		ParentHash:  *new(common.Hash),
		UncleHash:   *new(common.Hash),
		Coinbase:    *new(common.Address),
		Root:        *new(common.Hash),
		TxHash:      *new(common.Hash),
		ReceiptHash: *new(common.Hash),
		Bloom:       *new(types.Bloom),
		GasLimit:    *new(uint64),
		GasUsed:     *new(uint64),
		Time:        big.NewInt(100000000),
		MixDigest:   *new(common.Hash),
		Nonce:       *new(types.BlockNonce)}

	block := types.NewBlock(&header, nil, nil, nil)

	return block, nil
}

func (EthMock) HeaderByHash(ctx context.Context, hash common.Hash) (*types.Header, error) {
	panic("implement me")
}

func (EthMock) HeaderByNumber(ctx context.Context, number *big.Int) (*types.Header, error) {
	panic("implement me")
}

func (EthMock) TransactionCount(ctx context.Context, blockHash common.Hash) (uint, error) {
	panic("implement me")
}

func (EthMock) TransactionInBlock(ctx context.Context, blockHash common.Hash, index uint) (*types.Transaction, error) {
	panic("implement me")
}

func (EthMock) SubscribeNewHead(ctx context.Context, ch chan<- *types.Header) (ethereum.Subscription, error) {
	panic("implement me")
}

func InitializePlatformEthereumMock(key *ecdsa.PrivateKey) *PlatformEthereumApplication {
	client := GetMockEthClient(key)
	return &PlatformEthereumApplication{client, InitializeMatcherData(), &EthereumInternal{InitializeMatcherData(), client}}
}

//GetMockEthClient initializing and return ethereum rpc client
func GetMockEthClient(key *ecdsa.PrivateKey) *EthMock {
	account1 := crypto.PubkeyToAddress(key.PublicKey)

	sim := backends.NewSimulatedBackend(
		core.GenesisAlloc{account1: core.GenesisAccount{PrivateKey: crypto.FromECDSA(key),
			Balance: big.NewInt(1000000000000000000)}},
		1000000000000000)
	var ethMock EthMock
	ethMock.SimulatedBackend = sim

	return &ethMock
}

//InitializeMatcherData initializing matcher data for event listener
func InitializeMatcherData() *MatcherData {
	env := environment.GetEnv()

	return &MatcherData{
		StorageAddress:        env["ETHEREUM_IO_STORAGE_CONTRACT_ADDRESS"],
		LakeDiamond:           env["ETHEREUM_IO_CONTRACT_ADDRESS"],
		NewOrder:              env["ETHEREUM_NEW_ORDER_EVENT"],
		CloseOrder:            env["ETHEREUM_CLOSE_ORDER_EVENT"],
		NewProposal:           env["ETHEREUM_NEW_PROPOSAL_EVENT"],
		WithdrawProposal:      env["ETHEREUM_WITHDRAW_PROPOSAL_EVENT"],
		CompleteProposal:      env["ETHEREUM_COMPLETE_PROPOSAL_EVENT"],
		NewOrderType:          env["ETHEREUM_NEW_ORDER_TYPE_EVENT"],
		CompleteOrder:         env["ETHEREUM_ORDER_COMPLETE_EVENT"],
		NewSubOwner:           env["ETHEREUM_NEW_SUBOWNER_EVENT"],
		RemovedSubOwner:       env["ETHEREUM_REMOVED_SUBOWNER_EVENT"],
		IOContractAddress:     env["ETHEREUM_IO_CONTRACT_ADDRESS"],
		RPCPort:               env["RPC_PORT"],
		PrivateKeyStorageKey:  env["ETHEREUM_PRIVATE_KEY_STORAGE_KEY"],
		PrivateKeyStorageData: env["ETHEREUM_PRIVATE_KEY_STORAGE_DATA"],
	}
}
