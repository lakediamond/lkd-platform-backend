package migrations

import (
	"time"

	"github.com/satori/go.uuid"
)

type Status string
type Role string

const (
	Active    Status = "active"
	Withdrawn Status = "withdrawn"
	Closed    Status = "closed"

	UserRole  Role = "customer"
	AdminRole Role = "admin"
)

type User struct {
	ID            *uuid.UUID `gorm:"type:uuid; primary_key"`
	FirstName     string
	LastName      string
	Email         string `gorm:"unique"`
	EmailVerified bool
	Role          Role
	Locked        bool
	Password      string
	EthAddress    string `gorm:"unique"`
}

type OrderType struct {
	ID   uint `gorm:"primary_key"`
	Name string
}

type Order struct {
	ID           uint `gorm:"primary_key"`
	BlockchainID uint
	Types        OrderType
	TxHash       string
	Metadata     string
	TypeID       uint
	Price        uint
	EthAmount    uint
	TokenAmount  uint
	CreatedBy    string
}

type Proposal struct {
	ID           uint `gorm:"primary_key"`
	BlockchainID uint
	Price        uint
	Amount       uint
	User         User
	UserId       *uuid.UUID `gorm:"type:uuid"`
	TxHash       string
	Status       Status
}

type SupportData struct {
	ID        uint `gorm:"primary_key"`
	LastBlock uint
}

type JwtToken struct {
	ID          uint `gorm:"primary_key"`
	TokenString string
	IsBlocked   bool
}

//AuthToken database table definition
type AuthToken struct {
	ID              *uuid.UUID `gorm:"type:uuid;primary_key"`
	CreatedAt       time.Time
	UpdatedAt       time.Time
	UserId          *uuid.UUID `gorm:"type:uuid REFERENCES users(id)"`
	SerializedToken string
	Description     string
}
