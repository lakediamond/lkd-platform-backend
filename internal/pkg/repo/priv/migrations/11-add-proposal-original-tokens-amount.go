package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddProposalOriginalTokensAmountField gormigrate.Migration = gormigrate.Migration{
	ID: "201812121500",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE proposals ADD original_tokens DECIMAL;
ALTER TABLE proposals RENAME COLUMN amount TO balance;
`

		return tx.Exec(req).Error
	},
}
