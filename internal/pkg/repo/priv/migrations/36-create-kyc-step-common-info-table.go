package migrations

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"gopkg.in/gormigrate.v1"
	"time"
)

type KYCStepCommonInfo struct {
	ID                   *uuid.UUID `gorm:"type:uuid; primary_key"`
	UserID               *uuid.UUID `gorm:"type:uuid REFERENCES users(id);not null"`
	CommonID             string     `gorm:"not null"`
	RegAddressCountry    string     `gorm:"not null"`
	RegAddressZIP        string     `gorm:"not null"`
	RegAddressCity       string     `gorm:"not null"`
	RegAddressDetails    string     `gorm:"not null"`
	RegAddressDetailsExt string
	ContributionOption   string `gorm:"not null"`
	BankName             string
	BankIBAN             string
	BankAddress          string
	Phone                string
	State                int
	CreatedAt            time.Time
	UpdatedAt            time.Time
}

// TableName return table name
func (KYCStepCommonInfo) TableName() string {
	return "kyc_step_common_info"
}

var CreateKYCStepCommonInfoTable gormigrate.Migration = gormigrate.Migration{

	ID: "201903071420",
	Migrate: func(tx *gorm.DB) error {

		tx.AutoMigrate(
			&KYCStepCommonInfo{})

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("kyc_step_common_info").Error
	},
}
