package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddAuthTokenTable gormigrate.Migration = gormigrate.Migration{
	ID: "2019022811916",
	Migrate: func(tx *gorm.DB) error {
		tx.AutoMigrate(&AuthToken{})

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("auth_tokens").Error
	},
}
