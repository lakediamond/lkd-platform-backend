package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddProposalMainIndex gormigrate.Migration = gormigrate.Migration{
	ID: "201904041009",
	Migrate: func(tx *gorm.DB) error {
		var req = `CREATE INDEX proposal_main_index
  ON proposals (price ASC, created_on DESC, original_tokens ASC, withdrawn_amount ASC);`

		return tx.Exec(req).Error
	},
}
