package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddOAuthColumn gormigrate.Migration = gormigrate.Migration{
	ID: "201901271300",
	Migrate: func(tx *gorm.DB) error {
		var req = `
CREATE EXTENSION IF NOT EXISTS pgcrypto;
ALTER TABLE users ADD oauth_token bytea;
`

		return tx.Exec(req).Error
	},
}
