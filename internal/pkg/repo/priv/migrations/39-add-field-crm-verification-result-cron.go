package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddFieldCrmVerificationResultCron gormigrate.Migration = gormigrate.Migration{

	ID: "201903131334",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE kyc_step_crons 
ADD IF NOT EXISTS crm_verification_result_id uuid default null;

CREATE INDEX kyc_step_crons_event_state_idx ON kyc_step_crons(event, state);
`
		return tx.Exec(req).Error
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `ALTER TABLE kyc_step_crons 
		DROP COLUMN IF EXISTS crm_verification_result_id;
		DROP INDEX kyc_step_crons_event_state_idx;
		`
		return tx.Exec(req).Error
	},
}
