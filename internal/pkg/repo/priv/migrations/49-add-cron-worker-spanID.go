package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddCronWorkerSpanID gormigrate.Migration = gormigrate.Migration{
	ID: "201904091047",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE cron_worker 
ADD IF NOT EXISTS span_id numeric default 0;
`
		return tx.Exec(req).Error
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `ALTER TABLE cron_worker 
		DROP COLUMN IF EXISTS span_id;
		`
		return tx.Exec(req).Error
	},
}
