package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddCompletedOnProposalColumn gormigrate.Migration = gormigrate.Migration{
	ID: "201903101630",
	Migrate: func(tx *gorm.DB) error {
		var req = `ALTER TABLE proposals ADD completed_on timestamp;`

		return tx.Exec(req).Error
	},
}
