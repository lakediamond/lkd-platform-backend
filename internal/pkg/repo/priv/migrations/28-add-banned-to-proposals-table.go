package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddBannedToProposalsTable gormigrate.Migration = gormigrate.Migration{
	ID: "201901181500",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE proposals ADD banned boolean;
`

		return tx.Exec(req).Error
	},
}
