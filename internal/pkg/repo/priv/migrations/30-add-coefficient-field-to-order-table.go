package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddCoefficientToOrdersTable gormigrate.Migration = gormigrate.Migration{
	ID: "201901221800",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE orders ADD coefficient int;
`

		return tx.Exec(req).Error
	},
}
