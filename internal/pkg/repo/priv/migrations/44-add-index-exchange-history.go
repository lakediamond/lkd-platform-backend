package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddIndexExchangeHistory gormigrate.Migration = gormigrate.Migration{
	ID: "201903271044",
	Migrate: func(tx *gorm.DB) error {
		var req = `
			CREATE INDEX exchange_history_code_idx ON exchange_history(code);
		`
		return tx.Exec(req).Error
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `
		DROP INDEX exchange_history_code_idx;
		`
		return tx.Exec(req).Error
	},
}
