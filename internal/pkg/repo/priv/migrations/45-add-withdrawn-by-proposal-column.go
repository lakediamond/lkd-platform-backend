package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddWithdrawnByProposalColumn gormigrate.Migration = gormigrate.Migration{
	ID: "201904021009",
	Migrate: func(tx *gorm.DB) error {
		var req = `ALTER TABLE proposals ADD withdrawn_by text;`

		return tx.Exec(req).Error
	},
}
