package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"

	"github.com/satori/go.uuid"
)

type ScopeMetainfo struct {
	ID          *uuid.UUID `gorm:"type:uuid; primary_key"`
	Code        string
	Description string
}

// set ScopeMetainfo's table name to be `profiles`
func (ScopeMetainfo) TableName() string {
	return "scope_metainfo"
}

var CreateScopeMetainfoTable gormigrate.Migration = gormigrate.Migration{

	ID: "201811271330",
	Migrate: func(tx *gorm.DB) error {

		tx.AutoMigrate(
			&ScopeMetainfo{})

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("scope_metainfo").Error
	},
}
