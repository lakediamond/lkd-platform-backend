package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddCRMDealID gormigrate.Migration = gormigrate.Migration{
	ID: "201901080800",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE users ADD crm_deal_id integer;
`

		return tx.Exec(req).Error
	},
}
