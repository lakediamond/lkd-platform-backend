package migrations

import (
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
	"github.com/satori/go.uuid"
	"gopkg.in/gormigrate.v1"
)

type Matches struct {
	ID                   *uuid.UUID `gorm:"type:uuid; primary_key"`
	ProposalBlockchainID uint
	OrderBlockchainID    uint
	TxHash               string
	Tokens               decimal.Decimal `json:"tokens" gorm:"type:decimal"`
	Ether                decimal.Decimal `json:"ether" gorm:"type:decimal"`
}

var CreateMatchesTable gormigrate.Migration = gormigrate.Migration{

	ID: "201812051200",
	Migrate: func(tx *gorm.DB) error {

		tx.AutoMigrate(
			&Matches{})

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("matches").Error
	},
}
