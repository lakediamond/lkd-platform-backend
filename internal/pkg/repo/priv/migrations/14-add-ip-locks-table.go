package migrations

import (
	"time"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"

	"github.com/satori/go.uuid"
)

type IPLock struct {
	ID        *uuid.UUID `gorm:"type:uuid; primary_key"`
	Email     string
	IPAddress string
	Reason    string
	TimeTo    time.Time `gorm:"type:time"`
}

var CreateIPLocksTable gormigrate.Migration = gormigrate.Migration{

	ID: "201812141630",
	Migrate: func(tx *gorm.DB) error {

		tx.AutoMigrate(
			&IPLock{})

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("ip_locks").Error
	},
}
