package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var UpdateKYCTiersLabels gormigrate.Migration = gormigrate.Migration{
	ID: "201904220900",
	Migrate: func(tx *gorm.DB) error {
		var req = `
update kyc_investment_plan
set description = 'Up to 100’000 LKD',
max_limit = 100000
where code = 0;

update kyc_investment_plan
set description = 'Up to 200’000 LKD',
max_limit=200000
where code = 1;

update kyc_investment_plan
set description = 'Up to 1’000’000 LKD',
max_limit=1000000
where code = 2;

update kyc_investment_plan
set description = 'Up to 5’000’000 LKD',
max_limit=5000000
where code = 3;
`
		return tx.Exec(req).Error
	},
}
