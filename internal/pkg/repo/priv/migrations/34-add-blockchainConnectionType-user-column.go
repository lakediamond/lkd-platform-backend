package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddblockchainConnectionTypeUserColumn gormigrate.Migration = gormigrate.Migration{
	ID: "201902251550",
	Migrate: func(tx *gorm.DB) error {
		var req = `ALTER TABLE users ADD blockchain_connection_type VARCHAR(50);`
		return tx.Exec(req).Error
	},
}
