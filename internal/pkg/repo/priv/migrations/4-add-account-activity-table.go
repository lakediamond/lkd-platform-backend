package migrations

import (
	"time"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"

	"github.com/satori/go.uuid"
)

type AccountActivity struct {
	ID        *uuid.UUID `gorm:"type:uuid; primary_key"`
	Timestamp time.Time
	Type      string
	Content   string `sql:"type:JSONB NOT NULL DEFAULT '{}'::JSONB"`
}

var CreateAccountActivityTable gormigrate.Migration = gormigrate.Migration{

	ID: "201811261500",
	Migrate: func(tx *gorm.DB) error {

		tx.AutoMigrate(
			&AccountActivity{})

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("account_activity").Error
	},
}
