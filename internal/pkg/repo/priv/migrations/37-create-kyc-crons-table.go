package migrations

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"gopkg.in/gormigrate.v1"
	"time"
)

type KYCStepCrons struct {
	ID        *uuid.UUID `gorm:"type:uuid; primary_key"`
	UserID    *uuid.UUID `gorm:"type:uuid REFERENCES users(id);not null"`
	Event     string
	Phone     string
	State     int
	CreatedAt time.Time
	UpdatedAt time.Time
}

// TableName return table name
func (KYCStepCrons) TableName() string {
	return "kyc_step_crons"
}

var CreateKYCStepCronsTable gormigrate.Migration = gormigrate.Migration{

	ID: "201903121318",
	Migrate: func(tx *gorm.DB) error {
		tx.DropTable("kyc_step_common_info")
		tx.AutoMigrate(
			&KYCStepCrons{})

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("kyc_step_crons").Error
	},
}
