package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddFieldPhoneVerificationCron gormigrate.Migration = gormigrate.Migration{

	ID: "201903131714",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE kyc_step_crons 
ADD IF NOT EXISTS phone_verification_id uuid default null;
`
		return tx.Exec(req).Error
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `ALTER TABLE kyc_step_crons 
		DROP COLUMN IF EXISTS phone_verification_id;
		`
		return tx.Exec(req).Error
	},
}
