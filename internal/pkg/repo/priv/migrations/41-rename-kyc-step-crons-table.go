package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var RenameKycStepCronsTable gormigrate.Migration = gormigrate.Migration{

	ID: "201903141243",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE IF EXISTS kyc_step_crons
RENAME TO cron_worker;
`
		return tx.Exec(req).Error
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `ALTER TABLE IF EXISTS cron_worker
			RENAME TO kyc_step_crons;
		`
		return tx.Exec(req).Error
	},
}
