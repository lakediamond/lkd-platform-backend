package migrations

import (
	"time"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"

	"github.com/satori/go.uuid"
)

type KYCTier struct {
	ID             *uuid.UUID `gorm:"type:uuid; primary_key"`
	Code           int
	Status         string
	UserID         *uuid.UUID             `gorm:"type:uuid REFERENCES users(id);not null"`
	Communications []KYCTierCommunication `gorm:"foreignkey:KYCTierID"`
	Steps          []KYCTierStep          `gorm:"foreignkey:KYCTierID"`
	Flags          []KYCTierFlag          `gorm:"foreignkey:KYCTierID"`
	CreatedAt      time.Time
	UpdatedAt      time.Time
	DeletedAt      *time.Time
}

type KYCTierStep struct {
	ID           *uuid.UUID `gorm:"type:uuid; primary_key"`
	Status       string
	StepOrder    int
	Title        string
	Code         string
	ContentItems []KYCTierStepContentItem `gorm:"foreignkey:KYCTierStepID"`
	KYCTierID    *uuid.UUID               `gorm:"type:uuid REFERENCES kyc_tier(id);not null"`
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    *time.Time
}

type KYCTierStepContentItem struct {
	ID            *uuid.UUID `gorm:"type:uuid; primary_key"`
	Status        string
	ItemOrder     int
	KYCTierStepID *uuid.UUID `gorm:"type:uuid REFERENCES kyc_tier_step(id) ON DELETE CASCADE; not null"`
	Code          string
	Value         string
	Enabled       bool
	EnabledAt     *time.Time
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeletedAt     *time.Time
}

type KYCTierCommunication struct {
	ID        *uuid.UUID `gorm:"type:uuid; primary_key"`
	KYCTierID *uuid.UUID `gorm:"type:uuid REFERENCES kyc_tier(id);not null"`
	Type      string
	Message   string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

type KYCTierFlag struct {
	ID                      *uuid.UUID `gorm:"type:uuid; primary_key"`
	KYCTierID               *uuid.UUID `gorm:"type:uuid REFERENCES kyc_tier(id);not null"`
	Code                    string
	Value                   string
	CRMPipelineFailureStage int64 `gorm:"default:-1"`
	CreatedAt               time.Time
	UpdatedAt               time.Time
	DeletedAt               *time.Time
}

// TableName overrides gorm built-in table name definition
func (KYCTier) TableName() string {
	return "kyc_tier"
}

// TableName overrides gorm built-in table name definition
func (KYCTierStep) TableName() string {
	return "kyc_tier_step"
}

func (KYCTierStepContentItem) TableName() string {
	return "kyc_tier_step_item"
}

func (KYCTierCommunication) TableName() string {
	return "kyc_tier_communication"
}

func (KYCTierFlag) TableName() string {
	return "kyc_tier_flag"
}

var CreateKYCTables gormigrate.Migration = gormigrate.Migration{

	ID: "201812190800",
	Migrate: func(tx *gorm.DB) error {

		tx.AutoMigrate(
			&KYCTier{},
			&KYCTierStep{},
			&KYCTierStepContentItem{},
			&KYCTierCommunication{},
			&KYCTierFlag{})

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("kyc_tier", "kyc_tier_step", "kyc_tier_step_item", "kyc_tier_communication", "kyc_tier_flag").Error
	},
}
