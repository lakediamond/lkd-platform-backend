package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddWithdrawalTxHashOnProposalColumn gormigrate.Migration = gormigrate.Migration{
	ID: "201903281009",
	Migrate: func(tx *gorm.DB) error {
		var req = `ALTER TABLE proposals ADD withdrawal_tx_hash text;`

		return tx.Exec(req).Error
	},
}
