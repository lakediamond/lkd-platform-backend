package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddCRMStageID gormigrate.Migration = gormigrate.Migration{
	ID: "201901101500",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE kyc_tier ADD crm_stage_id integer;
`

		return tx.Exec(req).Error
	},
}
