package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
	"lkd-platform-backend/internal/pkg/repo/models"
)

var CreatedExchangeRate gormigrate.Migration = gormigrate.Migration{
	ID: "201903261300",
	Migrate: func(tx *gorm.DB) error {
		tx.DropTable("kyc_step_common_info")
		tx.AutoMigrate(
			&models.ExchangeRate{},
			&models.ExchangeHistory{})

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("exchange_rate", "exchange_history").Error
	},
}
