package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var ClearTables gormigrate.Migration = gormigrate.Migration{
	ID: "201812051304",
	Migrate: func(tx *gorm.DB) error {
		var req = `
			TRUNCATE TABLE account_activities, jwt_tokens, order_types, orders, proposals, scope_metainfo, sub_owners, support_data, users, matches CASCADE;
		`
		return tx.Exec(req).Error
	},
}
