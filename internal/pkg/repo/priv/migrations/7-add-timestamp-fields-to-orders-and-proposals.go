package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddTimestampFieldsToOrderAndProposalTables gormigrate.Migration = gormigrate.Migration{
	ID: "201811291400",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE proposals ADD created_on timestamp;
ALTER TABLE proposals ADD withdrawn_amount DECIMAL;
ALTER TABLE proposals ADD withdrawn_on timestamp;
ALTER TABLE orders ADD created_on timestamp;
ALTER TABLE orders ADD completed_on timestamp;
`

		return tx.Exec(req).Error
	},
}
