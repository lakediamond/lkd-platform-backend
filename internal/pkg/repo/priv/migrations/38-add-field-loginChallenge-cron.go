package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddFieldLoginChallengeCron gormigrate.Migration = gormigrate.Migration{

	ID: "201903130920",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE kyc_step_crons 
ADD IF NOT EXISTS login_challenge TEXT,
ADD IF NOT EXISTS access_recovery_request_id uuid default null
;
`
		return tx.Exec(req).Error
	},
	Rollback: func(tx *gorm.DB) error {
		var req = `ALTER TABLE kyc_step_crons 
		DROP COLUMN IF EXISTS login_challenge,
		DROP COLUMN IF EXISTS access_recovery_request_id;`
		return tx.Exec(req).Error
	},
}
