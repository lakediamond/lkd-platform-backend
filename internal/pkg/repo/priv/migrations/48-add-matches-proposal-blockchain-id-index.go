package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddMatchesProposalBlockchainIdIndex gormigrate.Migration = gormigrate.Migration{
	ID: "201904051009",
	Migrate: func(tx *gorm.DB) error {
		var req = `CREATE INDEX matches_proposal_blockchain_id_index  ON matches (proposal_blockchain_id);`

		return tx.Exec(req).Error
	},
}
