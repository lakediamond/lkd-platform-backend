package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var AddInvestmentPlanToUsersTable gormigrate.Migration = gormigrate.Migration{
	ID: "201901311630",
	Migrate: func(tx *gorm.DB) error {
		var req = `
ALTER TABLE users ADD investment_plan INTEGER;
`

		return tx.Exec(req).Error
	},
}
