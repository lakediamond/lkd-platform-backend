package migrations

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

type SubOwners struct {
	EthAddress string `gorm:"primary_key"`
	IsOwner    bool
}

var CreateSubOwnersTable gormigrate.Migration = gormigrate.Migration{

	ID: "201811261200",
	Migrate: func(tx *gorm.DB) error {

		tx.AutoMigrate(
			&SubOwners{})

		return tx.Error
	},
	Rollback: func(tx *gorm.DB) error {
		return tx.DropTable("sub_owners").Error
	},
}
