package priv

import (
	"lkd-platform-backend/internal/pkg/repo/priv/migrations"
	"log"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

// Migration to create actual database version
func Migration(db *gorm.DB) {

	db.LogMode(true)

	m := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		&migrations.CreateAllTables,
		&migrations.ChangeOrderTable,
		&migrations.CreateSubOwnersTable,
		&migrations.CreateAccountActivityTable,
		&migrations.CreateScopeMetainfoTable,
		&migrations.ChangeOrdersAndProposalsTablesToBigInt,
		&migrations.AddTimestampFieldsToOrderAndProposalTables,
		&migrations.CreatePhoneVericationTable,
		&migrations.MakeBlockchainIDUinque,
		&migrations.CreateMatchesTable,
		&migrations.ClearTables,
		&migrations.AddProposalOriginalTokensAmountField,
		&migrations.AddOrderEthAmountLeftField,
		&migrations.Add2FaForUser,
		&migrations.CreateIPLocksTable,
		&migrations.CreateKYCTables,
		&migrations.CreateVerificationResultTable,
		&migrations.AddIPInfoToAccountActivities,
		&migrations.AddCRMPersonID,
		&migrations.AddBirthDate,
		&migrations.AddTargetTier,
		&migrations.CreateKYCInvestmentPlanTable,
		&migrations.CreateKYCCountryBlacklistTable,
		&migrations.AddCRMDealID,
		&migrations.AddCRMStageID,
		&migrations.CreateUserAccessRecoveryRequestTable,
		&migrations.AddBannedToProposalsTable,
		&migrations.CreateDictionaryCountryPhoneCodeTable,
		&migrations.AddCoefficientToOrdersTable,
		&migrations.AddOAuthColumn,
		&migrations.AddInvestmentPlanToUsersTable,
		&migrations.AddTimeColumsToUsersTable,
		&migrations.AddblockchainConnectionTypeUserColumn,
		&migrations.AddAuthTokenTable,
		&migrations.CreateKYCStepCommonInfoTable,
		&migrations.CreateKYCStepCronsTable,
		&migrations.AddFieldLoginChallengeCron,
		&migrations.AddFieldCrmVerificationResultCron,
		&migrations.AddFieldPhoneVerificationCron,
		&migrations.RenameKycStepCronsTable,
		&migrations.AddCompletedOnProposalColumn,
		&migrations.AddWithdrawalTxHashOnProposalColumn,
		&migrations.CreatedExchangeRate,
		&migrations.AddIndexExchangeHistory,
		&migrations.AddWithdrawnByProposalColumn,
		&migrations.AddProposalMainIndex,
		&migrations.AddMatchesProposalBlockchainIdIndex,
		&migrations.AddCronWorkerSpanID,
		&migrations.UpdateKYCTiersLabels,
	})

	if err := m.Migrate(); err != nil {
		log.Fatalf("Could not migrate: %v", err)
	}

	log.Printf("Migration did run successfully")
}
