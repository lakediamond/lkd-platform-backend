package repo

import (
	"github.com/jinzhu/gorm"

	"lkd-platform-backend/internal/pkg/repo/models"
)

//OrderRepository contains all using methods for orders table
type MatchesRepository interface {
	CreateNewMatch(match *models.Matches) error
	GetAllMatches() ([]models.Matches, error)
	GetAllMatchesByOrderID(orderBlockchainID int) ([]models.Matches, error)
	GetMatchByOrderIDAndProposalId(orderBlockchainID int, proposalBlockchainId int) (*models.Matches, error)
}

//OrderRepo is OrderRepository main implementation
type MatchesRepo struct {
	db *gorm.DB
}

//CreateNewMatch creating new match
func (repo *MatchesRepo) CreateNewMatch(match *models.Matches) error {
	err := repo.db.Create(match)
	if err != nil {
		return err.Error
	}
	return nil
}

//GetAllMatches returns all matches
func (repo *MatchesRepo) GetAllMatches() ([]models.Matches, error) {
	var matches []models.Matches

	err := repo.db.Find(&matches).Error
	if err != nil {
		return matches, err
	}

	return matches, nil
}

func (repo *MatchesRepo) GetAllMatchesByOrderID(orderBlockchainID int) ([]models.Matches, error) {
	var matches []models.Matches

	err := repo.db.Where("order_blockchain_id = ?", orderBlockchainID).Find(&matches).Error
	if err != nil {
		return matches, err
	}

	return matches, nil
}

//GetMatchByOrderIDAndProposalId returns first match with same proposal and order blockchain id or nil.
func (repo *MatchesRepo) GetMatchByOrderIDAndProposalId(orderBlockchainID int, proposalBlockchainId int) (*models.Matches, error) {
	var match models.Matches

	err := repo.db.
		Where("order_blockchain_id = ? AND proposal_blockchain_id = ?", orderBlockchainID, proposalBlockchainId).
		First(&match).
		Error

	if gorm.IsRecordNotFoundError(err) {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return &match, nil
}
