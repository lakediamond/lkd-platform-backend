package repo

import (
	"encoding/json"
	"errors"

	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"golang.org/x/oauth2"

	"lkd-platform-backend/internal/pkg/repo/models"
)

//UserRepository contains all using methods for users table
type UserRepository interface {
	UpdateTargetTier(user *models.User, investmentPlan models.KYCInvestmentPlan) error
	CreateNewUser(user *models.User) error
	GetUserByEmail(email string) (models.User, error)
	GetAllUsers() ([]models.User, error)
	UpdateUser(user *models.User) error
	UpdateUserCRMDeal(user *models.User) error
	GetUserByEthAddress(ethAddress string) (models.User, error)
	GetAllAdmins() ([]models.User, error)
	IsExistsByID(id *uuid.UUID) bool
	GetUserByID(id *uuid.UUID) (models.User, error)
	GetUserByDealID(dealID int) (models.User, error)
	UpdateUserOAuthToken(user *models.User, token *oauth2.Token) error
	UpdateBlockchainProvider(id *uuid.UUID, blockchainConnectionType string) error
}

//UserRepo is UserRepository main implementation
type UserRepo struct {
	db *gorm.DB
}

func (repo *UserRepo) UpdateUserOAuthToken(user *models.User, token *oauth2.Token) error {
	bytes, err := json.Marshal(token)
	if err != nil {
		return err
	}
	return repo.db.Model(&models.User{}).Where("id = ?", user.ID.String()).Select("oauth_token").Updates(map[string]interface{}{"oauth_token": bytes}).Error
}

//UpdateTargetTier creating new user, if user exists or cannot create - returns error
func (repo *UserRepo) UpdateTargetTier(user *models.User, investmentPlan models.KYCInvestmentPlan) error {
	return repo.db.Model(&models.User{}).Where("id = ?", user.ID.String()).Select("target_tier").Updates(map[string]string{"target_tier": investmentPlan.ID.String()}).Error
}

//CreateNewUser creating new user, if user exists or cannot create - returns error
func (repo *UserRepo) CreateNewUser(user *models.User) error {

	err := repo.db.Create(&user).Error
	if err != nil {
		if err.Error() == "pq: duplicate key value violates unique constraint \"users_email_key\"" {
			return errors.New("User account with such email address already exists")
		}

		if err.Error() == "pq: duplicate key value violates unique constraint \"users_eth_address_key\"" {
			return errors.New("User account with such Ethereum address already exists")
		}

		return err
	}

	return nil
}

//GetUserByEmail returns user finding by id, if no record with actual id - returns error
func (repo *UserRepo) GetUserByEmail(email string) (models.User, error) {
	var user models.User

	repo.db.Where("email = ?", email).Preload("KYCInvestmentPlan").Find(&user)

	if user.FirstName == "" {
		return user, errors.New("cannot find user with email " + email)
	}

	return user, nil
}

//GetUserByEthAddress returns user finding by eth_address, if no record with actual eth_address - returns error
func (repo *UserRepo) GetUserByEthAddress(ethAddress string) (models.User, error) {
	var user models.User

	err := repo.db.Where("eth_address = ?", ethAddress).Find(&user)
	if err != nil {
		return user, err.Error
	}

	return user, nil
}

//GetAllUsers returns all users
func (repo *UserRepo) GetAllUsers() ([]models.User, error) {
	var users []models.User

	err := repo.db.Find(&users).Error
	if err != nil {
		return users, err
	}

	return users, nil
}

//GetAllAdmins returns all users with role == admin
func (repo *UserRepo) GetAllAdmins() ([]models.User, error) {
	var users []models.User

	err := repo.db.Where("role = ?", models.AdminRole).Find(&users)
	if err != nil {
		return users, err.Error
	}

	return users, nil
}

//UpdateUser creating or updating user, if cannot create/update - returns error
func (repo *UserRepo) UpdateUser(user *models.User) error {

	err := repo.db.Save(user).Error
	if err != nil {
		return err
	}

	return nil
}

//UpdateUser creating or updating user, if cannot create/update - returns error
func (repo *UserRepo) UpdateUserCRMDeal(user *models.User) error {

	err := repo.db.Model(user).Select("crm_deal_id").Updates(map[string]interface{}{"crm_deal_id": user.CRMDealID}).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *UserRepo) IsExistsByID(id *uuid.UUID) bool {

	var user models.User

	err := repo.db.Where("id = ?", id).Find(&user).Error
	if err != nil {
		return false
	}

	return true
}

func (repo *UserRepo) GetUserByID(id *uuid.UUID) (models.User, error) {

	var user models.User

	err := repo.db.Where("id = ?", id).Preload("KYCInvestmentPlan").Find(&user).Error
	if err != nil {
		return user, err
	}

	return user, nil
}

func (repo *UserRepo) GetUserByDealID(dealID int) (models.User, error) {

	var user models.User

	err := repo.db.Where("crm_deal_id = ?", dealID).Preload("KYCInvestmentPlan").Find(&user).Error
	if err != nil {
		return user, err
	}

	return user, nil
}

func (repo *UserRepo) UpdateBlockchainProvider(id *uuid.UUID, blockchainConnectivityType string) error {
	err := repo.db.Model(models.User{}).
		Update("blockchain_connection_type", blockchainConnectivityType).
		Where("id = ?", id).
		Error

	if err != nil {
		return err
	}

	return nil
}
