package repo

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/repo/models"
)

//UserRepository contains all using methods for users table
type VerificationRepository interface {
	Create(verificationResult *models.VerificationResult) error
	CreateByID(verificationResult *models.VerificationResult) (*uuid.UUID, error)
	GetLastResultRecord(user *models.User) (models.VerificationResult, error)

	GetByID(id string) (*models.VerificationResult, error)
}

//UserRepo is UserRepository main implementation
type VerificationRepo struct {
	db *gorm.DB
}

func (repo *VerificationRepo) GetByID(id string) (*models.VerificationResult, error) {

	var verificationResult models.VerificationResult

	err := repo.db.Where("id = ?", id).Find(&verificationResult).Error
	if err != nil {
		return nil, err
	}

	return &verificationResult, nil
}

//CreateNewUser and return id creating new user, if user exists or cannot create - returns error
func (repo *VerificationRepo) CreateByID(verificationResult *models.VerificationResult) (*uuid.UUID, error) {

	err := repo.db.Create(&verificationResult).Error
	if err != nil {
		return nil, err
	}

	return verificationResult.ID, nil
}

//CreateNewUser creating new user, if user exists or cannot create - returns error
func (repo *VerificationRepo) Create(verificationResult *models.VerificationResult) error {

	err := repo.db.Create(verificationResult).Error
	if err != nil {
		return err
	}

	return nil
}

//GetLastResultRecord returns last verification result received from KYC layer for given user, ordered by callback_date
func (repo *VerificationRepo) GetLastResultRecord(user *models.User) (models.VerificationResult, error) {
	var result models.VerificationResult
	err := repo.db.Model(&models.VerificationResult{}).Where("user_id = ?", user.ID.String()).Order("callback_date desc").Limit(1).Find(&result).Error
	if err != nil {
		return result, err
	}

	return result, nil
}
