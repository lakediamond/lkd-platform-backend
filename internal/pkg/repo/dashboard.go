package repo

import (
	"errors"
	"math/big"
	"strconv"
	"strings"

	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
)

//DashboardRepository contains all using methods for orders table
type DashboardRepository interface {
	GetMainChartRows(daysToLoad int) ([]MainChartRow, error)
	GetAveragePriceConversion(daysToLoad int) (*decimal.Decimal, error)
	GetLastPriceConversion(daysToLoad int) (*decimal.Decimal, error)
	GetProposalQueueEstimation(price *big.Int) (*decimal.Decimal, *decimal.Decimal)
}

var (
	dashboardRecordNotFoundErr = errors.New("record not found")
)

//DashboardRepo is OrderRepository main implementation
type DashboardRepo struct {
	db *gorm.DB
}

//GetMainChartData return main chart data
func (repo *DashboardRepo) GetMainChartRows(daysToLoad int) ([]MainChartRow, error) {
	var mainChartRows []MainChartRow

	query := strings.Replace(mainChartQueryTemplate, "{daysToLoad}", strconv.Itoa(daysToLoad), -1)

	err := repo.db.Raw(query).Scan(&mainChartRows).Error
	if err != nil {
		if err.Error() == dashboardRecordNotFoundErr.Error() {
			return mainChartRows, err
		}

		return nil, err
	}

	return mainChartRows, nil
}

func (repo *DashboardRepo) GetAveragePriceConversion(daysToLoad int) (*decimal.Decimal, error) {
	result := struct {
		AveragePrice decimal.Decimal `gorm:"column:avg_price"`
	}{}

	query := strings.Replace(getAveragePriceConversionQueryTemplate, "{daysToLoad}", strconv.Itoa(daysToLoad), -1)

	err := repo.db.Raw(query).Find(&result).Error
	if err != nil {
		if err.Error() == dashboardRecordNotFoundErr.Error() {
			return &result.AveragePrice, err
		}

		return nil, err
	}

	return &result.AveragePrice, nil
}

func (repo *DashboardRepo) GetLastPriceConversion(daysToLoad int) (*decimal.Decimal, error) {
	result := struct {
		Price decimal.Decimal `gorm:"column:price"`
	}{}

	query := strings.Replace(getLastPriceConversionQueryTemplate, "{daysToLoad}", strconv.Itoa(daysToLoad), -1)

	err := repo.db.Raw(query).First(&result).Error
	if err != nil {
		if err.Error() == dashboardRecordNotFoundErr.Error() {
			return &result.Price, err
		}

		return nil, err
	}

	return &result.Price, nil
}

func (repo *DashboardRepo) GetProposalQueueEstimation(price *big.Int) (*decimal.Decimal, *decimal.Decimal) {
	result := struct {
		TokensAmount    *decimal.Decimal
		ProposalsAmount *decimal.Decimal
	}{}

	err := repo.db.
		Select("SUM(balance) as tokens_amount, Count(id) as proposals_amount").
		Table("proposals").
		Where("status='active' AND price <= ?", price.String()).
		Scan(&result).Error
	if err != nil {
		return nil, nil
	}

	return result.TokensAmount, result.ProposalsAmount
}

//MainChartData type for describe main chart data
type MainChartRow struct {
	MinValue           decimal.Decimal `gorm:"column:min_value"`
	MaxValue           decimal.Decimal `gorm:"column:max_value"`
	PastOrderVolume    uint            `gorm:"column:past_order_volume"`
	OpenProposalVolume uint            `gorm:"column:open_proposal_volume"`
}

const getLastPriceConversionQueryTemplate = `
SELECT price FROM proposals INNER JOIN matches ON matches.proposal_blockchain_id = proposals.blockchain_id
WHERE completed_on > current_timestamp - interval '{daysToLoad} days'
ORDER BY completed_on DESC`

const getAveragePriceConversionQueryTemplate = `
SELECT AVG(price) as avg_price FROM proposals INNER JOIN matches ON matches.proposal_blockchain_id = proposals.blockchain_id
WHERE completed_on > current_timestamp - interval '{daysToLoad} days'`

const mainChartQueryTemplate = `
WITH
	dateFrom AS (SELECT date FROM (VALUES (current_timestamp - interval '{daysToLoad} days')) AS dateFrom(date)),
	prices AS (
		SELECT proposals.price / 100 AS price
		FROM proposals, dateFrom
		WHERE proposals.created_on > dateFrom.date
		GROUP BY price
		UNION
		SELECT orders.price / 100 AS price
		FROM orders, dateFrom
		WHERE orders.completed_on > dateFrom.date
		GROUP BY price
	),
	step AS (select step_value FROM (VALUES (0.1)) AS step(step_value)),
	order_stats AS (
		SELECT
			CASE
				WHEN MIN(price) < MAX(price) THEN ROUND(cast((MIN(price)) AS numeric), 1)
           		ELSE 0
        	END AS min,
			ROUND(CAST((MAX(price)) as numeric), 1) + MAX(step_value) AS max
    	FROM prices, step
	),
	range_stats AS (
		SELECT CAST(((max - min) / step_value) AS INTEGER) AS stepsCount
		FROM order_stats, step
	),
	histogram AS (
		SELECT WIDTH_BUCKET(price, min, max, stepsCount) AS bucket
		FROM prices, order_stats, range_stats
		GROUP BY bucket
		ORDER BY bucket
	)
	SELECT
		(min + (bucket - 1) * step_value) AS min_value,
		(min + bucket * step_value) AS max_value,
		(
			SELECT SUM(token_amount) FROM orders
			WHERE price >= (min + (bucket - 1) * step_value) * 100 AND price < (min + bucket * step_value * 100 )
			AND orders.completed_on > dateFrom.date
		) AS past_order_volume,
		(
			SELECT SUM(original_tokens - withdrawn_amount) FROM proposals
			WHERE price >= (min + (bucket - 1) * step_value) * 100 AND price < (min + bucket * step_value) * 100
			AND created_on > dateFrom.date
		) AS open_proposal_volume
	FROM histogram, order_stats, step, dateFrom`
