package repo

import (
	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/repo/models"
)

/*
 *Table support_data needed to store last received Ethereum block number.
 *This block needed to avoid double check events after program reboot
 */

//SupportDataRepository contains all using methods for support_data table
type SupportDataRepository interface {
	GetLastBlock() uint
	SetLastBlock(value uint) error
}

//SupportDataRepo is SupportDataRepository main implementation
type SupportDataRepo struct {
	db *gorm.DB
}

//GetLastBlock getting last listened ethereum block number
func (repo *SupportDataRepo) GetLastBlock() uint {
	var data models.SupportData

	repo.db.Where("id = ?", "1").Find(&data)

	return data.LastBlock
}

//SetLastBlock setting last listened ethereum block number
func (repo *SupportDataRepo) SetLastBlock(value uint) error {
	log.Debug().Msgf("Setting last block by %d", value)

	var data models.SupportData

	data.ID = 1
	data.LastBlock = value

	err := repo.db.Save(&data)
	if err != nil {
		return err.Error
	}

	return nil
}
