package repo

import (
	"lkd-platform-backend/internal/pkg/repo/models"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
)

type AuthTokenRepository interface {
	Create(token *models.AuthToken) error
	GetLockedRecord(id *uuid.UUID) (*LockedAuthTokenRecord, error)
	UpdateLockedRecord(lockedRecord *LockedAuthTokenRecord) error
	UnlockRecord(lockedRecord *LockedAuthTokenRecord) error
}

type LockedAuthTokenRecord struct {
	AuthToken *models.AuthToken
	tx        *gorm.DB
}

type AuthTokenRepo struct {
	db *gorm.DB
}

func (repo *AuthTokenRepo) Create(token *models.AuthToken) error {
	return repo.db.Create(token).Error
}

func (repo *AuthTokenRepo) GetLockedRecord(id *uuid.UUID) (*LockedAuthTokenRecord, error) {
	var token models.AuthToken

	tx := repo.db.Begin()
	err := tx.Raw("SELECT * from auth_tokens where id = ? FOR UPDATE", id.String()).Find(&token).Error
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	return &LockedAuthTokenRecord{AuthToken: &token, tx: tx}, nil

}

func (repo *AuthTokenRepo) UpdateLockedRecord(lockedRecord *LockedAuthTokenRecord) error {
	lockedRecord.AuthToken.UpdatedAt = time.Now()
	err := lockedRecord.tx.Save(lockedRecord.AuthToken).Error
	if err != nil {
		lockedRecord.tx.Rollback()
		return err
	}
	return nil
}

func (repo *AuthTokenRepo) UnlockRecord(lockedRecord *LockedAuthTokenRecord) error {
	return lockedRecord.tx.Commit().Error
}
