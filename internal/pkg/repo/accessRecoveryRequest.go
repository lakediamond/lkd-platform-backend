package repo

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/repo/models"
)

type UserAccessRecoveryRequestRepository interface {
	Save(request *models.UserAccessRecoveryRequest) error
	GetRequestByID(id *uuid.UUID) (models.UserAccessRecoveryRequest, error)
	GetRequestByUserID(userID *uuid.UUID) (models.UserAccessRecoveryRequest, error)

	GetRequestByLast(userID *uuid.UUID, status []string) (models.UserAccessRecoveryRequest, error)
}

var (
	errRecordNotFound = errors.New("record not found")
)

type UserAccessRecoveryRequestRepo struct {
	db *gorm.DB
}

func (repo *UserAccessRecoveryRequestRepo) Save(request *models.UserAccessRecoveryRequest) error {
	err := repo.db.Save(request)
	if err != nil {
		return err.Error
	}

	return nil
}

func (repo *UserAccessRecoveryRequestRepo) GetRequestByID(id *uuid.UUID) (models.UserAccessRecoveryRequest, error) {

	var userAccessRecoveryRequest models.UserAccessRecoveryRequest

	err := repo.db.Where("id = ?", id).Find(&userAccessRecoveryRequest).Error
	if err != nil {
		return userAccessRecoveryRequest, err
	}

	return userAccessRecoveryRequest, nil
}

// TO DO deprecated
func (repo *UserAccessRecoveryRequestRepo) GetRequestByUserID(userID *uuid.UUID) (models.UserAccessRecoveryRequest, error) {

	t := time.Now().Add(-30 * time.Minute)

	var userAccessRecoveryRequest models.UserAccessRecoveryRequest

	err := repo.db.Where("user_id = ? and created_at >= ?", userID, t).Find(&userAccessRecoveryRequest).Error
	if err != nil {
		return userAccessRecoveryRequest, err
	}

	return userAccessRecoveryRequest, nil
}

func (repo *UserAccessRecoveryRequestRepo) GetRequestByLast(userID *uuid.UUID, status []string) (models.UserAccessRecoveryRequest, error) {

	var userAccessRecoveryRequest models.UserAccessRecoveryRequest
	err := repo.db.Where("user_id = ? AND status IN(?)", userID, status).Order("updated_at desc").Limit(1).Find(&userAccessRecoveryRequest).Error
	if err != nil {
		if err.Error() != errRecordNotFound.Error() {
			return userAccessRecoveryRequest, err
		}
	}

	return userAccessRecoveryRequest, nil
}
