package repo

import (
	"github.com/jinzhu/gorm"
	//postgres database driver
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"lkd-platform-backend/internal/pkg/repo/kyc"
)

//Repo contains all table repositories
type Repo struct {
	DB                    *gorm.DB
	SupportData           SupportDataRepository
	User                  UserRepository
	JwtToken              JwtTokenRepository
	OrderTypes            OrderTypesRepository
	Order                 OrderRepository
	Proposal              ProposalRepository
	SubOwner              SubOwnerRepository
	AccountActivity       AccountActivityRepository
	Scopes                ScopeRepository
	Matches               MatchesRepository
	PhoneVerification     PhoneVerificationRepository
	IPLock                IPLockRepository
	KYC                   kyc.Repository
	VerificationResult    VerificationRepository
	AccessRecoveryRequest UserAccessRecoveryRequestRepository
	AuthToken             AuthTokenRepository
	Dashboard             DashboardRepository
	Exchange              ExchangeRateRepository
}
