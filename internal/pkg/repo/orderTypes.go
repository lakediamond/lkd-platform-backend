package repo

import (
	"github.com/jinzhu/gorm"

	"lkd-platform-backend/internal/pkg/repo/models"
)

//OrderTypesRepository contains all using methods for order_types table
type OrderTypesRepository interface {
	SaveOrderType(*models.OrderType) error
	GetOrderTypeByID(id uint) (models.OrderType, error)
	GetAllOrderTypes() ([]models.OrderType, error)
}

//OrderTypesRepo is OrderTypesRepository main implementation
type OrderTypesRepo struct {
	db *gorm.DB
}

//SaveOrderType creating or updating order_type record
func (repo *OrderTypesRepo) SaveOrderType(orderType *models.OrderType) error {

	err := repo.db.Save(orderType)
	if err != nil {
		return err.Error
	}

	return nil
}

//GetOrderTypeByID returns order_type record by id, returns err if not found
func (repo *OrderTypesRepo) GetOrderTypeByID(id uint) (models.OrderType, error) {
	var orderType models.OrderType

	err := repo.db.Where("id = ?", id).Find(&orderType)
	if err != nil {
		return orderType, err.Error
	}

	return orderType, nil
}

//GetAllOrderTypes returns all order_types
func (repo *OrderTypesRepo) GetAllOrderTypes() ([]models.OrderType, error) {
	var orderTypes []models.OrderType

	err := repo.db.Find(&orderTypes).Error
	if err != nil {
		return orderTypes, err
	}

	return orderTypes, nil
}
