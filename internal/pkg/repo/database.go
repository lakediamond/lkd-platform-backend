package repo

import (
	"fmt"
	"lkd-platform-backend/internal/pkg/repo/kyc"

	"github.com/jinzhu/gorm"

	"lkd-platform-backend/pkg/gormlogger"
)

//GetDbClient initializing new database client
func GetDbClient(name, user, pass, host, port string) (*Repo, error) {
	db, err := gorm.Open("postgres", fmt.Sprintf(
		"user=%s password=%s dbname=%s host=%s port=%s sslmode=disable",
		user, pass, name, host, port))

	db.SetLogger(&gormlogger.GormLogger{})

	db.LogMode(true)
	if err != nil {
		return nil, err
	}

	return &Repo{
		db,
		&SupportDataRepo{db},
		&UserRepo{db},
		&JwtTokenRepo{db},
		&OrderTypesRepo{db},
		&OrderRepo{db},
		&ProposalRepo{db},
		&SubOwnerRepo{db},
		&AccountActivityRepo{db},
		&ScopeRepo{db},
		&MatchesRepo{db},
		&PhoneVerificationRepo{db},
		&IPLockRepo{db},
		kyc.NewRepo(db),
		&VerificationRepo{db},
		&UserAccessRecoveryRequestRepo{db},
		&AuthTokenRepo{db},
		&DashboardRepo{db},
		&ExchangeRateRepo{db},
	}, nil
}
