package repo

import (
	"math/big"
	"strings"

	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"

	"lkd-platform-backend/internal/pkg/pagination"
	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/internal/pkg/repo/response"
)

//ProposalRepository contains all using methods for proposals table
type ProposalRepository interface {
	UpdateProposal(proposal *models.Proposal) error
	GetProposalByID(id uint) (models.Proposal, error)
	GetProposalByBlockchainID(blockchainID uint) (models.Proposal, error)
	GetProposalByTxHash(txHash string) (models.Proposal, error)
	GetAllProposals() ([]models.Proposal, error)
	GetOwnerProposal() ([]models.Proposal, error)
	GetAllUserProposals(filters map[string]interface{}) ([]models.Proposal, error)
	GetAllProposalsToMatch(price decimal.Decimal) ([]models.Proposal, error)
	GetAllProposalsToMatchSimulate(price decimal.Decimal) ([]models.Proposal, error)
	SetAllProposalsBannedByEthAddress(address string) error
	GetPositionHint(price *big.Int) (uint, error)

	GetAllUserProposalsPagination(filters interface{}, orderBy map[string]string, offset int, limit int) pagination.Paginator
}

//ProposalRepo is ProposalRepository main implementation
type ProposalRepo struct {
	db *gorm.DB
}

//UpdateProposal creating new or updating proposal
func (repo *ProposalRepo) UpdateProposal(proposal *models.Proposal) error {
	err := repo.db.Save(proposal)
	if err != nil {
		return err.Error
	}
	return nil
}

//GetProposalByID returns proposal finding by id, if no record with actual id - returns error
func (repo *ProposalRepo) GetProposalByID(id uint) (models.Proposal, error) {
	var proposal models.Proposal

	err := repo.db.Where("id = ?", id).Find(&proposal)
	if err != nil {
		return proposal, err.Error
	}

	return proposal, nil
}

//GetProposalByBlockchainID returns proposal finding by blockchain_id, if no record with actual blockchain_id - returns error
func (repo *ProposalRepo) GetProposalByBlockchainID(blockchainID uint) (models.Proposal, error) {
	var proposal models.Proposal

	err := repo.db.Where("blockchain_id = ?", blockchainID).Find(&proposal)
	if err != nil {
		return proposal, err.Error
	}

	return proposal, nil
}

//GetAllProposals returns all proposals
func (repo *ProposalRepo) GetOwnerProposal() ([]models.Proposal, error) {
	var proposals []models.Proposal

	err := repo.db.Select("DISTINCT created_by").Find(&proposals).Error
	if err != nil {
		return proposals, err
	}

	return proposals, nil
}

//GetAllProposals returns all proposals
func (repo *ProposalRepo) GetAllProposals() ([]models.Proposal, error) {
	var proposals []models.Proposal

	err := repo.db.Find(&proposals).Error
	if err != nil {
		return proposals, err
	}

	return proposals, nil
}

//GetPositionHint returns all proposals
func (repo *ProposalRepo) GetPositionHint(price *big.Int) (uint, error) {
	var proposal models.Proposal

	err := repo.db.
		Select("blockchain_id").
		Where("price <= ? AND status = ?", price.String(), "active").
		Order("price desc, blockchain_id desc").
		Take(&proposal).
		Error

	if gorm.IsRecordNotFoundError(err) {
		return 0, nil
	}

	if err != nil {
		return 0, err
	}

	return proposal.BlockchainID, nil
}

func (repo *ProposalRepo) GetAllUserProposalsPagination(filters interface{}, orderBy map[string]string, offset int, limit int) pagination.Paginator {
	var proposalResponseDB []response.ProposalResponseDB

	orderByAlias := make([]string, 0)
	for field, sort := range orderBy {
		sortField, ok := pagination.MatchingFieldProposal(field)
		if !ok {
			continue
		}

		orderByAlias = append(orderByAlias, "p."+sortField+" "+sort)
	}

	queryWhere := pagination.QueryWhere(filters)
	query := strings.Replace(proposalsQuery, "{queryWhere}", queryWhere, -1)
	paginator := pagination.Paging(&pagination.Param{
		DB:           repo.db.Raw(query),
		DBCount:      repo.db.Table("proposals p").Where(queryWhere),
		Page:         offset,
		Limit:        limit,
		OrderBy:      orderByAlias,
		ShowSQL:      true,
		NotUsedModel: true,
	}, &proposalResponseDB)

	return paginator
}

const proposalsQuery = `
SELECT
	p.*,
	(
		SELECT json_agg(
			json_build_object(
				'matcher_id', m.id,
				'order_id', m.order_blockchain_id,
				'tokens_invested', m.tokens,
				'ether_invested', m.ether,
				'investment_tx', m.tx_hash,
				'created_on', o.created_on,
				'investment_share', ROUND((m.tokens/o.token_amount) *100, 2)
			)
		) AS matches_list
		FROM matches m
		LEFT JOIN orders o ON o.blockchain_id = m.order_blockchain_id
		WHERE m.proposal_blockchain_id = p.blockchain_id
	) as matches
FROM proposals p
WHERE {queryWhere}
`

//GetAllUserProposals returns all user proposals
func (repo *ProposalRepo) GetAllUserProposals(filters map[string]interface{}) ([]models.Proposal, error) {
	var proposals []models.Proposal

	err := repo.db.Where(filters).Find(&proposals).Error
	if err != nil {
		return proposals, err
	}

	return proposals, nil
}

//GetAllUserProposals returns all user proposals
func (repo *ProposalRepo) GetAllProposalsToMatch(price decimal.Decimal) ([]models.Proposal, error) {
	var proposals []models.Proposal

	err := repo.db.Where("price <= ? AND status = ? AND banned = ?", price, "active", false).Order("price, blockchain_id").Find(&proposals).Error
	if err != nil {
		return proposals, err
	}

	return proposals, nil
}

//GetAllUserProposals returns all user proposals
func (repo *ProposalRepo) GetAllProposalsToMatchSimulate(price decimal.Decimal) ([]models.Proposal, error) {
	var proposals []models.Proposal

	err := repo.db.Where("price <= ? AND status = ?", price, "active").Order("price, blockchain_id").Find(&proposals).Error
	if err != nil {
		return proposals, err
	}

	return proposals, nil
}

func (repo *ProposalRepo) GetProposalByTxHash(txHash string) (models.Proposal, error) {
	var proposal models.Proposal
	err := repo.db.Table("proposals").Where("tx_hash = ?", txHash).Find(&proposal).Error
	if err != nil {
		return proposal, err
	}

	return proposal, nil
}

func (repo *ProposalRepo) SetAllProposalsBannedByEthAddress(address string) error {
	err := repo.db.Table("proposals").Where("created_by = ?", address).Updates(map[string]interface{}{"banned": true}).Error
	if err != nil {
		return err
	}

	return nil
}
