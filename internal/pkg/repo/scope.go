package repo

import (
	"github.com/jinzhu/gorm"

	"lkd-platform-backend/internal/pkg/repo/models"
)

type ScopeRepository interface {
	GetAllScopes(scopeTokens []string) ([]models.ScopeMetainfo, error)
}

type ScopeRepo struct {
	db *gorm.DB
}

func (repo *ScopeRepo) GetAllScopes(scopeTokens []string) ([]models.ScopeMetainfo, error) {
	var scopes []models.ScopeMetainfo

	err := repo.db.Where("Code in (?)", scopeTokens).Find(&scopes).Error
	if err != nil {
		return scopes, err
	}

	return scopes, nil
}

// type OAuthScope struct {
// 	Code        string `json:"code"`
// 	Description string `json:"description"`
// }
