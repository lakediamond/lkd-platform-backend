package models

import (
	"time"

	"github.com/satori/go.uuid"
)

type VerificationResult struct {
	ID                          *uuid.UUID `gorm:"type:uuid; primary_key" json:"omitempty"`
	UserID                      *uuid.UUID `gorm:"type:uuid" json:"omitempty"`
	CallBackType                string     `json:"callBackType"`
	CallbackDate                time.Time  `json:"callbackDate"`
	CallbackUrl                 string     `json:"callback_url"`
	FirstAttemptDate            string     `json:"firstAttemptDate"`
	IDCheckDataPositions        string     `json:"idCheckDataPositions"`
	IDCheckDocumentValidation   string     `json:"idCheckDocumentValidation"`
	IDCheckHologram             string     `json:"idCheckHologram"`
	IDCheckMRZCode              string     `json:"idCheckMRZcode"`
	IDCheckMicroprint           string     `json:"idCheckMicroprint"`
	IDCheckSecurityFeatures     string     `json:"idCheckSecurityFeatures"`
	IDCheckSignature            string     `json:"idCheckSignature"`
	IDCountry                   string     `json:"idCountry"`
	IDDob                       string     `json:"idDob"`
	IDExpiry                    string     `json:"idExpiry"`
	IDFirstName                 string     `json:"idFirstName"`
	IDLastName                  string     `json:"idLastName"`
	IDNumber                    string     `json:"idNumber"`
	IDScanImage                 string     `json:"idScanImage"`
	IDScanImageFace             string     `json:"idScanImageFace"`
	IDScanSource                string     `json:"idScanSource"`
	IDScanStatus                string     `json:"idScanStatus"`
	IDType                      string     `json:"idType"`
	IdentityVerification        string     `sql:"type:JSONB NOT NULL DEFAULT '{}'::JSONB" json:"identityVerification, omitempty"`
	JumioIDScanReference        string     `json:"jumioIdScanReference"`
	MerchantIdScanReference     string     `json:"merchantIdScanReference"`
	PersonalNumber              string     `json:"personalNumber"`
	TransactionDate             string     `json:"transactionDate"`
	VerificationStatus          string     `json:"verificationStatus"`
	VerifiedNameSanctionsScreen struct {
		Matches []interface{} `json:"matches"`
	} `json:"verified_name_sanctions_screen"`
}

//BeforeSave for verification_result table generating random uuid
func (verificationResult *VerificationResult) BeforeSave() {
	if verificationResult.ID == nil {
		u := uuid.NewV4()
		verificationResult.ID = &u
	}

	return
}

func (verificationResult *VerificationResult) BeforeCreate() (err error) {
	if len(verificationResult.IdentityVerification) == 0 {
		verificationResult.IdentityVerification = "{}"
	}
	return
}
