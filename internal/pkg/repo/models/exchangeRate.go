package models

import (
	"time"
)

var (
	RATE_CODE_ETH_EUR = "etheur"
	RATE_CODE_EUR_CHF = "eurchf"
	CODE_RATES        = []string{RATE_CODE_ETH_EUR, RATE_CODE_EUR_CHF}
)

type ExchangeRate struct {
	ID        uint      `gorm:"primary_key" json:"id"`
	Code      string    `gorm:"column:code" json:"code"`
	Rate      float64   `gorm:"column:rate" json:"rate"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
}

func (ExchangeRate) TableName() string {
	return "exchange_rate"
}

type ExchangeHistory struct {
	ID        uint    `gorm:"primary_key"`
	Code      string  `gorm:"column:code"`
	Rate      float64 `gorm:"column:rate"`
	CreatedAt time.Time
}

func (ExchangeHistory) TableName() string {
	return "exchange_history"
}
