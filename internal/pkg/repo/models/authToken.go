package models

import (
	"time"

	"github.com/satori/go.uuid"
)

//AuthToken database table definition
type AuthToken struct {
	ID              *uuid.UUID `gorm:"type:uuid;primary_key"`
	CreatedAt       time.Time
	UpdatedAt       time.Time
	UserId          *uuid.UUID `gorm:"type:uuid REFERENCES users(id)"`
	SerializedToken string
	Description     string
}
