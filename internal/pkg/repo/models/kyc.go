package models

import (
	"database/sql/driver"
	"encoding/json"
	"math/big"
	"time"

	"github.com/satori/go.uuid"
)

type KYCTierCode = int

const Tier0 KYCTierCode = 0
const Tier1 KYCTierCode = 1
const Tier2 KYCTierCode = 2
const Tier3 KYCTierCode = 3

type CRMStageID = int

const StageTier0 CRMStageID = 7
const StageTier1 CRMStageID = 8
const StageTier2 CRMStageID = 9
const StageTier3 CRMStageID = 10

type KYCTierStatus = string

const TierOpened KYCTierStatus = "OPENED"
const TierSubmitted KYCTierStatus = "SUBMITTED"
const TierRework KYCTierStatus = "REWORK"
const TierLocked KYCTierStatus = "LOCKED"
const TierGranted KYCTierStatus = "GRANTED"
const TierRejected KYCTierStatus = "REJECTED"

type KYCTierStepStatus = string

const StepOpened KYCTierStepStatus = "OPENED"
const StepCustomerConfirmed KYCTierStepStatus = "CUSTOMER_CONFIRMED"
const StepVerified KYCTierStepStatus = "VERIFIED"
const StepLocked KYCTierStepStatus = "LOCKED"

type KYCTierStepCode = string

const PhoneVerificationStep KYCTierStepCode = "phone_verification"
const CommonInfo KYCTierStepCode = "common_info"
const IdentityScans KYCTierStepCode = "identity_scans"
const ComplianceReview KYCTierStepCode = "compliance_review"

type KYCMediaItemType = string

const IDFrontSide KYCMediaItemType = "front_image"
const IDBackSide KYCMediaItemType = "back_image"
const IDSelfie KYCMediaItemType = "face_image"

type KYCTierMediaItemStatus = string

const MediaItemUploaded KYCTierMediaItemStatus = "UPLOADED"
const MediaItemInvalid KYCTierMediaItemStatus = "INVALID"
const MediaItemValid KYCTierMediaItemStatus = "VALID"
const MediaItemProcessed KYCTierMediaItemStatus = "PROCESSED"

type KYCCustomerContributionOption = string

const OptEthereum KYCCustomerContributionOption = "ethereum"
const OptFiat KYCCustomerContributionOption = "fiat"

type JSONB map[string]interface{}

func (j JSONB) Value() (driver.Value, error) {
	valueString, err := json.Marshal(j)
	return string(valueString), err
}

func (j *JSONB) Scan(value interface{}) error {
	if err := json.Unmarshal(value.([]byte), &j); err != nil {
		return err
	}
	return nil
}

func (u *User) GetMediaTypes() []KYCMediaItemType {
	return []KYCMediaItemType{IDFrontSide, IDBackSide, IDSelfie}
}

func (u *User) IsValidMediaType(mediaType KYCTierMediaItemStatus) bool {
	for _, validItemType := range u.GetMediaTypes() {
		if validItemType == mediaType {
			return true
		}
	}
	return false
}

func (u *User) IsValidTier(tierCode KYCTierCode) bool {
	if tierCode >= Tier0 && tierCode <= Tier3 {
		return true
	}
	return false
}

type KYCInvestmentPlan struct {
	ID          *uuid.UUID `gorm:"type:uuid; primary_key" json:"id"`
	Code        int        `json:"code"`
	MaxLimit    int        `json:"max_limit"`
	Currency    string     `json:"currency"`
	Description string     `json:"description"`
}

type KYCTierCommunication struct {
	ID        *uuid.UUID `gorm:"type:uuid; primary_key" json:"id"`
	KYCTierID *uuid.UUID `gorm:"type:uuid REFERENCES kyc_tier(id);not null" json:"tier_id"`
	Type      string
	Message   string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

type KYCTierFlag struct {
	ID                      *uuid.UUID `gorm:"type:uuid; primary_key" json:"id"`
	KYCTierID               *uuid.UUID `gorm:"type:uuid REFERENCES kyc_tier(id);not null" json:"tier_id"`
	Code                    string
	Value                   string
	CRMPipelineFailureStage int `json:"-"`
	CreatedAt               time.Time
	UpdatedAt               time.Time
	DeletedAt               *time.Time
}

type KYCTierStepContentItem struct {
	ID            *uuid.UUID                   `gorm:"type:uuid; primary_key" json:"id"`
	Status        string                       `json:"status"`
	ItemOrder     int                          `json:"order"`
	KYCTierStepID *uuid.UUID                   `gorm:"type:uuid REFERENCES kyc_tier_step(id);not null" json:"step_id"`
	Code          string                       `json:"code"`
	Value         string                       `json:"value"`
	Enabled       bool                         `json:"enabled"`
	EnabledAt     time.Time                    `json:"enabled_at"`
	Options       []DictionaryCountryPhoneCode `json:"data"`

	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-"`
}

type KYCTierStep struct {
	ID           *uuid.UUID                `gorm:"type:uuid; primary_key" json:"id"`
	Status       KYCTierStepStatus         `json:"status"`
	StepOrder    int                       `json:"order"`
	Title        string                    `json:"title"`
	Code         string                    `json:"code"`
	ContentItems []*KYCTierStepContentItem `gorm:"foreignkey:KYCTierStepID" json:"content_items,omitempty"`
	KYCTierID    *uuid.UUID                `gorm:"type:uuid REFERENCES kyc_tier(id);not null" json:"tier_id"`
	CreatedAt    time.Time                 `json:"-"`
	UpdatedAt    time.Time                 `json:"-"`
	DeletedAt    *time.Time                `json:"-"`
}

type KYCTier struct {
	ID             *uuid.UUID             `gorm:"type:uuid; primary_key" json:"id"`
	Code           int                    `json:"code"`
	Status         KYCTierStatus          `json:"status"`
	CRMStageID     int                    `gorm:"column:crm_stage_id"`
	Steps          []KYCTierStep          `gorm:"foreignkey:KYCTierID" json:"steps"`
	Flags          []KYCTierFlag          `gorm:"foreignkey:KYCTierID" json:"flags,omitempty"`
	Communications []KYCTierCommunication `gorm:"foreignkey:KYCTierID" json:"communications,omitempty"`
	UserID         *uuid.UUID             `gorm:"type:uuid REFERENCES users(id);not null" json:"user_id"`
	CreatedAt      time.Time              `json:"-"`
	UpdatedAt      time.Time              `json:"-"`
	DeletedAt      *time.Time             `json:"-"`
}

type KYCSummaryEntry struct {
	ID          int    `json:"id"`
	Description string `json:"name"`
}

type KYCSummary struct {
	Achieved *KYCSummaryEntry `json:"achieved"`
	Ongoing  *KYCSummaryEntry `json:"ongoing"`
	Upcoming *KYCSummaryEntry `json:"upcoming"`
}

type KYCCountryBlacklist struct {
	ID   *uuid.UUID `gorm:"type:uuid; primary_key"`
	Code string     `json:"code" gorm:"type:varchar(2)"`
	Name string     `json:"name"`
}

type StepCommonInfoForm struct {
	ID                   string `json:"id" required:"true"`
	RegAddressCountry    string `json:"reg_address_country" required:"true"`
	RegAddressZIP        string `json:"reg_address_zip" required:"true"`
	RegAddressCity       string `json:"reg_address_city" required:"true"`
	RegAddressDetails    string `json:"reg_address_details_1" required:"true"`
	RegAddressDetailsExt string `json:"reg_address_details_2"`
	ContributionOption   string `json:"contribution_option" required:"true"`
	BankName             string `json:"contribution_bank_name"`
	BankIBAN             string `json:"contribution_iban"`
	BankAddress          string `json:"contribution_bank_address"`
	Phone                string `json:"-"`
	Email                string `json:"email,omitempty"`
}

type StepComplianceReviewForm struct {
	ID string `json:"id" required:"true"`
}

type Worker struct {
	ID                      *uuid.UUID `gorm:"type:uuid; primary_key"`
	UserID                  *uuid.UUID `gorm:"type:uuid REFERENCES users(id);not null"`
	Event                   string
	Phone                   string
	LoginChallenge          string     `gorm:"column:login_challenge"`
	AccessRecoveryRequestID *uuid.UUID `gorm:"type:uuid;column:access_recovery_request_id"`
	CrmVerificationResultID *uuid.UUID `gorm:"type:uuid;crm_verification_result_id"`
	PhoneVerificationID     *uuid.UUID `gorm:"type:uuid;phone_verification_id"`
	State                   int
	SpanID                  uint64 `gorm:"column:span_id"`
	CreatedAt               time.Time
	UpdatedAt               time.Time
}

//BeforeSave for kyc_step_common_info table generating random uuid
func (w *Worker) BeforeSave() {
	if w.ID == nil {
		u := uuid.NewV4()
		w.ID = &u
	}
	return
}

func (Worker) TableName() string {
	return "cron_worker"
}

type State = int
type CronEvent = string

const (
	EventCreateNewPerson             CronEvent = "eventCreateNewPerson"
	EventCreateUpdateCRMDeal         CronEvent = "eventCreateUpdateCRMDeal"
	EventCreateCRMDealNote           CronEvent = "eventCreateCRMDealNote"
	EventCreateEmailVerification     CronEvent = "eventCreateEmailVerification"
	EventCreateEmailRecoveryPassword CronEvent = "eventCreateEmailRecoveryPassword"
	EventSendSMS                     CronEvent = "eventSendSms"
	EventAddToWhitelist              CronEvent = "eventAddToWhitelist"

	StateNew          State = 0
	StateInProcessing State = 1
	StateDone         State = 2
	StateFailed       State = 3
)

type BtcsSelfieValidationResult struct {
	Validity   bool   `json:"validity" required:"true"`
	Similarity string `json:"similarity" required:"true"`
	Reason     string `json:"reason"`
}

func (kt *KYCTier) GetTierTokensLimit() *big.Int {
	var maxTokens *big.Int

	switch kt.CRMStageID {
	case 7:
		maxTokens = big.NewInt(5454)
	case 8:
		maxTokens = big.NewInt(27272)
	case 9:
		maxTokens = big.NewInt(181818)
	case 10:
		maxTokens = big.NewInt(9090909)
	}

	return maxTokens
}

// TableName overrides gorm built-in table name definition
func (KYCCountryBlacklist) TableName() string {
	return "kyc_country_blacklist"
}

//BeforeSave for kyc_country_blacklist table generating random uuid
func (kt *KYCCountryBlacklist) BeforeSave() {
	if kt.ID == nil {
		u := uuid.NewV4()
		kt.ID = &u
	}
	return
}

//BeforeSave for kyc_tier table generating random uuid
func (kt *KYCTier) BeforeSave() {
	if kt.ID == nil {
		u := uuid.NewV4()
		kt.ID = &u
	}
	return
}

func (kts *KYCTierStep) BeforeSave() {
	if kts.ID == nil {
		u := uuid.NewV4()
		kts.ID = &u
	}
	return
}

func (ktsci *KYCTierStepContentItem) BeforeSave() {
	if ktsci.ID == nil {
		u := uuid.NewV4()
		ktsci.ID = &u
	}
	return
}

func (ktf *KYCTierFlag) BeforeSave() {
	if ktf.ID == nil {
		u := uuid.NewV4()
		ktf.ID = &u
	}
	return
}

func (ktsc *KYCTierStepContentItem) AfterFind() (err error) {
	if ktsc.Status != StepVerified && time.Now().After(ktsc.EnabledAt) {
		ktsc.Enabled = true
		ktsc.EnabledAt = time.Time{}
	}
	return
}

func (KYCInvestmentPlan) TableName() string {
	return "kyc_investment_plan"
}

func (KYCTier) TableName() string {
	return "kyc_tier"
}

func (KYCTierStep) TableName() string {
	return "kyc_tier_step"
}

func (KYCTierStepContentItem) TableName() string {
	return "kyc_tier_step_item"
}

func (KYCTierFlag) TableName() string {
	return "kyc_tier_flag"
}
