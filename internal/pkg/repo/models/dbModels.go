package models

import (
	"encoding/json"
	"strings"
	"time"

	"github.com/satori/go.uuid"
	"github.com/shopspring/decimal"
	"golang.org/x/oauth2"
)

//Status is enum for proposal table
type Status string

//Role is enum for user table
type Role string

const (
	//Active when proposal is active
	Active Status = "active"
	//cancelled when proposal is withdrawn
	Cancelled Status = "cancelled"
	//completed when proposal is closed
	Completed Status = "completed"

	//UserRole role for customer
	UserRole Role = "customer"
	//AdminRole role for admin
	AdminRole Role = "admin"
)

//User database table definition
type User struct {
	ID                       *uuid.UUID           `gorm:"type:uuid; primary_key" json:"id"`
	FirstName                string               `json:"first_name"`
	LastName                 string               `json:"last_name"`
	Email                    string               `gorm:"unique" json:"email"`
	EmailVerified            bool                 `json:"email_verified"`
	Role                     Role                 `json:"role"`
	Locked                   bool                 `json:"locked"`
	Password                 string               `json:"-"`
	InvestmentPlan           int                  `json:"investment_plan"`
	EthAddress               string               `gorm:"unique" json:"eth_address"`
	BirthDate                string               `json:"birth_date"`
	Google2FASecret          string               `gorm:"column:google_2fa_secret" json:"-"`
	Google2FAEnabled         bool                 `gorm:"column:google_2fa_enabled" json:"google_2fa_enabled"`
	VerificationResults      []VerificationResult `gorm:"foreignkey:UserID"`
	KYCTiers                 []KYCTier            `json:"-"`
	CRMPersonID              uint                 `gorm:"column:crm_person_id" json:"crm_person_id"`
	CRMDealID                uint64               `gorm:"column:crm_deal_id" json:"crm_deal_id"`
	KYCSummary               KYCSummary           `json:"kyc_summary"`
	KYCInvestmentPlan        *KYCInvestmentPlan   `json:"target_tier"`
	KYCInvestmentPlanID      *uuid.UUID           `gorm:"column:target_tier" json:"-"`
	OAuthToken               []byte               `gorm:"column:oauth_token" json:"-"`
	CreatedAt                time.Time
	UpdatedAt                time.Time
	DeletedAt                *time.Time
	BlockchainConnectionType *string `gorm:"column:blockchain_connection_type" json:"blockchain_connection_type"`
}

func (user *User) GetOAuthToken() (*oauth2.Token, error) {
	var token oauth2.Token
	if err := json.Unmarshal(user.OAuthToken, &token); err != nil {
		return nil, err
	}
	return &token, nil
}

//OrderType database table definition
type OrderType struct {
	ID   uint `gorm:"primary_key"`
	Name string
}

//Order database table definition
type Order struct {
	ID            uint            `json:"id"`
	CreatedOn     time.Time       `gorm:"type:time"`
	CompletedOn   time.Time       `gorm:"type:time"`
	BlockchainID  uint            `json:"blockchain_id"`
	Types         OrderType       `json:"types"`
	TxHash        string          `json:"tx_hash"`
	Metadata      string          `json:"metadata"`
	TypeID        uint            `json:"type_id"`
	Price         decimal.Decimal `json:"price" gorm:"type:decimal"`
	EthAmount     decimal.Decimal `json:"eth_amount" gorm:"type:decimal"`
	TokenAmount   decimal.Decimal `json:"token_amount" gorm:"type:decimal"`
	Coefficient   int             `json:"coefficient"`
	EthAmountLeft decimal.Decimal `json:"eth_amount_left" gorm:"type:decimal"`
	CreatedBy     string          `json:"created_by"`
}

type ProposalInterface interface {
	GetTitleStatus() string
}

//Proposal database table definition
type Proposal struct {
	ID                 uint            `json:"id"`
	CreatedOn          time.Time       `gorm:"type:time"`
	WithdrawnAmount    decimal.Decimal `gorm:"type:decimal"`
	WithdrawnOn        *time.Time      `gorm:"type:time"`
	BlockchainID       uint            `json:"blockchain_id"`
	Price              decimal.Decimal `json:"price" gorm:"type:decimal"`
	Amount             decimal.Decimal `json:"balance" gorm:"type:decimal; column:balance"`
	OriginalTokens     decimal.Decimal `json:"original_tokens_amount" gorm:"type:decimal"`
	CreatedBy          string          `json:"created_by"`
	TxHash             string          `json:"tx_hash"`
	WithdrawalTxHash   string          `json:"withdrawal_tx_hash" gorm:"column:withdrawal_tx_hash"`
	Status             Status          `json:"status"`
	Banned             bool            `json:"banned"`
	CompletedOn        *time.Time      `json:"completed_on" gorm:"type:time; column:completed_on"`
	UpfrontQueueVolume decimal.Decimal `json:",omitempty" gorm:"-"`
	WithdrawnBy        string          `json:"withdrawn_by" gorm:"column:withdrawn_by"`
}

func (p *Proposal) GetTitleStatus() string {
	return strings.Title(string(p.Status))
}

type OAuthClientType = string

const OAuthICOAdminClient OAuthClientType = "ico-admin"

//SupportData database table definition
type SupportData struct {
	ID        uint `gorm:"primary_key"`
	LastBlock uint
}

//JwtToken database table definition
type JwtToken struct {
	ID          uint `gorm:"primary_key"`
	TokenString string
	IsBlocked   bool
}

//SubOwner database table definition
type SubOwner struct {
	EthAddress string `gorm:"primary_key"`
	IsOwner    bool
}

//AccountActivity database table definition
type AccountActivity struct {
	ID          *uuid.UUID `gorm:"type:uuid; primary_key"`
	Timestamp   time.Time
	Type        string
	CFIPAddress string `gorm:"column:cfip_address"`
	CFIPCountry string `gorm:"column:cfip_country"`
	Content     string `sql:"type:JSONB NOT NULL DEFAULT '{}'::JSONB"`
}

type ScopeMetainfo struct {
	ID          *uuid.UUID `gorm:"type:uuid; primary_key" json:"-"`
	Code        string     `json:"code"`
	Description string     `json:"description"`
}

type Matches struct {
	ID                   *uuid.UUID `gorm:"type:uuid; primary_key"`
	ProposalBlockchainID uint
	OrderBlockchainID    uint
	TxHash               string
	Tokens               decimal.Decimal `json:"tokens" gorm:"type:decimal"`
	Ether                decimal.Decimal `json:"ether" gorm:"type:decimal"`
}

type IPLock struct {
	ID        *uuid.UUID `gorm:"type:uuid; primary_key"`
	Email     string
	IPAddress string
	Reason    string
	TimeTo    time.Time `gorm:"type:time"`
}

type UserAccessRecoveryRequest struct {
	ID        *uuid.UUID `gorm:"type:uuid; primary_key"`
	UserID    *uuid.UUID
	Status    string
	Token     string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

const (
	StatusActivated string = "ACTIVATED"
	StatusPending   string = "PENDING"
	StatusCompleted string = "COMPLETED"
)

func (ScopeMetainfo) TableName() string {
	return "scope_metainfo"
}

func (user *User) BeforeSave() {
	if user.ID == nil {
		u := uuid.NewV4()
		user.ID = &u
	}

	if user.Role == "" {
		user.Role = "user"
	}
	return
}

func (userAccessRecoveryRequest *UserAccessRecoveryRequest) BeforeSave() {
	if userAccessRecoveryRequest.ID == nil {
		u := uuid.NewV4()
		userAccessRecoveryRequest.ID = &u
	}

	return
}

//BeforeSave for account_activity table generating random uuid
func (activity *AccountActivity) BeforeSave() {
	if activity.ID == nil {
		u := uuid.NewV4()
		activity.ID = &u
	}

	activity.Timestamp = time.Now()

	return
}

//BeforeSave for matches table generating random uuid
func (match *Matches) BeforeSave() {
	if match.ID == nil {
		u := uuid.NewV4()
		match.ID = &u
	}

	return
}

//BeforeSave for ip_locks table generating random uuid
func (lock *IPLock) BeforeSave() {
	if lock.ID == nil {
		u := uuid.NewV4()
		lock.ID = &u
	}

	return
}
