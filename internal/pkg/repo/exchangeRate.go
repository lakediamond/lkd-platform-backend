package repo

import (
	"lkd-platform-backend/internal/pkg/repo/models"

	"github.com/jinzhu/gorm"
)

//ProposalRepository contains all using methods for proposals table
type ExchangeRateRepository interface {
	UpdateExchange(exchangeRate *models.ExchangeRate) error
	UpdateExchangeRate(code string, rate float64) error
	GetExchangeRateByCode(code string) (models.ExchangeRate, error)
	CreateExchangeHistory(exchangeHistory *models.ExchangeHistory) error
}

type ExchangeRateRepo struct {
	db *gorm.DB
}

func (repo *ExchangeRateRepo) UpdateExchange(exchangeRate *models.ExchangeRate) error {
	err := repo.db.Save(exchangeRate)
	if err != nil {
		return err.Error
	}
	return nil
}

func (repo *ExchangeRateRepo) UpdateExchangeRate(code string, rate float64) error {
	err := repo.db.Model(models.ExchangeRate{}).
		Update("rate", rate).
		Where("code = ?", code).
		Error

	if err != nil {
		return err
	}

	return nil
}

func (repo *ExchangeRateRepo) GetExchangeRateByCode(code string) (models.ExchangeRate, error) {
	var exchangeRate models.ExchangeRate

	err := repo.db.Where("code = ?", code).Find(&exchangeRate)
	if err != nil {
		return exchangeRate, err.Error
	}

	return exchangeRate, nil
}

func (repo *ExchangeRateRepo) CreateExchangeHistory(exchangeHistory *models.ExchangeHistory) error {
	err := repo.db.Create(&exchangeHistory).Error
	if err != nil {
		return err
	}

	return nil
}
