package response

import (
	"encoding/json"
	"github.com/satori/go.uuid"
	"github.com/shopspring/decimal"
	"lkd-platform-backend/internal/pkg/repo/models"
)

type ProposalInvestmentsResponse struct {
	InvestmentID    *uuid.UUID      `json:"matcher_id"`
	OrderID         int             `json:"order_id"`
	TokensInvested  decimal.Decimal `json:"tokens_invested"`
	EtherInvested   decimal.Decimal `json:"ether_invested"`
	InvestmentTx    string          `json:"investment_tx"`
	CreatedOn       string          `json:"created_on"`
	InvestmentShare decimal.Decimal `json:"investment_share"`
}

type ProposalResponseDB struct {
	models.Proposal
	Matches json.RawMessage
}
