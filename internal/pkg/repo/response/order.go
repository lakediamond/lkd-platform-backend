package response

import (
	"encoding/json"
	"github.com/satori/go.uuid"
	"github.com/shopspring/decimal"
	"time"
)

type OrderDetailsInvestmentsResponse struct {
	InvestmentID   *uuid.UUID      `json:"matcher_id"`
	TxHash         string          `json:"matcher_tx_hash"`
	TokensInvested decimal.Decimal `json:"matcher_tokens"`
	EthReceived    decimal.Decimal `json:"matcher_ether"`
	ProposalID     int             `json:"proposal_id"`
	Price          decimal.Decimal `json:"proposal_price"`
	CreatedOn      string          `json:"proposal_created_on"`
	InvestorID     *uuid.UUID      `json:"user_id"`
	FirstName      string          `json:"user_first_name"`
	LastName       string          `json:"user_last_name"`
	Email          string          `json:"user_email"`
	EthAccount     string          `json:"user_eth_address"`
}

type OrderDetailsResponse struct {
	ID          uint            `json:"id"`
	CreatedOn   time.Time       `json:"created_on"`
	CreatedBy   string          `json:"created_by"`
	Type        string          `json:"type" gorm:"column:name"`
	Metadata    string          `json:"metadata"`
	TokenPrice  decimal.Decimal `json:"token_price" gorm:"column:price"`
	TokenAmount decimal.Decimal `json:"tokens_amount" gorm:"column:token_amount"`
	EthAmount   decimal.Decimal `json:"eth_amount" gorm:"column:eth_amount"`
	FilledRate  decimal.Decimal `json:"filled_rate" gorm:"filled_rate"`
	TxHash      string          `json:"tx_hash"`

	Investments []OrderDetailsInvestmentsResponse `json:"-,omitempty"`
}

type OrderDetailsResponseDB struct {
	OrderDetailsResponse
	Matches json.RawMessage
}

type OrderResponseDB struct {
	ID          uint            `json:"id"`
	CreatedOn   time.Time       `json:"created_on"`
	CreatedBy   string          `json:"created_by"`
	Type        string          `json:"type" gorm:"column:name"`
	Metadata    string          `json:"metadata"`
	TokenPrice  decimal.Decimal `json:"token_price" gorm:"column:price"`
	TokenAmount decimal.Decimal `json:"tokens_amount" gorm:"column:token_amount"`
	EthAmount   decimal.Decimal `json:"eth_amount" gorm:"column:eth_amount"`

	InvestmentID   *uuid.UUID      `json:"id" gorm:"type:uuid; column:matcher_id"`
	TxHash         string          `json:"tx_hash" gorm:"column:matcher_tx_hash"`
	TokensInvested decimal.Decimal `json:"tokens_invested" gorm:"column:matcher_tokens"`
	EthReceived    decimal.Decimal `json:"eth_received" gorm:"column:matcher_ether"`

	InvestorID *uuid.UUID `json:"id" gorm:"type:uuid; column:user_id"`
	FirstName  string     `json:"first_name" gorm:"column:user_first_name"`
	LastName   string     `json:"last_name" gorm:"column:user_last_name"`
	Email      string     `json:"email" gorm:"column:user_email"`
	EthAccount string     `json:"eth_account" gorm:"column:user_eth_address"`

	FilledRate decimal.Decimal `json:"filled_rate" gorm:"filled_rate"`
}
