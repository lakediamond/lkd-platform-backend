package repo

import (
	"github.com/jinzhu/gorm"

	"lkd-platform-backend/internal/pkg/repo/models"
)

//JwtTokenRepository contains all using methods for jwt_tokens table
type JwtTokenRepository interface {
	InvalidateToken(tokenString string)
	IsTokenLocked(tokenString string) bool
}

//JwtTokenRepo is JwtTokenRepo main implementation
type JwtTokenRepo struct {
	db *gorm.DB
}

//InvalidateToken stored or updating record with is_blocked = true
func (repo *JwtTokenRepo) InvalidateToken(tokenString string) {
	var jwtToken models.JwtToken
	jwtToken.TokenString = tokenString
	jwtToken.IsBlocked = true

	repo.db.Save(&jwtToken)
}

//IsTokenLocked returns true if token contains in table and is_locked == true, else returns false
func (repo *JwtTokenRepo) IsTokenLocked(tokenString string) bool {
	var jwtToken models.JwtToken

	err := repo.db.Where("token_string = ?", tokenString).Find(&jwtToken).Error
	if err != nil {
		return false
	}

	if jwtToken.IsBlocked {
		return true
	}

	return false
}
