package kyc

import (
	"lkd-platform-backend/internal/pkg/repo/models"
	"time"

	"github.com/jinzhu/gorm"
)

func (repo *Repo) GetTierByStepsStatus(user *models.User, stepStatus models.KYCTierStatus) (models.KYCTier, error) {
	var tier models.KYCTier
	var err error

	err = repo.db.Where(
		"user_id = ? and code = ?",
		user.ID.String(),
		user.KYCInvestmentPlan.Code).Preload(
		"Steps",
		"code = ? and status in (?)",
		models.CommonInfo,
		stepStatus).Preload("Steps.ContentItems").Find(&tier).Error
	if err != nil {
		return tier, err
	}
	return tier, nil
}

func (repo *Repo) GetTiers(user *models.User) ([]models.KYCTier, error) {
	var tiers []models.KYCTier
	var err error

	err = repo.db.Preload("KYCTiers", func(db *gorm.DB) *gorm.DB {
		return db.Where("code <= ? and status not in (?)", user.KYCInvestmentPlan.Code, []string{models.TierLocked}).Order("kyc_tier.code ASC")
	}).Preload("KYCTiers.Steps", func(db *gorm.DB) *gorm.DB {
		return db.Order("kyc_tier_step.step_order ASC")
	}).Find(&user).Error

	if err != nil {
		return tiers, err
	}
	return user.KYCTiers, nil
}

func (repo *Repo) GetUserTierByStatus(user *models.User, tierStatuses []models.KYCTierStatus) (models.KYCTier, error) {
	var tier models.KYCTier
	queryOrder := ""

	if len(tierStatuses) == 1 && tierStatuses[0] == models.TierLocked {
		queryOrder = "code"
	} else {
		queryOrder = "code desc"
	}

	err := repo.db.Where("user_id = ? and status in (?) and code <= ?", user.ID.String(), tierStatuses, user.KYCInvestmentPlan.Code).Order(queryOrder).Limit(1).Find(&tier).Error
	if err != nil {
		return tier, err
	}

	return tier, nil
}

func (repo *Repo) GetTierByCode(user *models.User, tierCode models.KYCTierCode) (models.KYCTier, error) {
	var tier models.KYCTier

	if err := repo.db.Model(&tier).Where("user_id = ? and code = ?", user.ID.String(), tierCode).Preload("Steps").Find(&tier).Error; err != nil {
		return tier, err
	}

	return tier, nil
}

func (repo *Repo) GetTierByStageID(user *models.User, stageID int) (models.KYCTier, error) {
	var tier models.KYCTier

	err := repo.db.Where("user_id = ? and crm_stage_id = ?", user.ID, stageID).Preload("Steps").Find(&tier).Error
	if err != nil {
		return tier, err
	}

	return tier, nil
}

func (repo *Repo) UnlockTierByCRMStageID(user *models.User, stageID models.CRMStageID) error {
	tier := models.KYCTier{
		CRMStageID: stageID,
		UserID:     user.ID,
	}

	return repo.unlockKYCTier(user, tier)
}

func (repo *Repo) UnlockTierByCode(user *models.User, tierCode models.KYCTierCode) error {
	tier := models.KYCTier{
		Code:   tierCode,
		UserID: user.ID,
	}

	return repo.unlockKYCTier(user, tier)
}

func (repo *Repo) CompleteTier(user *models.User, tierCode models.KYCTierCode) error {
	tier := models.KYCTier{}
	var err error

	err = repo.db.Where("user_id = ? and code = ?", user.ID.String(), tierCode).Preload("Steps").
		Preload("Steps.ContentItems").Find(&tier).Error
	if err != nil {
		return err
	}

	tx := repo.db.Begin()
	if err := tx.Error; err != nil {
		return err
	}

	for _, step := range tier.Steps {
		for _, stepContentItem := range step.ContentItems {
			if err = tx.Model(&stepContentItem).Select("status").Updates(map[string]interface{}{"status": models.StepVerified, "enabled": false, "enabled_at": time.Time{}}).Error; err != nil {
				tx.Rollback()
				return err
			}
		}
		if err = tx.Model(&step).Select("status").Updates(map[string]interface{}{"status": models.StepVerified}).Error; err != nil {
			tx.Rollback()
			return err
		}
	}

	if err = tx.Model(&tier).Select("status").Updates(map[string]interface{}{"status": models.TierGranted}).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

type tierFailureRate struct {
	FailureRate int
}

func (repo *Repo) GetTierFailureRate(user *models.User, tierCode models.KYCTierCode) (int, error) {
	failureRate := tierFailureRate{}
	err := repo.db.
		Raw(`
		select sum( 
			case
			when position(';' in value) > 0 then 1
			else 0
			end) as failure_rate
			from kyc_tier_flag 
			where 
			deleted_at is null and
			kyc_tier_id = (select id from kyc_tier where code = ? and user_id = ?)`, tierCode, user.ID.String()).Scan(&failureRate).Error
	if err != nil {
		return -1, err
	}
	return failureRate.FailureRate, nil
}

func (repo *Repo) UpdateTierByModel(kycTier *models.KYCTier) error {
	err := repo.db.Save(kycTier).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *Repo) UpdateTierStatus(user *models.User, tierCode models.KYCTierCode, tierStatus models.KYCTierStatus) error {
	tier := &models.KYCTier{}

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), tierCode).Find(&tier).Error
	if err != nil {
		return err
	}

	err = repo.db.Model(&tier).Select("status").Updates(map[string]string{"status": tierStatus}).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *Repo) SetReworkTier(user *models.User, tierCode models.KYCTierCode) error {
	err := repo.db.Model(&models.KYCTier{}).Where("code = ? and user_id = ?", tierCode, user.ID.String()).Select("status").Updates(map[string]interface{}{"status": models.TierRework}).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *Repo) unlockKYCTier(user *models.User, template models.KYCTier) error {
	tiers := []models.KYCTier{}
	var err error

	err = repo.db.Model(&template).Where("user_id = ? and code <= ?", template.UserID.String(), template.Code).Preload("Steps").Preload("Steps.ContentItems").Order("code asc").Find(&tiers).Error
	if err != nil {
		return err
	}

	tx := repo.db.Begin()
	if err := tx.Error; err != nil {
		return err
	}

	for idx, tier := range tiers {
		for _, step := range tier.Steps {
			if idx == 0 && step.StepOrder == 1 {
				if err = tx.Model(&step).Select("status").Updates(map[string]interface{}{"status": models.StepOpened}).Error; err != nil {
					tx.Rollback()
					return err
				}
			}
		}

		if err = tx.Model(&tier).Select("status").Updates(map[string]interface{}{"status": models.TierOpened}).Error; err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit().Error
}
