package kyc

import (
	"lkd-platform-backend/internal/pkg/repo/models"
	"time"
)

func (repo *Repo) GetWorkerByID(id string) (*models.Worker, error) {

	var worker models.Worker

	err := repo.db.Where("id = ?", id).Find(&worker).Error
	if err != nil {
		return nil, err
	}

	return &worker, nil
}

func (repo *Repo) CreateWorker(worker *models.Worker) error {
	err := repo.db.Create(worker).Error
	if err != nil {
		return err
	}
	return nil
}

func (repo *Repo) SetWorkerState(state models.State, worker *models.Worker) error {
	fields := make(map[string]interface{}, 0)
	fields["state"] = state
	fields["updated_at"] = time.Now()

	err := repo.db.Model(&worker).Updates(fields).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *Repo) GetWorkerByStateAndEvent(state models.State, event models.CronEvent) ([]*models.Worker, error) {
	var worker []*models.Worker

	err := repo.db.Where("state = ? and event = ?", state, event).Limit(100).Find(&worker).Error
	if err != nil {
		return worker, err
	}

	return worker, nil
}

func (repo *Repo) UpdateWorker(worker *models.Worker, fields map[string]interface{}) error {
	err := repo.db.Model(&worker).Updates(fields).Error
	if err != nil {
		return err
	}

	return nil
}
