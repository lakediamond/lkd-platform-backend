package kyc

import (
	"github.com/jinzhu/gorm"

	"lkd-platform-backend/internal/pkg/pipeDrive"
	"lkd-platform-backend/internal/pkg/repo/models"
)

type Repository interface {
	GetDictionaryCountryPhoneCodeRecords() ([]models.DictionaryCountryPhoneCode, error)
	GetCountryBlacklistByCode(code string) (models.KYCCountryBlacklist, error)
	GetAllBlacklistedUserCountries(email string) ([]models.KYCCountryBlacklist, error)

	GetTierFlag(user *models.User, tierCode int) ([]models.KYCTierFlag, error)
	GetTierFailedFlags(user *models.User, tierCode models.KYCTierCode) ([]models.KYCTierFlag, error)
	DeleteTierFlags(user *models.User, tierCode models.KYCTierCode, flagsToSkip []string) error
	CreateTierFlag(kycTierFlag *models.KYCTierFlag) error

	GetPlans(user *models.User) ([]models.KYCInvestmentPlan, error)
	GetPlanByCode(planCode int) (models.KYCInvestmentPlan, error)

	GetTierStep(user *models.User, tierCode int, stepCode string) (*models.KYCTierStep, error)
	UpdateTierStepByModel(kycTierStep *models.KYCTierStep) error
	UpdateTierStep(user *models.User, tierCode int, stepCode string, contentItems map[string]string) error
	UpdateTierStepCommonInfo(user *models.User, data *models.StepCommonInfoForm) error
	UpdateTierStepComplianceReview(user *models.User) error
	CompleteTierStepIdentityScans(user *models.User) error
	UnlockTierStep(user *models.User, tierCode models.KYCTierCode, stepCode models.KYCTierStepCode) error

	UpdateMediaItemStatus(user *models.User, itemType models.KYCMediaItemType, status models.KYCTierMediaItemStatus) error
	UpdateMediaItemCRMFileID(user *models.User, itemType models.KYCMediaItemType, crmFileId uint64) error
	GetMediaItemStatus(user *models.User, itemType models.KYCMediaItemType) (models.KYCTierMediaItemStatus, error)

	GetTiers(user *models.User) ([]models.KYCTier, error)
	GetTierByStepsStatus(user *models.User, stepStatus models.KYCTierStatus) (models.KYCTier, error)
	GetUserTierByStatus(user *models.User, tierStatuses []models.KYCTierStatus) (models.KYCTier, error)
	GetTierByCode(user *models.User, tierCode models.KYCTierCode) (models.KYCTier, error)
	GetTierByStageID(user *models.User, stageID int) (models.KYCTier, error)
	UnlockTierByCRMStageID(user *models.User, tierCode models.KYCTierCode) error
	UnlockTierByCode(user *models.User, tierCode models.KYCTierCode) error
	CompleteTier(user *models.User, tierCode models.KYCTierCode) error
	GetTierFailureRate(user *models.User, tierCode models.KYCTierCode) (int, error)
	UpdateTierByModel(kycTier *models.KYCTier) error
	UpdateTierStatus(user *models.User, tierCode models.KYCTierCode, tierStatus models.KYCTierStatus) error
	SetReworkTier(user *models.User, tierCode models.KYCTierCode) error

	CreateTemplateRecords(user *models.User, crmSettings *pipeDrive.Deal) error

	UpdateTierStepContentItem(kycTierStepContentItem *models.KYCTierStepContentItem) error
	GetTierStepContentItemByCode(kycTierStep *models.KYCTierStep, code string) (models.KYCTierStepContentItem, error)

	DeletePhoneVerification(kycTierStep *models.KYCTierStep) error

	GetWorkerByID(id string) (*models.Worker, error)
	CreateWorker(worker *models.Worker) error
	SetWorkerState(state models.State, worker *models.Worker) error
	GetWorkerByStateAndEvent(state models.State, event models.CronEvent) ([]*models.Worker, error)
	UpdateWorker(worker *models.Worker, fields map[string]interface{}) error
}

// Repo dba layer
type Repo struct {
	db *gorm.DB
}

func NewRepo(db *gorm.DB) *Repo {
	return &Repo{db: db}
}
