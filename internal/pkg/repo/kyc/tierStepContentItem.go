package kyc

import "lkd-platform-backend/internal/pkg/repo/models"

func (repo *Repo) UpdateTierStepContentItem(kycTierStepContentItem *models.KYCTierStepContentItem) error {
	err := repo.db.Save(kycTierStepContentItem).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *Repo) GetTierStepContentItemByCode(kycTierStep *models.KYCTierStep, code string) (models.KYCTierStepContentItem, error) {
	var kycTierStepContentItem models.KYCTierStepContentItem

	err := repo.db.Where("kyc_tier_step_id = ? and code = ?", kycTierStep.ID, code).Find(&kycTierStepContentItem).Error
	if err != nil {
		return kycTierStepContentItem, err
	}

	return kycTierStepContentItem, nil
}
