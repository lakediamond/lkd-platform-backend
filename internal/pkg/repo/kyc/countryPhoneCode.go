package kyc

import "lkd-platform-backend/internal/pkg/repo/models"

func (repo *Repo) GetDictionaryCountryPhoneCodeRecords() ([]models.DictionaryCountryPhoneCode, error) {
	var records []models.DictionaryCountryPhoneCode
	err := repo.db.Model(&records).Order("country asc").Find(&records).Error
	if err != nil {
		return records, err
	}

	return records, err
}

func (repo *Repo) GetCountryBlacklistByCode(code string) (models.KYCCountryBlacklist, error) {
	var kycCountryBlacklist models.KYCCountryBlacklist

	err := repo.db.Where("code = ?", code).Find(&kycCountryBlacklist).Error
	if err != nil {
		return kycCountryBlacklist, err
	}

	return kycCountryBlacklist, nil
}

func (repo *Repo) GetAllBlacklistedUserCountries(email string) ([]models.KYCCountryBlacklist, error) {
	var blacklist []models.KYCCountryBlacklist

	err := repo.db.
		Raw(`
		select bl.name
		from kyc_country_blacklist bl
       	inner join account_activities aa on bl.code = aa.cfip_country
		where  aa.content ->> 'email' = ?`, email).
		Scan(&blacklist).Error

	if err != nil {
		return blacklist, err
	}

	return blacklist, nil
}
