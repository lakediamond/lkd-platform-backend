package kyc

import (
	"lkd-platform-backend/internal/pkg/repo/models"
)

func (repo *Repo) GetPlans(user *models.User) ([]models.KYCInvestmentPlan, error) {
	var plan []models.KYCInvestmentPlan
	var validTiers []models.KYCTier
	var validTierCodes []int

	err := repo.db.Where("user_id = ? and status in (?)", user.ID.String(), []string{models.TierLocked}).Select("code").Find(&validTiers).Error
	if err != nil {
		return nil, err
	}

	for _, tier := range validTiers {
		validTierCodes = append(validTierCodes, tier.Code)
	}
	err = repo.db.Model(&plan).Where("code in (?)", validTierCodes).Order("code").Find(&plan).Error

	return plan, err
}

func (repo *Repo) GetPlanByCode(planCode int) (models.KYCInvestmentPlan, error) {
	var plan models.KYCInvestmentPlan

	err := repo.db.Where("code = ?", planCode).Find(&plan).Error

	return plan, err
}
