package kyc

import "lkd-platform-backend/internal/pkg/repo/models"

func (repo *Repo) GetTierFlag(user *models.User, tierCode int) ([]models.KYCTierFlag, error) {
	var flags []models.KYCTierFlag
	err := repo.db.
		Raw(`
		select fl.id, fl.kyc_tier_id, fl.code, fl.value, fl.created_at, fl.updated_at, fl.deleted_at
		from kyc_tier_flag fl
		inner join kyc_tier tr on tr.id = fl.kyc_tier_id
		where tr.user_id = ? and tr.code = ? ORDER BY fl.code, fl.created_at ASC`, user.ID.String(), tierCode).Scan(&flags).Error
	if err != nil {
		return flags, err
	}
	return flags, nil
}

func (repo *Repo) GetTierFailedFlags(user *models.User, tierCode models.KYCTierCode) ([]models.KYCTierFlag, error) {
	var flags []models.KYCTierFlag
	err := repo.db.
		Raw(`
		select fl.id, fl.kyc_tier_id, fl.code, fl.value, fl.crm_pipeline_failure_stage, fl.created_at, fl.updated_at, fl.deleted_at
		from kyc_tier_flag fl
		inner join kyc_tier tr on tr.id = fl.kyc_tier_id
		where tr.user_id = ? and tr.code = ?
		and position(';' in fl.value) > 0
		and fl.deleted_at is null`, user.ID.String(), tierCode).Scan(&flags).Error
	if err != nil {
		return flags, err
	}
	return flags, nil
}

func (repo *Repo) DeleteTierFlags(user *models.User, tierCode models.KYCTierCode, flagsToSkip []string) error {
	var tier models.KYCTier

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), tierCode).Find(&tier).Error
	if err != nil {
		return err
	}

	err = repo.db.Where("kyc_tier_id = ? and code not in (?)", tier.ID.String(), flagsToSkip).Delete(models.KYCTierFlag{}).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *Repo) CreateTierFlag(kycTierFlag *models.KYCTierFlag) error {
	err := repo.db.Create(kycTierFlag).Error
	if err != nil {
		return err
	}

	return nil
}
