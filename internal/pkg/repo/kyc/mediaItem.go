package kyc

import (
	"strconv"

	"github.com/jinzhu/gorm"

	"lkd-platform-backend/internal/pkg/repo/models"
)

func (repo *Repo) UpdateMediaItemStatus(user *models.User, itemType models.KYCMediaItemType, status models.KYCTierMediaItemStatus) error {
	err := repo.db.Preload("KYCTiers", "code  = ?", models.Tier0).
		Preload("KYCTiers.Steps", "code = ?", models.IdentityScans).
		Preload("KYCTiers.Steps.ContentItems", "code = ?", itemType).Find(&user).Error

	item := user.KYCTiers[0].Steps[0].ContentItems[0]

	err = repo.db.Model(&item).Select("status").Updates(map[string]interface{}{"status": status}).Error

	if err != nil {
		return err
	}
	return nil
}

func (repo *Repo) UpdateMediaItemCRMFileID(user *models.User, itemType models.KYCMediaItemType, crmFileId uint64) error {
	var tier models.KYCTier

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), models.Tier0).Preload("Steps", "code = ?", models.IdentityScans).Preload("Steps.ContentItems", "code = ?", itemType).Find(&tier).Error
	if err != nil {
		return err
	}

	item := tier.Steps[0].ContentItems[0]
	crmFileIdValue := strconv.FormatUint(crmFileId, 10)
	err = repo.db.Model(&item).Select("value").Updates(map[string]string{"value": crmFileIdValue}).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *Repo) GetMediaItemStatus(user *models.User, itemType models.KYCMediaItemType) (models.KYCTierMediaItemStatus, error) {
	err := repo.db.Preload("KYCTiers", func(db *gorm.DB) *gorm.DB {
		return db.Where("code  = ?", models.Tier0)
	}).Preload("KYCTiers.Steps", func(db *gorm.DB) *gorm.DB {
		return db.Where("code = ?", models.IdentityScans)
	}).Preload("KYCTiers.Steps.ContentItems", func(db *gorm.DB) *gorm.DB {
		return db.Where("code = ?", itemType)
	}).Find(&user).Error
	if err != nil {
		return "", err
	}

	item := user.KYCTiers[0].Steps[0].ContentItems[0]

	return item.Status, err
}
