package kyc

import (
	"time"

	"lkd-platform-backend/internal/pkg/repo/models"
)

func (repo *Repo) DeletePhoneVerification(kycTierStep *models.KYCTierStep) error {
	item1, err := repo.GetTierStepContentItemByCode(kycTierStep, "phone_number")
	if err != nil {
		return err
	}

	item1.Value = ""
	item1.Enabled = true
	item1.EnabledAt = time.Unix(0, 0)
	item1.Status = ""

	err = repo.db.Save(&item1).Error
	if err != nil {
		return err
	}

	item2, err := repo.GetTierStepContentItemByCode(kycTierStep, "country_code")
	if err != nil {
		return err
	}

	item2.Value = ""
	item2.Enabled = true
	item2.EnabledAt = time.Unix(0, 0)
	item2.Status = ""

	err = repo.db.Save(&item2).Error
	if err != nil {
		return err
	}

	return nil
}
