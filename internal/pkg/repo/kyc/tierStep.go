package kyc

import (
	"fmt"
	"strconv"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/repo/models"
)

func (repo *Repo) GetTierStep(user *models.User, tierCode int, stepCode string) (*models.KYCTierStep, error) {
	var tier models.KYCTier
	phoneVerificationEntry := models.PhoneVerification{}

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), tierCode).Preload("Steps", "code = ?", stepCode).Preload("Steps.ContentItems", func(db *gorm.DB) *gorm.DB {
		return db.Order("kyc_tier_step_item.item_order ASC")
	}).Find(&tier).Error
	if err != nil {
		return nil, err
	}

	step := tier.Steps[0]
	switch step.Code {
	case models.PhoneVerificationStep:
		if step.Status != models.StepVerified {
			if err = repo.db.Where("user_id = ?", user.ID).Order("updated_at desc").Limit(1).Find(&phoneVerificationEntry).Error; err != nil {
				switch err.Error() {
				case "record not found":
				default:
					return nil, err
				}
			}

			if phoneVerificationEntry.ID != nil {
				itemEnabled := false
				itemEnabledAt := phoneVerificationEntry.UpdatedAt.Add(time.Minute * 3)
				if !time.Now().Before(itemEnabledAt) {
					itemEnabled = true
					itemEnabledAt = time.Time{}
				}

				for _, item := range step.ContentItems {
					switch item.Code == "phone_number" || item.Code == "country_code" {
					case true:
						item.Enabled = itemEnabled
						item.EnabledAt = itemEnabledAt
						repo.db.Model(&item).Updates(map[string]interface{}{"enabled": itemEnabled, "enabled_at": itemEnabledAt})
					default:
					}
				}
			}
		}
	default:
		log.Info().Msg("reached default flow")
	}

	return &step, nil
}

func (repo *Repo) UpdateTierStepByModel(kycTierStep *models.KYCTierStep) error {
	err := repo.db.Save(kycTierStep).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *Repo) UpdateTierStep(user *models.User, tierCode int, stepCode string, contentItems map[string]string) error {
	tier := models.KYCTier{}
	phoneVerificationEntry := models.PhoneVerification{}

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), tierCode).Preload("Steps", "code = ? and status = ?", stepCode, models.StepOpened).Preload("Steps.ContentItems").Find(&tier).Error
	if err != nil {
		return err
	}

	step := tier.Steps[0]

	if tierCode != models.Tier0 ||
		stepCode != models.PhoneVerificationStep {
		return nil
	}

	if len(contentItems["phone_verified"]) == 0 {
		err = repo.db.Where("user_id = ?", user.ID.String()).Order("updated_at desc").Limit(1).Find(&phoneVerificationEntry).Error
		if err != nil {
			return err
		}

		itemEnabled := true
		itemEnabledAt := phoneVerificationEntry.UpdatedAt.Add(time.Minute * 3)
		if time.Now().Before(itemEnabledAt) {
			itemEnabled = false
		}

		for _, item := range step.ContentItems {
			if item.Code == "phone_number" || item.Code == "country_code" {
				repo.db.Model(&item).Updates(map[string]interface{}{"status": phoneVerificationEntry.Status, "value": contentItems[item.Code], "enabled": itemEnabled, "enabled_at": itemEnabledAt})
			}
		}

		return nil
	}

	err = repo.db.Where("user_id = ? and status = ?", user.ID.String(), models.PVVerified).Order("updated_at desc").Limit(1).Find(&phoneVerificationEntry).Error
	if err != nil {
		return err
	}

	tx := repo.db.Begin()

	if err := tx.Error; err != nil {
		return err
	}

	if phoneVerificationEntry.ID != nil {
		for _, item := range step.ContentItems {
			if item.Code == "phone_number" || item.Code == "country_code" {
				tx.Model(&item).Updates(map[string]interface{}{"status": phoneVerificationEntry.Status, "enabled": false, "enabled_at": time.Time{}})
			}
		}
	}

	tx.Model(&step).Select("status").Updates(map[string]interface{}{"status": models.StepVerified})
	tx.Model(&models.KYCTierStep{}).Where("kyc_tier_id = ? and code = ?", tier.ID, models.CommonInfo).Select("status").Updates(map[string]interface{}{"status": models.StepOpened})

	if err = tx.Commit().Error; err != nil {
		tx.Rollback()
		return err
	}

	return nil
}

func (repo *Repo) UpdateTierStepCommonInfo(user *models.User, data *models.StepCommonInfoForm) error {
	tier := models.KYCTier{}
	countryDictionaryRecord := models.DictionaryCountryPhoneCode{}

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), models.Tier0).Preload("Steps", "code = ? and status = ?", models.CommonInfo, models.StepOpened).Preload("Steps.ContentItems").Find(&tier).Error
	if err != nil {
		return err
	}

	if len(tier.Steps) == 0 {
		return fmt.Errorf("updateTierStepCommonInfo[tier.Steps] can not be empty")
	}

	step := tier.Steps[0]
	contentItems := step.ContentItems
	fmt.Println(len(contentItems))

	for _, item := range contentItems {
		switch item.Code {
		case "reg_address_country":
			item.Value = data.RegAddressCountry
			if len(data.RegAddressCountry) > 2 {
				if err = repo.db.Where("country = ?", data.RegAddressCountry).Find(&countryDictionaryRecord).Error; err != nil {
					return err
				}
				item.Value = countryDictionaryRecord.CountryCode
			}
		case "reg_address_zip":
			item.Value = data.RegAddressZIP
		case "reg_address_city":
			item.Value = data.RegAddressCity
		case "reg_address_details_1":
			item.Value = data.RegAddressDetails
		case "reg_address_details_2":
			item.Value = data.RegAddressDetailsExt
		case "contribution_option":
			item.Value = data.ContributionOption
		case "contribution_bank_name":
			if data.ContributionOption == models.OptFiat {
				item.Value = data.BankName
			}
		case "contribution_iban":
			if data.ContributionOption == models.OptFiat {
				item.Value = data.BankIBAN
			}
		case "contribution_bank_address":
			if data.ContributionOption == models.OptFiat {
				item.Value = data.BankAddress
			}
		}

		err = repo.db.Model(&item).Updates(map[string]interface{}{"status": models.StepCustomerConfirmed, "value": item.Value, "enabled": false}).Error
		if err != nil {
			return err
		}
	}

	step.Status = models.StepCustomerConfirmed
	err = repo.db.Model(&step).Select("status").Updates(map[string]interface{}{"status": models.StepCustomerConfirmed}).Error
	if err != nil {
		return err
	}

	return nil
}

func (repo *Repo) UpdateTierStepComplianceReview(user *models.User) error {
	tier := models.KYCTier{}

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), models.Tier1).Preload("Steps", "code = ? and status = ?", models.ComplianceReview, models.StepOpened).Preload("Steps.ContentItems").Find(&tier).Error
	if err != nil {
		return err
	}

	step := tier.Steps[0]
	contentItem := step.ContentItems[0]

	tx := repo.db.Begin()

	if err := tx.Error; err != nil {
		return err
	}

	if err = tx.Model(&contentItem).Updates(map[string]interface{}{"status": models.StepCustomerConfirmed, "value": strconv.FormatBool(true), "enabled": false}).Error; err != nil {
		tx.Rollback()
		return err
	}

	if err = tx.Model(&step).Select("status").Updates(map[string]interface{}{"status": models.StepCustomerConfirmed}).Error; err != nil {
		tx.Rollback()
		return err
	}

	if err = tx.Model(&tier).Select("status").Updates(map[string]interface{}{"status": models.TierSubmitted}).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

func (repo *Repo) CompleteTierStepIdentityScans(user *models.User) error {
	tier := models.KYCTier{}
	var err error

	err = repo.db.Where("user_id = ? and code = ?", user.ID.String(), models.Tier0).Preload("Steps", "code = ?", models.IdentityScans).
		Preload("Steps.ContentItems", "code in (?)", user.GetMediaTypes()).Find(&tier).Error
	if err != nil {
		return err
	}

	step := tier.Steps[0]

	tx := repo.db.Begin()

	if err := tx.Error; err != nil {
		return err
	}

	for _, item := range step.ContentItems {
		if err = tx.Model(&item).Select("status").Updates(map[string]interface{}{"status": models.MediaItemProcessed}).Error; err != nil {
			tx.Rollback()
			return err
		}
	}

	if err = tx.Model(&step).Select("status").Updates(map[string]interface{}{"status": models.StepCustomerConfirmed}).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

func (repo *Repo) UnlockTierStep(user *models.User, tierCode models.KYCTierCode, stepCode models.KYCTierStepCode) error {
	tier := models.KYCTier{}

	err := repo.db.Where("user_id = ? and code = ?", user.ID.String(), tierCode).Find(&tier).Error
	if err != nil {
		return err
	}

	err = repo.db.Model(&models.KYCTierStep{}).Where("kyc_tier_id = ? and code = ?", tier.ID.String(), stepCode).Select("status").Updates(map[string]interface{}{"status": models.StepOpened}).Error
	if err != nil {
		return err
	}

	return nil
}
