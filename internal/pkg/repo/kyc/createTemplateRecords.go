package kyc

import (
	"strconv"

	"github.com/satori/go.uuid"

	"lkd-platform-backend/internal/pkg/pipeDrive"
	"lkd-platform-backend/internal/pkg/repo/models"
)

func (repo *Repo) CreateTemplateRecords(user *models.User, crmSettings *pipeDrive.Deal) error {

	tx := repo.db.Begin()

	if err := tx.Error; err != nil {
		return err
	}

	var kycTier models.KYCTier
	u := uuid.NewV4()
	kycTier.ID = &u
	kycTier.Code = 0
	kycTier.Status = models.TierLocked
	kycTier.UserID = user.ID
	kycTier.CRMStageID = crmSettings.KYCTier0StageID

	if err := tx.Save(&kycTier).Error; err != nil {
		tx.Rollback()
		return err
	}

	var kycTierStep models.KYCTierStep
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepOpened
	kycTierStep.StepOrder = 1
	kycTierStep.Title = "Mobile phone verification"
	kycTierStep.Code = "phone_verification"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	var kycTierStepContentItem models.KYCTierStepContentItem
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 1
	kycTierStepContentItem.Code = "country_code"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 2
	kycTierStepContentItem.Code = "phone_number"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 2
	kycTierStep.Title = "Contributor information"
	kycTierStep.Code = "common_info"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 1
	kycTierStepContentItem.Code = "reg_address_country"

	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 2
	kycTierStepContentItem.Code = "reg_address_zip"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 3
	kycTierStepContentItem.Code = "reg_address_city"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 4
	kycTierStepContentItem.Code = "reg_address_details_1"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 5
	kycTierStepContentItem.Code = "reg_address_details_2"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 6
	kycTierStepContentItem.Code = "contribution_option"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 7
	kycTierStepContentItem.Code = "contribution_bank_name"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 8
	kycTierStepContentItem.Code = "contribution_iban"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 9
	kycTierStepContentItem.Code = "contribution_bank_address"
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 3
	kycTierStep.Title = "Identity document verification"
	kycTierStep.Code = "identity_scans"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 1
	kycTierStepContentItem.Code = models.IDFrontSide
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 2
	kycTierStepContentItem.Code = models.IDBackSide
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 3
	kycTierStepContentItem.Code = models.IDSelfie
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTier = *new(models.KYCTier)
	u = uuid.NewV4()
	kycTier.ID = &u
	kycTier.Code = 1
	kycTier.Status = models.TierLocked
	kycTier.UserID = user.ID
	kycTier.CRMStageID = crmSettings.KYCTier1StageID

	if err := tx.Save(&kycTier).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 1
	kycTierStep.Title = "Compliance officer review"
	kycTierStep.Code = "compliance_review"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStepContentItem = *new(models.KYCTierStepContentItem)
	u = uuid.NewV4()
	kycTierStepContentItem.ID = &u
	kycTierStepContentItem.KYCTierStepID = kycTierStep.ID
	kycTierStepContentItem.ItemOrder = 1
	kycTierStepContentItem.Code = "review_consent"
	kycTierStepContentItem.Value = strconv.FormatBool(false)
	kycTierStepContentItem.Enabled = true

	if err := tx.Save(&kycTierStepContentItem).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTier = *new(models.KYCTier)
	u = uuid.NewV4()
	kycTier.ID = &u
	kycTier.Code = 2
	kycTier.Status = models.TierLocked
	kycTier.UserID = user.ID
	kycTier.CRMStageID = crmSettings.KYCTier2StageID

	if err := tx.Save(&kycTier).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 1
	kycTierStep.Title = "Proof of address verification"
	kycTierStep.Code = "address_ownership"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 2
	kycTierStep.Title = "UBO verification"
	kycTierStep.Code = "ubo"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 3
	kycTierStep.Title = "PEP verification"
	kycTierStep.Code = "pep"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 4
	kycTierStep.Title = "Source of funds verification"
	kycTierStep.Code = "funds_source"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTier = *new(models.KYCTier)
	u = uuid.NewV4()
	kycTier.ID = &u
	kycTier.Code = 3
	kycTier.Status = models.TierLocked
	kycTier.UserID = user.ID
	kycTier.CRMStageID = crmSettings.KYCTier3StageID

	if err := tx.Save(&kycTier).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 1
	kycTierStep.Title = "Video call verification"
	kycTierStep.Code = "video_call"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	kycTierStep = *new(models.KYCTierStep)
	u = uuid.NewV4()
	kycTierStep.ID = &u
	kycTierStep.KYCTierID = kycTier.ID
	kycTierStep.Status = models.StepLocked
	kycTierStep.StepOrder = 2
	kycTierStep.Title = "Legal entity verification"
	kycTierStep.Code = "legal_entity"

	if err := tx.Save(&kycTierStep).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}
