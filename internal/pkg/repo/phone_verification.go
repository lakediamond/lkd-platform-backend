package repo

import (
	"crypto/rand"
	"io"
	"strconv"

	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog/log"

	"lkd-platform-backend/internal/pkg/repo/models"
)

type PhoneVerificationRepository interface {
	CreatePhoneVerification(user *models.User, phone, countryCode string) (models.PhoneVerification, error)
	CheckVerificationCount(user *models.User) (int, error)
	Exists(phone string, statuses []models.PVStatus) (bool, error)
	CancelPrevious(user *models.User, phone string) error
	Verify(pv *models.PhoneVerification, code int) (bool, error)
	GetVerificationByUser(user *models.User) (models.PhoneVerification, error)
	GetVerificationByPhone(phone string) (models.PhoneVerification, error)
	GetVerifiedUserPhone(user *models.User) (models.PhoneVerification, error)
	GetByID(id string) (*models.PhoneVerification, error)
}

// PhoneVerificationRepo dba layer
type PhoneVerificationRepo struct {
	db *gorm.DB
}

// CreatePhoneVerification peristance layer
func (repo *PhoneVerificationRepo) CreatePhoneVerification(user *models.User, phone, countryCode string) (models.PhoneVerification, error) {
	pv := models.NewPhoneVerifcation()
	pv.UserID = user.ID
	pv.Phone = phone
	pv.CountryCode = countryCode
	pv.Code = generateOTP(6)
	log.Debug().Msgf("=== OTP CODE: %d ===", pv.Code)
	err := repo.db.Create(&pv).Error
	if err != nil {
		return pv, err
	}
	return pv, nil
}

// CheckVerificationCount return verifications have in database for phone TODAY
func (repo *PhoneVerificationRepo) CheckVerificationCount(user *models.User) (int, error) {
	var pvs []models.PhoneVerification
	err := repo.db.Where("user_id = ? and created_at >= now() - interval '1 year'", user.ID).Find(&pvs).Error
	if err != nil {
		return -1, err
	}
	return len(pvs), nil
}

// Exists return verifications have in database for phone TODAY
func (repo *PhoneVerificationRepo) Exists(phone string, statuses []models.PVStatus) (bool, error) {
	var pvs []models.PhoneVerification
	tx := repo.db.Where("phone = ?", phone)
	if len(statuses) > 0 {
		tx = tx.Where("status in (?)", statuses)
	}
	err := tx.Find(&pvs).Error
	if err != nil {
		return false, err
	}
	return len(pvs) > 0, nil
}

func (repo *PhoneVerificationRepo) CancelPrevious(user *models.User, phone string) error {
	var pv models.PhoneVerification
	err := repo.db.Model(&pv).Where("status = ? and user_id = ?", models.PVNew, user.ID).Update("status", models.PVCancelled).Error
	if err != nil {
		return err
	}
	return nil
}

func (repo *PhoneVerificationRepo) Verify(pv *models.PhoneVerification, code int) (bool, error) {
	if pv.Code != code {
		pv.Attempts++
		err := repo.db.Save(&pv).Error
		if err != nil {
			panic(err)
		}
		return false, err
	}

	err := repo.doVerify(pv)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (repo *PhoneVerificationRepo) doVerify(pv *models.PhoneVerification) error {
	tx := repo.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if tx.Error != nil {
		return tx.Error
	}

	var pvv models.PhoneVerification
	if err := tx.Model(&pvv).Where("status = ? and user_id = ?", models.PVVerified, pv.UserID).Update("status", models.PVCancelled).Error; err != nil {
		tx.Rollback()
		return err
	}
	pv.Status = models.PVVerified
	if err := tx.Save(&pv).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

func (repo *PhoneVerificationRepo) GetVerificationByUser(user *models.User) (models.PhoneVerification, error) {
	var pv models.PhoneVerification
	err := repo.db.Where("user_id = ? and status = ?", user.ID, models.PVNew).Order("created_at desc").Limit(1).Find(&pv).Error
	if err != nil {
		return pv, err
	}
	return pv, nil
}

func (repo *PhoneVerificationRepo) GetVerificationByPhone(phone string) (models.PhoneVerification, error) {
	var pv models.PhoneVerification
	err := repo.db.Where("phone = ?", phone).Order("created_at desc").Limit(1).Find(&pv).Error
	if err != nil {
		return pv, err
	}
	return pv, nil
}

func (repo *PhoneVerificationRepo) GetVerifiedUserPhone(user *models.User) (models.PhoneVerification, error) {
	var pv models.PhoneVerification
	err := repo.db.Where("user_id = ? AND status = ?", user.ID, models.PVVerified).Order("created_at desc").Limit(1).Find(&pv).Error
	if err != nil {
		return pv, err
	}
	return pv, nil
}

func (repo *PhoneVerificationRepo) GetByID(id string) (*models.PhoneVerification, error) {

	var phoneVerification models.PhoneVerification

	err := repo.db.Where("id = ?", id).Find(&phoneVerification).Error
	if err != nil {
		return nil, err
	}

	return &phoneVerification, nil
}

func generateOTP(max int) int {
	var table = [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}
	b := make([]byte, max)
	n, err := io.ReadAtLeast(rand.Reader, b, max)
	if n != max {
		panic(err)
	}
	for i := 0; i < len(b); i++ {
		b[i] = table[int(b[i])%len(table)]
	}
	r, err := strconv.Atoi(string(b))
	if err != nil {
		panic(err)
	}
	return r
}
