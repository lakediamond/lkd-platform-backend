package googleCloud

import (
	"context"
	"encoding/base64"
	_ "encoding/base64"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	"github.com/rs/zerolog/log"
	"google.golang.org/api/iterator"

	"lkd-platform-backend/internal/pkg/repo/models"
	"lkd-platform-backend/pkg/environment"
)

type GCApplication struct {
	Client     *storage.Client
	BucketName string
	Bucket     *storage.BucketHandle
	AccessID   string
	//PrivateKey            string
	PrivateKeyStorageData string
	PrivateKeyStorageKey  string
	GcpKycImageSignedUrl  string

	ctx context.Context
}

func NewGCApplication() *GCApplication {
	var gc GCApplication

	client, err := storage.NewClient(context.Background())
	if err != nil {
		log.Fatal().Err(err).Msg("cannot initialize GC client")
	}

	gc.Client = client

	env := environment.GetEnv()
	//gc.BucketName = strings.Split(env["ETHEREUM_PRIVATE_KEY_STORAGE_DATA"], "/")[0]
	gc.BucketName = strings.Split(env["STORAGE_BUCKET_NAME"], "/")[0]
	gc.Bucket = client.Bucket(gc.BucketName)
	gc.AccessID = env["GOOGLE_CLOUD_ACCESS_ID"]
	//gc.PrivateKey = env["GOOGLE_CLOUD_PRIVATE_KEY"]
	gc.PrivateKeyStorageData = env["GC_PRIVATE_KEY_STORAGE_DATA"]
	gc.PrivateKeyStorageKey = env["ETHEREUM_PRIVATE_KEY_STORAGE_KEY"]
	gc.GcpKycImageSignedUrl = os.Getenv("GCP_KYC_IMAGE_SIGNED_URL")

	return &gc
}

// listBucket lists the contents of a bucket in Google Cloud Storage.
func (d *GCApplication) ExistsImage(userID, imageType string) (bool, error) {
	query := &storage.Query{Prefix: fmt.Sprintf("%s/identity-scans/%s", userID, imageType)}
	it := d.Bucket.Objects(d.ctx, query)
	for {
		obj, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return false, err
		}
		log.Print(obj.Name)
		return true, nil
	}

	return false, nil
}

func (d *GCApplication) GeneratePresignedURL(userID, imageType string) (string, error) {
	ct := strings.Split(imageType, ".")

	log.Print(imageType)
	log.Print(ct)

	contentType := ""
	if ct[1] == "jpg" {
		contentType = "jpeg"
	} else {
		contentType = ct[1]
	}
	log.Print(contentType)

	// privKey, err := d.getPrivateKey()
	// if err != nil {
	// 	return "", err
	// }

	if d.GcpKycImageSignedUrl == "" {
		return "", errors.New("Google Private key can not be empty")
	}

	data, err := base64.StdEncoding.DecodeString(d.GcpKycImageSignedUrl)
	if err != nil {
		return "", err
	}

	url, err := storage.SignedURL(d.BucketName, fmt.Sprintf("%s/identity-scans/%s", userID, imageType), &storage.SignedURLOptions{
		GoogleAccessID: d.AccessID,
		//TODO: urgently refactor
		PrivateKey:  data,
		Method:      "PUT",
		ContentType: fmt.Sprintf("image/%s", contentType),
		Expires:     time.Now().Add(48 * time.Hour),
	})
	if err != nil {
		return "", err
	}

	return d.applyCORSSpecifics(url), nil
}

func (d *GCApplication) GetKYCImages(userID string) ([]*storage.ObjectAttrs, error) {
	items := []*storage.ObjectAttrs{}
	query := &storage.Query{
		Prefix:    fmt.Sprintf("%s/identity-scans/", userID),
		Delimiter: "/"}
	it := d.Bucket.Objects(context.Background(), query)

	for {
		imageAttributes, err := it.Next()
		if err != nil {
			if err == iterator.Done {
				break
			}
			return nil, err
		}
		log.Print(imageAttributes.Name)

		items = append(items, imageAttributes)
	}
	return items, nil
}

func (d *GCApplication) CopyImageToDeal(userID, dealID string, imageAttributes *storage.ObjectAttrs) error {
	srcObject := d.Bucket.Object(fmt.Sprintf("%s/identity-scans/%s", userID, imageAttributes.Name))
	dstObjectName := fmt.Sprintf("%s/deals%s/%s", userID, dealID, imageAttributes.Name)

	log.Print(srcObject)
	log.Print(dstObjectName)

	_, err := d.Bucket.Object(dstObjectName).CopierFrom(srcObject).Run(context.Background())
	if err != nil {
		return err
	}

	return nil
}

func (d *GCApplication) GetImageReader(user *models.User, imageName string) (*storage.Reader, error) {
	objectHandle := d.Bucket.Object(fmt.Sprintf("%s/identity-scans/%s", user.ID.String(), imageName))
	return objectHandle.NewReader(context.Background())
}

func (d *GCApplication) GetImageMetaInfo(userID string, imageName string) (*storage.ObjectAttrs, error) {
	query := &storage.Query{
		Prefix:    fmt.Sprintf("%s/identity-scans/", userID),
		Delimiter: "/"}
	it := d.Bucket.Objects(context.Background(), query)

	for {
		imageAttributes, err := it.Next()
		if err != nil {
			if err == iterator.Done {
				break
			}
			return nil, err
		}
		log.Print(imageAttributes.Name)

		if imageAttributes.Name == imageName || strings.Split(imageAttributes.Name, ".")[0] == imageName {
			return imageAttributes, nil
		}
	}
	return nil, nil
}

func (d *GCApplication) applyCORSSpecifics(url string) string {
	url = strings.Replace(url, d.BucketName+"/", "", 1)
	url = strings.Replace(url, "https://", "https://"+d.BucketName+".", 1)
	return url
}

//func (d *GCApplication) getPrivateKey() (string, error) {
//	ctx := context.Background()
//	client, err := storage.NewClient(ctx)
//	if err != nil {
//		return "", err
//	}
//
//	sep := strings.Split(d.PrivateKeyStorageData, "/")
//	if len(sep) != 2 {
//		log.Debug().Msgf(d.PrivateKeyStorageData)
//		return "", fmt.Errorf("invalid ETHEREUM_PRIVATE_KEY_STORAGE_DATA")
//	}
//
//	rc, err := client.Bucket(sep[0]).Object(sep[1]).NewReader(ctx)
//	if err != nil {
//		return "", err
//	}
//	defer rc.Close()
//
//	c, err := ioutil.ReadAll(rc)
//	if err != nil {
//		return "", err
//	}
//
//	b, err := cr.DecryptRSA(d.PrivateKeyStorageKey, c)
//	if err != nil {
//		log.Error().Err(err).Msg("decryptRSA error")
//		return "", err
//	}
//
//	resp := strings.Replace(string(b), "\n", "", -1)
//
//	return resp, nil
//}
