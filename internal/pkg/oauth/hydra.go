package oauth

import (
	"encoding/gob"
	"errors"
	"fmt"
	"lkd-platform-backend/pkg/environment"
	"net/url"
	"os"

	"github.com/gorilla/sessions"
	"github.com/ory/go-convenience/urlx"
	"github.com/ory/hydra/sdk/go/hydra"
	"github.com/ory/hydra/sdk/go/hydra/swagger"
	"github.com/ory/x/cmdx"
	"github.com/rs/zerolog/log"
	"golang.org/x/oauth2"
)

type IdentityManagementClient interface {
	AuthURL() string
	TokenURL() string
	OAuth2Config() *oauth2.Config
	Logout(email string, token string) error
	GetCookies() *sessions.CookieStore
	GetOAuthClient() *hydra.CodeGenSDK
	GetClientLogoutRedirectURL() string
	GetClientRedirectURL() string
	GetStateToken() string
	GetAuthTokenAudience() []string
	GetLoginRememberFor() int64
	GetConsentRememberFor() int64
	RefreshSession(oauthToken *oauth2.Token) (*swagger.OAuth2TokenIntrospection, error)
	RefreshSessionRecord(sessionRecord interface{}) (*swagger.OAuth2TokenIntrospection, error)
}

type IdentityManagement struct {
	OAuthClient             *hydra.CodeGenSDK
	LoginRememberFor        int64
	ConsentRememberFor      int64
	ClientLogoutRedirectURL string
	ClientRedirectURL       string
	AuthTokenAudience       []string
	Cookies                 *sessions.CookieStore
	StateToken              string
}

type UserConsent struct {
	Decision    string   `json:"decision"`
	GrantScopes []string `json:"grant_scopes"`
	Challenge   string   `json:"consent_challenge"`
}

func (client *IdentityManagement) GetCookies() *sessions.CookieStore {
	return client.Cookies
}

func (client *IdentityManagement) GetOAuthClient() *hydra.CodeGenSDK {
	return client.OAuthClient
}

func (client *IdentityManagement) GetClientLogoutRedirectURL() string {
	return client.ClientLogoutRedirectURL
}

func (client *IdentityManagement) GetClientRedirectURL() string {
	return client.ClientRedirectURL
}

func (client *IdentityManagement) GetStateToken() string {
	return client.StateToken
}

func (client *IdentityManagement) GetAuthTokenAudience() []string {
	return client.AuthTokenAudience
}

func (client *IdentityManagement) GetLoginRememberFor() int64 {
	return client.LoginRememberFor
}

func (client *IdentityManagement) GetConsentRememberFor() int64 {
	return client.ConsentRememberFor
}

func (client *IdentityManagement) AuthURL() string {
	apiURL, err := url.Parse(client.OAuthClient.Configuration.PublicURL)
	cmdx.Must(err, `Unable to parse url ("%s"): %s`, client.OAuthClient.Configuration.PublicURL, err)
	return urlx.AppendPaths(apiURL, "/oauth2/auth").String()
}

func (client *IdentityManagement) TokenURL() string {
	apiURL, err := url.Parse(client.OAuthClient.Configuration.PublicURL)
	cmdx.Must(err, `Unable to parse url ("%s"): %s`, client.OAuthClient.Configuration.PublicURL, err)
	return urlx.AppendPaths(apiURL, "/oauth2/token").String()
}

func (client *IdentityManagement) OAuth2Config() *oauth2.Config {
	return &oauth2.Config{
		ClientID:     client.OAuthClient.Configuration.ClientID,
		ClientSecret: client.OAuthClient.Configuration.ClientSecret,

		Endpoint: oauth2.Endpoint{
			TokenURL: client.TokenURL(),
			AuthURL:  client.AuthURL(),
		},

		RedirectURL: client.ClientRedirectURL,
		Scopes:      client.OAuthClient.Configuration.Scopes,
	}
}

type IAMConfig struct {
	AdminURL     string
	PublicURL    string
	ClientID     string
	ClientSecret string
	Scopes       []string

	LoginRemember   int64
	ConsentRemember int64

	ClientLogoutRedirectURL string
	ClientRedirectURL       string
	AuthTokenAudience       []string
	StateToken              string

	CookieStoreHash32 []byte
	CookieStoreHash16 []byte
}

func GetIAMClient(config *IAMConfig) (IdentityManagementClient, error) {
	sdk, err := hydra.NewSDK(&hydra.Configuration{
		AdminURL:     config.AdminURL,
		PublicURL:    config.PublicURL,
		ClientID:     config.ClientID,
		ClientSecret: config.ClientSecret,
		Scopes:       config.Scopes,
	})

	if err != nil {
		return nil, err
	}
	sdk.PublicApi = swagger.NewPublicApiWithBasePath(config.PublicURL)
	sdk.PublicApi.Configuration.Username = config.ClientID
	sdk.PublicApi.Configuration.Password = config.ClientSecret

	store := sessions.NewCookieStore(config.CookieStoreHash32, config.CookieStoreHash16)
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   86400 * 7,
		HttpOnly: true,
	}

	gob.Register(&oauth2.Token{})
	store.Options.Secure = true

	if !environment.CheckDockerEnv(os.Getenv("ENV")) {
		store.Options.Secure = false

		return &IdentityManagementClientLocal{
			OAuthClient:             sdk,
			LoginRememberFor:        config.LoginRemember,
			ConsentRememberFor:      config.ConsentRemember,
			ClientLogoutRedirectURL: config.ClientLogoutRedirectURL,
			ClientRedirectURL:       config.ClientRedirectURL,
			AuthTokenAudience:       config.AuthTokenAudience,
			Cookies:                 store,
			StateToken:              config.StateToken,
		}, nil
	}

	sdk.PublicApi = swagger.NewPublicApi()
	return &IdentityManagement{
		OAuthClient:             sdk,
		LoginRememberFor:        config.LoginRemember,
		ConsentRememberFor:      config.ConsentRemember,
		ClientLogoutRedirectURL: config.ClientLogoutRedirectURL,
		ClientRedirectURL:       config.ClientRedirectURL,
		AuthTokenAudience:       config.AuthTokenAudience,
		Cookies:                 store,
		StateToken:              config.StateToken,
	}, nil
}

func (client *IdentityManagement) Logout(email string, token string) error {
	if email != "" {
		revokeAuthenticationSessionData, err := client.OAuthClient.AdminApi.RevokeAuthenticationSession(email)
		if err != nil || revokeAuthenticationSessionData.Response.StatusCode >= 300 {
			return errors.New("failed to revoke authentication session")
		}
		log.Debug().Msgf("user authentication session %s revoked, Hydra message %s", email, revokeAuthenticationSessionData.Message)
	}

	if token != "" {
		revokeOAuth2TokenData, err := client.OAuthClient.PublicApi.RevokeOAuth2Token(email)
		if err != nil || revokeOAuth2TokenData.Response.StatusCode >= 300 {
			return errors.New("failed to revoke oauth token")
		}

		log.Debug().Msgf("user token %s revoked, Hydra message %s", email, revokeOAuth2TokenData.Message)
	}

	return nil
}

func (client *IdentityManagement) RefreshSessionRecord(sessionRecord interface{}) (*swagger.OAuth2TokenIntrospection, error) {
	tokenDetails, err := client.RefreshSession(sessionRecord.(*oauth2.Token))
	if err != nil {
		log.Debug().Err(err).Msg("refresh session error")
		return nil, err
	}

	return tokenDetails, nil
}

func (client *IdentityManagement) RefreshSession(oauthToken *oauth2.Token) (*swagger.OAuth2TokenIntrospection, error) {
	if oauthToken.AccessToken == "" {
		return nil, errors.New("invalid access token provided")
	}

	tokenDetails, _, err := client.OAuthClient.AdminApi.IntrospectOAuth2Token(oauthToken.AccessToken, "")
	if err != nil {
		return nil, err
	}

	if !tokenDetails.Active {
		log.Debug().Msg(fmt.Sprintf("OAuth token to be refreshed - %v", oauthToken))
		oauthToken, err = client.OAuth2Config().TokenSource(oauth2.NoContext, oauthToken).Token()
		if err != nil || !oauthToken.Valid() {
			return nil, fmt.Errorf("Refresh call to Hydra for OAuth token %v failed or refreshed token %v is not valid, [recovery]", tokenDetails, oauthToken)
		}
		log.Debug().Msgf("OAuth token after refresh ----- ", oauthToken)

		tokenDetails, _, err = client.OAuthClient.AdminApi.IntrospectOAuth2Token(oauthToken.AccessToken, "")
		if err != nil {
			return nil, err
		}
	}

	return tokenDetails, nil
}
