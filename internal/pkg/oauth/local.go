package oauth

import (
	"context"
	"errors"
	"net/url"

	"github.com/gorilla/sessions"
	"github.com/ory/go-convenience/urlx"
	"github.com/ory/hydra/sdk/go/hydra"
	"github.com/ory/hydra/sdk/go/hydra/swagger"
	"github.com/ory/x/cmdx"
	"github.com/rs/zerolog/log"
	"golang.org/x/oauth2"
)

type IdentityManagementClientLocal struct {
	OAuthClient             *hydra.CodeGenSDK
	LoginRememberFor        int64
	ConsentRememberFor      int64
	ClientLogoutRedirectURL string
	ClientRedirectURL       string
	AuthTokenAudience       []string
	Cookies                 *sessions.CookieStore
	StateToken              string
}

type UserConsentLocal struct {
	Decision    string   `json:"decision"`
	GrantScopes []string `json:"grant_scopes"`
	Challenge   string   `json:"consent_challenge"`
}

func (client *IdentityManagementClientLocal) GetCookies() *sessions.CookieStore {
	return client.Cookies
}

func (client *IdentityManagementClientLocal) GetOAuthClient() *hydra.CodeGenSDK {
	return client.OAuthClient
}

func (client *IdentityManagementClientLocal) GetClientLogoutRedirectURL() string {
	return client.ClientLogoutRedirectURL
}

func (client *IdentityManagementClientLocal) GetClientRedirectURL() string {
	return client.ClientRedirectURL
}

func (client *IdentityManagementClientLocal) GetStateToken() string {
	return client.StateToken
}

func (client *IdentityManagementClientLocal) GetAuthTokenAudience() []string {
	return client.AuthTokenAudience
}

func (client *IdentityManagementClientLocal) GetLoginRememberFor() int64 {
	return client.LoginRememberFor
}

func (client *IdentityManagementClientLocal) GetConsentRememberFor() int64 {
	return client.ConsentRememberFor
}

func (client *IdentityManagementClientLocal) AuthURL() string {
	apiURL, err := url.Parse(client.OAuthClient.Configuration.PublicURL)
	cmdx.Must(err, `Unable to parse url ("%s"): %s`, client.OAuthClient.Configuration.PublicURL, err)
	return urlx.AppendPaths(apiURL, "/oauth2/auth").String()
}

func (client *IdentityManagementClientLocal) TokenURL() string {
	apiURL, err := url.Parse(client.OAuthClient.Configuration.PublicURL)
	cmdx.Must(err, `Unable to parse url ("%s"): %s`, client.OAuthClient.Configuration.PublicURL, err)
	return urlx.AppendPaths(apiURL, "/oauth2/token").String()
}

func (client *IdentityManagementClientLocal) OAuth2Config() *oauth2.Config {
	return &oauth2.Config{
		ClientID:     client.OAuthClient.Configuration.ClientID,
		ClientSecret: client.OAuthClient.Configuration.ClientSecret,
		Endpoint: oauth2.Endpoint{
			TokenURL: client.TokenURL(),
			AuthURL:  client.AuthURL(),
		},
		RedirectURL: client.ClientRedirectURL,
		Scopes:      client.OAuthClient.Configuration.Scopes,
	}
}

func (client *IdentityManagementClientLocal) Logout(email string, token string) error {
	if email != "" {
		revokeAuthenticationSessionData, err := client.OAuthClient.AdminApi.RevokeAuthenticationSession(email)
		if err != nil || revokeAuthenticationSessionData.Response.StatusCode >= 300 {
			return errors.New("failed to revoke authentication session")
		}
		log.Debug().Msgf("user authentication session %s revoked, Hydra message %s", email, revokeAuthenticationSessionData.Message)
	}

	if token != "" {
		revokeOAuth2TokenData, err := client.OAuthClient.PublicApi.RevokeOAuth2Token(email)
		if err != nil || revokeOAuth2TokenData.Response.StatusCode >= 300 {
			return errors.New("failed to revoke oauth token")
		}

		log.Debug().Msgf("user token %s revoked, Hydra message %s", email, revokeOAuth2TokenData.Message)
	}

	return nil
}

func (client *IdentityManagementClientLocal) RefreshSessionRecord(sessionRecord interface{}) (*swagger.OAuth2TokenIntrospection, error) {
	tokenDetails, err := client.RefreshSession(sessionRecord.(*oauth2.Token))
	if err != nil {
		log.Debug().Err(err).Msg("refresh session error")
		return nil, err
	}

	return tokenDetails, nil
}

func (client *IdentityManagementClientLocal) RefreshSession(oauthToken *oauth2.Token) (*swagger.OAuth2TokenIntrospection, error) {
	if oauthToken.AccessToken == "" {
		return nil, errors.New("invalid access token provided")
	}

	tokenDetails, _, err := client.OAuthClient.AdminApi.IntrospectOAuth2Token(oauthToken.AccessToken, "")
	if err != nil {
		return nil, err
	}

	if !tokenDetails.Active {
		log.Debug().Msgf("trying to refresh expired token ----- ", oauthToken)
		client.OAuth2Config().Client(context.Background(), oauthToken)
		log.Debug().Msgf("token state after refresh ----- ", oauthToken)
		if !oauthToken.Valid() {
			return nil, errors.New("failed to refresh access token")
		}
	}

	return tokenDetails, nil
}
