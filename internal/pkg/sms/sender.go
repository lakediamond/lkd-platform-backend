package sms

//Sender SMS sender interface
type Sender interface {
	Send(phoneNumber string, message string) error
}
