package sms

import (
	"github.com/rs/zerolog/log"
)

//SenderMock sms sender client mock
type SenderMock struct {
}


//NewSenderMock return new sms sender client mock instance
func NewSenderMock() *SenderMock {
	return &SenderMock{}
}

//Send mock for Send method
func (connector *SenderMock) Send(phoneNumber string, message string) error {
	log.Debug().Msgf("SMS to phone %s with code %s just sent", phoneNumber, message)
	return nil
}
