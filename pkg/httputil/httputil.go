package httputil

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/rs/zerolog/log"
)

// ReadBody return request body or empty string
func ReadBody(body io.ReadCloser) []byte {
	b, err := ioutil.ReadAll(body)
	if err != nil {
		return []byte("")
	}
	return b
}

func ParseBody(w http.ResponseWriter, body io.ReadCloser, str interface{}) error {
	b := ReadBody(body)
	err := json.Unmarshal(b, str)
	if err != nil {
		log.Debug().Err(err).Msgf("invalid data format. Input data: %s", string(b))
		resp, _ := json.Marshal(map[string]string{"error_message": "invalid data format"})
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write(resp)
		return err
	}

	return nil
}

type ErrResponse struct {
	ErrorMessage []string `json:"error_message"`
}

func WriteErrorMsg(w http.ResponseWriter, err error) {
	var errs = []error{err}
	WriteMultiplyErrorMsg(w, errs)
}

func WriteMultiplyErrorMsg(w http.ResponseWriter, errs []error) {

	var data []string

	for _, err := range errs {
		data = append(data, err.Error())
	}
	errResponse := ErrResponse{data}

	resp, _ := json.Marshal(errResponse)
	w.Write(resp)
}

//RemoveCookies removing lkd-platform session cookies
func RemoveCookies(w http.ResponseWriter) {
	cookie := http.Cookie{
		Name:     "lkd-platform",
		Value:    "",
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)
}

func MarshalStructure(w http.ResponseWriter, str interface{}) ([]byte, error) {
	respData, err := json.Marshal(str)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		WriteErrorMsg(w, err)
		return nil, err
	}

	return respData, nil
}

// ReadRequestBodyAsString return request body as a string or empty string
func ReadRequestBodyAsString(body io.ReadCloser) string {
	return string(ReadBody(body))
}
