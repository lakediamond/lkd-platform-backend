package httputil

import (
	"net"
	"net/http"

	"github.com/gorilla/context"
	"github.com/rs/zerolog/log"
)

//IPConfig contains information about client IP configuration
type IPConfig struct {
	Address string
	Country string
}

func WithIPConfig(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var ipConfig IPConfig

		ipConfig.Address = r.Header.Get("Cf-Connecting-Ip")
		ipConfig.Country = r.Header.Get("Cf-Ipcountry")

		if ipConfig.Address == "" {
			ip, err := getRequestIP(r)
			if err != nil {
				log.Debug().Err(err).Msg("GetRequestIP error")
			}
			ipConfig.Address = ip
			ipConfig.Country = "unknown"
		}

		setIPDetails(r, ipConfig)
		h.ServeHTTP(w, r)
	})
}

func getRequestIP(r *http.Request) (string, error) {
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return "", err
	}

	return ip, nil
}

func setIPDetails(r *http.Request, ipDetails IPConfig) {
	context.Set(r, "ipDetails", ipDetails)
}

func GetIPDetails(r *http.Request) IPConfig {
	return context.Get(r, "ipDetails").(IPConfig)
}
