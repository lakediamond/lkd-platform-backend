package handlers

import (
	"bufio"
	"net"
	"net/http"
	"runtime/debug"
	"time"

	"github.com/gorilla/handlers"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type recoveryLogger struct {
	zerolog.Logger
}

func (log recoveryLogger) Println(v ...interface{}) {
	log.Error().Str("error.stack", string(debug.Stack())).Str("error.message", v[0].(string)).Msg("")
}

var RecoveryHandler = handlers.RecoveryHandler(
	handlers.RecoveryLogger(recoveryLogger{log.Logger}),
	handlers.PrintRecoveryStack(false))

type httpLoggingHandler struct {
	handler http.Handler
}

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
	length     int
}

func (lrw *loggingResponseWriter) Write(b []byte) (int, error) {
	if lrw.statusCode == 0 {
		lrw.statusCode = http.StatusOK
	}
	n, err := lrw.ResponseWriter.Write(b)
	lrw.length += n
	return n, err
}

func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

func (lrw *loggingResponseWriter) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	hj := lrw.ResponseWriter.(http.Hijacker)
	return hj.Hijack()
}

func (h httpLoggingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	lrw := loggingResponseWriter{ResponseWriter: w}
	h.handler.ServeHTTP(&lrw, r)
	elapsed := time.Since(start)

	log.Info().
		Int("http.status_code", lrw.statusCode).
		Str("http.request_id", r.Header.Get("X-Request-Id")).
		Str("http.url_details.path", r.URL.Path).
		Str("http.method", r.Method).
		Str("duration", elapsed.String()).
		Msg("")
}

func LoggingHandler(h http.Handler) http.Handler {
	return httpLoggingHandler{h}
}

type applicationHandler struct {
	handler http.HandlerFunc
}

func JSONContentTypeHandler(h http.Handler) http.Handler {
	return handlers.ContentTypeHandler(h, "application/json")
}

func (c applicationHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c.handler(w, r)
}

func ApplicationHandler(h http.HandlerFunc) http.Handler {
	return applicationHandler{h}
}
