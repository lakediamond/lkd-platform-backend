package resource

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"

	"github.com/pkg/errors"
	"lkd-platform-backend/pkg/ws/model"
	"lkd-platform-backend/pkg/ws/ws-client"
)

//
// ClientsPool type
//
type ClientsPool struct {
	rw      *sync.RWMutex
	clients map[string]*ws_client.Client
}

//
// NewClientsPool constructor
//
func NewClientsPool() *ClientsPool {
	return &ClientsPool{
		rw:      &sync.RWMutex{},
		clients: make(map[string]*ws_client.Client),
	}
}

//
// Add method
//
func (rcv *ClientsPool) Add(client *ws_client.Client) {
	rcv.rw.Lock()
	defer rcv.rw.Unlock()
	rcv.add(client)
}

func (rcv *ClientsPool) add(client *ws_client.Client) {
	rcv.clients[client.ID()] = client
}

//
// Delete method
//
func (rcv *ClientsPool) Delete(clientID string) {
	rcv.rw.Lock()
	defer rcv.rw.Unlock()
	rcv.delete(clientID)
}

func (rcv *ClientsPool) delete(clientID string) {
	delete(rcv.clients, clientID)
}

//
// Broadcast method
//
func (rcv *ClientsPool) Broadcast(message model.Message) {
	rcv.rw.RLock()
	defer rcv.rw.RUnlock()
	rcv.broadcast(message)
}

func (rcv *ClientsPool) broadcast(message model.Message) {
	bytes := marshalMessage(message)

	for _, c := range rcv.clients {
		c.SendMessage(bytes)
	}
}

//
// SendToUser method
//
func (rcv *ClientsPool) SendToUser(userID string, message model.Message) error {
	rcv.rw.RLock()
	defer rcv.rw.RUnlock()
	return rcv.sendToUser(userID, message)
}

func (rcv *ClientsPool) sendToUser(userID string, message model.Message) error {
	bytes := marshalMessage(message)

	//TODO userID mapping to connects
	isUserFound := false
	for _, c := range rcv.clients {
		if c.User() != nil {
			if c.User().ID() == userID {
				c.SendMessage(bytes)
				isUserFound = true
			}
		}
	}

	if isUserFound {
		return nil
	}

	return errors.New(
		fmt.Sprintf("User with id %s is not authorized", userID),
	)
}

//
// SendToClient method
//
func (rcv *ClientsPool) SendToClient(clientID string, message model.Message) error {
	rcv.rw.RLock()
	defer rcv.rw.RUnlock()
	return rcv.sendToClient(clientID, message)
}

func (rcv *ClientsPool) sendToClient(clientID string, message model.Message) error {
	bytes := marshalMessage(message)

	c, ok := rcv.clients[clientID]
	if !ok {
		return errors.New(
			fmt.Sprintf("Client with id %s is not found", clientID),
		)
	}

	c.SendMessage(bytes)

	return nil
}

//
// Length method
//
func (rcv *ClientsPool) Length() int {
	rcv.rw.RLock()
	defer rcv.rw.RUnlock()
	return rcv.length()
}

func (rcv *ClientsPool) length() int {
	return len(rcv.clients)
}

//
// marshalMessage helper
//
func marshalMessage(message model.Message) []byte {
	jsonRequest, err := json.Marshal(message)
	if err != nil {
		log.Printf("clients-pool %s\n", err)
	}

	return jsonRequest
}
