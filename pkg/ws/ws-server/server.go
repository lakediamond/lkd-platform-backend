package ws_server

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"lkd-platform-backend/pkg/ws/model"
	"lkd-platform-backend/pkg/ws/resource"
	"lkd-platform-backend/pkg/ws/ws-client"
)

type Server struct {
	clientsPool *resource.ClientsPool
	upgrader    *websocket.Upgrader
}

func NewServer(clientsPool *resource.ClientsPool) *Server {
	return &Server{
		clientsPool: clientsPool,
		upgrader: &websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		},
	}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	conn, err := s.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error().Err(err).Msg("Ws ServeHTTP errors")
		return
	}

	client := ws_client.NewClient(conn)
	if client == nil {
		return
	}

	s.clientsPool.Add(client)
	log.Info().Msgf(
		"Ws ServeHTTP [ws-server] %s",
		fmt.Sprintf("Added new client [%s]. Now %d clients connected.", client.ID(), s.clientsPool.Length()),
	)

	//Init front webSocket client
	if err := s.clientsPool.SendToClient(
		client.ID(), model.Message{
			Type: "Ping",
			Payload: struct {
				status string
			}{"Ping"},
			Created: time.Now().Unix(),
		}); err != nil {
		fmt.Println("Send to client error", err)
	}

	go client.Writer()

	client.Listen()

	client.Die()

	s.clientsPool.Delete(client.ID())
	log.Info().Msgf(
		"[ws-server] %s",
		fmt.Sprintf("Removed client [%s] from pool. Now %d clients connected.", client.ID(), s.clientsPool.Length()),
	)
}

func (s *Server) SendToAllUsers(message model.Message) {
	s.clientsPool.Broadcast(message)
}

func (s *Server) SendToUser(userID string, message model.Message) {
	if err := s.clientsPool.SendToUser(userID, message); err != nil {
		log.Error().Err(err).Msg("[ws-server] send to user returned err")
	}
}
