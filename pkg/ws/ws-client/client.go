package ws_client

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"lkd-platform-backend/pkg/ws/model"

	"github.com/rs/zerolog/log"
)

type Client struct {
	id      string
	ws      *websocket.Conn
	writeCh chan []byte
	doneCh  chan bool
	user    *model.User
}

func NewClient(ws *websocket.Conn) *Client {
	if ws == nil {
		log.Error().Msg("ws cannot be nil")
		return nil
	}

	return &Client{
		id:      uuid.New().String(),
		ws:      ws,
		writeCh: make(chan []byte, 10),
		doneCh:  make(chan bool),
	}
}

func (c *Client) ID() string {
	return c.id
}

func (c *Client) User() *model.User {
	return c.user
}

func (c *Client) ClearUserID() {
	c.user = nil

	log.Debug().Msgf("[ws-client:%s] user deauthorized \n", c.id)
}

func (c *Client) SetUserID(uID string) error {

	c.user = model.NewUser(uID)

	log.Debug().Msgf("[ws-client:%s] user [%s] authorized \n", c.id, c.user.ID())

	return nil
}

func (c *Client) Listen() {
	defer func() {
		//nolint
		c.ws.WriteControl(websocket.CloseMessage, []byte{}, time.Now().Add(time.Second*5))
		//nolint
		c.ws.Close()
	}()

	log.Info().Msgf("[ws-client:%s] listening...", c.id)

	for {
		_, data, err := c.ws.ReadMessage()
		if err != nil {
			log.Error().Err(err).Msg("ws listen readMessage")
			return
		}

		err = c.processMessage(data)
		if err != nil {
			log.Error().Err(err).Msg("ws listen processMessage")
		}
	}
}

func (c *Client) Writer() {
	for {
		select {
		case msg := <-c.writeCh:
			if err := c.ws.WriteMessage(websocket.BinaryMessage, msg); err != nil {
				log.Error().Err(err).Msg("ws listen Writer")
			}

			log.Info().Msgf("to client %s ||| msg %s\n", c.ID(), string(msg))

		case <-c.doneCh:
			return
		}
	}
}

func (c *Client) SendMessage(bytes []byte) {
	defer func() {
		if r := recover(); r != nil {
			log.Error().Msgf("defer error", r)
		}
	}()

	c.writeCh <- bytes
}

func (c *Client) processMessage(data []byte) error {
	message := &model.MessageClientRequest{}
	if err := json.Unmarshal(data, message); err != nil {
		return err
	}

	log.Info().Msgf("[ws-client:%s] received message with [%s] message type\n", c.id, message.Type)

	return nil
}

func (c *Client) Die() {
	c.doneCh <- true
	close(c.doneCh)
	close(c.writeCh)
}
