package ws_service

import (
	"time"

	"lkd-platform-backend/pkg/ws/model"
)

func (s Service) HandlePingEvent(e interface{}) {
	s.server.SendToAllUsers(model.Message{
		Type:    "ping",
		Created: time.Now().Unix(),
	})
}
