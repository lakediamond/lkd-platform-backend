package ws_service

import (
	"time"

	"lkd-platform-backend/pkg/ws/ws-server"
)

type Service struct {
	server *ws_server.Server
}

func NewService(Server *ws_server.Server) *Service {
	return &Service{
		server: Server,
	}
}

func (s Service) Subscribe() {
	go s.doEveryPing(10 * time.Second)
}

func (s Service) doEveryPing(d time.Duration) {
	for range time.Tick(d) {
		s.HandlePingEvent(struct {
			Type string
		}{"ping"})
	}
}
