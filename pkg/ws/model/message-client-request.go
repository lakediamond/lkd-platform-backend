package model

import "encoding/json"

//
// MessageClientRequest type
//
type MessageClientRequest struct {
	Type    string          `json:"type"`
	Created int64           `json:"created"`
	Payload json.RawMessage `json:"payload,omitempty"`
}
