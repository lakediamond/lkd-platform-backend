package model

type Message struct {
	Type    string      `json:"type"`
	Created int64       `json:"created"`
	Payload interface{} `json:"payload,omitempty"`
}
