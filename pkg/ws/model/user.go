package model

import (
	"github.com/rs/zerolog/log"
)

type User struct {
	id string
}

func NewUser(uID string) *User {
	if uID == "" {
		log.Debug().Msg("model.User: ws userID cannot be nil")
		return nil
	}

	return &User{id: uID}
}

func (rcv *User) ID() string {
	return rcv.id
}
