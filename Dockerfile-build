FROM golang:1.11.2-alpine3.8 as builder

ARG LD_PUBSUB_CI_TOKEN
ARG LD_PUBSUB_CI_SECRET

RUN apk add --no-cache --virtual .build-deps \
    alpine-sdk \
    cmake \
    sudo \
    libssh2 libssh2-dev\
    git \
    xz

WORKDIR /go/src/app

RUN git config --global url."https://$LD_PUBSUB_CI_TOKEN:$LD_PUBSUB_CI_SECRET@gitlab.com/".insteadOf "https://gitlab.com/"

ENV GO111MODULE=on

COPY go.mod .
COPY go.sum .
RUN ls -lah

RUN go mod download -json

ADD . .

RUN CGO_ENABLED=1 GOOS=linux go build -a -installsuffix cgo -o main cmd/backend/main.go
RUN CGO_ENABLED=1 GOOS=linux go build -a -installsuffix cgo -o migrate cmd/migrate/main.go
RUN CGO_ENABLED=1 GOOS=linux go build -a -installsuffix cgo -o fulfillment-service cmd/matcher/main.go

# strip and compress the binary
RUN strip --strip-unneeded main
RUN strip --strip-unneeded migrate
RUN strip --strip-unneeded fulfillment-service